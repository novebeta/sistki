<?php
namespace frontend\assets;
use yii\web\AssetBundle;
/**
 * Main backend application asset bundle.
 */
class LightBoxAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/lightbox.min.css',
	];
	public $js = [
		'js/lightbox.min.js',
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}

<?php
namespace frontend\assets;
use yii\web\AssetBundle;
/**
 * Main frontend application asset bundle.
 */
class BiodataAsset extends AssetBundle {
//	public $sourcePath = '@vendor/almasaeed2010/adminlte/dist';
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
//        'css/site.css',
		'css/datatables.min.css',
		'css/buttons.dataTables.min.css',
	];
	public $js = [
		'js/datatables.min.js',
		'js/dataTables.buttons.min.js',
		'js/pages/biodata.js'
	];
	public $depends = [
		'dmstr\web\AdminLteAsset',
	];
}

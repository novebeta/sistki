<?php
/* @var $this yii\web\View */
/* @var $model app\models\Paspor */
/* @var $modelLunas \app\models\Bayar|array|null */
/* @var $modelDP \app\models\Bayar|array|null */
$this->title = Yii::t( 'app', 'Update Paspor: {nameAttribute}', [
	'nameAttribute' => $model->paspor_id,
] );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'biodata/index' ] ];
$this->params['breadcrumbs'][] = [
	'label' => $biodata->name,
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id ]
];
$this->params['breadcrumbs'][] = [
	'label' => Yii::t( 'app', 'Paspor' ),
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_5' ]
];
$this->params['breadcrumbs'][] = [ 'label' => $model->paspor_id, 'url' => [ 'view', 'id' => $model->paspor_id ] ];
$this->params['breadcrumbs'][] = Yii::t( 'app', 'Update' );
?>


<?= $this->render( '_form', [
	'model' => $model,
	'item'  => $item,
] ) ?>


<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Paspor */
/* @var $modelLunas \app\models\Bayar */
/* @var $modelDP \app\models\Bayar */
$this->title = Yii::t('app', 'Create Paspor');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Paspors'), 'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_5' ]];
$this->params['breadcrumbs'][] = $this->title;
?>


<?= $this->render('_form', [
	'model' => $model,
	'item'  => $item,
]) ?>


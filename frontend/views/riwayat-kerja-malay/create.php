<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RiwayatKerjaMalay */

$this->title = Yii::t('app', 'Create Riwayat Kerja Malay');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Riwayat Kerja Malays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="riwayat-kerja-malay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

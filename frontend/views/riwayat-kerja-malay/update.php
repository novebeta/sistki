<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RiwayatKerjaMalay */

$this->title = Yii::t('app', 'Update Riwayat Kerja Malay: {nameAttribute}', [
    'nameAttribute' => $model->riwayat_kerja_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Riwayat Kerja Malays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->riwayat_kerja_id, 'url' => ['view', 'id' => $model->riwayat_kerja_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="riwayat-kerja-malay-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

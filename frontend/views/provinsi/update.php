<?php
/* @var $this yii\web\View */
/* @var $model app\models\Provinsi */
$this->title = Yii::t('app', 'Update Provinsi: {nameAttribute}', [
    'nameAttribute' => $model->location_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Provinsi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->location_id, 'url' => ['view', 'id' => $model->location_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="provinsi-update">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

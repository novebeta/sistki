<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemPot */

$this->title = Yii::t('app', 'Update Item Pot: ' . $model->item_pot_id, [
    'nameAttribute' => '' . $model->item_pot_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Item Pots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->item_pot_id, 'url' => ['view', 'id' => $model->item_pot_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="item-pot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

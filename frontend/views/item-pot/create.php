<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ItemPot */

$this->title = Yii::t('app', 'Create Item Pot');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Item Pots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-pot-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

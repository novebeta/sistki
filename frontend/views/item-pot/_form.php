<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemPot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-pot-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_pot_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_order_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bulan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pot')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

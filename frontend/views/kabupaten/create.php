<?php
/* @var $this yii\web\View */
/* @var $model app\models\Kabupaten */
$this->title = Yii::t('app', 'Create Kabupaten');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kabupaten'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kabupaten-create">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

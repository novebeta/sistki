<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RiwayatKerjaIndo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="riwayat-kerja-indo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'riwayat_kerja_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_perusahaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'posisi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'masa_kerja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biodata_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

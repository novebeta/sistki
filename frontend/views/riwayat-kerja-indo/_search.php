<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RiwayatKerjaIndoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="riwayat-kerja-indo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'riwayat_kerja_id') ?>

    <?= $form->field($model, 'nama_perusahaan') ?>

    <?= $form->field($model, 'lokasi') ?>

    <?= $form->field($model, 'posisi') ?>

    <?= $form->field($model, 'masa_kerja') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RiwayatKerjaIndo */

$this->title = Yii::t('app', 'Create Riwayat Kerja Indo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Riwayat Kerja Indos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="riwayat-kerja-indo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
use kartik\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin( [

    ]); ?>
    <div class="box-body">
        <div class="form-group">
			<?= Html::label( 'No. Visa', 'no_visa' ) ?>
			<?= Html::textInput( 'no_visa' ); ?>
        </div>
        <div class="form-group">
			<?= Html::submitButton( 'Submit', [ 'class' => 'btn btn-primary' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end() ?>
</div>

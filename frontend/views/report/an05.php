<?php
use kartik\datecontrol\DateControl;
use kartik\helpers\Html;
use yii\widgets\ActiveForm;
$this->title                   = 'AN05';
$this->params['breadcrumbs'][] = 'AN05';
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin( [
	] ); ?>
    <div class="box-body">
        <div class="form-group">
			<?= Html::label( 'Tgl Awal', 'tgl_awal' ) ?>
			<? try {
				echo DateControl::widget( [
					'name'          => 'tgl_awal',
					'type'          => DateControl::FORMAT_DATE,
					'autoWidget'    => true,
//					'displayFormat' => 'php:d-M-Y',
//					'saveFormat'    => 'php:Y-M-d'
				] );
			} catch ( Exception $e ) {
				echo $e->getMessage();
			} ?>
        </div>
        <div class="form-group">
			<?= Html::label( 'Tgl Akhir', 'tgl_akhir' ) ?>
			<? try {
				echo DateControl::widget( [
					'name'          => 'tgl_akhir',
					'type'          => DateControl::FORMAT_DATE,
					'autoWidget'    => true,
//					'displayFormat' => 'php:d-M-Y',
//					'saveFormat'    => 'php:Y-M-d'
				] );
			} catch ( Exception $e ) {
				echo $e->getMessage();
			} ?>
        </div>
        <div class="form-group">
			<?= Html::submitButton( 'Submit', [ 'class' => 'btn btn-primary' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end() ?>
</div>

<?php
use kartik\helpers\Html;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
$this->title                   = 'Cost Summary';
$this->params['breadcrumbs'][] = 'Cost Summary';
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin( [
	] ); ?>
    <div class="box-body">
        <div class="form-group">
			<?= Html::label( 'Job Order', 'job_order_id' ) ?>
            <div class="form-group">
				<?php
				$format = <<< SCRIPT
function repoFormatResult(repo) {
               var markup =
'<div class="row">' + 
    '<div class="col-sm-3">' + repo.text + '</div>' +
    '<div class="col-sm-3">' + repo.tgl + '</div>' +
    '<div class="col-sm-3">' + repo.approval_date + '</div>' +
    '<div class="col-sm-3">' + repo.job_title + '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
        }

        function repoFormatSelection(repo) {
            return repo.text;
        }
SCRIPT;
				$this->registerJs( $format, View::POS_HEAD );
				$url = \yii\helpers\Url::to( [ 'job-order/list' ] );
				//				$cityDesc = empty( $model->job_order_id ) ? '' : \app\models\JobOrder::findOne( $model->job_order_id )->company->name;
				try {
					echo Select2::widget( [
						'name'          => 'job_order_id',
						//					'initValueText' => $cityDesc, // set the initial display text
						'options'       => [ 'placeholder' => 'Search for a job order ...' ],
						'pluginOptions' => [
							'allowClear'         => true,
							'minimumInputLength' => 3,
							'language'           => [ 'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ), ],
							'ajax'               => [
								'url'      => $url,
								'dataType' => 'json',
								'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
							],
							'escapeMarkup'       => new JsExpression( 'function (markup) { return markup; }' ),
							'templateResult'     => new JsExpression( 'repoFormatResult' ),
							'templateSelection'  => new JsExpression( 'repoFormatSelection' ),
						],
					] );
				} catch ( Exception $e ) {
					echo $e->getMessage();
				}
				?>
            </div>
        </div>
        <div class="form-group">
			<?= Html::submitButton( 'Submit', [ 'class' => 'btn btn-primary' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end() ?>
</div>
<?php
use app\models\Dokumen;
use app\models\DokumenUpload;
use app\modules\admin\components\Helper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\JobOrder */
$this->title                   = $model->approval_date;
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Job Orders' ), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
$css                           = <<<CSS
        .btn:focus, .btn:active, button:focus, button:active {
        outline: none !important;
        box-shadow: none !important;
        }

        #image-gallery .modal-footer{
        display: block;
        }

        .thumb{
        margin-top: 15px;
        margin-bottom: 15px;
        }
CSS;
$this->registerCss( $css );
?>
    <div class="job-order-view">
        <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
        <p>
			<? if ( Helper::checkRoute( 'update' ) ) {
				echo Html::a( Yii::t( 'app', 'Update' ), [
					'update',
					'id' => $model->job_order_id
				], [ 'class' => 'btn btn-primary' ] );
			}
			if ( Helper::checkRoute( 'delete' ) ) {
				echo Html::a( Yii::t( 'app', 'Delete' ), [ 'delete', 'id' => $model->job_order_id ], [
					'class' => 'btn btn-danger',
					'data'  => [
						'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
						'method'  => 'post',
					],
				] );
			} ?>
        </p>
		<?= DetailView::widget( [
			'model'      => $model,
			'attributes' => [
//            'job_order_id',
				'no_register',
				'job_title',
				'tgl',
				'approval_date',
				'levy_paid_receipt',
				'qouta',
				'age',
				'sex',
				'education',
				'expected_arrival',
				'durasi_kontrak',
				'working_hours',
				'basic_salary',
				'meal_allowance',
				'attendance_allowace',
				'medical_allowance',
				'transport',
				'akomodasi',
				'return_air_ticket',
				'bonus',
				'draft_contract',
				'skill_allowance',
				'morning_shift',
				'night_shift',
				'levy_plks_process',
				'payment_levy',
				'monthly_deduction',
				'name_pptkis',
				'sipptki',
				'address_agent',
				'other_information:ntext',
				'lama_pot',
				'jml_pot',
			],
		] )
		?>
        <div class="col-md-4">
            <table id="w0" class="table table-striped table-bordered detail-view">
                <thead>
                <tr>
                    <th>Bulan</th>
                    <th>Potongan</th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ( $model->itemPot as $item ) : ?>
                    <tr>
                        <th><?= $item->bulan ?></th>
                        <td><?= $item->pot; ?></td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="container">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?= DokumenUpload::initialPreview( Dokumen::JOB_ORDER, $model->job_order_id, false ); ?>?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                   data-target="#image-gallery">
                    <img class="img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::JOB_ORDER, $model->job_order_id, true );; ?>?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                         alt="Another alt text">
                </a>
            </div>
        </div>
        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i
                                    class="fa fa-arrow-left"></i>
                        </button>
                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i
                                    class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
    let modalId = $('#image-gallery');
    $(document)
        .ready(function () {
            loadGallery(true, 'a.thumbnail');

            //This function disables buttons when needed
            function disableButtons(counter_max, counter_current) {
                $('#show-previous-image, #show-next-image')
                    .show();
                if (counter_max === counter_current) {
                    $('#show-next-image')
                        .hide();
                } else if (counter_current === 1) {
                    $('#show-previous-image')
                        .hide();
                }
            }

            /**
             *
             * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
             * @param setClickAttr  Sets the attribute for the click handler.
             */

            function loadGallery(setIDs, setClickAttr) {
                let current_image,
                    selector,
                    counter = 0;
                $('#show-next-image, #show-previous-image')
                    .click(function () {
                        if ($(this)
                            .attr('id') === 'show-previous-image') {
                            current_image--;
                        } else {
                            current_image++;
                        }
                        selector = $('[data-image-id="' + current_image + '"]');
                        updateGallery(selector);
                    });

                function updateGallery(selector) {
                    let sel=selector;
                    current_image=sel.data('image-id');
                    $('#image-gallery-title')
                        .text(sel.data('title'));
                    $('#image-gallery-image')
                        .attr('src', sel.data('image'));
                    disableButtons(counter, sel.data('image-id'));
                }

                if (setIDs == true) {
                    $('[data-image-id]')
                        .each(function () {
                            counter++;
                            $(this)
                                .attr('data-image-id', counter);
                        });
                }
                $(setClickAttr)
                    .on('click', function () {
                        updateGallery($(this));
                    });
            }
        });
    // build key actions
    $(document)
        .keydown(function (e) {
            switch (e.which) {
                case 37: // left
                    if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                        $('#show-previous-image')
                            .click();
                    }
                    break;
                case 39: // right
                    if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                        $('#show-next-image')
                            .click();
                    }
                    break;
                default:
                    return; // exit this handler for other keys
            }
            e.preventDefault(); // prevent the default action (scroll / move caret)
        });
JS;
$this->registerJs( $js, View::POS_END );
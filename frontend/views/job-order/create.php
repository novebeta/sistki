<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JobOrder */

$this->title = Yii::t('app', 'Create Job Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Job Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-order-create">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'item' => $item,
    ]) ?>

</div>

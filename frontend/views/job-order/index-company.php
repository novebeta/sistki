<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\JobOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = Yii::t( 'app', 'Job Orders' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="box box-primary">
	<?
	try {
		echo \kartik\grid\GridView::widget(
			[
				"dataProvider"       => $dataProvider,
				//				'filterModel'        => $searchModel,
				"condensed"          => true,
				"hover"              => true,
				'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				//				'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'pjax'               => true,
				//				'toolbar'            => [
				//					[
				//						'content' => ( Yii::$app->user->can( 'akes_tab' ) ?
				//							Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
				//								'type'    => 'button',
				//								'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title ),
				//								'class'   => 'btn btn-success',
				//								'onclick' => 'location.href="create"'
				//							] ) : '') . ' ' .
				//							Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
				//								'data-pjax' => 0,
				//								'class'     => 'btn btn-default',
				//								'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
				//							] )
				//					],
				//					'{export}',
				//					'{toggleData}',
				//				],
				//				'export'             => [
				//					'fontAwesome' => true
				//				],
				'responsive'         => true,
				'floatHeader'        => true,
				'floatHeaderOptions' => [ 'scrollingTop' => true ],
				'showPageSummary'    => false,
				'toggleDataOptions'  => [ 'minCount' => 10 ],
				'panel'              => [
					'footer' => false,
					'heading'  => Html::button( '<i class="glyphicon glyphicon-user"> </i> Biodata No Job', [
						'type'    => 'button',
						'class'   => 'btn btn-success',
						'onclick' => 'location.href="'.\yii\helpers\Url::toRoute(['biodata/no-jobs-company']).'"'
					] ),
				],
				"columns"            => [
					[
						'class'          => 'kartik\grid\SerialColumn',
						'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
						'width'          => '36px',
						'header'         => '',
						'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
					],
					'no_register',
					'job_title',
					'totalNumberComplete',
					'readyNumber',
					//					'selectedNumber',
					[
						'label'  => 'Action',
						'format' => 'raw',
						'value'  => function ( $data ) {
							return Html::a( "View", [
								'job-order/select-job-order',
								't'            => md5( date( 'Y-m-d H:i:s' ) ),
								'job_order_id' => $data->job_order_id
							] );
						},
					],
					//					[
					//					        'class' => 'yii\grid\ActionColumn',
					//					        'template' => Helper::filterActionColumn('{view}{delete}{posting}'),
					//                    ],
					//					[
					//						'class'      => 'kartik\grid\ActionColumn',
					//						'template'   => Yii::$app->user->can( 'akes_tab' ) ? '{view} {update} {delete}' : '{view} {update}',
					//					],
				]
			]
		);
	} catch ( Exception $e ) {
		echo $e->getMessage();
	}
	?>
</div>
<!---->
<!--<p>-->
<!--    --><? //= Html::a(Yii::t('app', 'Create Job Order'), ['create'], ['class' => 'btn btn-success']) ?>
<!--</p>-->
<!---->
<!---->
<!--<div class="box box-primary">-->
<!--    <div class="box-header"><h3 class="box-title"><i class="fa fa-table"></i>&nbsp;Table</h3></div>-->
<!--    <div class="box-body">-->
<!--        --><?php //Pjax::begin(); ?>
<!--        --><? //=
//        \yiister\adminlte\widgets\grid\GridView::widget(
//            [
//                "dataProvider" => $dataProvider,
//                "condensed" => true,
//                "hover" => true,
//                "columns" => [
//                    ['class' => 'yii\grid\SerialColumn'],
//                    'no_register',
//                    'approval_date',
//                    'levy_paid_receipt',
//                    ['class' => 'yii\grid\ActionColumn'],
//                ]
//            ]
//        );
//        ?>
<!--        --><?php //Pjax::end(); ?>
<!--    </div>-->
<!--</div>-->



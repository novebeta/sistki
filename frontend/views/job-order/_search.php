<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JobOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

<!--    --><?//= $form->field($model, 'job_order_id') ?>

    <?= $form->field($model, 'no_register') ?>

    <?= $form->field($model, 'approval_date') ?>

    <?= $form->field($model, 'levy_paid_receipt') ?>

    <?= $form->field($model, 'qouta') ?>

    <?php // echo $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'education') ?>

    <?php // echo $form->field($model, 'expected_arrival') ?>

    <?php // echo $form->field($model, 'durasi_kontrak') ?>

    <?php // echo $form->field($model, 'working_hours') ?>

    <?php // echo $form->field($model, 'basic_salary') ?>

    <?php // echo $form->field($model, 'meal_allowance') ?>

    <?php // echo $form->field($model, 'attendance_allowace') ?>

    <?php // echo $form->field($model, 'medical_allowance') ?>

    <?php // echo $form->field($model, 'transport') ?>

    <?php // echo $form->field($model, 'akomodasi') ?>

    <?php // echo $form->field($model, 'return_air_ticket') ?>

    <?php // echo $form->field($model, 'bonus') ?>

    <?php // echo $form->field($model, 'draft_contract') ?>

    <?php // echo $form->field($model, 'skill_allowance') ?>

    <?php // echo $form->field($model, 'morning_shift') ?>

    <?php // echo $form->field($model, 'night_shift') ?>

    <?php // echo $form->field($model, 'levy_plks_process') ?>

    <?php // echo $form->field($model, 'payment_levy') ?>

    <?php // echo $form->field($model, 'monthly_deduction') ?>

    <?php // echo $form->field($model, 'name_pptkis') ?>

    <?php // echo $form->field($model, 'sipptki') ?>

    <?php // echo $form->field($model, 'address_agent') ?>

    <?php // echo $form->field($model, 'other_information') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

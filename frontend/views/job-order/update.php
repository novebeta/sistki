<?php
/* @var $this yii\web\View */
/* @var $model app\models\JobOrder */
$this->title                   = Yii::t( 'app', 'Update Job Order: {nameAttribute}', [
	'nameAttribute' => $model->approval_date,
] );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Job Orders' ), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [
	'label' => $model->approval_date,
	'url'   => [ 'view', 'id' => $model->job_order_id ]
];
$this->params['breadcrumbs'][] = Yii::t( 'app', 'Update' );
?>
<div class="job-order-update">
    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
	<?= $this->render( '_form', [
		'item'  => $item,
		'model' => $model,
	] ) ?>
</div>

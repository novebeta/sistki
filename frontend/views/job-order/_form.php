<?php
use app\models\Dokumen;
use app\models\DokumenUpload;
use frontend\assets\BiodataAsset;
use frontend\assets\LightBoxAsset;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
BiodataAsset::register( $this );
LightBoxAsset::register( $this );
$disabled = ( ! Yii::$app->user->can( 'akes_tab' ) );
/* @var $this yii\web\View */
/* @var $model app\models\JobOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<script>
    var dataItemArr = <?=isset( $item ) ? $item : '[]';?>;
</script>
<div class="box box-primary">
	<?php $form = ActiveForm::begin( [
		'options' => [ 'enctype' => 'multipart/form-data' ],
		'id'      => 'job-order-form-id'
	] ); ?>
    <div class="box-body">
        <div class="form-group">
			<?php
			$url      = \yii\helpers\Url::to( [ 'company/list' ] );
			$cityDesc = empty( $model->company_id ) ? '' : \app\models\Company::findOne( $model->company_id )->name;
			echo $form->field( $model, 'company_id' )->widget( Select2::classname(), [
				'initValueText' => $cityDesc, // set the initial display text
				'options'       => [ 'placeholder' => 'Search for a company ...' ],
				'pluginOptions' => [
					'allowClear'         => true,
					'minimumInputLength' => 3,
					'language'           => [
						'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ),
					],
					'ajax'               => [
						'url'      => $url,
						'dataType' => 'json',
						'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
					],
					'escapeMarkup'       => new JsExpression( 'function (markup) { return markup; }' ),
					'templateResult'     => new JsExpression( 'function(city) { return city.text; }' ),
					'templateSelection'  => new JsExpression( 'function (city) { return city.text; }' ),
				],
				'disabled'      => $disabled
			] );
			?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'job_title' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'no_register' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'approval_date' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
				'type'           => DateControl::FORMAT_DATE,
				'ajaxConversion' => false,
				'widgetOptions'  => [
					'pluginOptions' => [
						'autoclose' => true
					]
				],
				'disabled'       => $disabled
			] );
			?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'levy_paid_receipt' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'qouta' )->textInput( [ 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'age' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'sex' )->dropDownList( [
				'MALE'   => 'MALE',
				'FEMALE' => 'FEMALE',
				'ALL'    => 'ALL',
			], [ 'prompt' => '', 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'education' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'expected_arrival' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'durasi_kontrak' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'working_hours' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'basic_salary' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'meal_allowance' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'attendance_allowace' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'medical_allowance' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'transport' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'akomodasi' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'return_air_ticket' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'bonus' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'draft_contract' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'skill_allowance' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'morning_shift' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'night_shift' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'levy_plks_process' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'payment_levy' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'monthly_deduction' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'name_pptkis' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'sipptki' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'address_agent' )->textInput( [
				'maxlength' => true,
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'lama_pot' )->textInput( [
				'maxlength' => true,
				'type'      => 'number',
				'disabled'  => $disabled
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'jml_pot' )->textInput( [ 'maxlength' => true, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Detil Potongan</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body">
                    <table id="table-pot" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Bulan</th>
                            <th>Potongan</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'other_information' )->textarea( [ 'rows' => 6, 'disabled' => $disabled ] ) ?>
        </div>
        <div class="form-group">
            <label class="control-label" style="display: block; width: 100px;">Upload</label>
            <div>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::JOB_ORDER, $model->job_order_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="<?= Dokumen::JOB_ORDER ?>">
                    <img src="<?= \app\models\DokumenUpload::initialPreview( Dokumen::JOB_ORDER,
						$model->job_order_id ); ?>" class="img-thumbnail" width="265"
                         height="125"
                         alt="<?= Dokumen::JOB_ORDER; ?>">
                </a>
				<?php
				$pluginEvents  = [];
				$pluginOptions = [
					'showPreview'           => false,
					'showCaption'           => false,
					'browseClass'           => 'btn btn-default btn-secondary',
					'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
					'showUpload'            => false
				];
				if ( ! $model->isNewRecord && Yii::$app->user->can( 'akes_tab' ) ) {
					$pluginOptions['showUpload']      = true;
					$pluginOptions['uploadUrl']       = Url::to( [ '/dokumen-upload/upload' ] );
					$pluginOptions['uploadExtraData'] = [
						'biodata_id' => $model->job_order_id
					];
					$pluginEvents                     = [
						"fileuploaded" => "function() {  window.location='" . Url::to( [
								'/job-order/update',
								'id' => $model->job_order_id,
								't'  => md5( date( 'Y-m-d H:i:s' ) )
							], true ) . "'; }"
					];
				}
				try {
					echo FileInput::widget( [
						'name'          => Dokumen::JOB_ORDER,
						'pluginOptions' => $pluginOptions,
						'pluginEvents'  => $pluginEvents
					] );
				} catch ( Exception $e ) {
					echo $e->getMessage();
				}
				?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'disabled' )->checkbox( [ 'disabled' => $disabled ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'closed' )->checkbox( [ 'disabled' => $disabled ] ) ?>
            </div>
			<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                <div class="form-group">
					<?= Html::submitButton( Yii::t( 'app', 'Save' ), [
						'class' => 'btn btn-success',
						'id'    => 'job-order-form-button-id'
					] ) ?>
                </div>
			<? endif; ?>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
    <div class="modal fade" id="modal-pot">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Item Potongan</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <form role="form" id="form-pot">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Bulan</label>
                                <input type="text" class="form-control" id="bulan-pot">
                            </div>
                            <div class="form-group">
                                <label>Potongan</label>
                                <input type="text" class="form-control" id="jml-pot">
                            </div>
                            <input id="updateId" type="hidden">
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-pot-add-row">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
	<?php
	$this->registerJs( $this->render( 'js.js' ) );
	?>

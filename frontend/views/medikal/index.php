<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MedikalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Yii::t( 'app', 'Medikal' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-body">
	<?php Pjax::begin(); ?>
	<?
	try {
		echo \kartik\grid\GridView::widget(
			[
				"dataProvider"       => $dataProviderMedikal,
				'filterModel'        => $searchModelMedikal,
				"condensed"          => true,
				"hover"              => true,
				'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'pjax'               => true,
				'toolbar'            => [
					[
						'content' =>
							Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
								'type'    => 'button',
								'title'   => Yii::t( 'kvgrid', 'Add Medikal' ),
								'class'   => 'btn btn-success',
								'onclick' => 'location.href="' . Url::toRoute( "medikal/create" ) . '"'
							] ) . ' ' .
							Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'medikal/index' ], [
								'data-pjax' => 0,
								'class'     => 'btn btn-default',
								'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
							] )
					],
					'{export}',
					'{toggleData}',
				],
				'export'             => [
					'fontAwesome' => true
				],
				'responsive'         => true,
				'floatHeader'        => true,
				'floatHeaderOptions' => [ 'scrollingTop' => true ],
				'showPageSummary'    => false,
				'toggleDataOptions'  => [ 'minCount' => 10 ],
				'panel'              => [
					'heading' => false,
//					'footer'  => false,
					//							'type'    => GridView::TYPE_PRIMARY,
					//							'heading' => '<i class="glyphicon glyphicon-book"></i>  Medikal',
				],
				"columns"            => [
					[
						'class'          => 'kartik\grid\SerialColumn',
						'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
						'width'          => '36px',
						'header'         => '',
						'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
					],
					'tgl',
					'medikal_center',
					'biaya',
					'input_data',
					[
						'class'      => 'kartik\grid\ActionColumn',
						'template'   => '{update} {delete}',
						'urlCreator' => function ( $action, $model, $key, $index ) {
							$url = Url::toRoute( [
								'medikal/' . $action,
								'id' => $key
							] ); // your own url generation logic
							return $url;
						}
					]
				]
			]
		);
	} catch ( Exception $e ) {
		echo $e->getMessage();
	}
	?>
	<?php Pjax::end(); ?>
</div>
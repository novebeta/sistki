<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MedikalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="medikal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'medikal_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'medikal_center') ?>

    <?= $form->field($model, 'biaya') ?>

    <?= $form->field($model, 'input_data') ?>

    <?php // echo $form->field($model, 'note_') ?>

    <?php // echo $form->field($model, 'expired') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use frontend\assets\BiodataAsset;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\widgets\Typeahead;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
BiodataAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model app\models\Medikal */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelLunas */
/* @var $modelDP */
$dispOptions = [ 'class' => 'form-control kv-monospace' ];
$saveOptions = [
	'type'     => 'text',
	'label'    => '<label>Saved Value: </label>',
	'class'    => 'kv-saved',
	'readonly' => true,
	'tabindex' => 1000
];
$saveCont    = [ 'class' => 'kv-saved-cont' ];
$listDataKabupaten = \app\models\MedicalCenter::find()->select( 'nama' )->column();
?>
<script>
    var dataItemArr = <?=isset( $item ) ? $item : '[]';?>;
</script>
<div class="box box-primary">
	<?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <h3 class="box-title">Medikal</h3>
    </div>
	<?php $form = ActiveForm::begin( [
		'id' => 'medikal_form_input'
	] ); ?>
	<?php //echo Html::hiddenInput( 'refresh', 0, [ 'id' => 'hidden_refresh' ] ); ?>
    <div class="box-body">
        <div class="form-group">
			<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
				'type'           => DateControl::FORMAT_DATE,
				'ajaxConversion' => true,
				'widgetOptions'  => [
					'pluginOptions' => [
						'autoclose' => true
					],
					'pluginEvents'  => [
						'changeDate' => "function(e) {  
						    var date = e.date;
						    var newDate = new Date(date.setMonth(date.getMonth()+3));
						    $('#medikal-expired-disp-kvdate').kvDatepicker('update', newDate);
						    $('#medikal-expired').val(toJSONLocal(newDate));
						}"
					]
				]
			] );
			?>
        </div>
        <div class="form-group">
		    <?= $form->field( $model, 'medikal_center' )->widget( Typeahead::classname(), [
			    'options'       => [ 'placeholder' => 'Filter as you type ...' ],
			    'pluginOptions' => [ 'highlight' => true ],
			    'dataset'       => [
				    [
					    'local' => $listDataKabupaten,
					    'limit' => 10
				    ]
			    ]
		    ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'input_data' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'note_' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'expired' )->widget( DateControl::classname(), [
//				'disabled'       => true,
//				'options'        => [ 'id' => 'medikal_expired_form_id' ],
//				'pluginOptions' => [
//					'format' => 'dd-mm-yyyy',
//					'todayHighlight' => true
//				],
				'type'           => DateControl::FORMAT_DATE,
				'ajaxConversion' => true,
//				'widgetOptions'  => [
//					'pluginOptions' => [
//						'autoWidget' => false,
//						'autoclose'  => true
//					]
//				]
			] );
			?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'tipe_bayar' )->dropDownList( [
				'SPONSOR' => 'SPONSOR',
				'SENDIRI' => 'SENDIRI',
				'PINJAMAN' => 'PINJAMAN',
			],
				[
					'prompt'   => '',
					'onchange' => new JsExpression(" 
					var val_ = $(this).val();
					if(val_ === 'SPONSOR'){
					    $('.bayar').hide();
					}else{
					    $('.bayar').show();
					}
						" )
				] ) ?>
        </div>
        <div class="form-group bayar">
			<?php
			echo $form->field( $model, 'biaya' )->widget( NumberControl::classname(), [
				'maskedInputOptions' => [
					'prefix'     => 'Rp',
					'allowMinus' => false
				],
				'displayOptions'     => [ '' ],
				'name'               => 'amount_dp',
			] );
			?>
        </div>
        <div class="box box-default collapsed-box bayar">
            <div class="box-header with-border">
                <h3 class="box-title">Pembayaran</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body">
                    <table id="table-medikal-bayar" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Voucher</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="modal fade" id="modal-medikal-bayar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pembayaran</h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <form role="form" id="form-medikal-bayar">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Amount</label>
	                                <?=NumberControl::widget( [
		                                'id'=> "amount-medikal-bayar",
		                                'name'=> "amount-medikal-bayar",
		                                'maskedInputOptions' => [
			                                'prefix'     => 'Rp',
			                                'allowMinus' => false
		                                ],
		                                'options' => [
		                                ]
	                                ]);?>
                                </div>
                                <div class="form-group">
                                    <label>Voucher</label>
                                    <input type="text" class="form-control" id="voucher-medikal-bayar">
                                </div>
                                <div class="form-group">
                                    <label>Note</label>
                                    <input type="text" class="form-control" id="note-medikal-bayar">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-medikal-bayar-add-row">Save changes
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
	<?php Pjax::end(); ?>
</div>
<?php
$this->registerJs( $this->render( 'js.php' ) );
?>

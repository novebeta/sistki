<?php
use app\models\Biodata;
/* @var $this yii\web\View */
/* @var $model app\models\Medikal */
/** @var Biodata $biodata */
//var_dump($biodata);
$this->title                   = Yii::t( 'app', 'Create Medikal' );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'biodata/index' ] ];
$this->params['breadcrumbs'][] = [
	'label' => $biodata->name,
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id ]
];
$this->params['breadcrumbs'][] = [
	'label' => Yii::t( 'app', 'Medikal' ),
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_2' ]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medikal-create">
    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
	<?= $this->render( '_form', [
		'model' => $model,
		'item'  => $item,
	] ) ?>
</div>

<?php
/* @var $this yii\web\View */
/* @var $model app\models\Medikal */
$this->title                   = Yii::t( 'app', 'Update Medikal: {nameAttribute}', [
	'nameAttribute' => $model->medikal_center,
] );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'biodata/index' ] ];
$this->params['breadcrumbs'][] = [
	'label' => $biodata->name,
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id ]
];
$this->params['breadcrumbs'][] = [
	'label' => Yii::t( 'app', 'Medikal' ),
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_2' ]
];
$this->params['breadcrumbs'][] = [ 'label' => $model->medikal_center, 'url' => [ 'view', 'id' => $model->medikal_id ] ];
$this->params['breadcrumbs'][] = Yii::t( 'app', 'Update' );
?>
<div class="medikal-update">
    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
	<?= $this->render( '_form', [
		'model' => $model,
		'item'  => $item,
	] ) ?>
</div>

<?php
/* @var $this yii\web\View */
/* @var $model app\models\Kecamatan */
$this->title = Yii::t('app', 'Update Kecamatan: {nameAttribute}', [
    'nameAttribute' => $model->location_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kecamatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->location_id, 'url' => ['view', 'id' => $model->location_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kecamatan-update">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

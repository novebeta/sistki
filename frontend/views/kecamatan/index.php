<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KecamatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Kecamatan');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
<?=
\kartik\grid\GridView::widget(
	[
		"dataProvider"       => $dataProvider,
		'filterModel'        => $searchModel,
		"condensed"          => true,
		"hover"              => true,
		'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
		'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
		'pjax'               => true,
		'toolbar'            => [
			[
				'content' =>
					Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
						'type'    => 'button',
						'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title  ),
						'class'   => 'btn btn-success',
						'onclick' => 'location.href="create"'
					] ) . ' ' .
					Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
						'data-pjax' => 0,
						'class'     => 'btn btn-default',
						'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
					] )
			],
			'{export}',
			'{toggleData}',
		],
		'export'             => [
			'fontAwesome' => true
		],
		'responsive'         => true,
		'hover'              => true,
		'floatHeader'        => true,
		'floatHeaderOptions' => [ 'scrollingTop' => true ],
		'showPageSummary'    => false,
		'toggleDataOptions'  => [ 'minCount' => 10 ],
		'panel'              => [
			'heading' => false,
//			'footer'  => true,
//			'type'    => GridView::TYPE_PRIMARY,
//			'heading' => '<i class="glyphicon glyphicon-book"></i> ' . $this->title ,
		],
		"columns"            => [
			[
				'class'          => 'kartik\grid\SerialColumn',
				'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
				'width'          => '36px',
				'header'         => '',
				'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
			],
			'nama',
			['class' => 'yii\grid\ActionColumn'],
		]
	]
);
?>
</div>
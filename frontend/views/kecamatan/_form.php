<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Kecamatan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="form-group">
            <!--    --><? //= $form->field($model, 'location_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'nama' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'tipe' )->hiddenInput( [ 'value' => 'KEC' ] )->label( false ) ?>
        </div>
        <div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>

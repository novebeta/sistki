<?php
/* @var $this yii\web\View */
/* @var $model app\models\Kecamatan */
$this->title = Yii::t('app', 'Create Kecamatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kecamatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kecamatan-create">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

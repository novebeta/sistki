<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Isc */

$this->title = Yii::t('app', 'Update Isc: {nameAttribute}', [
    'nameAttribute' => $model->isc_id,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Iscs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->isc_id, 'url' => ['view', 'id' => $model->isc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="isc-update">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'item'  => $item,
    ]) ?>

</div>

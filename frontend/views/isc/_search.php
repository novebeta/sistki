<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IscSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="isc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'isc_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'rbb_duta') ?>

    <?= $form->field($model, 'pic') ?>

    <?= $form->field($model, 'note_') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'fex') ?>

    <?php // echo $form->field($model, 'kode_bayar') ?>

    <?php // echo $form->field($model, 'expired') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

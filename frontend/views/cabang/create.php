<?php
/* @var $this yii\web\View */
/* @var $model app\models\Cabang */
$this->title = Yii::t('app', 'Create Cabang');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cabang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabang-create">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

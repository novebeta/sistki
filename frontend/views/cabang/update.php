<?php
/* @var $this yii\web\View */
/* @var $model app\models\Cabang */
$this->title = Yii::t('app', 'Update Cabang: {nameAttribute}', [
    'nameAttribute' => $model->cabang_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cabang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cabang_id, 'url' => ['view', 'id' => $model->cabang_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cabang-update">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

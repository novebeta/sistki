<?php
use app\models\Biodata;
use app\models\Dokumen;
use app\models\DokumenUpload;
use kartik\select2\Select2;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BiodataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var Biodata $model */
$this->title                     = Yii::t( 'app', 'Biodata' );
$this->params[ 'breadcrumbs' ][] = $this->title;
global $data;
$data = \app\models\JobOrder::find()->select2( 'job_order_id', [ 'job_title', 'no_register' ], [ 'job_title' => SORT_ASC ], [ 'condition' => [ 'company_id' => \Yii::$app->session->get( 'company_id' ) ] ] );
?>
    <style>
        .riwayat table, th, td {
            border: none;
            padding: 2px;
        }
    </style>
    <div class="box box-primary">
		<?
		try {
			echo \kartik\grid\GridView::widget(
				[
					"dataProvider"         => $dataProvider,
					//				'filterModel'        => $searchModel,
					"condensed"            => true,
					"hover"                => true,
					'headerRowOptions'     => [ 'class' => 'kartik-sheet-style' ],
					'filterRowOptions'     => [ 'class' => 'kartik-sheet-style' ],
					'pjax'                 => true,
					'toolbar'              => [
						'{export}',
						'{toggleData}',
					],
					'export'               => [
						'fontAwesome' => true
					],
					'responsive'           => true,
					'floatHeader'          => true,
					'floatHeaderOptions'   => [ 'scrollingTop' => true ],
					'showPageSummary'      => false,
					'toggleDataOptions'    => [ 'minCount' => 10 ],
					'panelBeforeTemplate'  => '<div class="row"><div class="col-md-8">{before}</div><div class="col-md-4">{toolbarContainer}</div></div><div class="clearfix"></div>',
					'panel'                => [
//					'heading' => false,
'before' => '<form class="form-inline"><div class="form-group">
                            <label style="padding-right: 15px">Nama : </label>' . Html::textInput( 'BiodataNojobSearch[name]', '', [ 'class' => 'form-control' ] )
            . '</div><button type="submit" style="padding-left: 15px" class="btn btn-default">Search</button></form>'
					],
					'panelHeadingTemplate' => "{summary}
{title}
<div class=\"kv-panel-pager\">
    {pager}
</div>
<div class=\"clearfix\"></div>",
					"columns"              => [
						[
							'class'          => 'kartik\grid\SerialColumn',
							'vAlign'         => 'top',
							'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
							'width'          => '36px',
							'header'         => 'No.',
							'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
						],
						[
							'label'          => '',
							'attribute'      => 'name',
							'format'         => 'raw',
							'header'         => 'Job Order',
							'contentOptions' => [ 'style' => 'min-width: 200px;' ],
							'value'          => function ( $model, $key, $index, $column ) {
								global $data;
								return Select2::widget( [
									'name'          => '',
									'data'          => $data,
									'theme'         => Select2::THEME_DEFAULT,
									'options'       => [ 'placeholder' => 'Select a job ...' ],
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'allowClear'        => true
									],
									'pluginEvents'  => [
										"change" => "function(e) { 
	                                    console.log(e.currentTarget.value); 
	                                    $.ajax({
						                    type: 'POST',
						                    url: 'select-joborder',
						                    data: {
						                        biodata_id: '$key',
						                        job_order_id: e.currentTarget.value
						                    },
						                    success: function(d){
						                        $('.grid-view').yiiGridView('applyFilter');
						                    },
						                    datatype: 'json',
						                });
                                    }",
									]
								] );
							}
						],
						[
							'label'     => '',
							'attribute' => 'name',
							'format'    => 'raw',
							//						'header'         => 'Photo',
							'value'     => function ( $model, $key, $index, $column ) {
								$src    = DokumenUpload::initialPreview( Dokumen::PAS_PHOTO, $model->biodata_id, true );
								$script = <<< HTML
							<img class="pull-right  img-thumbnail" width="152px" height="200px"
                         src="$src"
                         alt="Another alt text">
HTML;
								return $script;
							}
						],
						[
							'label'     => '',
							'attribute' => 'name',
							'format'    => 'raw',
							//						'header'         => 'Profile',
							'value'     => function ( $model, $key, $index, $column ) {
								$nama     = strtoupper( $model->name );
								$sex      = $model->sex == 'F' ? 'FEMALE' : 'MALE';
								$dob      = strtoupper( \Yii::$app->formatter->asDate( $model->birthdate, 'dd MMMM YYYY' ) );
								$age      = $model->age;
								$district = strtoupper( $model->kabupaten );
								$height   = $model->tb;
								$weight   = $model->bb;
								$graduate = $model->sekolah;
								$indo     = "INDONESIA<table class='riwayat'><tr><td>Company</td><td>Location</td><td>Job Title</td><td>Duration</td></tr>";
								foreach ( $model->riwayatIndo as $r ) {
									$indo .= "<tr><td>$r->nama_perusahaan</td><td>$r->lokasi</td><td>$r->posisi</td><td>$r->masa_kerja</td></tr>";
								}
								$indo  .= "</table>";
								$malay = "MALAYSIA<table class='riwayat'><tr><td>Company</td><td>Location</td><td>Job Title</td><td>Duration</td></tr>";
								foreach ( $model->riwayatMalay as $r ) {
									$malay .= "<tr><td>$r->nama_perusahaan</td><td>$r->lokasi</td><td>$r->posisi</td><td>$r->masa_kerja</td></tr>";
								}
								$malay      .= "</table>";
								$experience = ( ( sizeof( $model->riwayatIndo ) > 0 ) ? $indo : '' ) . " " . ( ( sizeof( $model->riwayatMalay ) ) ? $malay : '' );
								$script     = <<< HTML
<table class="biodata">
  <tr>
    <td width="75">NAMA</td>
    <td width="600">: $nama</td>
  </tr>
  <tr>
    <td>D.O.B</td>
    <td>: $dob</td>
  </tr>
  <tr>
    <td>AGE</td>
    <td>: $age</td>
  </tr>
  <tr>
    <td>SEX</td>
    <td>: $sex </td>
  </tr>
  <tr>
    <td>DISTRICT</td>
    <td>: $district</td>
  </tr>
  <tr>
    <td>HEIGHT</td>
    <td>: $height</td>
  </tr>
  <tr>
    <td>WEIGHT</td>
    <td>: $weight</td>
  </tr>
  <tr>
    <td>GRADUATE</td>
    <td>: $graduate</td>
  </tr>
  <tr>
    <td>EXPERIENCE</td>
    <td>$experience</td>
  </tr>
</table>
HTML;
								return $script;
							}
						],
					]
				]
			);
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}
		?>
    </div>
<?php
$this->registerJs( <<<JS
    $(document).on('pjax:success', function() {
        $(window).scrollTop(0); 
        // console.log('success');
    })
JS
);
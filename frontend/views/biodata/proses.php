<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BiodataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var \app\models\Biodata $model */
$this->title                   = Yii::t( 'app', 'Biodata' );
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<p>-->
<!--	--><? //= Html::a( Yii::t( 'app', 'Create Biodata' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
<!--</p>-->
<!--<div class="box box-primary">-->
<!--    <div class="box-header"><h3 class="box-title"><i class="fa fa-table"></i>&nbsp;Table</h3></div>-->
<!--    <div class="box-body">-->
<div class="box box-primary">
	<?php Pjax::begin(); ?>
	<?
	try {
		echo GridView::widget(
			[
				"dataProvider" => $dataProvider,
//				'filterModel'  => $searchModel,
//				"condensed"          => true,
//				"hover"              => true,
//				'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
//				'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
//				'pjax'               => true,
//				'toolbar'            => [
//					[
//						'content' =>
//							Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
//								'type'    => 'button',
//								'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title ),
//								'class'   => 'btn btn-success',
//								'onclick' => 'location.href="create"'
//							] ) . ' ' .
//							Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
//								'data-pjax' => 0,
//								'class'     => 'btn btn-default',
//								'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
//							] )
//					],
//					'{export}',
//					'{toggleData}',
//				],
//				'export'             => [
//					'fontAwesome' => true
//				],
//				'responsive'         => true,
//				'floatHeader'        => true,
//				'floatHeaderOptions' => [ 'scrollingTop' => true ],
//				'showPageSummary'    => false,
//				'toggleDataOptions'  => [ 'minCount' => 10 ],
//				'panel'              => [
//					'heading' => false,
//					'footer'  => false,
//					//			'type'    => GridView::TYPE_PRIMARY,
//					//			'heading' => '<i class="glyphicon glyphicon-book"></i>  ' . $this->title,
//				],
				"columns"      => [
					[
						'class' => 'yii\grid\CheckboxColumn',
						// you may configure additional properties here
					],
//					[
//						'class'          => 'kartik\grid\SerialColumn',
//						'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
//						'width'          => '36px',
//						'header'         => 'No.',
//						'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
//					],
					[
						'label'     => 'Nama',
						'attribute' => 'name',
						'format'    => 'raw',
						'value'     => function ( $model, $key, $index, $column ) {
							return Html::a( strtoupper( $model->name ),
								[ 'biodata/view', 'id' => $model->biodata_id ] );
						}
					],
					'no_hp',
					'address',
					'birthdate',
					'companyName',
					'jobOrderRegister',
					'cabangName',
				]
			]
		);
	} catch ( Exception $e ) {
		echo $e->getMessage();
	}
	?>
	<?php Pjax::end(); ?>
    <!--    </div>-->
    <!--</div>-->
</div>
<?php
use app\models\Dokumen;
use app\models\DokumenUpload;
use app\models\Kabupaten;
use app\models\Kecamatan;
use app\models\Location;
use app\models\Negara;
use app\models\Provinsi;
use dosamigos\multiselect\MultiSelect;
use frontend\assets\BiodataAsset;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\Typeahead;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
//use kartik\datecontrol\Module;
BiodataAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model app\models\Biodata */
/* @var $form yii\widgets\ActiveForm */
$listDataNegara    = Negara::find()->select( 'nama' )->column();
$listDataProvinsi  = Provinsi::find()->select( 'nama' )->column();
$listDataKabupaten = Kabupaten::find()->select( 'nama' )->column();
$listDataKecamatan = Kecamatan::find()->select( 'nama' )->column();
$rekruiter =  Location::find()->select(['nama as value','nama as label'])
    ->where(['tipe'=>Location::RECRUITER_TYPE])
    ->asArray()
    ->all();
$rekruiter = ArrayHelper::map($rekruiter,'value','label');
//$rekruiter = ArrayHelper::map($rekruiter, 'nama', 'nama');
//if(sizeof($rekruiter) == 0){
//    $rekruiter[] = '';
//}
//?>
<script>
    var dataIndoArr = <?=isset( $indo ) ? $indo : '[]';?>;
    var dataMalayArr = <?=isset( $malay ) ? $malay : '[]';?>;
</script>
<?php $form = ActiveForm::begin( [
	'action'  => $model->isNewRecord ? 'create' : 'update?id=' . $model->biodata_id,
	'options' => [ 'enctype' => 'multipart/form-data' ],
	'id'      => 'biodata_basic_id'
] ); ?>
<div class="box-body">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
				<?= $form->field( $model, 'no_absen' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'name' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'sex' )->dropDownList( [
					'F' => 'FEMALE',
					'M' => 'MALE',
				], [ 'prompt' => '' ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'nik' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'no_hp' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
				<?= $form->field( $model, 'no_paspor' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'address' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'kecamatan' )->widget( Typeahead::classname(), [
					'options'       => [ 'placeholder' => 'Filter as you type ...' ],
					'pluginOptions' => [ 'highlight' => true ],
					'dataset'       => [
						[
							'local' => $rekruiter,
							'limit' => 10
						]
					]
				] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'kabupaten' )->widget( Typeahead::classname(), [
					'options'       => [ 'placeholder' => 'Filter as you type ...' ],
					'pluginOptions' => [ 'highlight' => true ],
					'dataset'       => [
						[
							'local' => $listDataKabupaten,
							'limit' => 10
						]
					]
				] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'propinsi' )->widget( Typeahead::classname(), [
					'options'       => [ 'placeholder' => 'Filter as you type ...' ],
					'pluginOptions' => [ 'highlight' => true ],
					'dataset'       => [
						[
							'local' => $listDataProvinsi,
							'limit' => 10
						]
					]
				] ) ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label" for="upload_pas_photo" style="display: block; width: 100px;">Photo
                    Preview</label>
                <div>
                    <a href="#" id="upload_pas_photo">
                        <img class="img-thumbnail"
                             src="<?= DokumenUpload::initialPreview( Dokumen::PAS_PHOTO, $model->biodata_id ); ?>"
                             alt="">
                    </a>
					<?php
					$pluginEvents  = [];
					$pluginOptions = [
						'showPreview'           => false,
						'showCaption'           => false,
						'browseClass'           => 'btn btn-default btn-secondary',
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'showUpload'            => false
					];
					if ( ! $model->isNewRecord ) {
						$pluginOptions['showUpload']      = true;
						$pluginOptions['uploadUrl']       = Url::to( [ '/dokumen-upload/upload' ] );
						$pluginOptions['uploadExtraData'] = [
							'biodata_id' => $model->biodata_id
						];
						$pluginEvents                     = [
							"fileuploaded" => "function() {  window.location='" . Url::to( [
									'/biodata/view',
									'id' => $model->biodata_id,
									't'  => md5( date( 'Y-m-d H:i:s' ) ),
									'#'  => 'tab_1'
								], true ) . "'; }"
						];
					}
					echo FileInput::widget( [
						'name'          => Dokumen::PAS_PHOTO,
						'pluginOptions' => $pluginOptions,
						'pluginEvents'  => $pluginEvents
					] );
					?>

                </div>
                <div style="padding-top: 10px">
		            <?= \yii\helpers\Html::a( 'Delete Photo', url::to([ 'biodata/delete-photo', 'id' => $model->biodata_id ]), [
			            'class' => 'btn btn-danger',
			            'data'  => [
				            'confirm' => Yii::t( 'app', 'Are you sure you want to delete this photo?' ),
				            'method'  => 'post',
			            ],
		            ] ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
				<?= $form->field( $model, 'birthdate' )->widget( DateControl::classname(), [
					'type'           => DateControl::FORMAT_DATE,
					'ajaxConversion' => false,
					'widgetOptions'  => [ 'pluginOptions' => [ 'autoclose' => true ] ]
				] );
				?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'tempat_lahir' )->widget( Typeahead::classname(), [
					'options'       => [ 'placeholder' => 'Filter as you type ...' ],
					'pluginOptions' => [ 'highlight' => true ],
					'dataset'       => [
						[
							'local' => $listDataKabupaten,
							'limit' => 10
						]
					]
				] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'agama' )->dropDownList( [
					'ISLAM'   => 'ISLAM',
					'KATOLIK' => 'KATOLIK',
					'KRISTEN' => 'KRISTEN',
					'HINDU'   => 'HINDU',
					'BUDHA'   => 'BUDHA',
				], [ 'prompt' => '' ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'tb' )->textInput() ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
				<?= $form->field( $model, 'bb' )->textInput() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'suku' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'ayah' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'nikah' )->dropDownList( [
					'Married' => 'Married',
					'Single'  => 'Single',
					'Divorce' => 'Divorce',
				], [ 'prompt' => '' ] ) ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
				<?= $form->field( $model, 'pendidikan' )->dropDownList( [
					'SD'  => 'SD',
					'SMP' => 'SMP',
					'SMU' => 'SMU',
					'SMK' => 'SMK',
					'D1'  => 'D1',
					'D3'  => 'D3',
					'S1'  => 'S1',
				], [ 'prompt' => '' ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'sekolah' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'jurusan' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'lulus' )->textInput( [ 'maxlength' => true ] ) ?>
            </div>
        </div>
    </div>
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">If you ever worked in Indonesian</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="box-body">
                <table id="table-indo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Perusahaan</th>
                        <th>Lokasi</th>
                        <th>Posisi Jabatan</th>
                        <th>Masa Kerja</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="modal fade" id="modal-riwayat">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Riwayat Perusahaan Indonesia</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <form role="form" id="form-riwayat">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input type="text" class="form-control" id="riwayat-nama">
                            </div>
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input type="text" class="form-control" id="riwayat-lokasi">
                            </div>
                            <div class="form-group">
                                <label>Posisi</label>
                                <input type="text" class="form-control" id="riwayat-posisi">
                            </div>
                            <div class="form-group">
                                <label>Masa Kerja</label>
                                <input type="text" class="form-control" id="riwayat-masa-kerja">
                            </div>
                            <input type="hidden" class="form-control" id="riwayat-type">
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-riwayat-add-row">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">If you ever worked in Malaysia</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="box-body">
                <table id="table-malay" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Perusahaan</th>
                        <th>Lokasi</th>
                        <th>Posisi Bagian</th>
                        <th>Masa Kerja</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
				<?= $form->field( $model, 'punya_sodara' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'siap_kontrak' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'siap_ikut_aturan' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'resign' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'resign_note' )->textInput() ?>
            </div>
			<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                <div class="form-group">
					<?php
					$format = <<< SCRIPT
function repoFormatResult(repo) {
               var markup =
'<div class="row">' + 
    '<div class="col-sm-3">' + repo.text + '</div>' +
    '<div class="col-sm-3">' + repo.tgl + '</div>' +
    '<div class="col-sm-3">' + repo.no_register + '</div>' +
    '<div class="col-sm-3">' + repo.job_title + '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
        }

        function repoFormatSelection(repo) {
            return repo.text;
        }
SCRIPT;
					$this->registerJs( $format, View::POS_HEAD );
					$url      = \yii\helpers\Url::to( [ 'job-order/list' ] );
					$cityDesc = empty( $model->job_order_id ) ? '' : \app\models\JobOrder::findOne( $model->job_order_id )->company->name;
					echo $form->field( $model, 'job_order_id' )->widget( Select2::classname(), [
						'initValueText' => $cityDesc, // set the initial display text
						'options'       => [ 'placeholder' => 'Search for a job order ...' ],
						'pluginOptions' => [
							'allowClear'         => true,
							'minimumInputLength' => 3,
							'language'           => [ 'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ), ],
							'ajax'               => [
								'url'      => $url,
								'dataType' => 'json',
								'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
							],
							'escapeMarkup'       => new JsExpression( 'function (markup) { return markup; }' ),
							'templateResult'     => new JsExpression( 'repoFormatResult' ),
							'templateSelection'  => new JsExpression( 'repoFormatSelection' ),
						],
					] );
					?>
                </div>
			<?php endif; ?>
            <div class="form-group">
				<?= $form->field( $model, 'formal' )->dropDownList( [
					'FORMAL'   => 'FORMAL',
					'INFORMAL' => 'INFORMAL',
				], [ 'prompt' => '' ] ) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
				<?= $form->field( $model, 'alasan' )->textInput() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'siapkerja' )->widget( MultiSelect::className(), [
					"options"       => [ 'multiple' => "multiple" ],
					'data'          => [
						'Rajin'      => 'Rajin',
						'Disiplin'   => 'Disiplin',
						'KerjaKeras' => 'Kerja Keras',
						'Diperintah' => 'Diperintah',
						'TiapHari'   => 'Tiap Hari',
					],
					"clientOptions" =>
						[
							"includeSelectAllOption" => true,
						],
				] ) ?>
                <!--				--><? //= $form->field( $model, 'siap_kerja[]' )->dropDownList(
				//					[
				//						'Rajin'      => 'Rajin',
				//						'Disiplin'   => 'Disiplin',
				//						'KerjaKeras' => 'KerjaKeras',
				//						'Diperintah' => 'Diperintah',
				//						'TiapHari'   => 'TiapHari',
				//					],
				//					[ 'multiple' => 'multiple' ]
				//				) ?>
            </div>
            <div class="form-group">
				<?
				$option = [];
				if ( ! Yii::$app->user->can( 'akes_tab' ) ) {
					$option['disabled'] = true;
				}
				echo $form->field( $model, 'pl' )->textInput( $option ) ?>
            </div>
            <div class="form-group">
                <? echo $form->field($model, 'note_')->widget(Select2::classname(), [
	                'data' => $rekruiter,
	                'options' => ['placeholder' => 'Select a recruiter ...'],
	                'pluginOptions' => [
		                'allowClear' => true
	                ],
                ]);?>
            </div>
        </div>
    </div>
    <div class="form-group">
		<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
		<?= Html::a( Yii::t( 'app', 'Delete' ), [ 'delete', 'id' => $model->biodata_id ], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
				'method'  => 'post',
			],
		] ) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php
$this->registerJs( $this->render( 'js.php' ) );
?>

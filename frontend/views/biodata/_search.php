<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\BiodataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="biodata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <!--    --><? //= $form->field($model, 'biodata_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'no_hp') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'kecamatan') ?>

    <?php // echo $form->field($model, 'kabupaten') ?>

    <?php // echo $form->field($model, 'propinsi') ?>

    <?php // echo $form->field($model, 'birthdate') ?>

    <?php // echo $form->field($model, 'tempat_lahir') ?>

    <?php // echo $form->field($model, 'agama') ?>

    <?php // echo $form->field($model, 'tb') ?>

    <?php // echo $form->field($model, 'bb') ?>

    <?php // echo $form->field($model, 'suku') ?>

    <?php // echo $form->field($model, 'ayah') ?>

    <?php // echo $form->field($model, 'nikah') ?>

    <?php // echo $form->field($model, 'pendidikan') ?>

    <?php // echo $form->field($model, 'sekolah') ?>

    <?php // echo $form->field($model, 'jurusan') ?>

    <?php // echo $form->field($model, 'lulus') ?>

    <?php // echo $form->field($model, 'punya_sodara') ?>

    <?php // echo $form->field($model, 'alasan') ?>

    <?php // echo $form->field($model, 'siap_kontrak') ?>

    <?php // echo $form->field($model, 'siap_kerja') ?>

    <?php // echo $form->field($model, 'path_foto') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'pl') ?>

    <?php // echo $form->field($model, 'status_proses') ?>

    <?php // echo $form->field($model, 'no_paspor') ?>

    <?php // echo $form->field($model, 'kode_rekomid') ?>

    <?php // echo $form->field($model, 'no_visa') ?>

    <?php // echo $form->field($model, 'tgl_visa') ?>

    <?php // echo $form->field($model, 'endorse') ?>

    <?php // echo $form->field($model, 'asuransi') ?>

    <?php // echo $form->field($model, 'pap_tgl') ?>

    <?php // echo $form->field($model, 'pap_note') ?>

    <?php // echo $form->field($model, 'tiket_tgl') ?>

    <?php // echo $form->field($model, 'tiket_note') ?>

    <?php // echo $form->field($model, 'no_pk') ?>

    <?php // echo $form->field($model, 'cabang_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

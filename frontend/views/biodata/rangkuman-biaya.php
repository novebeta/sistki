<?php
/** @var Biodata $model */
use app\models\Biodata;
use app\models\Fwcms;
use app\models\Medikal; ?>
<div class="box-body">
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="form-group">
                <label for="inputNama" class="control-label col">Nama</label>
                <div>
                    <input readonly="true" class="form-control" id="inputNama" placeholder="Nama"
                           value="<?= $model->name ?>">
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="form-group">
                <label for="inputID" class=" control-label">ID</label>
                <div>
                    <input readonly="true" class="form-control" id="inputID" placeholder="ID"
                           value="<?= $model->no_absen ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="form-group">
                <label for="inputVisa" class=" control-label">No. Calling Visa</label>
                <div>
                    <input readonly="true" class="form-control" id="inputVisa" placeholder="ID"
                           value="<?= $model->no_visa ?>">
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="form-group">
                <label for="inputTgl" class=" control-label">Tanggal Terbang</label>
                <div>
                    <input readonly="true" class="form-control" id="inputTgl" placeholder="Tgl"
                           value="<?= $model->tiket_tgl ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="form-group">
                <label for="inputLoan" class=" control-label">Pinjaman</label>
                <div>
                    <input readonly="true" class="form-control numeric" id="inputLoan" placeholder="loan"
                           value="<?= Yii::$app->formatter->asDecimal($model->loan) ?>">
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="form-group">
                <label for="inputLoanNote" class=" control-label">Keterangan</label>
                <div>
                    <input readonly="true" class="form-control" id="inputLoanNote" placeholder="Loan Note"
                           value="<?= $model->loan_note ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="form-group">
                <label class=" control-label">MEDIKAL</label>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>Voucher</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
						<? $i = 1;
						foreach ((array)$model->getLastExpiredMedikal()->bayar as $b ) : ?>
                            <tr>
                                <td><?= $i;
									$i ++; ?></td>
                                <td><?= Yii::$app->formatter->asDecimal($b->amount) ?></td>
                                <td><?= $b->voucher ?></td>
                                <td><?= $b->note_ ?></td>
                            </tr>
						<? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="form-group">
                <label class=" control-label">PASPOR</label>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>Voucher</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
						<? $i = 1;
						foreach ( (array) $model->getLastPaspor()->bayar as $b ) : ?>
                            <tr>
                                <td><?= $i;
									$i ++; ?></td>
                                <td><?= Yii::$app->formatter->asDecimal($b->amount) ?></td>
                                <td><?= $b->voucher ?></td>
                                <td><?= $b->note_ ?></td>
                            </tr>
						<? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="form-group">
                <label class=" control-label">ISC</label>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>Voucher</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
						<? $i = 1;
						foreach ( (array) $model->getLastIsc()->bayar as $b ) : ?>
                            <tr>
                                <td><?= $i;
									$i ++; ?></td>
                                <td><?= Yii::$app->formatter->asDecimal($b->amount) ?></td>
                                <td><?= $b->voucher ?></td>
                                <td><?= $b->note_ ?></td>
                            </tr>
						<? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="form-group">
                <label class=" control-label">FWCMS</label>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>Voucher</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
						<? $i = 1;
						/** @var Fwcms $fwcms */
						$fwcms = $model->getLastFwcms();
						foreach ( (array) $fwcms->bayar as $b ) : ?>
                            <tr>
                                <td><?= $i;
									$i ++; ?></td>
                                <td><?=Yii::$app->formatter->asDecimal($b->amount) ?></td>
                                <td><?= $b->voucher ?></td>
                                <td><?= $b->note_ ?></td>
                            </tr>
						<? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

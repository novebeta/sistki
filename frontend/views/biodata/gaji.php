<?php /** Biodata $model  */?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>A4</title>
    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: A4;
            margin: 15mm 20mm 20mm 15mm;
        }

        body {
            margin: 15mm 20mm 20mm 15mm;
        }
    </style>
</head>
<body class="A4" onload="window.print();">
<p style="text-align:center;"><strong><span style="font-family:Calibri;font-weight:bold;font-size:21px;">DEKLARASI&nbsp;PENGURANGAN&nbsp;GAJI</span></strong>
</p>
<p>
    <br>
</p>
<p><span style="font-family:Calibri;font-size:15px;">Saya&nbsp;yang&nbsp;bertanda&nbsp;tangan&nbsp;dibawah&nbsp;ini&nbsp;:</span>
</p>
<p style="margin-left: 40px;"><span style="font-family:Calibri;font-size:15px;">Nama</span><span
            style="font-family:Calibri;font-size:15px;">:&nbsp;</span><strong><span
                style="font-family:Calibri;font-weight:bold;font-size:15px;"><?= strtoupper( $model->name ); ?></span></strong>
</p>
<p style="margin-left: 40px;"><span style="font-family:Calibri;font-size:15px;">Tempat/&nbsp;Tgl.&nbsp;Lahir</span><span
            style="font-family:Calibri;font-size:15px;">:&nbsp;</span><strong><span
                style="font-family:Calibri;font-weight:bold;font-size:15px;"><?= strtoupper( $model->tempat_lahir ); ?>,&nbsp;<?= ( new DateTime( $model->birthdate ) )->format( 'd-m-Y' ); ?></span></strong>
</p>
<p style="margin-left: 40px;"><span style="font-family:Calibri;font-size:15px;">Paspor&nbsp;Nomor</span><span
            style="font-family:Calibri;font-size:15px;">:&nbsp;</span><strong><span
                style="font-family:Calibri;font-weight:bold;font-size:15px;"><?= strtolower( $model->no_paspor ); ?></span></strong>
</p>
<p><span style="font-family:Calibri;font-size:15px;">Dengan&nbsp;ini&nbsp;menyatakan&nbsp;bahwa&nbsp;saya&nbsp;sepenuhnya&nbsp;memahami&nbsp;dan&nbsp;menyetujui&nbsp;bahwa&nbsp;
        gaji&nbsp;saya&nbsp;akan&nbsp;dipotong&nbsp;untuk&nbsp;pengembalian&nbsp;biaya&nbsp;proses&nbsp;untuk&nbsp;proses&nbsp;penempatan&nbsp;saya&nbsp;
        dengan&nbsp;pihak&nbsp;perusahaan&nbsp;<?= $model->getCompanyName(); ?>
        &nbsp;di&nbsp;<?= $model->getCompanyCity(); ?>,&nbsp;<?= $model->getCompanyCountry(); ?>.</span>
</p>
<p><span style="font-family:Calibri;font-size:15px;">Pengurangan&nbsp;total&nbsp;<?= $model->getTotalPotongan(); ?>,&nbsp;yang&nbsp;akan&nbsp;dikurangi&nbsp;secara&nbsp;merata&nbsp;di&nbsp;atas&nbsp;<?= $model->getLamaPotongan(); ?>
        &nbsp;bulan.&nbsp;
        Lebih&nbsp;jelasnya&nbsp;adalah&nbsp;sebagai&nbsp;berikut&nbsp;:</span>
	<?
	/** @var null|\app\models\ItemPot[] $items */
	$items = $model->getDetailsPotongan();
	if ( $items != null ) :
	    foreach ( $items as $r ) :
	?>
    </p>
        <p style="margin-left: 40px;"><strong><span style="font-family:Calibri;font-weight:bold;font-size:15px;">
            <?= $r->bulan ?> &ndash; <?= $r->pot; ?>
        </span></strong>
    </p>
    <? endforeach; endif; ?>
<p><span style="font-family:Calibri;font-size:15px;">Saya&nbsp;juga&nbsp;setuju&nbsp;bahwa&nbsp;jika&nbsp;karena&nbsp;alasan&nbsp;apapun&nbsp;kontrak&nbsp;saya&nbsp;dihentikan&nbsp;atau&nbsp;
        saya&nbsp;mengundurkan&nbsp;diri&nbsp;sebelum&nbsp;<?= $model->getTotalPotongan(); ?>&nbsp;telah&nbsp;dibayarkan&nbsp;kembali,&nbsp;
        maka&nbsp;saya&nbsp;wajib&nbsp;membayara&nbsp;hutang&nbsp;saya&nbsp;di&nbsp;PT.&nbsp;DUTA&nbsp;WIBAWA&nbsp;MANDA&nbsp;PUTRA&nbsp;untuk&nbsp;saldo&nbsp;piutang.</span>
</p>
<p><span style="font-family:Calibri;font-size:15px;">Surat&nbsp;ini&nbsp;dibuat&nbsp;sadar&nbsp;besar&nbsp;saya&nbsp;tanpa&nbsp;tekanan&nbsp;dari&nbsp;siapapun&nbsp;dan&nbsp;
        saya&nbsp;akan&nbsp;bertanggung&nbsp;jawab&nbsp;untuk&nbsp;apa&nbsp;yang&nbsp;saya&nbsp;telah&nbsp;menyatakan.</span>
</p>
<p>
    <br>
</p>
<table style="width: 100%;">
    <tbody>
    <tr>
        <td style="width: 155.8pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="33.333333333333336%">
            <p style="margin-bottom:0.0000pt;text-align:center;"><span style="font-family:Calibri;font-size:15px;">Klaten,&nbsp;07&nbsp;Juni&nbsp;2018</span>
            </p>
        </td>
        <td style="width: 127.45pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="27.214170692431562%">
            <p style="margin-bottom:0.0000pt;">
                <br>
            </p>
        </td>
        <td style="width: 184.25pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="39.452495974235106%">
            <p style="margin-bottom:0.0000pt;text-align:center;"><span style="font-family:Calibri;font-size:15px;">Witness,</span>
            </p>
        </td>
    </tr>
    <tr>
        <td style="width: 155.8pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="33.333333333333336%">
            <p style="margin-bottom:0.0000pt;">
                <br>
            </p>
        </td>
        <td style="width: 127.45pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="27.214170692431562%">
            <p style="margin-bottom:0.0000pt;">
                <br>
            </p>
        </td>
        <td style="width: 184.25pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="39.452495974235106%">
            <p style="margin-bottom:0.0000pt;">
                <br>
            </p>
        </td>
    </tr>
    <tr>
        <td style="width: 155.8pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="33.333333333333336%">
            <p style="margin-bottom:0.0000pt;text-align:center;"><strong><span
                            style="font-family:Calibri;font-weight:bold;font-size:15px;"><?= strtoupper( $model->name ); ?></span></strong>
            </p>
            <p style="margin-bottom:0.0000pt;">
                <br>
            </p>
        </td>
        <td style="width: 127.45pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="27.214170692431562%">
            <p style="margin-bottom:0.0000pt;">
                <br>
            </p>
        </td>
        <td style="width: 184.25pt;padding: 0pt 5.4pt;vertical-align: top;" valign="top" width="39.452495974235106%">
            <p style="margin-bottom:0.0000pt;text-align:center;"><strong><span
                            style="font-family:Calibri;font-weight:bold;font-size:15px;">PT.&nbsp;DUTA&nbsp;WIBAWA&nbsp;MANDA&nbsp;PUTRA</span></strong>
            </p>
        </td>
    </tr>
    </tbody>
</table>
<p>
    <br>
</p>
</body>
</html>
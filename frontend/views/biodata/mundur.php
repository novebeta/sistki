<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BiodataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'CPMI Mundur');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header"><h3 class="box-title"><i class="fa fa-table"></i>&nbsp;Table</h3></div>
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?=
        \yiister\adminlte\widgets\grid\GridView::widget(
            [
                "dataProvider" => $dataProvider,
                "condensed" => true,
                "hover" => true,
                "columns" => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    'no_hp',
                    'address',
                    'tempat_lahir',
                    'birthdate',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{download} {view} {update} {delete}',
                        'buttons' => [
                            'download' => function ($url, $model, $key) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-play"></span>',
                                    ['biodata/select', 'id' => $model->biodata_id],
                                    [
                                        'title' => 'Select',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                        ]
                    ],
                ]
            ]
        );
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>



<?php
use app\models\Dokumen;
use app\models\DokumenUpload;
use frontend\assets\LightBoxAsset;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\Biodata */
/* @var $dataProviderFwcms \yii\data\ActiveDataProvider */
/* @var $searchModelMedikal \app\models\MedikalSearch */
/* @var $searchModelRekomPaspor \app\models\RekomPasporSearch */
/* @var $dataProviderMedikal \yii\data\ActiveDataProvider */
/* @var $searchModelFwcms \app\models\FwcmsSearch */
/* @var $dataProviderIsc \yii\data\ActiveDataProvider */
/* @var $dataProviderPaspor \yii\data\ActiveDataProvider */
/* @var $searchModelIsc \app\models\IscSearch */
/* @var $dataProviderRekomid \yii\data\ActiveDataProvider */
/* @var $searchModelPaspor \app\models\PasporSearch */
/* @var $searchModelRekomid \app\models\RekomidSearch */
/* @var $dataProviderRekomPaspor \yii\data\ActiveDataProvider */
/* @var $pra \app\models\Pra */
/* @var $mp \app\models\Mp */
LightBoxAsset::register( $this );
$this->title                   = $model->name;
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
//var_dump($_SESSION);
$menikah      = [
	'Married' => [
		'MENIKAH',
		'Married'
	],
	'Single'  => [
		'BELUM MENIKAH',
		'Single'
	],
	'Divorce' => [
		'CERAI',
		'Divorce'
	]
];
$riwayatIndo  = Json::decode( $indo );
$riwayatMalay = Json::decode( $malay );
$tab2         = '';
if ( $model->getMedikals()->count( 'medikal_id' ) == 0 ) {
	$tab2 = 'style="color:red" ';
}
$tab3 = '';
if ( $model->getRekoms()->count( 'rekom_id' ) == 0 ) {
	$tab3 = 'style="color:red" ';
}
$tab4 = '';
if ( $model->getRekomPaspor()->count( 'rekom_paspor_id' ) == 0 ) {
	$tab4 = 'style="color:red" ';
}
$tab5 = '';
if ( $model->getPaspor()->count( 'paspor_id' ) == 0 ) {
	$tab5 = 'style="color:red" ';
}
$tab6 = '';
if ( $model->getFwcms()->count( 'fwcms_id' ) == 0 ) {
	$tab6 = 'style="color:red" ';
}
$tab7 = '';
if ( $model->getIscs()->count( 'isc_id' ) == 0 ) {
	$tab7 = 'style="color:red" ';
}
$tab14 = '';
if ( $model->apply_visa == '' ) {
	$tab14 = 'style="color:red" ';
}
$tab8 = '';
if ( $model->no_visa == '' && $model->tgl_visa == '' ) {
	$tab8 = 'style="color:red" ';
}
$tab16 = '';
if ( $model->tipe_bayar_copvisa == '' ) {
	$tab16 = 'style="color:red" ';
}
$tab10 = '';
if ( $model->pap_note == '' && $model->pap_tgl == '' && $model->no_pk == '' ) {
	$tab10 = 'style="color:red" ';
}
$tab11 = '';
if ( $model->tiket_note == '' && $model->tiket_tgl == '' ) {
	$tab11 = 'style="color:red" ';
}
$tab15 = '';
if ( $model->kasus == '' && $model->note_kasus == '' ) {
	$tab15 = 'style="color:red" ';
}
$tab17 = '';
if ( $pra->no_polis == '' && $pra->tgl == '' && $pra->tgl_expired == '' && $pra->note_ == '' ) {
	$tab17 = 'style="color:red" ';
}
$tab18 = '';
if ( $mp->no_polis == '' && $mp->tgl == '' && $mp->tgl_expired == '' && $mp->note_ == '' ) {
	$tab18 = 'style="color:red" ';
}
$tab19 = '';
if ( floatval( $model->loan ) == 0 && $model->loan_note == '' ) {
	$tab19 = 'style="color:red" ';
}
$tab12 = 0;
//foreach ( \app\models\Dokumen::JENIS_ARR as $item ) {
//	if ( \app\models\DokumenUpload::isFileDokumenExists( $item,
//		$model->biodata_id ) ) {
//		$tab12 = '';
//	}
//}
?>
    <style>
        .table-general td {
            padding: 0pt 5pt 0pt 5pt;
            border: 1.0000pt solid windowtext;
        }

        .table-inside td {
            border: 0pt none;
        }

        .coret {
            text-decoration: line-through;;
        }

        .kv-grid-container {
            overflow-x: hidden;
        }
    </style>
    <div class="nav-tabs-custom" id="tabs">
        <ul class="nav nav-tabs" id="prodTabs">
			<?php if ( ! $model->isNewRecord ) : ?>
                <li class="active"><a href="#tab_13" data-toggle="tab">GENERAL</a></li>
			<?php endif; ?>
            <li><a href="#tab_1" data-toggle="tab">BIODATA</a></li>
			<?php if ( ! $model->isNewRecord ) : ?>
                <li><a href="#tab_2" <?= $tab2; ?> data-toggle="tab"
                       data-url="<?= Url::toRoute( [ 'medikal/index' ] ) ?>">MEDIKAL</a></li>
                <li><a href="#tab_3" <?= $tab3; ?> data-toggle="tab"
                       data-url="<?= Url::toRoute( [ 'rekomid/index' ] ) ?>">REKOM ID</a></li>
                <li><a href="#tab_17" <?= $tab17; ?> data-toggle="tab">PRA</a></li>
                <li><a href="#tab_4" <?= $tab4; ?> data-toggle="tab"
                       data-url="<?= Url::toRoute( [ 'rekom-paspor/index' ] ) ?>">REKOM PASPOR</a></li>
                <li><a href="#tab_5" <?= $tab5; ?> data-toggle="tab"
                       data-url="<?= Url::toRoute( [ 'paspor/index' ] ) ?>">PASPOR</a></li>
				<?php //if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                    <li><a href="#tab_6" <?= $tab6; ?> data-toggle="tab"
                           data-url="<?= Url::toRoute( [ 'fwcms/index' ] ) ?>">FWCMS</a></li>
                    <li><a href="#tab_7" <?= $tab7; ?> data-toggle="tab"
                           data-url="<?= Url::toRoute( [ 'isc/index' ] ) ?>">ISC</a>
                    </li>
                    <li><a href="#tab_14" <?= $tab14; ?> data-toggle="tab">APPLY VISA</a></li>
                    <li><a href="#tab_8" <?= $tab8; ?> data-toggle="tab">CALLING VISA</a></li>
                    <li><a href="#tab_18" <?= $tab18; ?> data-toggle="tab">MP</a></li>
                    <li><a href="#tab_16" <?= $tab16; ?> data-toggle="tab">COP VISA</a></li>
				<?php //endif; ?>
				<?php if ( $model->no_visa != null ) : ?>
                    <li><a href="#tab_9" data-toggle="tab">ENDORSE</a></li>
				<?php endif; ?>
                <li><a href="#tab_10" <?= $tab10; ?> data-toggle="tab">PAP</a></li>
				<?php //if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                    <li><a href="#tab_11" <?= $tab11; ?> data-toggle="tab">JADWAL TERBANG</a></li>
                    <li><a href="#tab_15" <?= $tab15; ?> data-toggle="tab">KASUS</a></li>
				<?php //endif; ?>
                <li><a href="#tab_12" data-toggle="tab"
                       data-url="<?= Url::toRoute( [ 'dokumen-upload/biodata' ] ) ?>">DOKUMEN</a></li>
                <li><a href="#tab_19" <?= $tab19; ?> data-toggle="tab">PINJAMAN</a></li>
			<?php endif; ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_13">
                <div class="box-body">
                    <img class="pull-right  img-thumbnail" width="152px" height="200px"
                         src="<?= DokumenUpload::initialPreview( Dokumen::PAS_PHOTO, $model->biodata_id, true ); ?>"
                         alt="Another alt text">
                    <table class="table-general" style="width: 650pt; border-collapse: collapse; font-size: 10pt;"
                           border="1">
                        <tbody>
                        <tr>
                            <td style="width: 46.0500pt; padding: 0pt 5pt 0pt 5pt; border: 1.0000pt solid windowtext;"
                                rowspan="2" valign="top" width="61">
                                <p style="font-size: 12pt;"><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">NO.&nbsp;ABSENSI&nbsp;</span></strong>
                                </p>
                            </td>
                            <td rowspan="2" valign="top" width="71">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->no_absen; ?></span></strong>
                                </p>
                            </td>
                            <td valign="top" width="96">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">TANGGAL</span></strong>
                                </p>
                            </td>
                            <td valign="top" width="372">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><? ?></span></strong>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="96">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">PERUSAHAAN</span></strong>
                                </p>
                            </td>
                            <td valign="top" width="372">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->jobOrder->company->name; ?></span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">&nbsp;</span></strong>
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"><span
                                style="font-family: Calibri; font-size: 1.0000pt;">&nbsp;</span></p>
                    <table class="table-general"
                           style="height: 135px; width: 650pt; border-collapse: collapse; font-size: 10pt;" border="1">
                        <tbody>
                        <tr style="height: 25.1500pt;">
                            <td style="width: 109.625px; padding: 0pt 5.4pt; border: 1pt solid windowtext; height: 40px;"
                                valign="center">
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">NAMA&nbsp;LENGKAP</span></strong>
                                </p>
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><span style="font-family: Calibri; font-size: 8.0000pt;">(Full&nbsp;name)</span>
                                </p>
                            </td>
                            <td style="height: 40px; width: 260px;" valign="top">
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 14pt;"><?= $model->name; ?></span></strong>
                                </p>
                            </td>
                            <td style="height: 40px; width: 39px;" valign="top">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">NO&nbsp;HP&nbsp;</span></strong>
                                </p>
                            </td>
                            <td style="height: 40px; width: 322px;" valign="top">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->no_hp; ?></span></strong>
                                </p>
                            </td>
                        </tr>
                        <tr style="height: 18.6000pt;">
                            <td style="height: 86px; width: 122px;" rowspan="2" valign="center">
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">ALAMAT&nbsp;LENGKAP</span></strong>
                                </p>
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><span style="font-family: Calibri; font-size: 8.0000pt;">(Home&nbsp;address)</span>
                                </p>
                            </td>
                            <td style="height: 46px; width: 627px;" colspan="3">
                                <p><?= $model->address; ?></p>
                            </td>
                        </tr>
                        <tr style="height: 26.7000pt;">
                            <td style="height: 40px; width: 627px;" colspan="3" valign="top">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">KECAMATAN&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KABUPATEN&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROPINSI&nbsp;:</span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->kecamatan; ?>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $model->kabupaten; ?>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $model->propinsi; ?>
                                            &nbsp;</span></strong>
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"><span
                                style="font-family: Calibri; font-size: 6.0000pt;">&nbsp;</span></p>
                    <table class="table-general" style="width: 100%; border-collapse: collapse; font-size: 10pt;"
                           border="1">
                        <tbody>
                        <tr style="height: 35.5000pt;">
                            <td style="width: 85.5000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;"
                                valign="center" width="114">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">TGL&nbsp;LAHIR&nbsp;</span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 8.0000pt;">(Date&nbsp;of&nbsp;birth)</span>
                                </p>
                            </td>
                            <td valign="center" width="96">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 10.0000pt;"><?= $model->birthdate; ?></span>
                                </p>
                            </td>
                            <td colspan="2" valign="center" width="108">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">TEMPAT&nbsp;LAHIR&nbsp;</span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 8.0000pt;">(Place&nbsp;of&nbsp;birth)</span>
                                </p>
                            </td>
                            <td valign="center" width="144">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 10.0000pt;"><?= $model->tempat_lahir; ?></span>
                                </p>
                            </td>
                            <td valign="center" width="102">
                                <table class="table-inside">
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">USIA</span></strong>
                                            </p>
                                        </td>
                                        <td rowspan="2">
											<?= $model->age; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Age)</span>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="center" width="192">
                                <table class="table-inside">
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">AGAMA</span></strong>
                                            </p>
                                        </td>
                                        <td rowspan="2">
											<?= $model->agama; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Religion)</span>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 31.0000pt;">
                            <td valign="center" width="114">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">TINGGI&nbsp;BADAN&nbsp;</span></strong><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">(Height)</span></p>
                            </td>
                            <td valign="center" width="96">
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->tb; ?></span></strong>
                                </p>
                            </td>
                            <td valign="center" width="60">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">BERAT&nbsp;BADAN&nbsp;</span></strong><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">(Weight)</span></p>
                            </td>
                            <td valign="center" width="48">
                                <p style="text-align: center; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"
                                   align="center"><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->bb; ?></span></strong>
                                </p>
                            </td>
                            <td valign="center" width="144">
                                <table class="table-inside">
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">SUKU</span></strong>
                                            </p>
                                        </td>
                                        <td rowspan="2">
											<?= $model->suku; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Race)</span>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="2" valign="center" width="294">
                                <table class="table-inside">
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">NAMA&nbsp;AYAH</span></strong>
                                            </p>
                                        </td>
                                        <td rowspan="2">
											<?= $model->ayah; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Father&nbsp;name)</span>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 27.4000pt;">
                            <td colspan="2" valign="top" width="210">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">STATUS&nbsp;PERNIKAHAN</span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 8.0000pt;">(Marital&nbsp;status)</span>
                                </p>
                            </td>
                            <td colspan="5" valign="center" width="546">
								<? if ( $model->nikah != null ) : ?>
                                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                        <strong><span
                                                    style="font-family: Calibri; color: #000000; font-weight: bold; font-size: 10.0000pt;"><?= $menikah[ $model->nikah ][0]; ?></span></strong>
                                    </p>
                                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                        <span style="font-family: Calibri; color: #000000; font-size: 8.0000pt;">(<?= $menikah[ $model->nikah ][1]; ?>
                                            )</span></p>
								<? endif; ?>
                            </td>
                        </tr>
                        <tr style="height: 7.1500pt;">
                            <td colspan="7" valign="top" width="756">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 7.0000pt;">&nbsp;&nbsp;</span></strong>
                                </p>
                            </td>
                        </tr>
                        <tr style="height: 39.1000pt;">
                            <td colspan="7" valign="top" width="756">
                                <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">LULUSAN&nbsp;PENDIDIKAN</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">&nbsp;</span></strong><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">(Education&nbsp;graduated)</span><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">&nbsp;:&nbsp;&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;"><?= $model->pendidikan; ?></span></strong>
                                </p>
                                <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">NAMA&nbsp;SEKOLAH</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">&nbsp;</span></strong><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">(Name&nbsp;of&nbsp;school): </span><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $model->sekolah ?>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">JURUSAN</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 8.0000pt;">(</span></strong><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">Majoring)</span><span
                                            class="15"
                                            style="font-family: 'Times New Roman';">&nbsp;</span><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">: <?= $model->jurusan; ?></span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LULUS&nbsp;TAHUN&nbsp;(</span></strong><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">Year&nbsp;of&nbsp;graduated)&nbsp;: <?= $model->lulus; ?></span>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">&nbsp;</span></strong>
                                </p>
                            </td>
                        </tr>
						<? if ( count( $riwayatIndo ) > 0 ) : ?>
                            <tr style="height: 49.9000pt;">
                                <td colspan="7" valign="top" width="756">
                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                        <strong><span
                                                    style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">JIKA&nbsp;PERNAH&nbsp;BEKERJA&nbsp;DI PERUSAHAAN INDONESIA </span></strong><span
                                                class="15" style="font-family: Calibri; font-size: 8.0000pt;">(If you ever worked in Indonesian)</span>
                                    </p>
                                    <table class="table-inside">
                                        <tr>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">NAMA PERUSAHAAN</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Company name)</span>
                                            </td>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">LOKASI</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Location)</span>
                                            </td>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">POSISI JABATAN</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Position as)</span>
                                            </td>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">MASA KERJA</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Working period)</span>
                                            </td>
                                        </tr>
										<?php $counter = 1;
										foreach ( $riwayatIndo as $item ) : ?>
                                            <tr>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $counter . '. ' . $item[0]; ?></span></strong>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $item[1]; ?></span></strong>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $item[2]; ?></span></strong>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $item[3]; ?></span></strong>
                                                    </p>
                                                </td>
                                            </tr>
											<?php $counter ++; endforeach; ?>
                                    </table>
                                </td>
                            </tr>
						<? endif; ?>
						<? if ( count( $riwayatMalay ) > 0 ) : ?>
                            <tr style="height: 63.6500pt;">
                                <td colspan="7" valign="top" width="756">
                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                        <strong><span
                                                    style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">JIKA&nbsp;PERNAH&nbsp;BEKERJA&nbsp;DI PERUSAHAAN MALAYSIA </span></strong><span
                                                class="15" style="font-family: Calibri; font-size: 8.0000pt;">(If you ever worked in Malaysia)</span>
                                    </p>
                                    <table class="table-inside">
                                        <tr>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">NAMA PERUSAHAAN</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Company name)</span>
                                            </td>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">LOKASI</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Location)</span>
                                            </td>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">POSISI JABATAN</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Position as)</span>
                                            </td>
                                            <td>
                                                <strong><span
                                                            style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">MASA KERJA</span></strong>
                                                <span style="font-family: Calibri; font-size: 8.0000pt;">(Working period)</span>
                                            </td>
                                        </tr>
										<?php $counter = 1;
										foreach ( $riwayatMalay as $item ) : ?>
                                            <tr>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $counter . '. ' . $item[0]; ?></span></strong>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $item[1]; ?></span></strong>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $item[2]; ?></span></strong>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p style="line-height: 150%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                                        <strong><span
                                                                    style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;"><?= $item[3]; ?></span></strong>
                                                    </p>
                                                </td>
                                            </tr>
											<?php $counter ++; endforeach; ?>
                                    </table>
                                </td>
                            </tr>
						<? endif; ?>
                        <tr style="height: 31.0000pt;">
                            <td colspan="7" valign="center" width="756">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">APAKAH&nbsp;ANDA&nbsp;MEMILIKI&nbsp;TEMAN&nbsp;/&nbsp;SAUDARA&nbsp;DI&nbsp;MALAYSIA&nbsp;&nbsp;</span></strong><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">(Do&nbsp;you</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">have&nbsp;a</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">friend</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">/</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">family</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">in</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">Malaysia</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">)</span><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 8.0000pt;">&nbsp;?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; color: #000000; font-weight: bold; font-size: 9.5000pt;">
                                            <?= $model->punya_sodara ? 'Yes' : 'No'; ?></span></strong>
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"><span
                                style="font-family: Calibri; font-size: 5.0000pt;">&nbsp;</span></p>
                    <table class="table-general"
                           style="height: 280px; width: 100%; border-collapse: collapse; font-size: 10pt;" border="1">
                        <tbody>
                        <tr style="height: 24.7000pt;">
                            <td style="width: 1156.62px; padding: 0pt 5.4pt; border: 1pt solid windowtext; height: 80px;"
                                valign="top">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">SEBUTKAN&nbsp;ALASAN&nbsp;INGIN&nbsp;BEKERJA&nbsp;DI&nbsp;MALAYSIA&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 8.0000pt;">&nbsp;</span></strong><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">(Specify&nbsp;the</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">reason</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">to&nbsp;apply</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">job&nbsp;in&nbsp;&nbsp;Malaysia</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">)&nbsp;?</span></p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span class="16"
                                                  style="font-family: Calibri; font-weight: bold; font-size: 8.0000pt;"><?= $model->alasan; ?></span></strong>
                                </p>
                            </td>
                        </tr>
                        <tr style="height: 22.4500pt;">
                            <td style="height: 40px; width: 1169px;" valign="center">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">SIAP&nbsp;TERIKAT&nbsp;KONTRAK&nbsp;KERJA&nbsp;MINIMAL&nbsp;2&nbsp;TAHUN&nbsp;TIDAK&nbsp;PULANG&nbsp;&nbsp;</span></strong><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">(Able&nbsp;to</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">be&nbsp;bound&nbsp;by</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">working&nbsp;contract</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">for&nbsp;</span><span
                                            class="16" style="font-family: Calibri; font-size: 8.0000pt;">2&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">years</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">not</span><span
                                            class="16"
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">go&nbsp;home)&nbsp;?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong><span
                                                style="font-family: Calibri; color: #000000; font-weight: bold; font-size: 9.5000pt;"><?= $model->siap_kontrak ? 'Yes' : 'No'; ?></span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span class="15" style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </p>
                            </td>
                        </tr>
                        <tr style="height: 27.8500pt;">
                            <td style="height: 80px; width: 1169px;" valign="center">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">SIAP&nbsp;UNTUK&nbsp;BEKERJA&nbsp;</span></strong><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">Able&nbsp;to&nbsp;working&nbsp;</span><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">:&nbsp;(dilingkari&nbsp;yg&nbsp;dipilh)</span>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span></p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span <?= ( $model->siap_kerja != 'Rajin' ) ? 'class="coret"' : ''; ?>
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">RAJIN&nbsp;</span></strong>
                                    <span <?= ( $model->siap_kerja != 'Rajin' ) ? 'class="coret"' : ''; ?>
                                            style="font-family: Calibri; font-size: 8.0000pt;">Diligent</span>
                                    <strong><span style="font-family: Calibri; font-size: 8.0000pt;">/</span></strong>
                                    <strong><span <?= ( $model->siap_kerja != 'Disiplin' ) ? 'class="coret"' : ''; ?>
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;">DISIPLIN&nbsp;</span></strong>
                                    <span <?= ( $model->siap_kerja != 'Disiplin' ) ? 'class="coret"' : ''; ?>
                                            style="font-family: Calibri; font-size: 8.0000pt;">Discipline</span>
                                    <strong><span style="font-family: Calibri; font-size: 8.0000pt;">/</span></strong>
                                    <strong><span <?= ( $model->siap_kerja != 'KerjaKeras' ) ? 'class="coret"' : ''; ?>
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;"> KERJA&nbsp;KERAS&nbsp;</span></strong>
                                    <span <?= ( $model->siap_kerja != 'KerjaKeras' ) ? 'class="coret"' : ''; ?>
                                            style="font-family: Calibri; font-size: 8.0000pt;">Hard&nbsp;working</span>
                                    <strong><span style="font-family: Calibri; font-size: 8.0000pt;">/</span></strong>
                                    <strong><span <?= ( $model->siap_kerja != 'Diperintah' ) ? 'class="coret"' : ''; ?>
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;"> DIPERINTAH </span></strong>
                                    <span <?= ( $model->siap_kerja != 'Diperintah' ) ? 'class="coret"' : ''; ?>
                                            style="font-family: Calibri; font-size: 8.0000pt;">Obedient</span>
                                    <strong><span style="font-family: Calibri; font-size: 8.0000pt;">/</span></strong>
                                    <strong><span <?= ( $model->siap_kerja != 'TiapHari' ) ? 'class="coret"' : ''; ?>
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.5000pt;"> SETIAP&nbsp;HARI&nbsp;KERJA&nbsp;12&nbsp;JAM </span></strong>
                                    <span <?= ( $model->siap_kerja != 'TiapHari' ) ? 'class="coret"' : ''; ?>
                                            style="font-family: Calibri; font-size: 8.0000pt;">Working&nbsp;for&nbsp;12&nbsp;hours&nbsp;everyday</span>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span style="font-family: Calibri; font-size: 9.5000pt;">&nbsp;</span></p>
                            </td>
                        </tr>
                        <tr style="height: 22.9000pt;">
                            <td style="height: 80px; width: 1169px;" valign="bottom">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 4.0000pt;">&nbsp;</span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">SIAP&nbsp;MENGIKUTI&nbsp;SELURUH&nbsp;PERATURAN&nbsp;YANG&nbsp;DIBERLAKUKAN&nbsp;OLEH&nbsp;PIHAK&nbsp;MANAJEMEN&nbsp;PABRIK&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">DI&nbsp;MALAYSIA</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong><strong><span
                                                style="font-family: Calibri; color: #000000; font-weight: bold; font-size: 9.5000pt;"><?= $model->siap_ikut_aturan ? 'Yes' : 'No'; ?></span></strong>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span class="15"
                                          style="font-family: Calibri; font-size: 8.0000pt;">Able&nbsp;to</span><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">follow</span><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;all&nbsp;of&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">rules</span><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">imposed&nbsp;by</span><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;</span><span
                                            class="15"
                                            style="font-family: Calibri; font-size: 8.0000pt;">entire</span><span
                                            style="font-family: Calibri; font-size: 8.0000pt;">&nbsp;company&nbsp;</span><span
                                            class="15" style="font-family: Calibri; font-size: 8.0000pt;">management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </p>
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span style="font-family: Calibri; font-weight: bold; font-size: 6.0000pt;">&nbsp;</span></strong>
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"><span
                                style="font-family: Calibri; font-size: 5.0000pt;">&nbsp;</span></p>
                    <!--
                    <table class="table-general" style="width: 50%; border-collapse: collapse; font-size: 10pt;"
                           border="1">
                        <tbody>
                        <tr style="height: 63.2000pt;">
                            <td style="width: 108.0000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;"
                                valign="center" width="144">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">INTERVIEW&nbsp;RESULT</span></strong>
                                </p>
                            </td>
                            <td valign="top" width="174">
                                <p style="line-height: 200%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span
                                            style="font-family: Calibri; font-size: 10.0000pt;">Accepted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </p>
                                <p style="line-height: 200%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span
                                            style="font-family: Calibri; font-size: 10.0000pt;">KIV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </p>
                                <p style="line-height: 200%; margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <span
                                            style="font-family: Calibri; font-size: 10.0000pt;">Rejected</span></p>
                            </td>
                            <td rowspan="2" valign="top" width="438">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">REMARK&nbsp;:</span></strong>
                                </p>
                            </td>
                        </tr>
                        <tr style="height: 20.9000pt;">
                            <td valign="center" width="144">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">INTERVIEWER&nbsp;NAME</span></strong>
                                </p>
                            </td>
                            <td valign="center" width="174">
                                <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;">
                                    <strong><span
                                                style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">&nbsp;</span></strong>
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    -->
                    <p style="margin: 0pt 0pt 0.0001pt; font-family: 'Times New Roman'; font-size: 12pt;"><span
                                style="font-family: 'Times New Roman'; font-size: 10.0000pt;">&nbsp;</span></p>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Rangkuman Biaya</h4>
                                </div>
                                <div class="modal-body" id="rangkuman-konten">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>
					<?
					echo Html::button( '<i class="glyphicon glyphicon-print"></i> Download Biodata', [
						'type'    => 'button',
						'title'   => Yii::t( 'kvgrid', 'Download Biodata' ),
						'class'   => 'btn btn-primary',
						'onclick' => 'window.open("' . Url::toRoute( [
								"biodata/print",
								'id' => $model->biodata_id
							] ) . '")'
					] );
					echo "&nbsp;";
					echo Html::button( '<i class="glyphicon glyphicon-piggy-bank"></i> Rangkuman Biaya', [
						'id'          => 'modal-rangkuman-biaya',
						'remote'      => Url::toRoute( "biodata/rangkuman" ),
						'type'        => 'button',
						'title'       => Yii::t( 'kvgrid', 'Rangkuman Biaya' ),
						'class'       => 'btn btn-success',
						'data-toggle' => 'modal',
						'data-target' => "#exampleModal"
					] );
					?>
                </div>
            </div>
            <div class="tab-pane" id="tab_1">
				<?= $this->render( '_form', [
					'model' => $model,
					'indo'  => $indo,
					'malay' => $malay,
				] ) ?>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <div class="box-body">
					<?php Pjax::begin(); ?>
					<?
					try {
						echo \kartik\grid\GridView::widget(
							[
								"dataProvider"       => $dataProviderMedikal,
								'filterModel'        => $searchModelMedikal,
								"condensed"          => true,
								"hover"              => true,
								'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'pjax'               => true,
								'toolbar'            => [
									[
										'content' =>
											Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
												'type'    => 'button',
												'title'   => Yii::t( 'kvgrid', 'Add Medikal' ),
												'class'   => 'btn btn-success',
												'onclick' => 'location.href="' . Url::toRoute( "medikal/create" ) . '"'
											] ) . ' ' .
											Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'medikal/index' ], [
												'data-pjax' => 0,
												'class'     => 'btn btn-default',
												'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
											] )
									],
									'{export}',
									'{toggleData}',
								],
								'export'             => [
									'fontAwesome' => true
								],
								'responsive'         => true,
								'floatHeader'        => true,
								'floatHeaderOptions' => [ 'scrollingTop' => true ],
								'showPageSummary'    => false,
								'toggleDataOptions'  => [ 'minCount' => 10 ],
								'panel'              => [
									'heading' => false,
//									'footer'  => false,
									//							'type'    => GridView::TYPE_PRIMARY,
									//							'heading' => '<i class="glyphicon glyphicon-book"></i>  Medikal',
								],
								"columns"            => [
									[
										'class'          => 'kartik\grid\SerialColumn',
										'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
										'width'          => '36px',
										'header'         => '',
										'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
									],
									'tgl',
									'medikal_center',
									'biaya',
									'input_data',
									[
										'class'      => 'kartik\grid\ActionColumn',
										'template'   => '{update} {delete}',
										'urlCreator' => function ( $action, $model, $key, $index ) {
											$url = Url::toRoute( [
												'medikal/' . $action,
												'id' => $key
											] ); // your own url generation logic
											return $url;
										}
									]
								]
							]
						);
					} catch ( Exception $e ) {
						echo $e->getMessage();
					}
					?>
					<?php Pjax::end(); ?>
                </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3">
                <div class="box-body">
					<?php Pjax::begin(); ?>
					<?
					try {
						echo \kartik\grid\GridView::widget(
							[
								"dataProvider"       => $dataProviderRekomid,
								'filterModel'        => $searchModelRekomid,
								"condensed"          => true,
								"hover"              => true,
								'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'pjax'               => true,
								'toolbar'            => [
									[
										'content' =>
											Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
												'type'    => 'button',
												'title'   => Yii::t( 'kvgrid', 'Add Rekom ID' ),
												'class'   => 'btn btn-success',
												'onclick' => 'location.href="' . Url::toRoute( "rekomid/create" ) . '"'
											] ) . ' ' .
											Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'rekomid/index' ], [
												'data-pjax' => 0,
												'class'     => 'btn btn-default',
												'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
											] )
									],
									'{export}',
									'{toggleData}',
								],
								'export'             => [
									'fontAwesome' => true
								],
								'responsive'         => true,
								'floatHeader'        => true,
								'floatHeaderOptions' => [ 'scrollingTop' => true ],
								'showPageSummary'    => false,
								'toggleDataOptions'  => [ 'minCount' => 10 ],
								'panel'              => [
									'heading' => false,
//									'footer'  => false,
									'before'  => Html::button( '<i class="glyphicon glyphicon-print"></i> Print Perjanjian Penempatan', [
										'type'    => 'button',
										'title'   => Yii::t( 'kvgrid', 'Print Perjanjian Penempatan' ),
										'class'   => 'btn btn-primary',
										'onclick' => 'window.open("' . Url::toRoute( [
												"rekomid/print",
												'id' => $_SESSION[ "BIODATA" ]
											] ) . '")'
									] ),
									//							'type'    => GridView::TYPE_PRIMARY,
									//							'heading' => '<i class="glyphicon glyphicon-book"></i>  Rekom ID',
								],
								"columns"            => [
									[
										'class'          => 'kartik\grid\SerialColumn',
										'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
										'width'          => '36px',
										'header'         => '',
										'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
									],
									'tgl',
									'rekomid',
									'rbb_duta',
									'pic',
									'note_',
									[
										'class'      => 'kartik\grid\ActionColumn',
										'template'   => '{update} {delete}',
										'urlCreator' => function ( $action, $model, $key, $index ) {
											$url = Url::toRoute( [
												'rekomid/' . $action,
												'id' => $key
											] ); // your own url generation logic
											return $url;
										}
									],
								]
							]
						);
					} catch ( Exception $e ) {
						echo $e->getMessage();
					}
					?>
					<?php Pjax::end(); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_17">
				<?php $form = ActiveForm::begin( [
					'action' => ( $pra->isNewRecord ) ? Url::toRoute( [
						"pra/create",
						'id' => $model->biodata_id
					] ) : Url::toRoute( [
						"pra/update",
						'id' => $model->biodata_id
					] ),
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $pra, 'no_polis' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $pra, 'tgl' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => true,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								],
								'pluginEvents'  => [
									'changeDate' => "function(e) {  
						                var date = e.date;
						                var newDate = new Date(date.setMonth(date.getMonth()+5));
						                $('#pra-tgl_expired-disp-kvdate').kvDatepicker('update', newDate);
						                $('#pra-tgl_expired').val(toJSONLocal(newDate));
						            }"
								]
							]
						] );
						?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $pra, 'tgl_expired' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => true,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] );
						?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $pra, 'note_' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
					<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                        <div class="form-group">
							<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                        </div>
					<?php endif; ?>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_4">
                <div class="box-body">
					<?php Pjax::begin(); ?>
					<?
					try {
						echo \kartik\grid\GridView::widget(
							[
								"dataProvider"       => $dataProviderRekomPaspor,
								'filterModel'        => $searchModelRekomPaspor,
								"condensed"          => true,
								"hover"              => true,
								'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'pjax'               => true,
								'toolbar'            => [
									[
										'content' =>
											Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
												'type'    => 'button',
												'title'   => Yii::t( 'kvgrid', 'Add Rekom Paspor' ),
												'class'   => 'btn btn-success',
												'onclick' => 'location.href="' . Url::toRoute( "rekom-paspor/create" ) . '"'
											] ) . ' ' .
											Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'rekom-paspor/index' ], [
												'data-pjax' => 0,
												'class'     => 'btn btn-default',
												'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
											] )
									],
									'{export}',
									'{toggleData}',
								],
								'export'             => [
									'fontAwesome' => true
								],
								'responsive'         => true,
								'floatHeader'        => true,
								'floatHeaderOptions' => [ 'scrollingTop' => true ],
								'showPageSummary'    => false,
								'toggleDataOptions'  => [ 'minCount' => 10 ],
								'panel'              => [
									'heading' => false,
//									'footer'  => false,
									//							'type'    => GridView::TYPE_PRIMARY,
									//							'heading' => '<i class="glyphicon glyphicon-book"></i>  Rekom Paspor',
								],
								"columns"            => [
									[
										'class'          => 'kartik\grid\SerialColumn',
										'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
										'width'          => '36px',
										'header'         => '',
										'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
									],
									'tgl',
									'prov',
									'imigrasi',
									'pic',
									'note_',
									[
										'class'      => 'kartik\grid\ActionColumn',
										'template'   => '{update} {delete}',
										'urlCreator' => function ( $action, $model, $key, $index ) {
											$url = Url::toRoute( [
												'rekom-paspor/' . $action,
												'id' => $key
											] ); // your own url generation logic
											return $url;
										}
									],
								]
							]
						);
					} catch ( Exception $e ) {
						echo $e->getMessage();
					}
					?>
					<?php Pjax::end(); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_5">
                <div class="box-body">
					<?php Pjax::begin(); ?>
					<?
					try {
						echo \kartik\grid\GridView::widget(
							[
								"dataProvider"       => $dataProviderPaspor,
								'filterModel'        => $searchModelPaspor,
								"condensed"          => true,
								"hover"              => true,
								'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'pjax'               => true,
								'toolbar'            => [
									[
										'content' =>
											Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
												'type'    => 'button',
												'title'   => Yii::t( 'kvgrid', 'Add Paspor' ),
												'class'   => 'btn btn-success',
												'onclick' => 'location.href="' . Url::toRoute( "paspor/create" ) . '"'
											] ) . ' ' .
											Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'paspor/index' ], [
												'data-pjax' => 0,
												'class'     => 'btn btn-default',
												'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
											] )
									],
									'{export}',
									'{toggleData}',
								],
								'export'             => [
									'fontAwesome' => true
								],
								'responsive'         => true,
								'floatHeader'        => true,
								'floatHeaderOptions' => [ 'scrollingTop' => true ],
								'showPageSummary'    => false,
								'toggleDataOptions'  => [ 'minCount' => 10 ],
								'panel'              => [
									'heading' => false,
//									'footer'  => false,
									//							'type'    => GridView::TYPE_PRIMARY,
									//							'heading' => '<i class="glyphicon glyphicon-book"></i> Paspor',
								],
								"columns"            => [
									[
										'class'          => 'kartik\grid\SerialColumn',
										'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
										'width'          => '36px',
										'header'         => '',
										'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
									],
									'tgl',
									'prov',
									'imigrasi',
									'pic',
									'note_',
									[
										'class'      => 'kartik\grid\ActionColumn',
										'template'   => '{update} {delete}',
										'urlCreator' => function ( $action, $model, $key, $index ) {
											$url = Url::toRoute( [
												'paspor/' . $action,
												'id' => $key
											] ); // your own url generation logic
											return $url;
										}
									],
								]
							]
						);
					} catch ( Exception $e ) {
						echo $e->getMessage();
					}
					?>
					<?php Pjax::end(); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_6">
                <div class="box-body">
					<?php Pjax::begin(); ?>
					<?
					try {
						echo \kartik\grid\GridView::widget(
							[
								"dataProvider"       => $dataProviderFwcms,
								'filterModel'        => $searchModelFwcms,
								"condensed"          => true,
								"hover"              => true,
								'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'pjax'               => true,
								'toolbar'            => [
									[
										'content' => ( Yii::$app->user->can( 'akes_tab' ) ?
												Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
													'type'    => 'button',
													'title'   => Yii::t( 'kvgrid', 'Add FWCMS' ),
													'class'   => 'btn btn-success',
													'onclick' => 'location.href="' . Url::toRoute( "fwcms/create" ) . '"'
												] ) : '' ) . ' ' .
										             Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'fwcms/index' ], [
											             'data-pjax' => 0,
											             'class'     => 'btn btn-default',
											             'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
										             ] )
									],
									'{export}',
									'{toggleData}',
								],
								'export'             => [
									'fontAwesome' => true
								],
								'responsive'         => true,
								'floatHeader'        => true,
								'floatHeaderOptions' => [ 'scrollingTop' => true ],
								'showPageSummary'    => false,
								'toggleDataOptions'  => [ 'minCount' => 10 ],
								'panel'              => [
									'heading' => false,
//									'footer'  => false,
									//							'type'    => GridView::TYPE_PRIMARY,
									//							'heading' => '<i class="glyphicon glyphicon-book"></i> FWCMS',
								],
								"columns"            => [
									[
										'class'          => 'kartik\grid\SerialColumn',
										'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
										'width'          => '36px',
										'header'         => '',
										'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
									],
									'tgl',
									'no_paspor',
									'rbb_duta',
									'pic',
									'note_',
									'tgl_tugasan',
									'expired',
									[
										'class'      => 'kartik\grid\ActionColumn',
										'template'   => Yii::$app->user->can( 'akes_tab' ) ? '{view} {update} {delete}' : '{view}',
										'urlCreator' => function ( $action, $model, $key, $index ) {
											$url = Url::toRoute( [
												'fwcms/' . $action,
												'id' => $key
											] ); // your own url generation logic
											return $url;
										}
									],
								]
							]
						);
					} catch ( Exception $e ) {
					}
					?>
					<?php Pjax::end(); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_7">
                <div class="box-body">
					<?php Pjax::begin(); ?>
					<?
					try {
						echo \kartik\grid\GridView::widget(
							[
								"dataProvider"       => $dataProviderIsc,
								'filterModel'        => $searchModelIsc,
								"condensed"          => true,
								"hover"              => true,
								'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
								'pjax'               => true,
								'toolbar'            => [
									[
										'content' => ( Yii::$app->user->can( 'akes_tab' ) ?
												Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
													'type'    => 'button',
													'title'   => Yii::t( 'kvgrid', 'Add ISC' ),
													'class'   => 'btn btn-success',
													'onclick' => 'location.href="' . Url::toRoute( "isc/create" ) . '"'
												] ) : '' ) . ' ' .
										             Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'isc/index' ], [
											             'data-pjax' => 0,
											             'class'     => 'btn btn-default',
											             'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
										             ] )
									],
									'{export}',
									'{toggleData}',
								],
								'export'             => [
									'fontAwesome' => true
								],
								'responsive'         => true,
								'floatHeader'        => true,
								'floatHeaderOptions' => [ 'scrollingTop' => true ],
								'showPageSummary'    => false,
								'toggleDataOptions'  => [ 'minCount' => 10 ],
								'panel'              => [
									'heading' => false,
//									'footer'  => false,
									//							'type'    => GridView::TYPE_PRIMARY,
									//							'heading' => '<i class="glyphicon glyphicon-book"></i> ISC',
								],
								"columns"            => [
									[
										'class'          => 'kartik\grid\SerialColumn',
										'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
										'width'          => '36px',
										'header'         => '',
										'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
									],
									'tgl',
									'rbb_duta',
									'pic',
									'note_',
//									'kode_bayar',
									'expired',
									[
										'class'      => 'kartik\grid\ActionColumn',
										'template'   => Yii::$app->user->can( 'akes_tab' ) ? '{view} {update} {delete}' : '{view}',
										'urlCreator' => function ( $action, $model, $key, $index ) {
											$url = Url::toRoute( [
												'isc/' . $action,
												'id' => $key
											] ); // your own url generation logic
											return $url;
										}
									],
									//							[
									//								'class' => '\kartik\grid\ActionColumn',
									//								'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>']
									//							]
								]
							]
						);
					} catch ( Exception $e ) {
					}
					?>
					<?php Pjax::end(); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_8">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'no_visa' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $model, 'tgl_visa' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] );
						?>
                    </div>
					<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                        <div class="form-group">
							<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                        </div>
					<?php endif; ?>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_18">
				<?php $form = ActiveForm::begin( [
					'action' => ( $mp->isNewRecord ) ? Url::toRoute( [
						"mp/create",
						'id' => $model->biodata_id
					] ) : Url::toRoute( [
						"mp/update",
						'id' => $model->biodata_id
					] ),
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $mp, 'no_polis' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $mp, 'tgl' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => true,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								],
								'pluginEvents'  => [
									'changeDate' => "function(e) {  
						                var date = e.date;
						                var newDate = new Date(date.setMonth(date.getMonth()+24));
						                $('#mp-tgl_expired-disp-kvdate').kvDatepicker('update', newDate);
						                $('#mp-tgl_expired').val(toJSONLocal(newDate));
						            }"
								]
							]
						] );
						?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $mp, 'tgl_expired' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => true,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] );
						?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $mp, 'note_' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
					<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                        <div class="form-group">
							<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                        </div>
					<?php endif; ?>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_16">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'tipe_bayar_copvisa' )->dropDownList( [
							'SPONSOR' => 'SPONSOR',
							'SENDIRI' => 'SENDIRI',
						],
							[
								'prompt'   => '',
								'onchange' => new JsExpression( " 
					var val_ = $(this).val();
					if(val_ === 'SPONSOR'){
					    $('.copvisa-bayar').hide();
					}else{
					    $('.copvisa-bayar').show();
					}
						" )
							] ) ?>
                    </div>
                    <div class="form-group copvisa-bayar">
						<?php
						echo $form->field( $model, 'biaya_copvisa' )->widget( NumberControl::classname(), [
							'maskedInputOptions' => [
								'prefix'     => 'Rp',
								'allowMinus' => false
							],
							'displayOptions'     => [ '' ],
							'name'               => 'amount_dp',
						] );
						?>
                    </div>
                    <div class="box box-default collapsed-box copvisa-bayar">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pembayaran</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-body">
                                <table id="table-medikal-bayar" class="table table-striped table-bordered"
                                       cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Amount</th>
                                        <th>Voucher</th>
                                        <th>Note</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div class="modal fade" id="modal-medikal-bayar">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Pembayaran</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="box-body">
                                        <form role="form" id="form-medikal-bayar">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Amount</label>
												<?= NumberControl::widget( [
													'id'                 => "amount-medikal-bayar",
													'name'               => "amount-medikal-bayar",
													'maskedInputOptions' => [
														'prefix'     => 'Rp',
														'allowMinus' => false
													],
													'options'            => [
													]
												] ); ?>
                                            </div>
                                            <div class="form-group">
                                                <label>Voucher</label>
                                                <input type="text" class="form-control" id="voucher-medikal-bayar">
                                            </div>
                                            <div class="form-group">
                                                <label>Note</label>
                                                <input type="text" class="form-control" id="note-medikal-bayar">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
                                    </button>
                                    <button type="button" class="btn btn-primary" id="btn-medikal-bayar-add-row">Save
                                        changes
                                    </button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <div class="form-group">
						<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_14">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'apply_visa' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] );
						?>
                    </div>
					<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                        <div class="form-group">
							<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                        </div>
					<?php endif; ?>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_15">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'kasus' )->dropDownList( [
							'UNFIT'            => 'UNFIT',
							'KABUR'            => 'KABUR',
							'MINTA PULANG'     => 'MINTA PULANG',
							'MASALAH IMIGRASI' => 'MASALAH IMIGRASI',
							'LAIN-LAIN'        => 'LAIN-LAIN',
						], [ 'prompt' => '' ] ) ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $model, 'note_kasus' )->textarea() ?>
                    </div>
                    <div class="form-group">
						<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_9">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'endorse' )->checkbox() ?>
                    </div>
                    <div class="form-group">
						<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_10">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'pap_tgl' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] );
						?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $model, 'no_pk' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $model, 'pap_note' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
						<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                        <button type="button" class="btn btn-primary hidden-print"
                                onClick="(function(){
                                        var win = window.open('<?= Url::toRoute( [
							        'biodata/gaji',
							        'biodata_id' => $model->biodata_id
						        ] ) ?>', '_blank');
                                        win.focus();
                                        // console.log('test');
                                        })()">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print Deklarasi
                        </button>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_11">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'tiket_note' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $model, 'tiket_tgl' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] );
						?>
                    </div>
					<?php if ( Yii::$app->user->can( 'akes_tab' ) ) : ?>
                        <div class="form-group">
							<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                        </div>
					<?php endif; ?>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab_12">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <img
                                        src="<?= DokumenUpload::initialPreview( Dokumen::PAS_PHOTO, $model->biodata_id ); ?>"
                                        width="152" height="200">
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <table id="w0" class="table table-striped table-bordered detail-view">
                                <tbody>
                                <tr>
                                    <th>Nama</th>
                                    <td><?= $model->name; ?></td>
                                </tr>
                                <tr>
                                    <th>Tgl Lahir</th>
                                    <td><?= $model->birthdate; ?></td>
                                </tr>
                                <tr>
                                    <th>No. HP</th>
                                    <td><?= $model->no_hp; ?></td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td><?= $model->address; ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- /.form-group -->
                        </div>
						<?
						$lastPaspor = $model->lastPaspor;
						$imigrasi   = $masa_berlaku = '';
						if ( $lastPaspor != null ) {
							$imigrasi = $lastPaspor->imigrasi;
							if ( $lastPaspor->tgl != null ) {
								$date = new DateTime( $lastPaspor->tgl );
								$date->modify( '+5 years' );
								$masa_berlaku = \Yii::$app->formatter->asDate( $date, 'dd-MM-yyyy' );
							}
						}
						?>
                        <div class="col-md-4">
                            <table id="w0" class="table table-striped table-bordered detail-view">
                                <tbody>
                                <tr>
                                    <th>Paspor</th>
                                    <td><?= $model->no_paspor; ?></td>
                                </tr>
                                <tr>
                                    <th>Imigrasi</th>
                                    <td><?= $imigrasi; ?></td>
                                </tr>
                                <tr>
                                    <th>Masa Berlaku</th>
                                    <td><?= $masa_berlaku; ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <div class="box-header with-border">
                                <div class="box-tools pull-right">
									<?= Html::a( "Refresh", Url::to( [
										'/biodata/view',
										'id' => $model->biodata_id,
										't'  => md5( date( 'Y-m-d H:i:s' ) ),
										'#'  => 'tab_12'
									], true ), [ 'class' => 'btn btn-success' ] ) ?>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <tr>
                                    <th>NO</th>
                                    <th>DOKUMEN</th>
                                    <th>IMAGE</th>
                                    <th>UPLOAD</th>
                                </tr>
								<? $counter = 1; ?>
								<?php foreach ( \app\models\Dokumen::JENIS_ARR as $item ) : ?>
									<?php if ( $item == Dokumen::PAS_PHOTO || $item == Dokumen::JOB_ORDER || $item == Dokumen::AKI ) {
										continue;
									} ?>
                                    <tr>
                                        <td><?= $counter; ?></td>
                                        <td>
											<? $exists = \app\models\DokumenUpload::isFileDokumenExists( $item,
												$model->biodata_id );
											if ( $exists ) {
												$tab12 = 1;
											}
											?>
                                            <img src="<?= $exists ? Url::to( '@web/images/document_ok48.png', true ) :
												Url::to( '@web/images/document_error48.png', true ); ?>"
                                                 alt="<?= $item; ?>"
                                                 align="left"><span><?= \app\models\Dokumen::DESC_JENIS_ARR[ $item ]; ?></span>
                                        </td>
                                        <td>
                                            <a href="<?= $exists ? DokumenUpload::getUrlDokumen( $item, $model->biodata_id, false ) : DokumenUpload::getUrlNoImage(); ?>"
												<?= ( $item == Dokumen::PERJANJIAN_KERJA || $item == Dokumen::PERJANJIAN_PENEMPATAN ) ? 'target="_blank"' : 'data-lightbox="DOKUMEN"'; ?>
                                               data-title="<?= $item ?>">
                                                <img src="<?= $exists ? DokumenUpload::getUrlDokumen( $item,
													$model->biodata_id ) : DokumenUpload::getUrlNoImage(); ?>"
                                                     class="img-thumbnail" width="265"
                                                     height="125"
                                                     alt="<?= $item; ?>">
                                            </a>
                                        </td>
                                        <td>
											<?= \kartik\widgets\FileInput::widget( [
												'name'          => $item,
												'pluginOptions' => [
													'showPreview'           => false,
													'showCaption'           => false,
//		                                            'showBrowse'           => false,
													'browseClass'           => 'btn btn-default btn-secondary',
													'browseLabel'           => '',
													'uploadLabel'           => '',
													'removeLabel'           => '',
//                            'fileTypeSettings' => ['image'],
													'allowedFileExtensions' => ( $item == Dokumen::PERJANJIAN_KERJA || $item == Dokumen::PERJANJIAN_PENEMPATAN ) ?
														[ 'pdf' ] : [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
													'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
													'uploadExtraData'       => [
														'biodata_id' => $model->biodata_id
													],
												],
//												'pluginEvents'  => [
//													"fileuploaded" => "function() {  window.location='" . Url::to( [
//															'/biodata/view',
//															'id' => $model->biodata_id,
//															't'  => md5( date( 'Y-m-d H:i:s' ) ),
//															'#'  => 'tab_12'
//														], true ) . "';  }"
//												]
											] );
											?>
											<?= Html::a( '', [ '/dokumen-upload/delete-object' ], [
												'class' => 'btn btn-default btn-secondary glyphicon glyphicon-trash',
												'data'  => [
													'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
													'method'  => 'post',
													'params'  => [
														'id'   => $model->biodata_id,
														'item' => $item
													]
												],
											] ) ?>
                                        </td>
                                    </tr>
									<?php $counter ++; ?>
								<?php endforeach; ?>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div class="tab-pane" id="tab_19">
				<?php $form = ActiveForm::begin( [
					'action' => 'update?id=' . $model->biodata_id,
				] ); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= NumberControl::widget( [
							'model'              => $model,
							'attribute'          => 'loan',
							'maskedInputOptions' => [ 'digits' => 0 ],
//		                    'maskedInputOptions' => ['prefix' => '$ ', 'suffix' => ' c'],
						] ); ?>
                    </div>
                    <div class="form-group">
						<?= $form->field( $model, 'loan_note' )->textarea() ?>
                    </div>
                    <div class="form-group">
						<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
<?php
$jsTabs = "var tab12 =  " . $tab12 . ";";
$this->registerJs(
	$jsTabs,
	yii\web\View::POS_HEAD,
	'tab-color-script'
);
$script = <<< JS
// var url = window.location.href;
// var activeTab = url.substring(url.indexOf("#") + 1);
// $(".tab-pane").removeClass("active");
// $("#" + activeTab).addClass("active");
// $('#'+activeTab).tab('show');

var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
})
$('#exampleModal').on('show.bs.modal', function (e) {
    var button = $(e.relatedTarget);
    var modal = $(this);
    modal.find('#rangkuman-konten').load(button.attr("remote"));

});
if(tab12 === 0){
    $('a[href="#tab_12"]').css({ color: "red" });
}

// $('#tabs').on('click','.tablink,#prodTabs a',function (e) {
//     e.preventDefault();
//     var url = $(this).attr("data-url");
//     if (typeof url !== "undefined") {
//         var pane = $(this), href = this.hash;
//         $(href).load(url,function(result){      
//             pane.tab('show');
//         });
//     } else {
//         $(this).tab('show');
//     }
// });

JS;
$this->registerJs( $script );
<?php
/* @var $this yii\web\View */
/* @var $model app\models\Biodata */
$this->title                   = Yii::t( 'app', 'Create Biodata' );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biodata-create">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
    <div class="box box-primary">
		<?= $this->render( '_form', [
			'model' => $model,
		] ) ?>
    </div>
</div>

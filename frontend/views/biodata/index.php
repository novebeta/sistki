<?php
use frontend\widgets\demopager\DemoPager;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BiodataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Biodata */
$this->title                   = Yii::t( 'app', 'Biodata' );
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<p>-->
<!--	--><? //= Html::a( Yii::t( 'app', 'Create Biodata' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
<!--</p>-->
<!--<div class="box box-primary">-->
<!--    <div class="box-header"><h3 class="box-title"><i class="fa fa-table"></i>&nbsp;Table</h3></div>-->
<!--    <div class="box-body">-->
<div class="box box-primary">
	<?php Pjax::begin(); ?>
	<?
	try {
		echo \kartik\grid\GridView::widget(
			[
				"dataProvider"       => $dataProvider,
				'filterModel'        => $searchModel,
				"condensed"          => true,
				"hover"              => true,
				'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'pjax'               => true,
				'toolbar'            => [
					[
						'content' =>
							Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
								'type'    => 'button',
								'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title ),
								'class'   => 'btn btn-success',
								'onclick' => 'location.href="create"'
							] ) . ' ' .
							Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
								'data-pjax' => 0,
								'class'     => 'btn btn-default',
								'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
							] )
					],
					'{export}',
					'{toggleData}',
//                    '{pager}'
				],
				'export'             => [
					'fontAwesome' => true
				],
				'responsive'         => true,
//				'floatHeader'        => true,
				'floatHeaderOptions' => [ 'scrollingTop' => true ],
				'showPageSummary'    => false,
				'toggleDataOptions'  => [ 'minCount' => 10 ],
				'panelBeforeTemplate'=>'<div class="row"><div class="col-md-8">{before}</div><div class="col-md-4">{toolbarContainer}</div></div><div class="clearfix"></div>',
				'panel'              => [
//					'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Countries</h3>',
//					'type'=>'success',
					'heading' => false,
					'before'  => '<form class="form-inline"><div class="form-group">
                            <label style="padding-right: 15px">From : </label>'.DateControl::widget([
							'name' => 'dp_1',
							'value' => $_GET['dp_1'],
							'type' => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'pluginOptions' => [
								'autoclose'=>true,
							],
						]).'</div><div class="form-group"><label style="padding: 0 15px"> To : </label>'.DateControl::widget([
							'name' => 'dp_2',
							'value' => $_GET['dp_2'],
							'type' => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'pluginOptions' => [
								'autoclose'=>true
							],
						]).'</div><button type="submit" style="padding-left: 15px" class="btn btn-default">Search</button>'
//					'after'=>Html::a('<i class="fas fa-redo"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
				],
				'pager'              => [
					// Use custom pager widget class
					'class'               => DemoPager::className(),
					'firstPageLabel'      => 'First',
					'lastPageLabel'       => 'Last',
					'pager_layout'        => '{pageButtons} {goToPage}',
//					'maxButtonCount'      => 4,
					// Options for <ul> wrapper of default pager buttons are still available
					'options'             => [
						'class' => 'pagination',
						'style' => 'display:inline-block;float:left;margin:20px 10px 20px 0;width:auto;'
					],
//					 Style for page size select
//					'sizeListHtmlOptions' => [
//						'class' => 'form-control',
//						'style' => 'display:inline-block;float:left;margin:20px 10px 20px 0;width:auto;'
//					],
					// Style for go to page input
					'goToPageHtmlOptions' => [
						'class' => 'form-control',
						'style' => 'display:inline-block;float:left;margin:20px 10px 20px 0;width:75px;'
					],
				],
				"columns"            => [
					[
						'class'          => 'kartik\grid\SerialColumn',
						'vAlign'         => 'top',
						'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
						'width'          => '36px',
						'header'         => 'No.',
						'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
					],
					[
						'label'     => 'Nama',
//						'width'     => '250px',
						'attribute' => 'name',
						'format'    => 'raw',
						'value'     => function ( $model, $key, $index, $column ) {
							return '<strong>' . strtoupper( $model->name ) .
							       '</strong>' . ' (' . $model->no_absen . ') </br>' . '<font color="' . ( ( $model->sex == "F" ) ? "deeppink" : "blue" ) . '">' .
							       $model->getLabelSex() . '</font></br>' .
							       Html::a( 'Dokumen CTKI', [
								       'biodata/view',
								       'id' => $model->biodata_id,
								       '#'  => 'tab_12'
							       ] ) . '</br>' .
							       Html::a( 'View Detail', [
								       'biodata/view',
								       'id' => $model->biodata_id
							       ] );
						}
					],
					'no_paspor',
					'kabupaten',
					'sekolah',
					//			[
					//				'class'     => '\kartik\grid\DataColumn',
					//				'label'     => 'Job Order',
					//				'attribute' => 'address',
					//				'format' => 'raw',
					//				'content'     => function ( $data, $key, $index, $column ) {
					//					/** @var \app\models\Biodata $data */
					//					if ( $data->job_order_id != null ) {
					//						return \app\models\JobOrder::find()->where( [
					//							'job_order_id'=>
					//							$data->job_order_id
					//						] )->one()->no_register;
					//					}
					//					return '';
					//				},
					//			],
//					'birthdate',
//					'companyName',
					[
						'label'     => 'Company Name',
						'attribute' => 'companyName',
						'value'     => 'company.name'
					],
					[
						'label'     => 'Medikal',
						'attribute' => 'medikalCenter',
						'value'     => 'medikalLastexpired.medikal_center'
					],
//					'cabangName',
					[
						'label'     => 'User Name',
						'attribute' => 'username',
						'value'     => 'user.username'
					],
					'note_',
					[
						'class'    => 'kartik\grid\ActionColumn',
						'header'   => '',
						'template' => '{delete}'
					],
				]
			]
		);
	} catch ( Exception $e ) {
		echo $e->getMessage();
	}
	?>
	<?php Pjax::end(); ?>
    <!--    </div>-->
    <!--</div>-->
</div>
<?php
use app\models\Dokumen;
use app\models\DokumenUpload;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BiodataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var \app\models\Biodata $model */
/** @var \app\models\JobOrder $jobOrder */
$this->title                     = Yii::t( 'app', 'Biodata' ) . " [" . $jobOrder->job_title . " - " . $jobOrder->no_register . "]";
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
    <style>
        .riwayat table, th, td {
            border: none;
        }
    </style>
    <div class="box box-primary">
		<?
		try {
			echo \kartik\grid\GridView::widget(
				[
					"dataProvider"         => $dataProvider,
					"id"                   => 'grid-company-bio_id',
					//				'filterModel'        => $searchModel,
					"condensed"            => true,
					"hover"                => true,
					'headerRowOptions'     => [ 'class' => 'kartik-sheet-style' ],
					'filterRowOptions'     => [ 'class' => 'kartik-sheet-style' ],
					'pjax'                 => true,
					'toolbar'              => [
						[
							'content' =>
//							Html::button( '<i class="glyphicon glyphicon-save-file"></i>', [
//								'type'    => 'button',
//								'title'   => Yii::t( 'kvgrid', 'Save ' . $this->title ),
//								'class'   => 'btn btn-success',
//								'onclick' => 'location.href="create"'
//							] ) . ' ' .
								Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'indexcompany' ], [
									'data-pjax' => 0,
									'class'     => 'btn btn-default',
									'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
								] )
						],
						'{export}',
						'{toggleData}',
					],
					'export'               => [
						'fontAwesome' => true
					],
					'responsive'           => true,
					'floatHeader'          => true,
					'floatHeaderOptions'   => [ 'scrollingTop' => false ],
					'panelBeforeTemplate'  => '<div class="row"><div class="col-md-8">{before}</div><div class="col-md-4">{toolbarContainer}</div></div><div class="clearfix"></div>',
					'showPageSummary'      => false,
					'toggleDataOptions'    => [ 'minCount' => 10 ],
					'panel'                => [
//						'heading' => false,
						'before'  => '<form class="form-inline"></form>'
					],
					'panelHeadingTemplate' => "{summary}
{title}
<div class=\"kv-panel-pager\">
    {pager}
</div>
<div class=\"clearfix\"></div>",
					"columns"              => [
						[
							'class'          => 'kartik\grid\SerialColumn',
							'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
							'width'          => '36px',
							'header'         => 'No.',
							'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
						],
						[
							'class'    => 'kartik\grid\ActionColumn',
							'header'   => 'Delete',
							'template' => '{delete}',
							'buttons'  => [
								'delete' => function ( $url, $model ) {
									$url = Url::to( [ 'biodata/joborder-delete' ] );
									return Html::a( '<span class="glyphicon glyphicon-trash"></span>', $url, [
										'title'        => 'delete',
										'data-confirm' => Yii::t( 'yii', 'Are you sure you want to delete this data?' ),
										'data'         => [
											'method' => 'post',
											'params' => [ 'id' => $model->biodata_id, 'page' => ( $_GET[ 'page' ] == null ) ? 1 : $_GET[ 'page' ] ],
										]
									] );
								},
							]
						],
						//						[
						//							'class'           => 'kartik\grid\CheckboxColumn',
						//							'contentOptions'  => [ 'class' => 'kartik-sheet-style' ],
						//							'width'           => '36px',
						//							'header'          => '',
						//							'headerOptions'   => [ 'class' => 'kartik-sheet-style' ],
						//							'checkboxOptions' => function ( $model, $key, $index, $column ) {
						//								return [
						//									'checked' => ( $model->company_date != null ),
						//									'onclick' => '
						//								    console.log($(this)[0].checked);
						//								    $.ajax({
						//                                          type: "POST",
						//                                          url: "selected?t=' . md5( date( 'Y-m-d H:i:s' ) ) . '",
						//                                          data: {
						//                                              id: "' . $key . '",
						//                                              checked: ($(this)[0].checked)? "OK" : "NO"
						//                                          },
						//                                          success: function(d){
						//                                               console.log(d);
						//                                          },
						//                                          datatype: "json",
						//                                    });
						//								'
						//								];
						//							}
						//						],
						[
							'label'     => '',
							'attribute' => 'name',
							'format'    => 'raw',
							'value'     => function ( $model, $key, $index, $column ) {
								$src    = DokumenUpload::initialPreview( Dokumen::PAS_PHOTO, $model->biodata_id, true );
								$script = <<< HTML
							<img class="pull-right  img-thumbnail" width="152px" height="200px"
                         src="$src"
                         alt="Another alt text">
HTML;
								return $script;
							}
						],
						[
							'label'     => '',
							'attribute' => 'name',
							'format'    => 'raw',
							'value'     => function ( $model, $key, $index, $column ) {
								/** @var \app\models\Biodata $model */
								$nama     = strtoupper( $model->name );
								$sex      = $model->getLabelSex();
								$dob      = strtoupper( \Yii::$app->formatter->asDate( $model->birthdate, 'dd MMMM YYYY' ) );
								$age      = $model->age;
								$district = strtoupper( $model->kabupaten );
								$height   = $model->tb;
								$weight   = $model->bb;
								$graduate = $model->sekolah;
								$indo     = "INDONESIA <table class='riwayat'><tr><td>Company</td><td>Location</td><td>Job Title</td><td>Duration</td></tr>";
								foreach ( $model->riwayatIndo as $r ) {
									$indo .= "<tr><td>$r->nama_perusahaan</td><td>$r->lokasi</td><td>$r->posisi</td><td>$r->masa_kerja</td></tr>";
								}
								$indo  .= "</table>";
								$malay = "MALAYSIA <table class='riwayat'><tr><td>Company</td><td>Location</td><td>Job Title</td><td>Duration</td></tr>";
								foreach ( $model->riwayatMalay as $r ) {
									$malay .= "<tr><td>$r->nama_perusahaan</td><td>$r->lokasi</td><td>$r->posisi</td><td>$r->masa_kerja</td></tr>";
								}
								$malay      .= "</table>";
								$experience = ( ( sizeof( $model->riwayatIndo ) > 0 ) ? $indo : '' ) . " " . ( ( sizeof( $model->riwayatMalay ) ) ? $malay : '' );
								$script     = <<< HTML
<table class="biodata">
  <tr>
    <td width="75">NAMA</td>
    <td width="600">: $nama</td>
  </tr>
  <tr>
    <td>D.O.B</td>
    <td>: $dob</td>
  </tr>
  <tr>
    <td>AGE</td>
    <td>: $age</td>
  </tr>
  <tr>
    <td>SEX</td>
    <td>: $sex </td>
  </tr>
  <tr>
    <td>DISTRICT</td>
    <td>: $district</td>
  </tr>
  <tr>
    <td>HEIGHT</td>
    <td>: $height</td>
  </tr>
  <tr>
    <td>WEIGHT</td>
    <td>: $weight</td>
  </tr>
  <tr>
    <td>GRADUATE</td>
    <td>: $graduate</td>
  </tr>
  <tr>
    <td>EXPERIENCE</td>
    <td>$experience</td>
  </tr>
</table>
HTML;
								return $script;
							}
						],
					]
				]
			);
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}
		?>
    </div>
<? $this->registerCss( "
table.biodata, th, td {
  border: 1px solid grey;
  border-collapse: collapse;
  padding: 5px;
}
" );
$this->registerJs( <<<JS
    $(document).on('pjax:success', function() {
        $(window).scrollTop(0); 
        // console.log('success');
    })
JS
);
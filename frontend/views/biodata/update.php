<?php
/* @var $this yii\web\View */
/* @var $model app\models\Biodata */
$this->title                   = Yii::t( 'app', 'Update Biodata: {nameAttribute}', [
	'nameAttribute' => $model->name,
] );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->name, 'url' => [ 'view', 'id' => $model->biodata_id ] ];
$this->params['breadcrumbs'][] = Yii::t( 'app', 'Update' );
?>
<div class="biodata-update">
    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
	<?= $this->render( '_form', [
		'indo'  => $indo,
		'malay' => 'malay',
		'model' => $model,
	] ) ?>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BiodataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var \app\models\Biodata $model */
$this->title                   = Yii::t( 'app', 'Biodata Resign' );
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<p>-->
<!--	--><? //= Html::a( Yii::t( 'app', 'Create Biodata' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
<!--</p>-->
<!--<div class="box box-primary">-->
<!--    <div class="box-header"><h3 class="box-title"><i class="fa fa-table"></i>&nbsp;Table</h3></div>-->
<!--    <div class="box-body">-->
<div class="box box-primary">
	<?php Pjax::begin(); ?>
	<?
	try {
		echo \kartik\grid\GridView::widget(
			[
				"dataProvider"       => $dataProvider,
				'filterModel'        => $searchModel,
				"condensed"          => true,
				"hover"              => true,
				'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'pjax'               => true,
				'toolbar'            => [
					[
						'content' =>
							Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
								'type'    => 'button',
								'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title ),
								'class'   => 'btn btn-success',
								'onclick' => 'location.href="create"'
							] ) . ' ' .
							Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
								'data-pjax' => 0,
								'class'     => 'btn btn-default',
								'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
							] )
					],
					'{export}',
					'{toggleData}',
				],
				'export'             => [
					'fontAwesome' => true
				],
				'responsive'         => true,
				'floatHeader'        => true,
				'floatHeaderOptions' => [ 'scrollingTop' => true ],
				'showPageSummary'    => false,
				'toggleDataOptions'  => [ 'minCount' => 10 ],
				'panel'              => [
					'heading' => false,
//					'footer'  => false,
					//			'type'    => GridView::TYPE_PRIMARY,
					//			'heading' => '<i class="glyphicon glyphicon-book"></i>  ' . $this->title,
				],
				"columns"            => [
					[
						'class'          => 'kartik\grid\SerialColumn',
						'vAlign'         => 'top',
						'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
						'width'          => '36px',
						'header'         => 'No.',
						'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
					],
					[
						'label'     => 'Nama',
//						'width'     => '250px',
						'attribute' => 'name',
						'format'    => 'raw',
						'value'     => function ( $model, $key, $index, $column ) {
							return '<strong>' . strtoupper( $model->name ) .
							       '</strong>' . ' (' . $model->no_absen . ') </br>' . '<font color="' . ( ( $model->sex == "F" ) ? "deeppink" : "blue" ) . '">' .
							       $model->getLabelSex() . '</font></br>' .
							       Html::a( 'Dokumen CTKI', [
								       'biodata/view',
								       'id' => $model->biodata_id,
								       '#'  => 'tab_12'
							       ] ) . '</br>' .
							       Html::a( 'View Detail', [
								       'biodata/view',
								       'id' => $model->biodata_id
							       ] );
						}
					],
					'no_paspor',
					'kabupaten',
					'sekolah',
					//			[
					//				'class'     => '\kartik\grid\DataColumn',
					//				'label'     => 'Job Order',
					//				'attribute' => 'address',
					//				'format' => 'raw',
					//				'content'     => function ( $data, $key, $index, $column ) {
					//					/** @var \app\models\Biodata $data */
					//					if ( $data->job_order_id != null ) {
					//						return \app\models\JobOrder::find()->where( [
					//							'job_order_id'=>
					//							$data->job_order_id
					//						] )->one()->no_register;
					//					}
					//					return '';
					//				},
					//			],
//					'birthdate',
					'companyName',
					[
						'label'     => 'Medikal',
						'attribute' => 'medikalCenter',
						'value'     => 'medikalLastexpired.medikal_center'
					],
					'cabangName',
					'note_',
					//				[
					//					'class'    => 'kartik\grid\ActionColumn',
					//					'template' => '{view} {delete}'
					//				],
				]
			]
		);
	} catch ( Exception $e ) {
		echo $e->getMessage();
	}
	?>
	<?php Pjax::end(); ?>
    <!--    </div>-->
    <!--</div>-->
</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mp */

$this->title = 'Update Mp: ' . $model->mp_id;
$this->params['breadcrumbs'][] = ['label' => 'Mps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mp_id, 'url' => ['view', 'id' => $model->mp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mp-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'mp_id',
            'no_polis',
            'tgl',
            'tgl_expired',
            'note_',
            //'biodata_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

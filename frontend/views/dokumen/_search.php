<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokumen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'dokumen_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'penerima') ?>

    <?= $form->field($model, 'ktp') ?>

    <?= $form->field($model, 'kk') ?>

    <?php // echo $form->field($model, 'akte') ?>

    <?php // echo $form->field($model, 'ijazah') ?>

    <?php // echo $form->field($model, 'buku_nikah') ?>

    <?php // echo $form->field($model, 'paspor') ?>

    <?php // echo $form->field($model, 'surat_ijin') ?>

    <?php // echo $form->field($model, 'lain_lain') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dokumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokumen-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'dokumen_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl')->textInput() ?>

    <?= $form->field($model, 'penerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ktp')->checkbox() ?>

    <?= $form->field($model, 'kk')->checkbox() ?>

    <?= $form->field($model, 'akte')->checkbox() ?>

    <?= $form->field($model, 'ijazah')->checkbox() ?>

    <?= $form->field($model, 'buku_nikah')->checkbox() ?>

    <?= $form->field($model, 'paspor')->checkbox() ?>

    <?= $form->field($model, 'surat_ijin')->checkbox() ?>

    <?= $form->field($model, 'lain_lain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

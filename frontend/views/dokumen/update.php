<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dokumen */

$this->title = Yii::t('app', 'Update Dokumen: {nameAttribute}', [
    'nameAttribute' => $model->dokumen_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokumens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dokumen_id, 'url' => ['view', 'id' => $model->dokumen_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dokumen-update">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

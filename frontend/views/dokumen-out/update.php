<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenOut */

$this->title = 'Update Dokumen Out: ' . $model->biodataName;
$this->params['breadcrumbs'][] = ['label' => 'Berkas Keluar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->biodataName, 'url' => ['view', 'id' => $model->grup]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dokumen-out-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenOut */

$this->title = $model->biodataName;
$this->params['breadcrumbs'][] = ['label' => 'Berkas Keluar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dokumen-out-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->grup], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->grup], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'dokumen_id',
            'tgl',
            'penerima',
	        'KTP:boolean',
	        'KK:boolean',
	        'AKTE_KELAHIRAN:boolean',
	        'IJAZAH:boolean',
	        'BUKU_NIKAH:boolean',
	        'PASPOR:boolean',
	        'SURAT_IJIN_WALI:boolean',
//	        'PERJANJIAN_PENEMPATAN:boolean',
//	        'PERJANJIAN_KERJA:boolean',
//	        'VISA_KERJA:boolean',
	        'SERTIFIKAT_KESEHATAN:boolean',
	        'KARTU_PESERTA_ASURANSI:boolean',
	        'AKI:boolean',
            'note',
            'created_at',
//            'biodata_id',
//            'cabang_id',
//            'grup',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenOutSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokumen-out-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dokumen_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'penerima') ?>

    <?= $form->field($model, 'KTP') ?>

    <?= $form->field($model, 'KK') ?>

    <?php // echo $form->field($model, 'AKTE_KELAHIRAN') ?>

    <?php // echo $form->field($model, 'IJAZAH') ?>

    <?php // echo $form->field($model, 'BUKU_NIKAH') ?>

    <?php // echo $form->field($model, 'PASPOR') ?>

    <?php // echo $form->field($model, 'SURAT_IJIN_WALI') ?>

    <?php // echo $form->field($model, 'PERJANJIAN_PENEMPATAN') ?>

    <?php // echo $form->field($model, 'PERJANJIAN_KERJA') ?>

    <?php // echo $form->field($model, 'VISA_KERJA') ?>

    <?php // echo $form->field($model, 'KARTU_PESERTA_ASURANSI') ?>

    <?php // echo $form->field($model, 'SERTIFIKAT_KESEHATAN') ?>

    <?php // echo $form->field($model, 'AKI') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <?php // echo $form->field($model, 'cabang_id') ?>

    <?php // echo $form->field($model, 'grup') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

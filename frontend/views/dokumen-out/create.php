<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenOut */

$this->title = 'Create Dokumen Out';
$this->params['breadcrumbs'][] = ['label' => 'Berkas Keluar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-out-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

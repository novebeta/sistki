<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\SecurityRoles */
$this->title = Yii::t('app', 'Update Security Roles: {nameAttribute}', [
    'nameAttribute' => $model->security_roles_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Security Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->security_roles_id, 'url' => ['view', 'id' => $model->security_roles_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="security-roles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

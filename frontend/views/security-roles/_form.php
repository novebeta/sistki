<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\SecurityRoles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="security-roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'security_roles_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sections')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'up')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

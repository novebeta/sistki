<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\SecurityRolesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="security-roles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'security_roles_id') ?>

    <?= $form->field($model, 'role') ?>

    <?= $form->field($model, 'ket') ?>

    <?= $form->field($model, 'sections') ?>

    <?= $form->field($model, 'up') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

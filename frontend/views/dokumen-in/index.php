<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DokumenInSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Yii::t( 'app', 'Berkas Masuk' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
	<?php Pjax::begin(); ?>
	<?=
	\kartik\grid\GridView::widget(
		[
			"dataProvider"       => $dataProvider,
			'filterModel'        => $searchModel,
			"condensed"          => true,
			"hover"              => true,
			'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
			'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
			'pjax'               => true,
			'toolbar'            => [
				[
					'content' =>
						Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
							'type'    => 'button',
							'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title ),
							'class'   => 'btn btn-success',
							'onclick' => 'location.href="create"'
						] ) . ' ' .
						Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
							'data-pjax' => 0,
							'class'     => 'btn btn-default',
							'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
						] )
				],
				'{export}',
				'{toggleData}',
			],
			'export'             => [
				'fontAwesome' => true
			],
			'responsive'         => true,
			'hover'              => true,
			'floatHeader'        => true,
			'floatHeaderOptions' => [ 'scrollingTop' => true ],
			'showPageSummary'    => false,
			'toggleDataOptions'  => [ 'minCount' => 10 ],
			'panel'              => [
				'heading' => false,
//				'footer'  => false,
//			'type'    => GridView::TYPE_PRIMARY,
//			'heading' => '<i class="glyphicon glyphicon-book"></i>  ' . $this->title,
			],
			"columns"            => [
				[
					'class'          => 'kartik\grid\SerialColumn',
					'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
					'width'          => '36px',
					'header'         => '',
					'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
				],
				'tgl',
				[
					'label'     => 'Full Name',
					'attribute' => 'biodataName',
					'value'     => 'biodata.name',
				],
				'penerima',
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'KTP',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'KK',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'AKTE_KELAHIRAN',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'IJAZAH',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'BUKU_NIKAH',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'PASPOR',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'SURAT_IJIN_WALI',
					'vAlign'    => 'middle',
				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'SERTIFIKAT_KESEHATAN',
					'vAlign'    => 'middle',
				],
//				[
//					'class'     => 'kartik\grid\BooleanColumn',
//					'attribute' => 'PERJANJIAN_KERJA',
//					'vAlign'    => 'middle',
//				],
//				[
//					'class'     => 'kartik\grid\BooleanColumn',
//					'attribute' => 'VISA_KERJA',
//					'vAlign'    => 'middle',
//				],
//				[
//					'class'     => 'kartik\grid\BooleanColumn',
//					'attribute' => 'PERJANJIAN_PENEMPATAN',
//					'vAlign'    => 'middle',
//				],
				[
					'class'     => 'kartik\grid\BooleanColumn',
					'attribute' => 'AKI',
					'vAlign'    => 'middle',
				],
//				'lain_lain',
				'note',
				[
					'class'          => 'kartik\grid\ActionColumn',
//					'header' => 'Action',
					'template'       => '{update} {view} {delete}',
					'buttons'        => [
						'view'   => function ( $url, $model ) {
							$url = Url::to( [ 'dokumen-in/view', 'id' => $model->grup ] );
							return Html::a( '<span class="fa fa-eye"></span>', $url, [ 'title' => 'view' ] );
						},
						'update' => function ( $url, $model ) {
							$url = Url::to( [ 'dokumen-in/update', 'id' => $model->grup ] );
							return Html::a( '<span class="fa fa-pencil"></span>', $url, [ 'title' => 'update' ] );
						},
						'delete' => function ( $url, $model ) {
							$url = Url::to( [ 'dokumen-in/delete', 'id' => $model->grup ] );
							return Html::a( '<span class="fa fa-trash"></span>', $url, [
								'title'        => 'delete',
								'data-confirm' => Yii::t( 'yii', 'Are you sure you want to delete this item?' ),
								'data-method'  => 'post',
							] );
						},
					]
				],
			]
		]
	);
	?>
	<?php Pjax::end(); ?>
</div>
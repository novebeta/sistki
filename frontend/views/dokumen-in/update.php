<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenIn */

$this->title = Yii::t('app', 'Update Berkas Masuk: {nameAttribute}', [
    'nameAttribute' => $model->biodataName,
]);
$this->params['breadcrumbs'][] = ['label' => 'Berkas Masuk', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->biodataName, 'url' => ['view', 'id' => $model->grup]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dokumen-in-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\DokumenIn */
$this->title                   = $model->biodataName;
$this->params['breadcrumbs'][] = [ 'label' => 'Berkas Masuk', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-in-view">
    <p>
		<?= Html::a( Yii::t( 'app', 'Update' ), [
			'update',
			'id' => $model->dokumen_id
		], [ 'class' => 'btn btn-primary' ] ) ?>
		<?= Html::a( Yii::t( 'app', 'Delete' ), [ 'delete', 'id' => $model->dokumen_id ], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
				'method'  => 'post',
			],
		] ) ?>
    </p>
	<?= DetailView::widget( [
		'model'      => $model,
		'attributes' => [
//            'dokumen_id',
			'tgl',
			'penerima',
			'KTP:boolean',
			'KK:boolean',
			'AKTE_KELAHIRAN:boolean',
			'IJAZAH:boolean',
			'BUKU_NIKAH:boolean',
			'PASPOR:boolean',
			'SURAT_IJIN_WALI:boolean',
//			'PERJANJIAN_PENEMPATAN:boolean',
//			'PERJANJIAN_KERJA:boolean',
//			'VISA_KERJA:boolean',
			'SERTIFIKAT_KESEHATAN:boolean',
			'KARTU_PESERTA_ASURANSI:boolean',
			'AKI:boolean',
			'note',
			'created_at',
		],
	] ) ?>
</div>

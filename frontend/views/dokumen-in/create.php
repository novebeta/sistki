<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DokumenIn */

$this->title = Yii::t('app', 'Create Berkas Masuk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokumen Ins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-in-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

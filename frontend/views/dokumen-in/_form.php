<?php
use kartik\datecontrol\DateControl;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\DokumenIn */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="form-group">
			<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
				'type'           => DateControl::FORMAT_DATE,
				'ajaxConversion' => false,
				'widgetOptions'  => [
					'pluginOptions' => [
						'autoclose' => true
					]
				]
			] );
			?>
        </div>
        <div class="form-group">
			<?php
			$url      = \yii\helpers\Url::to( [ 'biodata/list' ] );
			$cityDesc = empty( $model->biodata_id ) ? '' : \app\models\Biodata::findOne( $model->biodata_id )->name;
			echo $form->field( $model, 'biodata_id' )->widget( Select2::classname(), [
				'initValueText' => $cityDesc, // set the initial display text
				'options'       => [ 'placeholder' => 'Search for a biodata ...' ],
				'pluginOptions' => [
					'allowClear'         => true,
					'minimumInputLength' => 3,
					'language'           => [
						'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ),
					],
					'ajax'               => [
						'url'      => $url,
						'dataType' => 'json',
						'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
					],
					'escapeMarkup'       => new JsExpression( 'function (markup) { return markup; }' ),
					'templateResult'     => new JsExpression( 'function(city) { return city.text; }' ),
					'templateSelection'  => new JsExpression( 'function (city) { return city.text; }' ),
				],
			] );
			?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'penerima' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-inline">
            <div class="form-group">
				<?= $form->field( $model, 'KTP' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'KK' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'AKTE_KELAHIRAN' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'IJAZAH' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'BUKU_NIKAH' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'PASPOR' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'SURAT_IJIN_WALI' )->checkbox() ?>
            </div>
            <div class="form-group">
				<?= $form->field( $model, 'SERTIFIKAT_KESEHATAN' )->checkbox() ?>
            </div>
<!--            <div class="form-group">-->
<!--				--><?//= $form->field( $model, 'PERJANJIAN_KERJA' )->checkbox() ?>
<!--            </div>-->
<!--            <div class="form-group">-->
<!--				--><?//= $form->field( $model, 'VISA_KERJA' )->checkbox() ?>
<!--            </div>-->
<!--            <div class="form-group">-->
<!--				--><?//= $form->field( $model, 'PERJANJIAN_PENEMPATAN' )->checkbox() ?>
<!--            </div>-->
            <div class="form-group">
				<?= $form->field( $model, 'AKI' )->checkbox() ?>
            </div>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'note' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>
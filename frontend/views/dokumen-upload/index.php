<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DokumenUploadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dokumen Uploads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-upload-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Dokumen Upload'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dokumen_upload_id',
            'ktp',
            'kk',
            'akte',
            'ijasah',
            //'buku_nikah',
            //'paspor',
            //'surat_ijin',
            //'lain1',
            //'lain2',
            //'lain3',
            //'biodata_id',
            //'ktp_note',
            //'kk_note',
            //'akte_note',
            //'ijazah_note',
            //'buku_nikah_note',
            //'paspor_note',
            //'surat_ijin_note',
            //'lain1_note',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

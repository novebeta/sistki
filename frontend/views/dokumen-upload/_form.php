<?php
use app\models\Biodata;
use app\models\Dokumen;
use app\models\DokumenIn;
use app\models\DokumenUpload;
use frontend\assets\LightBoxAsset;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\DokumenUpload */
/* @var $form yii\widgets\ActiveForm */
/** @var DokumenIn $doc */
LightBoxAsset::register( $this );
$doc = new DokumenIn();
?>
<!-- /.container -->
<div class="box box-primary">
	<?php $form = ActiveForm::begin( [
		'id' => 'dokumen-upload-id'
	] ); ?>
    <div class="box-body">
		<?php Pjax::begin(); ?>
        <div class="form-group">
			<?php
			$url      = \yii\helpers\Url::to( [ 'biodata/list' ] );
			$cityDesc = empty( $model->biodata_id ) ? '' : Biodata::findOne( $model->biodata_id )->name;
			echo $form->field( $model, 'biodata_id' )->widget( Select2::classname(), [
				'initValueText' => $cityDesc, // set the initial display text
				'options'       => [ 'placeholder' => 'Search for a biodata ...' ],
				'pluginOptions' => [
					'allowClear'         => true,
					'minimumInputLength' => 3,
					'language'           => [
						'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ),
					],
					'ajax'               => [
						'url'      => $url,
						'dataType' => 'json',
						'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
					],
				],
				'pluginEvents'  => [
					"select2:select" => "function() { document.getElementById('dokumen-upload-id').submit(); }",
				]
			] );
			?>
        </div>
		<?php if ( $model->biodata_id != null ) : ?>
        <div class="row text-lg-left">
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::KTP ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::KTP, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="KTP" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::KTP, $model->biodata_id ); ?>" alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::KTP,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::KK ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::KK, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="KK" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::KK, $model->biodata_id ); ?>" alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::KK,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::PASPOR ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::PASPOR, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="PASPOR" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::PASPOR, $model->biodata_id ); ?>" alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::PASPOR,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::IJAZAH ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::IJAZAH, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="IJAZAH" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::IJAZAH, $model->biodata_id ); ?>" alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::IJAZAH,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
        </div>
        <div class="row text-lg-left">
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::SERTIFIKAT_KESEHATAN ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::SERTIFIKAT_KESEHATAN, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="SERTIFIKAT_KESEHATAN" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::SERTIFIKAT_KESEHATAN, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::SERTIFIKAT_KESEHATAN,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::KARTU_PESERTA_ASURANSI ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::KARTU_PESERTA_ASURANSI, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="KARTU_PESERTA_ASURANSI" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::KARTU_PESERTA_ASURANSI, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::KARTU_PESERTA_ASURANSI,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::PERJANJIAN_KERJA ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::PERJANJIAN_KERJA, $model->biodata_id, false ); ?>"
                   data-title="PERJANJIAN_KERJA" class="d-block mb-4 h-100" target="_blank">
                    <img class="img-fluid img-thumbnail" style="height:213px;"
                         src="<?= DokumenUpload::initialPreview( Dokumen::PERJANJIAN_KERJA, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::PERJANJIAN_KERJA,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'pdf' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::VISA_KERJA ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::VISA_KERJA, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="VISA_KERJA" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::VISA_KERJA, $model->biodata_id ); ?>" alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::VISA_KERJA,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
        </div>
        <div class="row text-lg-left">
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::AKTE_KELAHIRAN ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::AKTE_KELAHIRAN, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="AKTE_KELAHIRAN" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::AKTE_KELAHIRAN, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::AKTE_KELAHIRAN,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::SURAT_IJIN_WALI ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::SURAT_IJIN_WALI, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="SURAT_IJIN_WALI" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::SURAT_IJIN_WALI, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::SURAT_IJIN_WALI,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::BUKU_NIKAH ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::BUKU_NIKAH, $model->biodata_id, false ); ?>"
                   data-lightbox="DOKUMEN" data-title="BUKU_NIKAH" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail"
                         src="<?= DokumenUpload::initialPreview( Dokumen::BUKU_NIKAH, $model->biodata_id ); ?>" alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::BUKU_NIKAH,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::PERJANJIAN_PENEMPATAN ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::PERJANJIAN_PENEMPATAN, $model->biodata_id, false ); ?>"
                   data-title="PERJANJIAN_PENEMPATAN" class="d-block mb-4 h-100" target="_blank">
                    <img class="img-fluid img-thumbnail " style="height:213px;"
                         src="<?= DokumenUpload::initialPreview( Dokumen::PERJANJIAN_PENEMPATAN, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::PERJANJIAN_PENEMPATAN,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'pdf' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
        </div>
        <div class="row text-lg-left">
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::COP_VISA ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::COP_VISA, $model->biodata_id, false ); ?>"
                   data-title="COP_VISA" class="d-block mb-4 h-100" target="_blank">
                    <img class="img-fluid img-thumbnail " style="height:213px;"
                         src="<?= DokumenUpload::initialPreview( Dokumen::COP_VISA, $model->biodata_id ); ?>"
                         alt="">
                </a>
		        <?php
		        echo \kartik\file\FileInput::widget( [
			        'name'          => Dokumen::COP_VISA,
			        'pluginOptions' => [
				        'showCaption'           => false,
				        'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
				        'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
				        'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
				        'uploadExtraData'       => [
					        'biodata_id' => $model->biodata_id
				        ],
			        ],
			        'pluginEvents'  => [
				        "fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
			        ]
		        ] );
		        ?>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <h3 class="box-title"><?= $doc->getAttributeLabel( Dokumen::MEDICAL ); ?></h3>
                <a href="<?= DokumenUpload::initialPreview( Dokumen::MEDICAL, $model->biodata_id, false ); ?>"
                   data-title="MEDICAL" class="d-block mb-4 h-100" target="_blank">
                    <img class="img-fluid img-thumbnail " style="height:213px;"
                         src="<?= DokumenUpload::initialPreview( Dokumen::MEDICAL, $model->biodata_id ); ?>"
                         alt="">
                </a>
				<?php
				echo \kartik\file\FileInput::widget( [
					'name'          => Dokumen::MEDICAL,
					'pluginOptions' => [
						'showCaption'           => false,
						'showPreview'           => false,
//                            'fileTypeSettings' => ['image'],
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/dokumen-upload/upload' ] ),
						'uploadExtraData'       => [
							'biodata_id' => $model->biodata_id
						],
					],
					'pluginEvents'  => [
						"fileuploaded" => "function() {  document.getElementById('dokumen-upload-id').submit(); }"
					]
				] );
				?>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>
<?php Pjax::end(); ?>
</div>
<?php ActiveForm::end(); ?>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenUpload */

$this->title = Yii::t('app', 'Update Dokumen Upload: {nameAttribute}', [
    'nameAttribute' => $model->dokumen_upload_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokumen Uploads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dokumen_upload_id, 'url' => ['view', 'id' => $model->dokumen_upload_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dokumen-upload-update">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

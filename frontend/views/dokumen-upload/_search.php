<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenUploadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokumen-upload-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'dokumen_upload_id') ?>

    <?= $form->field($model, 'ktp') ?>

    <?= $form->field($model, 'kk') ?>

    <?= $form->field($model, 'akte') ?>

    <?= $form->field($model, 'ijasah') ?>

    <?php // echo $form->field($model, 'buku_nikah') ?>

    <?php // echo $form->field($model, 'paspor') ?>

    <?php // echo $form->field($model, 'surat_ijin') ?>

    <?php // echo $form->field($model, 'lain1') ?>

    <?php // echo $form->field($model, 'lain2') ?>

    <?php // echo $form->field($model, 'lain3') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <?php // echo $form->field($model, 'ktp_note') ?>

    <?php // echo $form->field($model, 'kk_note') ?>

    <?php // echo $form->field($model, 'akte_note') ?>

    <?php // echo $form->field($model, 'ijazah_note') ?>

    <?php // echo $form->field($model, 'buku_nikah_note') ?>

    <?php // echo $form->field($model, 'paspor_note') ?>

    <?php // echo $form->field($model, 'surat_ijin_note') ?>

    <?php // echo $form->field($model, 'lain1_note') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

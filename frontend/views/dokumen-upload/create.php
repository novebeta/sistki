<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DokumenUpload */

$this->title = Yii::t('app', 'Create Dokumen Upload');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokumen Uploads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-upload-create">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

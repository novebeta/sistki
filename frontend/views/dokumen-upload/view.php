<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DokumenUpload */

$this->title = $model->dokumen_upload_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokumen Uploads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-upload-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->dokumen_upload_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->dokumen_upload_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dokumen_upload_id',
            'ktp',
            'kk',
            'akte',
            'ijasah',
            'buku_nikah',
            'paspor',
            'surat_ijin',
            'lain1',
            'lain2',
            'lain3',
            'biodata_id',
            'ktp_note',
            'kk_note',
            'akte_note',
            'ijazah_note',
            'buku_nikah_note',
            'paspor_note',
            'surat_ijin_note',
            'lain1_note',
        ],
    ]) ?>

</div>

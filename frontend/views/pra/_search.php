<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PraSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pra-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pra_id') ?>

    <?= $form->field($model, 'no_polis') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'tgl_expired') ?>

    <?= $form->field($model, 'note_') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pra */

$this->title = 'Update Pra: ' . $model->pra_id;
$this->params['breadcrumbs'][] = ['label' => 'Pras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pra_id, 'url' => ['view', 'id' => $model->pra_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

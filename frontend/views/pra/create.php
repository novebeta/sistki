<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pra */

$this->title = 'Create Pra';
$this->params['breadcrumbs'][] = ['label' => 'Pras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pra-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

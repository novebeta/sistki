<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pra_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_polis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl')->textInput() ?>

    <?= $form->field($model, 'tgl_expired')->textInput() ?>

    <?= $form->field($model, 'note_')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biodata_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

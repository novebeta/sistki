<?php
use kartik\datecontrol\DateControl;
use kartik\widgets\Typeahead;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
//use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
//use app\models\Country;
$listDataNegara    = \app\models\Negara::find()->select( 'nama' )->column();
$listDataKabupaten = \app\models\Kabupaten::find()->select( 'nama' )->column();
//use yii\helpers\ArrayHelper;
//$listData = array_values($countries);//ArrayHelper::map($countries, 'nama', 'nama');
//if (empty($listDataNegara)) {
//    $listDataNegara = ['Indonesia'];
//}
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin(); ?>
    <!--    --><? //= $form->field($model, 'company_id')->textInput(['maxlength' => true]) ?>
    <div class="box-body">
        <div class="form-group">
			<?= $form->field( $model, 'name' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'bisnis' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
				'type'           => DateControl::FORMAT_DATE,
				'ajaxConversion' => false,
				'widgetOptions'  => [
					'pluginOptions' => [
						'autoclose' => true
					]
				]
			] );
			?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'address' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'city' )->widget( Typeahead::classname(), [
				'options'       => [ 'placeholder' => 'Filter as you type ...' ],
				'pluginOptions' => [ 'highlight' => true ],
				'dataset'       => [
					[
						'local' => $listDataKabupaten,
						'limit' => 10
					]
				]
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'country' )->widget( Typeahead::classname(), [
				'options'       => [ 'placeholder' => 'Filter as you type ...' ],
				'pluginOptions' => [ 'highlight' => true ],
				'dataset'       => [
					[
						'local' => $listDataNegara,
						'limit' => 10
					]
				]
			] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'no_license' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'phone' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'employees' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'employee_ind' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
		<?php if ( ! $model->isNewRecord ) : ?>
            <div class="form-group">
				<?php echo \kartik\file\FileInput::widget( [
					'name'          => 'photos[]',
					'options'       => [
						'multiple' => true
					],
					'pluginOptions' => [
						'initialPreview'        => $model->photos,
						'allowedFileExtensions' => [ 'jpg', 'JPG', 'jpeg', 'JPEG' ],
						'uploadUrl'             => Url::to( [ '/company/upload' ] ),
						'uploadExtraData'       => [
							'company_id' => $model->company_id
						],
						'deleteUrl'             => Url::to( [ '/company/delete-photo' ] ),
						'deleteExtraData'       => [
							'company_id' => $model->company_id
						],
						'initialPreviewConfig'  => $model->photos_config,
						'initialPreviewAsData'  => true,
						'initialCaption'        => "Company Photos",
						'overwriteInitial'      => false,
						'maxFileSize'           => 2800
					]
				] );
				?>
            </div>
		<?php endif; ?>
        <div class="form-group">
            <div class="form-group">
				<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>

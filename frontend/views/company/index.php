<?php
use kartik\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Yii::t( 'app', 'Companies' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
	<?=
	\kartik\grid\GridView::widget(
		[
			"dataProvider"       => $dataProvider,
			'filterModel'        => $searchModel,
			"condensed"          => true,
			"hover"              => true,
			'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
			'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
			'pjax'               => true,
			'toolbar'            => [
				[
					'content' =>
						Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
							'type'    => 'button',
							'title'   => Yii::t( 'kvgrid', 'Add ' . $this->title ),
							'class'   => 'btn btn-success',
							'onclick' => 'location.href="create"'
						] ) . ' ' .
						Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
							'data-pjax' => 0,
							'class'     => 'btn btn-default',
							'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
						] )
				],
				'{export}',
				'{toggleData}',
			],
			'export'             => [
				'fontAwesome' => true
			],
			'responsive'         => true,
			'hover'              => true,
			'floatHeader'        => true,
			'floatHeaderOptions' => [ 'scrollingTop' => true ],
			'showPageSummary'    => false,
			'toggleDataOptions'  => [ 'minCount' => 10 ],
			'panel'              => [
				'heading' => false,
//				'footer'  => false,
//				'type'    => GridView::TYPE_PRIMARY,
//				'heading' => '<i class="glyphicon glyphicon-book"></i> ' . $this->title,
			],
			"columns"            => [
				[
					'class'          => 'kartik\grid\SerialColumn',
					'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
					'width'          => '36px',
					'header'         => '',
					'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
				],
				'name',
				'bisnis',
				'tgl',
				'address',
				'city',
				'country',
				'no_license',
				'phone',
				'employees',
				'employee_ind',
				[ 'class' => 'yii\grid\ActionColumn' ],
			]
		]
	);
	?>
    <!--<p>-->
    <!--	--><? //= Html::a( Yii::t( 'app', 'Create Company' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
    <!--</p>-->
    <!---->
    <!--<div class="box box-primary">-->
    <!--    <div class="box-header"><h3 class="box-title"><i class="fa fa-table"></i>&nbsp;Table</h3></div>-->
    <!--    <div class="box-body">-->
    <!--		--><?php //Pjax::begin(); ?>
    <!--		-->
    <!--		--><?php //Pjax::end(); ?>
    <!--    </div>-->
    <!--</div>-->
</div>
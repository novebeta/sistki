<?php
use app\modules\admin\components\MenuHelper;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
//$items = MenuHelper::getAssignedMenu( Yii::$app->user->id );
//var_dump($items);
?>
<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="#" class="navbar-brand">PT. DUTA WIBAWA</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <!--             --><? //= dmstr\widgets\Menu::widget(
			//            [
			//                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
			//                'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id)
			//            ]); ?>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<?= \yii\helpers\Url::home() ?>">Home <span
                                    class="sr-only">(current)</span></a></li>
                    <!--                    <li><a href="#">Link</a></li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">App Module<span
                                    class="caret"></span></a>
						<?= dmstr\widgets\Menu::widget( [
							'submenuTemplate' => "\n<ul class='dropdown-menu' {show}>\n{items}\n</ul>\n",
							'defaultIconHtml' => '',
							'options'         => [ 'class' => 'dropdown-menu', 'data-widget' => 'dropdown' ],
							'items'           => MenuHelper::getAssignedMenu( Yii::$app->user->id )
						] ); ?>
                    </li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                    </div>
                </form>
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= Yii::getAlias('@web/images/logo.jpg'); ?>" class="user-image"
                                 alt="User Image"/>
                            <span class="hidden-xs"><?= \Yii::$app->user->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= Yii::getAlias('@web/images/logo.jpg'); ?>" class="img-circle"
                                     alt="User Image"/>
                                <p>
									<?= \Yii::$app->user->username ?>
<!--                                    <small>Member since Nov. 2012</small>-->
                                </p>
                            </li>
                            <!-- Menu Body -->
<!--                            <li class="user-body">-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Followers</a>-->
<!--                                </div>-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Sales</a>-->
<!--                                </div>-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Friends</a>-->
<!--                                </div>-->
<!--                            </li>-->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= Url::to( '/admin/user/change-password' ) ?>" class=" btn btn-default btn-flat">Change Password</a>
                                </div>
                                <div class="pull-right">
									<?= Html::a(
										'Sign out',
										[ '/site/logout' ],
										[
											'data-method' => 'post',
											'class'       => 'btn btn-default btn-flat'
										]
									) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>


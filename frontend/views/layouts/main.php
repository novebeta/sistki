<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */
dmstr\web\AdminLteAsset::register( $this );
$directoryAsset = Yii::$app->assetManager->getPublishedUrl( '@vendor/almasaeed2010/adminlte/dist' );
$titleContent   = isset( $titleContent ) ? $titleContent : null;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
        <title><?= Html::encode( $this->title ) ?></title>
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu > .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -6px;
                margin-left: -1px;
                -webkit-border-radius: 0 6px 6px 6px;
                -moz-border-radius: 0 6px 6px;
                border-radius: 0 6px 6px 6px;
            }

            .dropdown-submenu:hover > .dropdown-menu {
                display: block;
            }

            .dropdown-submenu > a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover > a:after {
                border-left-color: #fff;
            }

            .dropdown-submenu.pull-left {
                float: none;
            }

            .dropdown-submenu.pull-left > .dropdown-menu {
                left: -100%;
                margin-left: 10px;
                -webkit-border-radius: 6px 0 6px 6px;
                -moz-border-radius: 6px 0 6px 6px;
                border-radius: 6px 0 6px 6px;
            }
        </style>
        <script>
            function formatNumber(num) {
                return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            }
        </script>
		<?php $this->head() ?>
    </head>
    <body class="hold-transition sidebar-mini skin-blue layout-top-nav">
	<?php $this->beginBody() ?>
    <div class="wrapper">
		<?= $this->render(
			'header.php',
			[ 'directoryAsset' => $directoryAsset ]
		) ?>
		<?= $this->render(
			'content.php',
			[
				'content' => $content,
			]
		) ?>
    </div>
	<?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
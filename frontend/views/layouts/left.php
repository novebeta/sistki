<?php
use app\models\Biodata;
use mdm\admin\components\MenuHelper;
use yii\bootstrap\Nav;
/** @var Biodata $biodata */
//var_dump($_SESSION);
//$biodata = $_SESSION[ "BIODATA" ];
//if (isset($_SESSION[ "BIODATA" ]) && $_SESSION[ "BIODATA" ] != null) {
//    $biodata = Biodata::find()->where("biodata_id = :biodata_id", [':biodata_id' => $id])->one();
//}
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- search form -->
        <form action="biodata/#" method="get" class="sidebar-form">
            <div class="input-group ">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id)
            ]);  ?>

    </section>

</aside>

<?php
/* @var $this yii\web\View */
$this->title = 'Dashboard';
$counter     = 1;
?>
<div class="row">
    <div class="col-xs-5">
        <div class="row">
            <div class="col-xs-6 column">
                <!-- small box -->
                <div class=" small-box bg-green">
                    <div class="inner">
                        <h3><?= \app\models\Biodata::countDenganJob(); ?></h3>
                        <p>CPMI Sudah dapat Job</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to( [ 'biodata/withjobs' ] ); ?>" class="small-box-footer">More
                        info
                        <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-xs-6 column">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= \app\models\Biodata::countTanpaJob(); ?></h3>
                        <p>CPMI Tunggu Job</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to( [ 'biodata/nojobs' ] ); ?>" class="small-box-footer">More
                        info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-7">
        <div class="row">
            <div class="col-xs-4 column">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?= \app\models\Biodata::countResign(); ?></h3>
                        <p>CPMI Resign</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to( [ 'biodata/resign' ] ); ?>" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-xs-4 column">
                <!-- small box -->
                <div class="small-box bg-black">
                    <div class="inner">
                        <h3><?= \app\models\Biodata::find()
						                           ->where( 'LENGTH(kasus) > 0' )
						                           ->count( 'biodata_id' ); ?></h3>
                        <p>PMI Bermasalah</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to( [ 'biodata/kasus' ] ); ?>" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-xs-4 column">
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3><?= \app\models\Biodata::find()
						                           ->where( 'loan > 0' )
						                           ->count( 'biodata_id' ); ?></h3>
                        <p>Pinjaman</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to( [ 'biodata/loan' ] ); ?>" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Job Order Terpenuhi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 5px">#</th>
                        <th>Tanggal</th>
                        <th>Compane Name</th>
                        <th style="width: 40px">Total</th>
                    </tr>
					<? foreach ( $jobOrders as $item ) : ?>
                        <tr <?=(($item['closed'] == 1) || ($item['qouta']==$item['jml']))?'style="color: red;"':'';?> >
                            <td><?= $counter; ?>.</td>
                            <td><?= $item['tgl'] ?></td>
                            <td><?= $item['name'] ?></td>
                            <td><?= $item['Total'] ?></td>
                        </tr>
						<? $counter ++; endforeach; ?>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-6">
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Medical Expired</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 5px">#</th>
                        <th>Name</th>
                        <th>Job</th>
                        <th>Expired</th>
                        <th>Days</th>
                    </tr>
					<? $counter = 1;
					foreach ( $medikalExpired as $item ) : ?>
                        <tr>
                            <td><?= $counter; ?>.</td>
                            <td><?= $item['name'] ?></td>
                            <td><?= $item['company_name'] ?></td>
                            <td><?= $item['expired'] ?></td>
                            <td><?= $item['diff'] ?></td>
                        </tr>
						<? $counter ++; endforeach; ?>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-6">
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Fingering Expired</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 5px">#</th>
                        <th>Name</th>
                        <th>Job</th>
                        <th>Expired</th>
                        <th>Days</th>
                    </tr>
					<? $counter = 1;
					foreach ( $mpExpired as $item ) : ?>
                        <tr>
                            <td><?= $counter; ?>.</td>
                            <td><?= $item['name'] ?></td>
                            <td><?= $item['company_name'] ?></td>
                            <td><?= $item['expired'] ?></td>
                            <td><?= $item['diff'] ?></td>
                        </tr>
						<? $counter ++; endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>
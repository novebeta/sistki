<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rekomid */

$this->title = $model->rekom_id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekomids'),
                                  'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_3' ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekomid-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->rekom_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->rekom_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rekom_id',
            'tgl',
            'rekomid',
            'rbb_duta',
            'pic',
            'note_',
            'biodata_id',
            'created_at',
        ],
    ]) ?>

</div>

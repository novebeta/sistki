<?php
/* @var $this yii\web\View */
/* @var $model app\models\Rekomid */
$this->title = Yii::t( 'app', 'Update Rekomid: {nameAttribute}', [
	'nameAttribute' => $model->rekom_id,
] );
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'app', 'Biodata' ), 'url' => [ 'biodata/index' ] ];
$this->params['breadcrumbs'][] = [
	'label' => $biodata->name,
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id ]
];
$this->params['breadcrumbs'][] = [
	'label' => Yii::t( 'app', 'Rekomids' ),
	'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_3' ]
];
$this->params['breadcrumbs'][] = [ 'label' => $model->rekom_id, 'url' => [ 'view', 'id' => $model->rekom_id ] ];
$this->params['breadcrumbs'][] = Yii::t( 'app', 'Update' );
?>
<div class="rekomid-update">
    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rekomid */

$this->title = Yii::t('app', 'Create Rekom ID');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekom ID'),
                                  'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_3' ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekomid-create">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

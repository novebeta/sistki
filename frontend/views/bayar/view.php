<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bayar */

$this->title = $model->bayar_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bayars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bayar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->bayar_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->bayar_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bayar_id',
            'tipe',
            'tipe_trans',
            'amount',
            'loan',
            'voucher',
            'note_',
            'biodata_id',
            'created_at',
        ],
    ]) ?>

</div>

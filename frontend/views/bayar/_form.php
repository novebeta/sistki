<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bayar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bayar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bayar_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipe')->textInput() ?>

    <?= $form->field($model, 'tipe_trans')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loan')->textInput() ?>

    <?= $form->field($model, 'voucher')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note_')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biodata_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FwcmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Fwcms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-body">
	<?php Pjax::begin(); ?>
	<?
	try {
		echo \kartik\grid\GridView::widget(
			[
				"dataProvider"       => $dataProviderFwcms,
				'filterModel'        => $searchModelFwcms,
				"condensed"          => true,
				"hover"              => true,
				'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
				'pjax'               => true,
				'toolbar'            => [
					[
						'content' => ( Yii::$app->user->can( 'akes_tab' ) ?
								Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
									'type'    => 'button',
									'title'   => Yii::t( 'kvgrid', 'Add FWCMS' ),
									'class'   => 'btn btn-success',
									'onclick' => 'location.href="' . Url::toRoute( "fwcms/create" ) . '"'
								] ) : '' ) . ' ' .
						             Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'fwcms/index' ], [
							             'data-pjax' => 0,
							             'class'     => 'btn btn-default',
							             'title'     => Yii::t( 'kvgrid', 'Reset Grid' )
						             ] )
					],
					'{export}',
					'{toggleData}',
				],
				'export'             => [
					'fontAwesome' => true
				],
				'responsive'         => true,
				'floatHeader'        => true,
				'floatHeaderOptions' => [ 'scrollingTop' => true ],
				'showPageSummary'    => false,
				'toggleDataOptions'  => [ 'minCount' => 10 ],
				'panel'              => [
					'heading' => false,
//					'footer'  => false,
					//							'type'    => GridView::TYPE_PRIMARY,
					//							'heading' => '<i class="glyphicon glyphicon-book"></i> FWCMS',
				],
				"columns"            => [
					[
						'class'          => 'kartik\grid\SerialColumn',
						'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
						'width'          => '36px',
						'header'         => '',
						'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
					],
					'tgl',
					'no_paspor',
					'rbb_duta',
					'pic',
					'note_',
					'tgl_tugasan',
					'expired',
					[
						'class'      => 'kartik\grid\ActionColumn',
						'template'   => Yii::$app->user->can( 'akes_tab' ) ? '{view} {update} {delete}' : '{view}',
						'urlCreator' => function ( $action, $model, $key, $index ) {
							$url = Url::toRoute( [
								'fwcms/' . $action,
								'id' => $key
							] ); // your own url generation logic
							return $url;
						}
					],
				]
			]
		);
	} catch ( Exception $e ) {
	}
	?>
	<?php Pjax::end(); ?>
</div>
<?php
use frontend\assets\BiodataAsset;
use kartik\checkbox\CheckboxX;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
BiodataAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model app\models\Fwcms */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelLunas */
/* @var $modelDP */
?>
<script>
    var dataItemArr = <?=isset( $item ) ? $item : '[]';?>;
</script>
<div class="box box-primary">
	<?php $form = ActiveForm::begin( [
		'id' => 'fwcms_form_input'
	] ); ?>
	<?php //echo Html::hiddenInput( 'refresh', 0, [ 'id' => 'hidden_refresh' ] ); ?>
    <div class="box-body">
        <div class="form-group">
			<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
				'type'           => DateControl::FORMAT_DATE,
				'ajaxConversion' => true,
				'widgetOptions'  => [
					'pluginOptions' => [
						'autoclose' => true
					],
					'pluginEvents'  => [
						'changeDate' => "function(e) {  
						    var date = e.date;
						    var newDate = new Date(date.setMonth(date.getMonth()+3));
						    $('#fwcms-expired-disp-kvdate').kvDatepicker('update', newDate);
						    $('#fwcms-expired').val(toJSONLocal(newDate));
						}"
					]
				]
			] );
			?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'no_paspor' )->textInput( [ 'maxlength' => true, 'disabled' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'rbb_duta' )->dropDownList( [
				'RBB'  => 'RBB',
				'DUTA' => 'DUTA',
			], [ 'prompt' => '' ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'pic' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'note_' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <!--        <div class="form-group">-->
        <!--            --><? //= $form->field($model, 'fex')->dropDownList(['F' => 'F', 'EX' => 'EX',], ['prompt' => '']) ?>
        <!--        </div>-->
        <div class="form-group">
			<?= $form->field( $model, 'mcu' )->textInput( [ 'maxlength' => true, 'disabled' => true ] ) ?>
        </div>
        <div class="form-group">
            <div class="form-group">
				<?= $form->field( $model, 'tgl_tugasan' )->widget( DateControl::classname(), [
					'type'           => DateControl::FORMAT_DATE,
					'ajaxConversion' => true,
					'widgetOptions'  => [
						'pluginOptions' => [
							'autoWidget' => false,
							'autoclose'  => true
						]
					]
				] );
				?>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
				<?= $form->field( $model, 'expired' )->widget( DateControl::classname(), [
					'type'           => DateControl::FORMAT_DATE,
					'ajaxConversion' => true,
					'widgetOptions'  => [
						'pluginOptions' => [
							'autoWidget' => false,
							'autoclose'  => true
						]
					]
				] );
				?>
            </div>
        </div>
        <div class="form-group">
		    <?= $form->field( $model, 'tipe_bayar' )->dropDownList( [
			    'SPONSOR' => 'SPONSOR',
			    'SENDIRI' => 'SENDIRI',
			    'PINJAMAN' => 'PINJAMAN',
		    ],
			    [
				    'prompt'   => '',
				    'onchange' => new JsExpression(" 
					var val_ = $(this).val();
					if(val_ === 'SPONSOR'){
					    $('.fwcms-bayar').hide();
					}else{
					    $('.fwcms-bayar').show();
					}
						" )
			    ] ) ?>
        </div>
        <div class="form-group fwcms-bayar">
		    <?php
		    echo $form->field( $model, 'biaya' )->widget( NumberControl::classname(), [
			    'maskedInputOptions' => [
				    'prefix'     => 'Rp',
				    'allowMinus' => false
			    ],
			    'displayOptions'     => [ '' ],
			    'name'               => 'amount_dp',
		    ] );
		    ?>
        </div>
        <div class="box box-default collapsed-box fwcms-bayar">
            <div class="box-header with-border">
                <h3 class="box-title">Pembayaran</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body">
                    <table id="table-medikal-bayar" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Voucher</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="modal fade" id="modal-medikal-bayar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pembayaran</h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <form role="form" id="form-medikal-bayar">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Amount</label>
	                                <?=NumberControl::widget( [
		                                'id'=> "amount-medikal-bayar",
		                                'name'=> "amount-medikal-bayar",
		                                'maskedInputOptions' => [
			                                'prefix'     => 'Rp',
			                                'allowMinus' => false
		                                ],
		                                'options' => [
		                                ]
	                                ]);?>
                                </div>
                                <div class="form-group">
                                    <label>Voucher</label>
                                    <input type="text" class="form-control" id="voucher-medikal-bayar">
                                </div>
                                <div class="form-group">
                                    <label>Note</label>
                                    <input type="text" class="form-control" id="note-medikal-bayar">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-medikal-bayar-add-row">Save changes
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<!--        <div class="box-header with-border">-->
<!--            <h3 class="box-title">Down Payment</h3>-->
<!--        </div>-->
<!--        <div class="form-group">-->
<!--			--><?php
//			echo $form->field( $modelDP, 'amount' )->widget( NumberControl::classname(), [
//				'maskedInputOptions' => [
//					'prefix'     => 'Rp',
//					'allowMinus' => false
//				],
//				'displayOptions'     => [ '' ],
//				'options'            => [ 'id' => 'amount_dp_id', 'name' => 'amount_dp' ],
//			] );
//			?>
<!--        </div>-->
<!--        <div class="form-group">-->
<!--			--><?//= $form->field( $modelDP, 'voucher', [
//				'inputOptions' => [
//					'name'  => 'voucher_dp',
//					'id'    => 'voucher_dp_id',
//					'class' => 'form-control'
//				]
//			] )->textInput( [ 'maxlength' => true ] ) ?>
<!--        </div>-->
<!--        <div class="form-group">-->
<!--			--><?php
//			echo $form->field( $modelDP, 'loan' )->widget( CheckboxX::classname(), [
////				'id'            => 'amount_dp',
//				'name'          => 'loan_dp',
//				'options'       => [ 'id' => 'loan_dp_id', 'name' => 'loan_dp' ],
//				'pluginOptions' => [ 'threeState' => false ]
//			] );
//			?>
<!--        </div>-->
<!--        <div class="box-header with-border">-->
<!--            <h3 class="box-title">Pelunasan</h3>-->
<!--        </div>-->
<!--        <div class="form-group">-->
<!--			--><?php
//			echo $form->field( $modelLunas, 'amount' )->widget( NumberControl::classname(), [
//				'maskedInputOptions' => [
//					'prefix'     => 'Rp',
//					'allowMinus' => false
//				],
//				'displayOptions'     => [ '' ],
//				'options'            => [ 'id' => 'amount_lunas_id', 'name' => 'amount_lunas' ]//
//			] );
//			?>
<!--        </div>-->
<!--        <div class="form-group">-->
<!--			--><?//= $form->field( $modelLunas, 'voucher', [
//				'inputOptions' => [
//					'name'  => 'voucher_lunas',
//					'id'    => 'voucher_lunas_id',
//					'class' => 'form-control'
//				]
//			] )->textInput() ?>
<!--        </div>-->
<!--        <div class="form-group">-->
<!--			--><?php
//			echo $form->field( $modelLunas, 'loan' )->widget( CheckboxX::classname(), [
//				'name'          => 'loan_lunas',
////			    'id'            => 'loan_lunas_id',
//				'options'       => [ 'id' => 'loan_lunas_id', 'name' => 'loan_lunas' ],
//				'pluginOptions' => [ 'threeState' => false ]
//			] );
//			?>
<!--        </div>-->
        <div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJs( $this->render( 'js.php' ) );
?>
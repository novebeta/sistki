<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Fwcms */
$this->title = Yii::t('app', 'Update Fwcms: {nameAttribute}', [
    'nameAttribute' => $model->fwcms_id,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fwcms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fwcms_id, 'url' => ['view', 'id' => $model->fwcms_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="fwcms-update">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'item'  => $item,
    ]) ?>

</div>

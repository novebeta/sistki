<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Fwcms */

$this->title = Yii::t('app', 'Create Fwcms');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fwcms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fwcms-create">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'item'    => $item,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FwcmsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fwcms-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'fwcms_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'no_paspor') ?>

    <?= $form->field($model, 'rbb_duta') ?>

    <?= $form->field($model, 'pic') ?>

    <?php // echo $form->field($model, 'note_') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'fex') ?>

    <?php // echo $form->field($model, 'mcu') ?>

    <?php // echo $form->field($model, 'tgl_tugasan') ?>

    <?php // echo $form->field($model, 'expired') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

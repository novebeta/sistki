<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recruiter */

$this->title = 'Create Recruiter';
$this->params['breadcrumbs'][] = ['label' => 'Recruiter', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recruiter-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

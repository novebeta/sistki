<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recruiter */

$this->title = 'Update Recruiter: ' . $model->location_id;
$this->params['breadcrumbs'][] = ['label' => 'Recruiter', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->location_id, 'url' => ['view', 'id' => $model->location_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recruiter-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RekomPaspor */

$this->title = Yii::t('app', 'Create Rekom Paspor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekom Paspors'),
                                  'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_4' ]];
$this->params['breadcrumbs'][] = $this->title;
?>

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


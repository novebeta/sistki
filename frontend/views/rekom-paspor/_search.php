<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RekomPasporSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekom-paspor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'rekom_paspor_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'prov') ?>

    <?= $form->field($model, 'imigrasi') ?>

    <?= $form->field($model, 'pic') ?>

    <?php // echo $form->field($model, 'note_') ?>

    <?php // echo $form->field($model, 'biodata_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

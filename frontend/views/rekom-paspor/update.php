<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RekomPaspor */

$this->title = Yii::t('app', 'Update Rekom Paspor: {nameAttribute}', [
    'nameAttribute' => $model->rekom_paspor_id,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biodata'), 'url' => ['biodata/index']];
$this->params['breadcrumbs'][] = ['label' => $biodata->name, 'url' => ['biodata/view', 'id' => $biodata->biodata_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekom Paspors'),'url'   => [ 'biodata/view', 'id' => $biodata->biodata_id, '#' => 'tab_4' ]];
$this->params['breadcrumbs'][] = ['label' => $model->rekom_paspor_id, 'url' => ['view', 'id' => $model->rekom_paspor_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


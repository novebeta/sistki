<?php
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */
$this->title                   = Yii::t( 'rbac-admin', 'Assignments' );
$this->params['breadcrumbs'][] = $this->title;
$columns                       = [
	[ 'class' => 'yii\grid\SerialColumn' ],
	$usernameField,
];
if ( ! empty( $extraColumns ) ) {
	$columns = array_merge( $columns, $extraColumns );
}
$columns[] = [
	'class'    => 'yii\grid\ActionColumn',
	'template' => '{view}'
];
?>
<div class="assignment-index">
    <div class="box box-primary">
        <div class="box-body">
			<?php Pjax::begin(); ?>
			<?=
			GridView::widget( [
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => $columns,
			] );
			?>
			<?php Pjax::end(); ?>
        </div>
    </div>
</div>

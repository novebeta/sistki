<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\admin\models\form\Signup */
$this->title                   = Yii::t( 'rbac-admin', 'Signup' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <p>Please fill out the following fields to signup:</p>
					<?= Html::errorSummary( $model ) ?>
					<?php $form = ActiveForm::begin( [ 'id' => 'form-signup' ] ); ?>
					<?= $form->field( $model, 'username' ) ?>
					<?= $form->field( $model, 'email' ) ?>
					<?= $form->field( $model, 'cabang_id' )->dropDownList(
						ArrayHelper::map( \app\models\Cabang::find()->all(), 'cabang_id', 'nama' ) ) ?>
                    <div class="form-group">
                        <label>Company</label>
						<?= Html::dropDownList( 'company_id', '',
							array_merge( ArrayHelper::map( \app\models\Company::find()->all(), 'company_id', 'name' ),
								[ '' => '' ] ), [
								'class' => 'form-control'
							]
						) ?>
                    </div>
					<?= $form->field( $model, 'password' )->passwordInput() ?>
					<?= $form->field( $model, 'retypePassword' )->passwordInput() ?>
                    <div class="box-footer">
						<?= Html::submitButton( Yii::t( 'rbac-admin', 'Signup' ), [
							'class' => 'btn btn-primary',
							'name'  => 'signup-button'
						] ) ?>
                    </div>
					<?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

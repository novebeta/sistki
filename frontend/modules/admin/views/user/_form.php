<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
				<?php $form = ActiveForm::begin(); ?>
                <div class="box-body">
                    <div class="form-group">
						<?= $form->field( $model, 'cabang_id' )->dropDownList(
							ArrayHelper::map( \app\models\Cabang::find()->all(), 'cabang_id', 'nama' )
						)->label( 'Cabang' ) ?>
                    </div>
                    <div class="form-group">
                        <label>Company</label>
						<?= Html::dropDownList( 'company_id', ( $uc == null ) ? '' : $uc->company_id,
							array_merge( ArrayHelper::map( \app\models\Company::find()->all(), 'company_id', 'name' ),
								[ '' => '' ] ), [
								'class' => 'form-control'
							]
						) ?>
                    </div>
                    <div class="box-footer">
						<?= Html::submitButton( $model->isNewRecord ? Yii::t( 'rbac-admin', 'Create' ) : Yii::t( 'rbac-admin', 'Update' ), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

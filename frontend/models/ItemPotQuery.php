<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ItemPot]].
 *
 * @see ItemPot
 */
class ItemPotQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ItemPot[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ItemPot|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

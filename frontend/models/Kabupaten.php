<?php
namespace app\models;
use app\models\base\KabupatenBase;
/**
 * This is the model class for table "{{%kabupaten}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class Kabupaten extends KabupatenBase
{
    public static function primaryKey()
    {
        return ['location_id'];
    }
}

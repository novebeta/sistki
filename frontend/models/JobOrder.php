<?php
namespace app\models;
use app\models\base\JobOrderBase;
use frontend\components\Cloud;
use OSS\Core\OssException;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%job_order}}".
 *
 * @property string $job_order_id
 * @property string $job_title
 * @property string $no_register Register Number
 * @property string $approval_date KDN. Approval No/Date
 * @property string $levy_paid_receipt Levy Paid Receipt
 * @property int $qouta Total Number
 * @property string $age Age
 * @property string $sex Sex
 * @property string $education Education
 * @property string $expected_arrival Expected Arrival
 * @property string $durasi_kontrak Duration of Contract
 * @property string $working_hours Working hours/Week
 * @property string $basic_salary Basic Salary per Month
 * @property string $meal_allowance Meal Allowance
 * @property string $attendance_allowace Attendance Allowace
 * @property string $medical_allowance Medical Allowance
 * @property string $transport Transport
 * @property string $akomodasi Accommodation
 * @property string $return_air_ticket Return Air Ticket
 * @property string $bonus Bonus
 * @property string $draft_contract Draft Master Employment Contract
 * @property string $skill_allowance Skill Allowance
 * @property string $morning_shift Morning Shift
 * @property string $night_shift Night Shift
 * @property string $levy_plks_process Levy, PLKS & Process
 * @property string $payment_levy Payment of Levy
 * @property string $monthly_deduction Monthly Deduction
 * @property string $name_pptkis Name of PPTKIS
 * @property string $sipptki
 * @property string $address_agent
 * @property string $totalNumberComplete
 * @property mixed $readyNumber
 * @property mixed $selectedNumber
 * @property string $other_information Other Information
 */
class JobOrder extends JobOrderBase {
	public $photos = '';
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression( 'NOW()' ),
			],
		];
	}
	public function getReadyNumber() {
		$count = Biodata::find()->where( [
			'job_order_id' => $this->job_order_id
		] )->count();
		return intval( $count );
	}
	public function getSelectedNumber() {
		$count = Biodata::find()->where( "job_order_id = :job_order_id AND company_date IS NOT NULL", [
			':job_order_id' => $this->job_order_id
		] )->count();
		return intval( $count );
	}
	public function getTotalNumberComplete() {
		return $this->qouta . "/" . $this->getReadyNumber();
	}
	public function afterDelete() {
		$bucket    = Cloud::getBucketName();
		$ossClient = Cloud::getOssClient();
		try {
			$item   = Dokumen::JOB_ORDER;
			$id     = $this->job_order_id;
			$object = Yii::$app->params['uploadDir']['path'] . DS . $item . DS . $id . '.jpg';
			$ossClient->deleteObject( $bucket, $object );
			$object_thumb = Yii::$app->params['uploadDir']['path'] . DS . $item . DS . 'thumb' . DS . $id . '.jpg';
			$ossClient->deleteObject( $bucket, $object_thumb );
			Biodata::updateAll( [ 'job_order_id' => null ], [ 'job_order_id' => $id ] );
		} catch ( OssException $e ) {
			Yii::error( $e->getMessage(), 'cloud' );
		}
	}
}

//ALTER TABLE `dutaw240_app`.`nscc_job_order` ADD COLUMN `closed` tinyint(4) NOT NULL DEFAULT 0 AFTER `jml_pot`;
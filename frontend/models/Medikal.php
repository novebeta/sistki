<?php
namespace app\models;
use app\models\base\ModelBase;
use Yii;
/**
 * This is the model class for table "{{%medikal}}".
 *
 * @property string $medikal_id
 * @property string $tgl Date
 * @property string $medikal_center Medikal Center
 * @property string $biaya
 * @property string $input_data Input Data
 * @property string $note_
 * @property string $expired
 * @property string $created_at
 * @property string $biodata_id
 *
 * @property Biodata $biodata
 * @property Bayar[] $bayar
 */
class Medikal extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%medikal}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'tgl', 'biodata_id' ], 'required' ],
			[ [ 'tgl', 'expired', 'created_at' ], 'safe' ],
			[ [ 'biaya' ], 'number' ],
			[ [ 'biaya' ], 'default', 'value' => 0 ],
			[ [ 'tipe_bayar' ], 'string', 'max' => 10 ],
			[ [ 'biodata_id' ], 'string', 'max' => 36 ],
			[ [ 'medikal_center' ], 'string', 'max' => 25 ],
			[ [ 'input_data' ], 'string', 'max' => 100 ],
			[ [ 'note_' ], 'string', 'max' => 600 ],
//            [['medikal_id'], 'unique'],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
			[
				[ 'tipe_bayar', 'medikal_center', 'input_data', 'note_' ],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'medikal_id'     => Yii::t( 'app', 'Medikal ID' ),
			'tgl'            => Yii::t( 'app', 'Date' ),
			'medikal_center' => Yii::t( 'app', 'Medikal Center' ),
			'biaya'          => Yii::t( 'app', 'Biaya' ),
			'input_data'     => Yii::t( 'app', 'Input Data' ),
			'note_'          => Yii::t( 'app', 'Note' ),
			'expired'        => Yii::t( 'app', 'Expired' ),
			'created_at'     => Yii::t( 'app', 'Created At' ),
			'biodata_id'     => Yii::t( 'app', 'Biodata ID' ),
			'tipe_bayar'     => Yii::t( 'app', 'Tipe Bayar' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBayar() {
		return $this->hasMany( Bayar::className(), [ 'biodata_id' => 'biodata_id' ] )
		            ->where( [
			            'tipe_trans' => Bayar::MEDIKAL,
			            'parent_id'  => $this->medikal_id
		            ] );
	}
	/**
	 * @inheritdoc
	 * @return MedikalQuery the active query used by this AR class.
	 */
	public static function find() {
		return new MedikalQuery( get_called_class() );
	}
	public function afterSave( $insert, $changedAttributes ) {
		$city = MedicalCenter::find()
		                     ->where( [ 'nama' => $this->medikal_center ] )
		                     ->one();
		if ( $city == null ) {
			$kab = new Location();
			$kab->createMedical( $this->medikal_center );
		}
		parent::afterSave( $insert, $changedAttributes );
	}
}

<?php
namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * CompanySearch represents the model behind the search form of `app\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'bisnis', 'tgl', 'address', 'city', 'country', 'no_license', 'phone', 'employees', 'employee_ind'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
        ]);
        $query->andFilterWhere(['like', 'company_id', $this->company_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'bisnis', $this->bisnis])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'no_license', $this->no_license])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'employees', $this->employees])
            ->andFilterWhere(['like', 'employee_ind', $this->employee_ind]);
        return $dataProvider;
    }
}

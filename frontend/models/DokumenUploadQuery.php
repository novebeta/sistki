<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DokumenUpload]].
 *
 * @see DokumenUpload
 */
class DokumenUploadQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DokumenUpload[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DokumenUpload|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ItemPot;

/**
 * ItemPotSearch represents the model behind the search form of `app\models\ItemPot`.
 */
class ItemPotSearch extends ItemPot
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_pot_id', 'job_order_id', 'bulan', 'pot'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ItemPot::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'item_pot_id', $this->item_pot_id])
            ->andFilterWhere(['like', 'job_order_id', $this->job_order_id])
            ->andFilterWhere(['like', 'bulan', $this->bulan])
            ->andFilterWhere(['like', 'pot', $this->pot]);

        return $dataProvider;
    }
}

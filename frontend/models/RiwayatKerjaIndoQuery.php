<?php
namespace app\models;
/**
 * This is the ActiveQuery class for [[RiwayatKerjaIndo]].
 *
 * @see RiwayatKerjaIndo
 */
class RiwayatKerjaIndoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    /**
     * @inheritdoc
     * @return RiwayatKerjaIndo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
    /**
     * @inheritdoc
     * @return RiwayatKerjaIndo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

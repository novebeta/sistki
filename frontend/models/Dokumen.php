<?php
namespace app\models;
use app\models\base\DokumenBase;
/**
 * This is the model class for table "{{%dokumen}}".
 *
 * @property string $dokumen_id
 * @property string $tgl
 * @property string $penerima Penerima Berkas
 * @property string $lain_lain
 * @property string $note Keterangan
 * @property string $created_at
 * @property mixed $biodataName
 * @property string $jenis_dokumen
 */
class Dokumen extends DokumenBase {
	const MASUK = '1';
	const KELUAR = '-1';
	const KTP = 'KTP';
	const KK = 'KK';
	const PAS_PHOTO = 'PAS_PHOTO';
	const JOB_ORDER = 'JOB_ORDER';
	const PASPOR = 'PASPOR';
	const IJAZAH = 'IJAZAH';
	const SERTIFIKAT_KESEHATAN = 'SERTIFIKAT_KESEHATAN';
	const KARTU_PESERTA_ASURANSI = 'KARTU_PESERTA_ASURANSI';
	const PERJANJIAN_KERJA = 'PERJANJIAN_KERJA';
	const VISA_KERJA = 'VISA_KERJA';
	const AKTE_KELAHIRAN = 'AKTE_KELAHIRAN';
	const SURAT_IJIN_WALI = 'SURAT_IJIN_WALI';
	const BUKU_NIKAH = 'BUKU_NIKAH';
	const PERJANJIAN_PENEMPATAN = 'PERJANJIAN_PENEMPATAN';
	const AKI = 'AKI';
	const COP_VISA = 'COP_VISA';
	const MEDICAL = 'MEDICAL';
	const JENIS_ARR = [
		'KTP',
		'KK',
		'PASPOR',
		'IJAZAH',
		'SERTIFIKAT_KESEHATAN',
		'KARTU_PESERTA_ASURANSI',
		'PERJANJIAN_KERJA',
		'VISA_KERJA',
		'AKTE_KELAHIRAN',
		'SURAT_IJIN_WALI',
		'BUKU_NIKAH',
		'PERJANJIAN_PENEMPATAN',
		'AKI',
		'PAS_PHOTO',
		'JOB_ORDER',
		'COP_VISA',
		'MEDICAL',
	];
	const DESC_JENIS_ARR = [
		'KTP'                    => '<b>KTP</b></br>Identitas resmi Penduduk sebagai bukti diri yang diterbitkan oleh Dinas Kependudukan dan Pencatatan Sipil Kabupaten/ Kota yang berlaku di seluruh wilayah Negara Kesatuan Republik Indonesia
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'KK'                     => '<b>Kartu Keluarga (KK)</b> </br>Identitas Keluarga yang memuat data tentang susunan, hubungan dan jumlah anggota keluarga. Kartu Keluarga wajib dimiliki oleh setiap keluarga. Kartu ini berisi data lengkap tentang identitas Kepala Keluarga dan anggota keluarganya.',
		'PASPOR'                 => '<b>Paspor</b> </br>Dokumen yang dikeluarkan oleh Pemerintah Republik Indonesia kepada warga negara Indonesia untuk melakukan perjalanan antarnegara yang berlaku selama jangka waktu tertentu.
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'IJAZAH'                 => '<b>Ijazah</b> </br>Surat Tanda Tamat Belajar',
		'SERTIFIKAT_KESEHATAN'   => '<b>Sertifikat Kesehatan</b> </br>Bukti tertulis yang berisi keterangan layak untuk bekerja (fit to work) yang dikeluarkan oleh sarana kesehatan yang melakukan pemeriksaan kesehatan calon PMI
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'KARTU_PESERTA_ASURANSI' => '<b>Kartu Peserta Asuransi (KPA)</b> </br>Kartu yang diberikan oleh penanggung atas nama calon PMI/PMI sebagai bukti keikutsertaan tertanggung dalam asuransi yang merupakan bagian yang tidak terpisahkan dari polis
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'PERJANJIAN_KERJA'       => '<b>Perjanjian Kerja</b> </br>Perjanjian tertulis antara PMI dengan pengguna yang memuat syarat-syarat kerja, hak, dan kewajiban masing-masing pihak. Seluruh halaman dijadikan dalam bentuk satu (1) file PDF.
file yang diterima : pdf, ukuran file <= (kurang dari) 3 MB, dan tidak boleh ada karakter spasi pada nama file',
		'VISA_KERJA'             => '<b>Visa Kerja</b> </br>Izin tertulis yang diberikan oleh pejabat yang berwenang pada perwakilan suatu Negara yang memuat persetujuan untuk masuk dan melakukan pekerjaan di negara yang bersangkutan
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'AKTE_KELAHIRAN'         => '<b>Akte Kelahiran</b> </br>Bukti Sah mengenai Status dan Peristiwa Kelahiran Seseorang yang dikeluarkan oleh Dinas Kependudukan dan Catatan Sipil
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'SURAT_IJIN_WALI'        => '<b>Surat Ijin Wali</b> </br>Surat yang menerangkan bahwa Suami/Istri/Orangtua/Wali memberikan ijin untuk bekerja ke luar negeri yang diketahui oleh Kepala Desa.
file yang diterima : jpg, ukuran file <= (kurang dari) 500 KB, tidak boleh ada karakter spasi pada nama file, pastikan gambar terscan dengan penuh dan dapat dibaca',
		'BUKU_NIKAH'             => '<b>Buku Nikah</b> <br/>Surat yang berisi penjelasan bahwa seorang perempuan dan seorang laki-laki kawin dengan sah',
		'PERJANJIAN_PENEMPATAN'  => '<b>Perjanjian Penempatan</b> </br>Perjanjian tertulis antara P3MI dengan calon PMI yang memuat hak dan kewajiban masing-masing pihak dalam rangka penempatan PMI di Negara tujuan sesuai pearturan perundang-undangan
file yang diterima : pdf, ukuran file <= (kurang dari) 3 MB, dan tidak boleh ada karakter spasi pada nama file',
		'PAS_PHOTO'              => '',
		'JOB_ORDER'              => '',
		'COP_VISA'              => '<b>Cop Visa</b>',
		'MEDICAL'              => '<b>Medical</b>',
	];
//	const DESC_JENIS_ARR = [
//		'KTP'                    => 'KTP',
//		'KK'                     => 'Kartu Keluarga (KK)',
//		'PASPOR'                 => 'Paspor',
//		'IJAZAH'                 => 'Surat Tanda Tamat Belajar (Ijazah)',
//		'SERTIFIKAT_KESEHATAN'   => 'Sertifikat Kesehatan',
//		'KARTU_PESERTA_ASURANSI' => 'Kartu Peserta Asuransi (KPA)',
//		'PERJANJIAN_KERJA'       => 'Perjanjian Kerja',
//		'VISA_KERJA'             => 'Visa Kerja',
//		'AKTE_KELAHIRAN'         => 'Akte Kelahiran',
//		'SURAT_IJIN_WALI'        => 'Surat Ijin Wali',
//		'BUKU_NIKAH'             => 'Buku Nikah',
//		'PERJANJIAN_PENEMPATAN'  => 'Perjanjian Penempatan',
//		'PAS_PHOTO'              => 'Pas Foto',
//		'JOB_ORDER'              => 'Job Order',
//	];
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
//	public function getBiodataName() {
//		return $this->biodata->name;
//	}
}

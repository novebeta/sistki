<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[BiodataNojob]].
 *
 * @see BiodataNojob
 */
class BiodataNojobQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BiodataNojob[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BiodataNojob|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

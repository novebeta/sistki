<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mp;

/**
 * MpSearch represents the model behind the search form of `app\models\Mp`.
 */
class MpSearch extends Mp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mp_id', 'no_polis', 'tgl', 'tgl_expired', 'note_', 'biodata_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'tgl_expired' => $this->tgl_expired,
        ]);

        $query->andFilterWhere(['like', 'mp_id', $this->mp_id])
            ->andFilterWhere(['like', 'no_polis', $this->no_polis])
            ->andFilterWhere(['like', 'note_', $this->note_])
            ->andFilterWhere(['like', 'biodata_id', $this->biodata_id]);

        return $dataProvider;
    }
}

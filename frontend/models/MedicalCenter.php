<?php

namespace app\models;

use app\models\base\MedicalCenterBase;
use Yii;

/**
 * This is the model class for table "{{%medical_center}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class MedicalCenter extends MedicalCenterBase
{

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%recruiter}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class Recruiter extends \yii\db\ActiveRecord
{
	public static function primaryKey()
	{
		return ['location_id'];
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%recruiter}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['location_id'], 'required'],
            [['location_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 100],
	        [ 'nama', 'filter', 'filter' => 'strtoupper' ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'location_id' => 'Location ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * {@inheritdoc}
     * @return RecruiterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RecruiterQuery(get_called_class());
    }
}

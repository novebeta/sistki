<?php
namespace app\models;
/**
 * This is the ActiveQuery class for [[RiwayatKerjaMalay]].
 *
 * @see RiwayatKerjaMalay
 */
class RiwayatKerjaMalayQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    /**
     * @inheritdoc
     * @return RiwayatKerjaMalay[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
    /**
     * @inheritdoc
     * @return RiwayatKerjaMalay|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

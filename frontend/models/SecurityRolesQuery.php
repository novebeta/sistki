<?php
namespace app\models;
use yii\db\ActiveQuery;
/**
 * This is the ActiveQuery class for [[SecurityRoles]].
 *
 * @see SecurityRole
 */
class SecurityRolesQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    /**
     * @inheritdoc
     * @return SecurityRoles[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
    /**
     * @inheritdoc
     * @return SecurityRoles|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

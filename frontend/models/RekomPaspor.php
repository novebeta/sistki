<?php

namespace app\models;

use app\models\base\ModelBase;
use Yii;
/**
 * This is the model class for table "{{%rekom_paspor}}".
 *
 * @property string $rekom_paspor_id
 * @property string $tgl Date
 * @property string $prov
 * @property string $imigrasi Imigrasi
 * @property string $pic PIC
 * @property string $note_ Note
 * @property string $biodata_id
 * @property string $created_at
 *
 * @property Biodata $biodata
 */
class RekomPaspor extends ModelBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rekom_paspor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rekom_paspor_id', 'tgl', 'prov', 'biodata_id'], 'required'],
            [['tgl', 'created_at'], 'safe'],
            [['rekom_paspor_id', 'biodata_id'], 'string', 'max' => 36],
            [['prov', 'imigrasi'], 'string', 'max' => 100],
            [['pic'], 'string', 'max' => 25],
            [['note_'], 'string', 'max' => 600],
            [['biodata_id'], 'unique'],
            [['rekom_paspor_id'], 'unique'],
            [['biodata_id'], 'exist', 'skipOnError' => true, 'targetClass' => Biodata::className(), 'targetAttribute' => ['biodata_id' => 'biodata_id']],
	        [
		        [ 'prov', 'imigrasi', 'pic', 'note_' ],
		        'filter', 'filter' => 'strtoupper'
	        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rekom_paspor_id' => Yii::t('app', 'Rekom Paspor ID'),
            'tgl' => Yii::t('app', 'Date'),
            'prov' => Yii::t('app', 'Prov'),
            'imigrasi' => Yii::t('app', 'Imigrasi'),
            'pic' => Yii::t('app', 'PIC'),
            'note_' => Yii::t('app', 'Note'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiodata()
    {
        return $this->hasOne(Biodata::className(), ['biodata_id' => 'biodata_id']);
    }

    /**
     * @inheritdoc
     * @return RekomPasporQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RekomPasporQuery(get_called_class());
    }
}

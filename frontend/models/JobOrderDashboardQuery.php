<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[JobOrderDashboard]].
 *
 * @see JobOrderDashboard
 */
class JobOrderDashboardQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return JobOrderDashboard[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return JobOrderDashboard|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

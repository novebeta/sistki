<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%medikal_lastexpired}}".
 *
 * @property string $medikal_id
 * @property string $tgl Date
 * @property string $medikal_center Medikal Center
 * @property string $biaya
 * @property string $input_data Input Data
 * @property string $note_
 * @property string $expired
 * @property string $created_at
 * @property string $biodata_id
 * @property string $tipe_bayar
 */
class MedikalLastexpired extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%medikal_lastexpired}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medikal_id', 'tgl', 'expired', 'biodata_id'], 'required'],
            [['tgl', 'expired', 'created_at'], 'safe'],
            [['biaya'], 'number'],
            [['medikal_id', 'biodata_id'], 'string', 'max' => 36],
            [['medikal_center'], 'string', 'max' => 25],
            [['input_data'], 'string', 'max' => 100],
            [['note_'], 'string', 'max' => 600],
            [['tipe_bayar'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'medikal_id' => Yii::t('app', 'Medikal ID'),
            'tgl' => Yii::t('app', 'Date'),
            'medikal_center' => Yii::t('app', 'Medikal Center'),
            'biaya' => Yii::t('app', 'Biaya'),
            'input_data' => Yii::t('app', 'Input Data'),
            'note_' => Yii::t('app', 'Note'),
            'expired' => Yii::t('app', 'Expired'),
            'created_at' => Yii::t('app', 'Created At'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
            'tipe_bayar' => Yii::t('app', 'Tipe Bayar'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return MedikalLastexpiredQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MedikalLastexpiredQuery(get_called_class());
    }
}

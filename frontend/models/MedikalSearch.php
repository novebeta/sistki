<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * MedikalSearch represents the model behind the search form of `app\models\Medikal`.
 */
class MedikalSearch extends Medikal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['medikal_id', 'tgl', 'medikal_center', 'input_data', 'note_', 'expired', 'created_at', 'biodata_id'], 'safe'],
            [['biaya'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Medikal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'biaya' => $this->biaya,
            'expired' => $this->expired,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'medikal_id', $this->medikal_id])
            ->andFilterWhere(['like', 'medikal_center', $this->medikal_center])
            ->andFilterWhere(['like', 'input_data', $this->input_data])
            ->andFilterWhere(['like', 'note_', $this->note_])
            ->andFilterWhere(['like', 'biodata_id', $this->biodata_id]);

        return $dataProvider;
    }
}

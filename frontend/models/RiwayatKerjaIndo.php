<?php
namespace app\models;
use app\models\base\RiwayatKerjaIndoBase;
/**
 * This is the model class for table "{{%riwayat_kerja_indo}}".
 *
 * @property string $riwayat_kerja_id
 * @property string $nama_perusahaan Company Name
 * @property string $lokasi Location
 * @property string $posisi Position as
 * @property string $masa_kerja Working Period
 * @property string $biodata_id
 */
class RiwayatKerjaIndo extends RiwayatKerjaIndoBase
{
    public static function primaryKey()
    {
        return ['riwayat_kerja_id'];
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RekomPaspor]].
 *
 * @see RekomPaspor
 */
class RekomPasporQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RekomPaspor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RekomPaspor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

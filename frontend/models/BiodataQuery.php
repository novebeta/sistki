<?php
namespace app\models;
/**
 * This is the ActiveQuery class for [[Biodata]].
 *
 * @see Biodata
 */
class BiodataQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * @inheritdoc
	 * @return Biodata[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * @inheritdoc
	 * @return Biodata|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	/**
	 * @return BiodataQuery
	 */
	public function searchBiodata() {
		$query = $this;
		$query->select( [
			'nscc_biodata.biodata_id',
			'no_absen',
			'nscc_biodata.sex',
			'nscc_biodata.name',
			'kabupaten',
			'sekolah',
			'no_paspor',
			'nscc_cabang.cabang_id',
			'nscc_biodata.job_order_id',
			'nscc_biodata.note_',
			'tuser',
			'nscc_user.id'
		] );
		$query->joinWith( 'user', true, 'LEFT JOIN' );
		$query->joinWith( 'cabang', true, 'INNER JOIN' );
		$query->joinWith( 'company', true, 'LEFT JOIN' );
		$query->joinWith( 'medikalLastexpired', true, 'LEFT JOIN' );
		return $query;
	}
	/**
	 * @return BiodataQuery
	 */
	public function searchKasus() {
		$query = $this;
		$query->select( [
			'nscc_biodata.biodata_id',
			'no_absen',
			'no_paspor',
			'nscc_biodata.sex',
			'nscc_biodata.name',
			'kabupaten',
			'kasus',
			'note_kasus',
			'nscc_biodata.note_',
		] );
//		$query->joinWith( 'user', true, 'LEFT JOIN' );
//		$query->joinWith( 'cabang', true, 'INNER JOIN' );
//		$query->joinWith( 'company', true, 'LEFT JOIN' );
//		$query->joinWith( 'medikalLastexpired', true, 'LEFT JOIN' );
		$query->where( [ '>', 'LENGTH(kasus)', 0 ] );
		return $query;
	}
	/**
	 * @return BiodataQuery
	 */
	public function searchLoan() {
		$query = $this;
		$query->select( [
			'nscc_biodata.biodata_id',
			'no_absen',
			'nscc_biodata.sex',
			'nscc_biodata.name',
			'nscc_biodata.job_order_id',
			'sekolah',
			'loan',
			'loan_note'
		] );
		$query->joinWith( 'company', true, 'LEFT JOIN' );
		$query->where( [ '>', 'loan', 0 ] );
		return $query;
	}
	/**
	 * @return BiodataQuery
	 */
	public function searchBiodataCompany() {
		$query = $this;
		$query->select( [
			'nscc_biodata.biodata_id',
			'no_absen',
			'nscc_biodata.sex',
			'nscc_biodata.name',
			'kabupaten',
			'sekolah',
			'no_paspor',
			'tb',
			'bb',
			'birthdate',
			'YEAR(CURDATE()) - YEAR(birthdate) AS age',
//			'nscc_cabang.cabang_id',
			'nscc_biodata.job_order_id',
			'nscc_biodata.note_',
			'company_date'
//			'tuser',
//			'nscc_user.id'
		] );
//		$query->joinWith( 'user', true, 'LEFT JOIN' );
//		$query->joinWith( 'cabang', true, 'INNER JOIN' );
		$query->joinWith( 'company', true, 'LEFT JOIN' );
		$query->joinWith( 'medikalLastexpired', true, 'LEFT JOIN' );
//		$company_id = UserCompany::find()
//		                         ->select( [ 'company_id' ] )
//		                         ->where( [ 'user_id' => Yii::$app->user->id ] )->scalar();
		$query->where( [ 'nscc_biodata.job_order_id' => \Yii::$app->session->get( 'job_order_id' ) ] );
		return $query;
	}
}

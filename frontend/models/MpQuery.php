<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Mp]].
 *
 * @see Mp
 */
class MpQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Mp[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Mp|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DokumenOut]].
 *
 * @see DokumenOut
 */
class DokumenOutQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DokumenOut[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DokumenOut|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

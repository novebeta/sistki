<?php
namespace app\models;
use app\models\base\KecamatanBase;
/**
 * This is the model class for table "{{%kecamatan}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class Kecamatan extends KecamatanBase
{
    public static function primaryKey()
    {
        return ['location_id'];
    }
}

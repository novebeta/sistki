<?php
namespace app\models;
use app\models\base\ModelBase;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%isc}}".
 *
 * @property string $isc_id
 * @property string $tgl Date
 * @property string $rbb_duta RBB/DUTA
 * @property string $pic PIC
 * @property string $note_ Note
 * @property string $biodata_id
 * @property string $created_at
 * @property string $kode_bayar KODE BAYAR
 * @property string $expired Expired
 *
 * @property Biodata $biodata
 * @property Bayar[] $bayar
 */
class Isc extends ModelBase {
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value'              => new Expression( 'NOW()' ),
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%isc}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'isc_id', 'tgl', 'biodata_id' ], 'required' ],
			[ [ 'tgl', 'created_at', 'updated_at', 'expired' ], 'safe' ],
			[ [ 'biaya' ], 'number' ],
			[ [ 'biaya' ], 'default', 'value' => 0 ],
			[ [ 'rbb_duta' ], 'string' ],
			[ [ 'isc_id', 'biodata_id' ], 'string', 'max' => 36 ],
			[ [ 'pic' ], 'string', 'max' => 25 ],
			[ [ 'note_' ], 'string', 'max' => 600 ],
			[ [ 'kode_bayar' ], 'string', 'max' => 50 ],
			[ [ 'isc_id' ], 'unique' ],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
			[
				[ 'rbb_duta', 'pic', 'note_', 'kode_bayar' ],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'isc_id'     => Yii::t( 'app', 'Isc ID' ),
			'tgl'        => Yii::t( 'app', 'Date' ),
			'rbb_duta'   => Yii::t( 'app', 'RBB/DUTA' ),
			'pic'        => Yii::t( 'app', 'PIC' ),
			'note_'      => Yii::t( 'app', 'Note' ),
			'biodata_id' => Yii::t( 'app', 'Biodata ID' ),
			'created_at' => Yii::t( 'app', 'Created At' ),
			'kode_bayar' => Yii::t( 'app', 'KODE BAYAR' ),
			'expired'    => Yii::t( 'app', 'Expired' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBayar() {
		return $this->hasMany( Bayar::className(), [ 'biodata_id' => 'biodata_id', ] )
		            ->where( [
			            'tipe_trans' => Bayar::ISC,
			            'parent_id'  => $this->isc_id
		            ] );
	}
	/**
	 * @inheritdoc
	 * @return IscQuery the active query used by this AR class.
	 */
	public static function find() {
		return new IscQuery( get_called_class() );
	}
}

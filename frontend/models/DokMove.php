<?php

namespace app\models;

use app\models\base\ModelBase;
use Yii;

/**
 * This is the model class for table "{{%dok_move}}".
 *
 * @property string $KTP
 * @property string $KK
 * @property string $AKTE_KELAHIRAN
 * @property string $IJAZAH
 * @property string $BUKU_NIKAH
 * @property string $PASPOR
 * @property string $SURAT_IJIN_WALI
 * @property string $PERJANJIAN_PENEMPATAN
 * @property string $PERJANJIAN_KERJA
 * @property string $VISA_KERJA
 * @property string $KARTU_PESERTA_ASURANSI
 * @property string $SERTIFIKAT_KESEHATAN
 * @property string $AKI
 * @property string $biodata_id
 */
class DokMove extends ModelBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%dok_move}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KTP', 'KK', 'AKTE_KELAHIRAN', 'IJAZAH', 'BUKU_NIKAH', 'PASPOR', 'SURAT_IJIN_WALI', 'PERJANJIAN_PENEMPATAN', 'PERJANJIAN_KERJA', 'VISA_KERJA', 'KARTU_PESERTA_ASURANSI', 'SERTIFIKAT_KESEHATAN', 'AKI'], 'number'],
            [['biodata_id'], 'string', 'max' => 36],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KTP' => Yii::t('app', 'Ktp'),
            'KK' => Yii::t('app', 'Kk'),
            'AKTE_KELAHIRAN' => Yii::t('app', 'Akte Kelahiran'),
            'IJAZAH' => Yii::t('app', 'Ijazah'),
            'BUKU_NIKAH' => Yii::t('app', 'Buku Nikah'),
            'PASPOR' => Yii::t('app', 'Paspor'),
            'SURAT_IJIN_WALI' => Yii::t('app', 'Surat Ijin Wali'),
            'PERJANJIAN_PENEMPATAN' => Yii::t('app', 'Perjanjian Penempatan'),
            'PERJANJIAN_KERJA' => Yii::t('app', 'Perjanjian Kerja'),
            'VISA_KERJA' => Yii::t('app', 'Visa Kerja'),
            'KARTU_PESERTA_ASURANSI' => Yii::t('app', 'Kartu Peserta Asuransi'),
            'SERTIFIKAT_KESEHATAN' => Yii::t('app', 'Sertifikat Kesehatan'),
            'AKI' => Yii::t('app', 'Aki'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DokMoveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DokMoveQuery(get_called_class());
    }
}

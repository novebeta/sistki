<?php
namespace app\models;
use app\models\base\ModelBase;
use frontend\components\Cloud;
use OSS\Core\OssException;
use Yii;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
/**
 * This is the model class for table "{{%dokumen_upload}}".
 *
 * @property string $dokumen_upload_id
 * @property string $ktp KTP
 * @property string $kk KK
 * @property string $akte AKTE
 * @property string $ijasah IJASAH
 * @property string $buku_nikah BUKU NIKAH/CERAI
 * @property string $paspor PASPOR
 * @property string $surat_ijin SURAT IJIN
 * @property string $lain1 LAIN-LAIN 1
 * @property string $lain2 LAIN-LAIN 2
 * @property string $lain3 LAIN-LAIN 3
 * @property string $biodata_id
 * @property string $ktp_note Note KTP :
 * @property string $kk_note
 * @property string $akte_note
 * @property string $ijazah_note
 * @property string $buku_nikah_note
 * @property string $paspor_note
 * @property string $surat_ijin_note
 * @property string $lain1_note
 *
 * @property Biodata $biodata
 */
class DokumenUpload extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%dokumen_upload}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
//            [['dokumen_upload_id', 'biodata_id'], 'required'],
//            [['dokumen_upload_id', 'biodata_id'], 'string', 'max' => 36],
			[
				[ 'ktp', 'kk', 'akte', 'ijasah', 'buku_nikah', 'paspor', 'surat_ijin', 'lain1', 'lain2', 'lain3' ],
				'string',
				'max' => 300
			],
			[
				[
					'ktp_note',
					'kk_note',
					'akte_note',
					'ijazah_note',
					'buku_nikah_note',
					'paspor_note',
					'surat_ijin_note',
					'lain1_note'
				],
				'string',
				'max' => 600
			],
//            [['dokumen_upload_id'], 'unique'],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'dokumen_upload_id' => Yii::t( 'app', 'Dokumen Upload ID' ),
			'ktp'               => Yii::t( 'app', 'KTP' ),
			'kk'                => Yii::t( 'app', 'KK' ),
			'akte'              => Yii::t( 'app', 'AKTE' ),
			'ijasah'            => Yii::t( 'app', 'IJASAH' ),
			'buku_nikah'        => Yii::t( 'app', 'BUKU NIKAH/CERAI' ),
			'paspor'            => Yii::t( 'app', 'PASPOR' ),
			'surat_ijin'        => Yii::t( 'app', 'SURAT IJIN' ),
			'lain1'             => Yii::t( 'app', 'LAIN-LAIN 1' ),
			'lain2'             => Yii::t( 'app', 'LAIN-LAIN 2' ),
			'lain3'             => Yii::t( 'app', 'LAIN-LAIN 3' ),
			'biodata_id'        => Yii::t( 'app', 'Biodata ID' ),
			'ktp_note'          => Yii::t( 'app', 'Note KTP :' ),
			'kk_note'           => Yii::t( 'app', 'Kk Note' ),
			'akte_note'         => Yii::t( 'app', 'Akte Note' ),
			'ijazah_note'       => Yii::t( 'app', 'Ijazah Note' ),
			'buku_nikah_note'   => Yii::t( 'app', 'Buku Nikah Note' ),
			'paspor_note'       => Yii::t( 'app', 'Paspor Note' ),
			'surat_ijin_note'   => Yii::t( 'app', 'Surat Ijin Note' ),
			'lain1_note'        => Yii::t( 'app', 'Lain1 Note' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @inheritdoc
	 * @return DokumenUploadQuery the active query used by this AR class.
	 */
	public static function find() {
		return new DokumenUploadQuery( get_called_class() );
	}
	public static function initialPreview( $type, $image_id, $thumb = true ) {
		$bucket    = Cloud::getBucketName();
		$ossClient = Cloud::getOssClient();
		$endpoint  = Cloud::getEndpoint();
//		$directoryAsset  = Yii::$app->assetManager->publish( '@vendor/almasaeed2010/adminlte/dist' );
		$pathImage = BaseUrl::base() . "/images/noimage.jpg";// Url::base(true).$directoryAsset . "/img/noimage.jpg";
		$ownFile   = Yii::$app->params['uploadDir']['path'] . DS . $type;
		try {
			if ( $thumb ) {
				if ( $type != Dokumen::PERJANJIAN_KERJA && $type != Dokumen::PERJANJIAN_PENEMPATAN ) {
					$ownFile .= DS . 'thumb';
				}
			}
			$ownFile .= DS . $image_id;
			if ( $type == Dokumen::PERJANJIAN_KERJA || $type == Dokumen::PERJANJIAN_PENEMPATAN ) {
				$ownFile .= '.pdf';
			} else {
				$ownFile .= '.jpg';
			}
			$exist = $ossClient->doesObjectExist( $bucket, $ownFile );
			if ( $exist !== false ) {
//				$md5       = md5_file( $ownFile );
				$pathImage = 'https://' . $bucket . '.' . $endpoint . '/' . Yii::$app->params['uploadDir']['path'] . DS . $type;
				if ( $thumb ) {
					if ( $type != Dokumen::PERJANJIAN_KERJA && $type != Dokumen::PERJANJIAN_PENEMPATAN ) {
						$pathImage .= DS . 'thumb';
					} else {
						return BaseUrl::base() . "/images/pdf.png";
					}
				}
				$pathImage .= DS . $image_id;
				if ( $type == Dokumen::PERJANJIAN_KERJA || $type == Dokumen::PERJANJIAN_PENEMPATAN ) {
					$pathImage .= '.pdf';
				} else {
					$pathImage .= '.jpg';
				}
			}
		} catch ( OssException $e ) {
		}
		return $pathImage;
	}
	public static function getUrlNoImage(){
		return Url::to("@web/images/noimage.jpg",true);
	}
	public static function getUrlDokumen($type, $image_id, $thumb = true ){
		$bucket    = Cloud::getBucketName();
		$endpoint  = Cloud::getEndpoint();
		$pathImage = 'https://' . $bucket . '.' . $endpoint . '/' . Yii::$app->params['uploadDir']['path'] . DS . $type;
		if ( $thumb ) {
			if ( $type != Dokumen::PERJANJIAN_KERJA && $type != Dokumen::PERJANJIAN_PENEMPATAN ) {
				$pathImage .= DS . 'thumb';
			} else {
				return Url::to("@web/images/pdf.png",true);
			}
		}
		$pathImage .= DS . $image_id;
		if ( $type == Dokumen::PERJANJIAN_KERJA || $type == Dokumen::PERJANJIAN_PENEMPATAN ) {
			$pathImage .= '.pdf';
		} else {
			$pathImage .= '.jpg';
		}
		return $pathImage;
	}

	public static function isFileDokumenExists( $type, $biodata_id ) {
		$bucket    = Cloud::getBucketName();
		$ossClient = Cloud::getOssClient();
		$endpoint  = Cloud::getEndpoint();
		try {
			if ( $type == Dokumen::PERJANJIAN_KERJA || $type == Dokumen::PERJANJIAN_PENEMPATAN ) {
				return $ossClient->doesObjectExist( $bucket, Yii::$app->params['uploadDir']['path'] . DS . $type . DS . $biodata_id . ".pdf" );
			} else {
				return $ossClient->doesObjectExist( $bucket, Yii::$app->params['uploadDir']['path'] . DS . $type . DS . $biodata_id . ".jpg" );
			}
		} catch ( OssException $e ) {
			return false;
		}
	}
}

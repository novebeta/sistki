<?php
namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * BiodataSearch represents the model behind the search form of `app\models\Biodata`.
 */
class BiodataSearch extends Biodata {
	public $companyName;
//	public $jobOrderRegister;
	public $cabangName;
//	public $lastExpiredMedikal;
	public $medikalCenter;
	public $username;
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'jobOrderRegister',
					'biodata_id',
					'name',
					'no_hp',
					'address',
					'kecamatan',
					'kabupaten',
					'propinsi',
					'birthdate',
					'tempat_lahir',
					'agama',
					'suku',
					'ayah',
					'nikah',
					'pendidikan',
					'sekolah',
					'jurusan',
					'lulus',
					'punya_sodara',
					'alasan',
					'siap_kontrak',
					'siap_kerja',
					'path_foto',
					'remark',
					'pl',
					'status_proses',
					'no_paspor',
					'kode_rekomid',
					'no_visa',
					'tgl_visa',
					'endorse',
					'asuransi',
					'pap_tgl',
					'pap_note',
					'tiket_tgl',
					'tiket_note',
					'no_pk',
					'cabang_id',
					'siap_ikut_aturan',
					'fex',
					'job_order_id',
					'kasus',
					'note_kasus',
					'companyName',
					'cabangName',
					'note_',
					'medikalCenter',
					'username',
					'loan',
					'loan_note',
				],
				'safe'
			],
			[ [ 'tb', 'bb' ], 'number' ],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @param BiodataQuery $query
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params,$query) {
		if ( ! Yii::$app->user->can( 'akes_tab' ) ) {
			$user_id = Yii::$app->user->id;
			/** @var User $user */
			$user      = User::find()
			                 ->where( [ 'id' => $user_id ] )->one();
			$cabang_id = - 1;
			if ( $user != null ) {
				$cabang_id = $user->cabang_id;
			}
			$query->andWhere( 'nscc_biodata.cabang_id = :cabang_id', [
				':cabang_id' => $cabang_id
			] );
		}
//		$query->joinWith('jobOrder',true,'LEFT JOIN');

		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
			'sort'  => [ 'defaultOrder' => [ 'tdate' => SORT_DESC ] ],
		] );
		$this->load( $params );
		if ( ! $this->validate() ) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
//		$query->andFilterWhere( [
//			'birthdate'    => $this->birthdate,
//			'tempat_lahir' => $this->tempat_lahir,
//			'agama'        => $this->agama,
//			'tb'           => $this->tb,
//			'bb'           => $this->bb,
//			'tgl_visa'     => $this->tgl_visa,
//			'pap_tgl'      => $this->pap_tgl,
//			'tiket_tgl'    => $this->tiket_tgl,
//		] );
		$query
//			->andFilterWhere( [ 'like', 'biodata_id', $this->biodata_id ] )
			->andFilterWhere( [ 'like', 'nscc_biodata.name', $this->name ] )
//			->andFilterWhere( [ 'like', 'no_hp', $this->no_hp ] )
//			->andFilterWhere( [ 'like', 'nscc_biodata.address', $this->address ] )
//			->andFilterWhere( [ 'like', 'kecamatan', $this->kecamatan ] )
			->andFilterWhere( [ 'like', 'kabupaten', $this->kabupaten ] )
//			->andFilterWhere( [ 'like', 'propinsi', $this->propinsi ] )
//			->andFilterWhere( [ 'like', 'suku', $this->suku ] )
//			->andFilterWhere( [ 'like', 'ayah', $this->ayah ] )
//			->andFilterWhere( [ 'like', 'nikah', $this->nikah ] )
//			->andFilterWhere( [ 'like', 'pendidikan', $this->pendidikan ] )
			->andFilterWhere( [ 'like', 'sekolah', $this->sekolah ] )
//			->andFilterWhere( [ 'like', 'jurusan', $this->jurusan ] )
//			->andFilterWhere( [ 'like', 'lulus', $this->lulus ] )
//			->andFilterWhere( [ 'like', 'punya_sodara', $this->punya_sodara ] )
//			->andFilterWhere( [ 'like', 'alasan', $this->alasan ] )
//			->andFilterWhere( [ 'like', 'siap_kontrak', $this->siap_kontrak ] )
//			->andFilterWhere( [ 'like', 'siap_kerja', $this->siap_kerja ] )
//			->andFilterWhere( [ 'like', 'path_foto', $this->path_foto ] )
//			->andFilterWhere( [ 'like', 'remark', $this->remark ] )
//			->andFilterWhere( [ 'like', 'pl', $this->pl ] )
//			->andFilterWhere( [ 'like', 'status_proses', $this->status_proses ] )
			->andFilterWhere( [ 'like', 'no_paspor', $this->no_paspor ] )
//			->andFilterWhere( [ 'like', 'kode_rekomid', $this->kode_rekomid ] )
//			->andFilterWhere( [ 'like', 'no_visa', $this->no_visa ] )
//			->andFilterWhere( [ 'like', 'endorse', $this->endorse ] )
//			->andFilterWhere( [ 'like', 'asuransi', $this->asuransi ] )
//			->andFilterWhere( [ 'like', 'pap_note', $this->pap_note ] )
//			->andFilterWhere( [ 'like', 'tiket_note', $this->tiket_note ] )
//			->andFilterWhere( [ 'like', 'no_pk', $this->no_pk ] )
//			->andFilterWhere( [ 'like', 'cabang_id', $this->cabang_id ] )
//			->andFilterWhere( [ 'like', 'siap_ikut_aturan', $this->siap_ikut_aturan ] )
//			->andFilterWhere( [ 'like', 'fex', $this->fex ] )
//			->andFilterWhere( [ 'like', 'job_order_id', $this->job_order_id ] )
			->andFilterWhere( [ 'like', 'kasus', $this->kasus ] )
			->andFilterWhere( [ 'like', 'note_kasus', $this->note_kasus ] )
			->andFilterWhere( [ 'like', 'nscc_company.name', $this->companyName ] )
			->andFilterWhere( [ 'like', 'nscc_cabang.nama', $this->cabangName ] )
			->andFilterWhere( [ 'like', 'nscc_biodata.note_', $this->note_ ] )
			->andFilterWhere( [ 'like', 'nscc_user.username', $this->username ] )
			->andFilterWhere( [ 'between', 'tiket_tgl', $params['dp_1'],$params['dp_2'] ] )
			->andFilterWhere( [ 'like', 'medikal_center', $this->medikalCenter ] );

//			->andFilterWhere( [ 'like', 'no_register', $this->jobOrderRegister ] );
//		$query->orderBy( [
//			'tdate' => SORT_DESC
//		] );
		// filter by country name
//		$query->joinWith( [
//			'jobOrder' => function ( $q ) {
//				$q->where( 'nscc_job_order.no_register LIKE "%' . $this->jobOrderRegister . '%"' );
//			}
//		], true, 'LEFT JOIN' );
		return $dataProvider;
	}
}

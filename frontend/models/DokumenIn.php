<?php
namespace app\models;
use Yii;
/**
 * This is the model class for table "{{%dokumen_in}}".
 *
 * @property string $dokumen_id
 * @property string $tgl
 * @property string $penerima Penerima Berkas
 * @property int $KTP
 * @property int $KK
 * @property int $AKTE_KELAHIRAN
 * @property int $IJAZAH
 * @property int $BUKU_NIKAH
 * @property int $PASPOR
 * @property int $SURAT_IJIN_WALI
 * @property int $PERJANJIAN_PENEMPATAN
 * @property int $PERJANJIAN_KERJA
 * @property int $VISA_KERJA
 * @property int $KARTU_PESERTA_ASURANSI
 * @property int $SERTIFIKAT_KESEHATAN
 * @property int $AKI
 * @property string $note Keterangan
 * @property string $created_at
 * @property string $biodata_id
 * @property string $cabang_id
 */
class DokumenIn extends Dokumen {
	public static function primaryKey() {
		return [ 'dokumen_id' ];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%dokumen_in}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'dokumen_id', 'tgl', 'biodata_id', 'cabang_id', 'penerima' ], 'required' ],
			[ [ 'tgl', 'created_at' ], 'safe' ],
			[
				[
					'KTP',
					'KK',
					'AKTE_KELAHIRAN',
					'IJAZAH',
					'BUKU_NIKAH',
					'PASPOR',
					'SURAT_IJIN_WALI',
					'PERJANJIAN_PENEMPATAN',
					'PERJANJIAN_KERJA',
					'VISA_KERJA',
					'SERTIFIKAT_KESEHATAN',
					'KARTU_PESERTA_ASURANSI',
					'AKI'
				],
				'integer'
			],
			[ [ 'dokumen_id', 'biodata_id', 'cabang_id' ], 'string', 'max' => 36 ],
			[ [ 'penerima' ], 'string', 'max' => 100 ],
			[ [ 'note' ], 'string', 'max' => 600 ],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'dokumen_id'             => Yii::t( 'app', 'Dokumen ID' ),
			'tgl'                    => Yii::t( 'app', 'Tgl' ),
			'penerima'               => Yii::t( 'app', 'Penerima Berkas' ),
			'KTP'                    => Yii::t( 'app', 'KTP' ),
			'KK'                     => Yii::t( 'app', 'KK' ),
			'AKTE_KELAHIRAN'         => Yii::t( 'app', 'Akte  Kelahiran' ),
			'IJAZAH'                 => Yii::t( 'app', 'Ijazah' ),
			'BUKU_NIKAH'             => Yii::t( 'app', 'Buku  Nikah' ),
			'PASPOR'                 => Yii::t( 'app', 'Paspor' ),
			'SURAT_IJIN_WALI'        => Yii::t( 'app', 'Surat  Ijin  Wali' ),
			'PERJANJIAN_PENEMPATAN'  => Yii::t( 'app', 'Perjanjian  Penempatan' ),
			'PERJANJIAN_KERJA'       => Yii::t( 'app', 'Perjanjian  Kerja' ),
			'VISA_KERJA'             => Yii::t( 'app', 'Visa  Kerja' ),
			'SERTIFIKAT_KESEHATAN'   => Yii::t( 'app', 'Sertifikat Kesehatan' ),
			'KARTU_PESERTA_ASURANSI' => Yii::t( 'app', 'Kartu  Peserta  Asuransi' ),
			'AKI'                    => Yii::t( 'app', 'AKI' ),
			'note'                   => Yii::t( 'app', 'Keterangan' ),
			'created_at'             => Yii::t( 'app', 'Created At' ),
			'biodata_id'             => Yii::t( 'app', 'Biodata ID' ),
			'cabang_id'              => Yii::t( 'app', 'Cabang ID' ),
		];
	}
	/**
	 * @inheritdoc
	 * @return DokumenInQuery the active query used by this AR class.
	 */
	public static function find() {
		return new DokumenInQuery( get_called_class() );
	}
}
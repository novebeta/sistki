<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[BiodataWithjob]].
 *
 * @see BiodataWithjob
 */
class BiodataWithjobQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BiodataWithjob[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BiodataWithjob|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

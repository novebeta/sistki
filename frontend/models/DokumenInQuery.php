<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DokumenIn]].
 *
 * @see DokumenIn
 */
class DokumenInQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DokumenIn[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DokumenIn|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

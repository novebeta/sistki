<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * DokumenSearch represents the model behind the search form of `app\models\Dokumen`.
 */
class DokumenSearch extends Dokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dokumen_id', 'tgl', 'penerima', 'ktp', 'kk', 'akte', 'ijazah', 'buku_nikah', 'paspor', 'surat_ijin', 'lain_lain', 'note', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dokumen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'dokumen_id', $this->dokumen_id])
            ->andFilterWhere(['like', 'penerima', $this->penerima])
            ->andFilterWhere(['like', 'ktp', $this->ktp])
            ->andFilterWhere(['like', 'kk', $this->kk])
            ->andFilterWhere(['like', 'akte', $this->akte])
            ->andFilterWhere(['like', 'ijazah', $this->ijazah])
            ->andFilterWhere(['like', 'buku_nikah', $this->buku_nikah])
            ->andFilterWhere(['like', 'paspor', $this->paspor])
            ->andFilterWhere(['like', 'surat_ijin', $this->surat_ijin])
            ->andFilterWhere(['like', 'lain_lain', $this->lain_lain])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}

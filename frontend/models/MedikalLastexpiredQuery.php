<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MedikalLastexpired]].
 *
 * @see MedikalLastexpired
 */
class MedikalLastexpiredQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MedikalLastexpired[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MedikalLastexpired|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

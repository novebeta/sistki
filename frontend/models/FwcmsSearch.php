<?php
namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * FwcmsSearch represents the model behind the search form of `app\models\Fwcms`.
 */
class FwcmsSearch extends Fwcms {
	public $mcu;
	public $no_paspor;
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'fwcms_id',
					'tgl',
					'no_paspor',
					'rbb_duta',
					'pic',
					'note_',
					'biodata_id',
					'created_at',
					'mcu',
					'tgl_tugasan',
					'expired'
				],
				'safe'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params ) {
		$query = Fwcms::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );
		$this->load( $params );
		if ( ! $this->validate() ) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere( [
			'tgl'         => $this->tgl,
			'created_at'  => $this->created_at,
			'tgl_tugasan' => $this->tgl_tugasan,
			'expired'     => $this->expired,
		] );
		$query->andFilterWhere( [ 'like', 'fwcms_id', $this->fwcms_id ] )
		      ->andFilterWhere( [ 'like', 'no_paspor', $this->no_paspor ] )
		      ->andFilterWhere( [ 'like', 'rbb_duta', $this->rbb_duta ] )
		      ->andFilterWhere( [ 'like', 'pic', $this->pic ] )
		      ->andFilterWhere( [ 'like', 'note_', $this->note_ ] )
		      ->andFilterWhere( [ 'like', 'biodata_id', $this->biodata_id ] )
		      ->andFilterWhere( [ 'like', 'mcu', $this->mcu ] );
		return $dataProvider;
	}
}

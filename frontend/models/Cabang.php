<?php
namespace app\models;
use app\models\base\CabangBase;
/**
 * This is the model class for table "{{%cabang}}".
 *
 * @property string $cabang_id
 * @property string $kode
 * @property string $nama
 *
 * @property Biodata[] $biodatas
 */
class Cabang extends CabangBase
{
//    public function rules()
//    {
//        return $this->makeSafe(parent::rules());
//    }
}

<?php
namespace app\models;
use app\models\base\ModelBase;
use Yii;
/**
 * This is the model class for table "{{%rekomid}}".
 *
 * @property string $rekom_id
 * @property string $tgl Date
 * @property string $rekomid ID
 * @property string $rbb_duta RBB/DUTA
 * @property string $pic PIC
 * @property string $note_ Note
 * @property string $biodata_id
 * @property string $created_at
 *
 * @property Biodata $biodata
 */
class Rekomid extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%rekomid}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'rekom_id', 'tgl', 'rekomid', 'biodata_id' ], 'required' ],
			[ [ 'tgl', 'created_at' ], 'safe' ],
			[ [ 'rbb_duta' ], 'string' ],
			[ [ 'rekom_id', 'biodata_id' ], 'string', 'max' => 36 ],
			[ [ 'rekomid' ], 'string', 'max' => 20 ],
			[ [ 'pic' ], 'string', 'max' => 25 ],
			[ [ 'note_' ], 'string', 'max' => 600 ],
			[ [ 'rekom_id' ], 'unique' ],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
			[
				[ 'rbb_duta', 'rekomid', 'pic', 'note_' ],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'rekom_id'   => Yii::t( 'app', 'Rekom ID' ),
			'tgl'        => Yii::t( 'app', 'Date' ),
			'rekomid'    => Yii::t( 'app', 'ID' ),
			'rbb_duta'   => Yii::t( 'app', 'RBB/DUTA' ),
			'pic'        => Yii::t( 'app', 'PIC' ),
			'note_'      => Yii::t( 'app', 'Note' ),
			'biodata_id' => Yii::t( 'app', 'Biodata ID' ),
			'created_at' => Yii::t( 'app', 'Created At' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @inheritdoc
	 * @return RekomidQuery the active query used by this AR class.
	 */
	public static function find() {
		return new RekomidQuery( get_called_class() );
	}
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * RekomPasporSearch represents the model behind the search form of `app\models\RekomPaspor`.
 */
class RekomPasporSearch extends RekomPaspor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rekom_paspor_id', 'tgl', 'prov', 'imigrasi', 'pic', 'note_', 'biodata_id', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RekomPaspor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'rekom_paspor_id', $this->rekom_paspor_id])
            ->andFilterWhere(['like', 'prov', $this->prov])
            ->andFilterWhere(['like', 'imigrasi', $this->imigrasi])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'note_', $this->note_])
            ->andFilterWhere(['like', 'biodata_id', $this->biodata_id]);

        return $dataProvider;
    }
}

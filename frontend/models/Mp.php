<?php

namespace app\models;

use app\models\base\ModelBase;
use Yii;

/**
 * This is the model class for table "{{%mp}}".
 *
 * @property string $mp_id
 * @property string $no_polis
 * @property string $tgl
 * @property string $tgl_expired
 * @property string $note_
 * @property string $biodata_id
 *
 * @property Biodata $biodata
 */
class Mp extends ModelBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%mp}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mp_id'], 'required'],
            [['tgl', 'tgl_expired'], 'safe'],
            [['mp_id', 'biodata_id'], 'string', 'max' => 36],
            [['no_polis'], 'string', 'max' => 50],
            [['note_'], 'string', 'max' => 600],
            [['mp_id'], 'unique'],
            [['biodata_id'], 'exist', 'skipOnError' => true, 'targetClass' => Biodata::className(), 'targetAttribute' => ['biodata_id' => 'biodata_id']],
	        [
		        [ 'no_polis', 'note_' ],
		        'filter', 'filter' => 'strtoupper'
	        ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mp_id' => Yii::t('app', 'Mp ID'),
            'no_polis' => Yii::t('app', 'No Polis'),
            'tgl' => Yii::t('app', 'Tgl'),
            'tgl_expired' => Yii::t('app', 'Tgl Expired'),
            'note_' => Yii::t('app', 'Note'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiodata()
    {
        return $this->hasOne(Biodata::className(), ['biodata_id' => 'biodata_id']);
    }

    /**
     * {@inheritdoc}
     * @return MpQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MpQuery(get_called_class());
    }
}

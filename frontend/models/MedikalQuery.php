<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Medikal]].
 *
 * @see Medikal
 */
class MedikalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Medikal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Medikal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

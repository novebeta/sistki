<?php
namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * DokumenInSearch represents the model behind the search form of `app\models\DokumenIn`.
 */
class DokumenInSearch extends DokumenIn
{
	public $biodataName;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['dokumen_id', 'tgl', 'penerima', 'note', 'created_at', 'biodata_id','biodataName', 'name', 'cabang_id'], 'safe'],
			[['KTP', 'KK', 'AKTE_KELAHIRAN', 'IJAZAH', 'BUKU_NIKAH', 'PASPOR', 'SURAT_IJIN_WALI', 'PERJANJIAN_PENEMPATAN', 'PERJANJIAN_KERJA', 'VISA_KERJA', 'KARTU_PESERTA_ASURANSI', 'AKI'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = DokumenIn::find();
		$query->joinWith('biodata',true);
		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'tgl' => $this->tgl,
			'KTP' => $this->KTP,
			'KK' => $this->KK,
			'AKTE_KELAHIRAN' => $this->AKTE_KELAHIRAN,
			'IJAZAH' => $this->IJAZAH,
			'BUKU_NIKAH' => $this->BUKU_NIKAH,
			'PASPOR' => $this->PASPOR,
			'SURAT_IJIN_WALI' => $this->SURAT_IJIN_WALI,
			'PERJANJIAN_PENEMPATAN' => $this->PERJANJIAN_PENEMPATAN,
			'PERJANJIAN_KERJA' => $this->PERJANJIAN_KERJA,
			'VISA_KERJA' => $this->VISA_KERJA,
			'KARTU_PESERTA_ASURANSI' => $this->KARTU_PESERTA_ASURANSI,
			'AKI' => $this->AKI,
			'created_at' => $this->created_at,
		]);

		$query->andFilterWhere(['like', 'dokumen_id', $this->dokumen_id])
		      ->andFilterWhere(['like', 'penerima', $this->penerima])
		      ->andFilterWhere(['like', 'note', $this->note])
		      ->andFilterWhere(['like', 'biodata_id', $this->biodata_id])
		      ->andFilterWhere(['like', 'nscc_biodata.name', $this->biodataName])
		      ->andFilterWhere(['like', 'cabang_id', $this->cabang_id]);

		return $dataProvider;
	}
}
<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * DokumenUploadSearch represents the model behind the search form of `app\models\DokumenUpload`.
 */
class DokumenUploadSearch extends DokumenUpload
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dokumen_upload_id', 'ktp', 'kk', 'akte', 'ijasah', 'buku_nikah', 'paspor', 'surat_ijin', 'lain1', 'lain2', 'lain3', 'biodata_id', 'ktp_note', 'kk_note', 'akte_note', 'ijazah_note', 'buku_nikah_note', 'paspor_note', 'surat_ijin_note', 'lain1_note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DokumenUpload::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'dokumen_upload_id', $this->dokumen_upload_id])
            ->andFilterWhere(['like', 'ktp', $this->ktp])
            ->andFilterWhere(['like', 'kk', $this->kk])
            ->andFilterWhere(['like', 'akte', $this->akte])
            ->andFilterWhere(['like', 'ijasah', $this->ijasah])
            ->andFilterWhere(['like', 'buku_nikah', $this->buku_nikah])
            ->andFilterWhere(['like', 'paspor', $this->paspor])
            ->andFilterWhere(['like', 'surat_ijin', $this->surat_ijin])
            ->andFilterWhere(['like', 'lain1', $this->lain1])
            ->andFilterWhere(['like', 'lain2', $this->lain2])
            ->andFilterWhere(['like', 'lain3', $this->lain3])
            ->andFilterWhere(['like', 'biodata_id', $this->biodata_id])
            ->andFilterWhere(['like', 'ktp_note', $this->ktp_note])
            ->andFilterWhere(['like', 'kk_note', $this->kk_note])
            ->andFilterWhere(['like', 'akte_note', $this->akte_note])
            ->andFilterWhere(['like', 'ijazah_note', $this->ijazah_note])
            ->andFilterWhere(['like', 'buku_nikah_note', $this->buku_nikah_note])
            ->andFilterWhere(['like', 'paspor_note', $this->paspor_note])
            ->andFilterWhere(['like', 'surat_ijin_note', $this->surat_ijin_note])
            ->andFilterWhere(['like', 'lain1_note', $this->lain1_note]);

        return $dataProvider;
    }
}

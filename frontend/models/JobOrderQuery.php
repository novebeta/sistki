<?php

namespace app\models;

use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[JobOrder]].
 *
 * @see JobOrder
 */
class JobOrderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return JobOrder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return JobOrder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function select2( $value, $label, $orderBy, $where = [ 'condition' => null, 'params' => [] ] ) {
		return ArrayHelper::map(
			$this->select( [ "CONCAT(" . implode( ",' - ',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all(), 'value', 'label' );
	}
}

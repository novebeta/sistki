<?php
namespace app\models;
/**
 * This is the ActiveQuery class for [[Cabang]].
 *
 * @see Cabang
 */
class CabangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    /**
     * @inheritdoc
     * @return Cabang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
    /**
     * @inheritdoc
     * @return Cabang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

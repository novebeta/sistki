<?php
namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * JobOrderSearch represents the model behind the search form of `app\models\JobOrder`.
 */
class JobOrderSearch extends JobOrder {
	public $companyName;
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'job_order_id',
					'no_register',
					'approval_date',
					'levy_paid_receipt',
					'age',
					'sex',
					'education',
					'expected_arrival',
					'durasi_kontrak',
					'working_hours',
					'basic_salary',
					'meal_allowance',
					'attendance_allowace',
					'medical_allowance',
					'transport',
					'akomodasi',
					'return_air_ticket',
					'bonus',
					'draft_contract',
					'skill_allowance',
					'morning_shift',
					'night_shift',
					'levy_plks_process',
					'payment_levy',
					'monthly_deduction',
					'name_pptkis',
					'sipptki',
					'address_agent',
					'other_information',
					'tgl',
					'company_id',
					'companyName'
				],
				'safe'
			],
			[ [ 'qouta' ], 'integer' ],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params ) {
		$query = JobOrder::find();
		$query->joinWith( [ 'company' ], false, 'INNER JOIN' );
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );
		$this->load( $params );
		if ( ! $this->validate() ) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere( [
			'qouta' => $this->qouta,
			'tgl'   => $this->tgl,
			'nscc_job_order.company_id'   => $this->company_id,
		] );
		$query->andFilterWhere( [ 'like', 'job_order_id', $this->job_order_id ] )
		      ->andFilterWhere( [ 'like', 'no_register', $this->no_register ] )
		      ->andFilterWhere( [ 'like', 'approval_date', $this->approval_date ] )
		      ->andFilterWhere( [ 'like', 'levy_paid_receipt', $this->levy_paid_receipt ] )
		      ->andFilterWhere( [ 'like', 'age', $this->age ] )
		      ->andFilterWhere( [ 'like', 'sex', $this->sex ] )
		      ->andFilterWhere( [ 'like', 'education', $this->education ] )
		      ->andFilterWhere( [ 'like', 'expected_arrival', $this->expected_arrival ] )
		      ->andFilterWhere( [ 'like', 'durasi_kontrak', $this->durasi_kontrak ] )
		      ->andFilterWhere( [ 'like', 'working_hours', $this->working_hours ] )
		      ->andFilterWhere( [ 'like', 'basic_salary', $this->basic_salary ] )
		      ->andFilterWhere( [ 'like', 'meal_allowance', $this->meal_allowance ] )
		      ->andFilterWhere( [ 'like', 'attendance_allowace', $this->attendance_allowace ] )
		      ->andFilterWhere( [ 'like', 'medical_allowance', $this->medical_allowance ] )
		      ->andFilterWhere( [ 'like', 'transport', $this->transport ] )
		      ->andFilterWhere( [ 'like', 'akomodasi', $this->akomodasi ] )
		      ->andFilterWhere( [ 'like', 'return_air_ticket', $this->return_air_ticket ] )
		      ->andFilterWhere( [ 'like', 'bonus', $this->bonus ] )
		      ->andFilterWhere( [ 'like', 'draft_contract', $this->draft_contract ] )
		      ->andFilterWhere( [ 'like', 'skill_allowance', $this->skill_allowance ] )
		      ->andFilterWhere( [ 'like', 'morning_shift', $this->morning_shift ] )
		      ->andFilterWhere( [ 'like', 'night_shift', $this->night_shift ] )
		      ->andFilterWhere( [ 'like', 'levy_plks_process', $this->levy_plks_process ] )
		      ->andFilterWhere( [ 'like', 'payment_levy', $this->payment_levy ] )
		      ->andFilterWhere( [ 'like', 'monthly_deduction', $this->monthly_deduction ] )
		      ->andFilterWhere( [ 'like', 'name_pptkis', $this->name_pptkis ] )
		      ->andFilterWhere( [ 'like', 'sipptki', $this->sipptki ] )
		      ->andFilterWhere( [ 'like', 'address_agent', $this->address_agent ] )
		      ->andFilterWhere( [ 'like', 'nscc_company.name', $this->companyName ] )
		      ->andFilterWhere( [ 'like', 'other_information', $this->other_information ] );
		$query->addOrderBy( [ 'created_at' => SORT_ASC ] );
		return $dataProvider;
	}
}

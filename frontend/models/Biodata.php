<?php
namespace app\models;
use app\models\base\BiodataBase;
use DateInterval;
use DateTime;
/**
 *
 * @property \app\models\Medikal|null $lastExpiredMedikal
 * @property mixed $lastFwcms
 * @property string $labelSex
 * @property string $totalPotongan
 * @property string $lamaPotongan
 * @property mixed $malays
 * @property mixed $lastExpiredRekomid
 * @property mixed $lastIsc
 * @property mixed $lastPaspor
 * @property mixed $indos
 * @property string $berkasDiambil
 * @property null $detailsPotongan
 * @property mixed $age
 */
class Biodata extends BiodataBase {
	/**
	 * @return Medikal|null
	 */
	public function getLastExpiredMedikal() {
		return Medikal::find()->where( [
			'biodata_id' => $this->biodata_id
		] )->orderBy( [ 'expired' => SORT_DESC ] )->one();
	}
	public function getLastExpiredRekomid() {
		return Rekomid::find()->where( [
			'biodata_id' => $this->biodata_id
		] )->orderBy( 'tgl DESC' )->one();
	}
	public function getLastIsc() {
		return Isc::find()->where( [
			'biodata_id' => $this->biodata_id
		] )->orderBy( 'expired DESC' )->one();
	}
	public function getLastFwcms() {
		return Fwcms::find()->where( [
			'biodata_id' => $this->biodata_id
		] )->orderBy( 'expired DESC' )->one();
	}
	public function getLastPaspor() {
		return Paspor::find()->where( [
			'biodata_id' => $this->biodata_id
		] )->orderBy( 'tgl DESC' )->one();
	}
	public static function printDataSession( $attribute ) {
		if ( isset( $_SESSION[ "BIODATA" ] ) && $_SESSION[ "BIODATA" ] != null ) {
			$biodata = Biodata::find()->where(
				"biodata_id = :biodata_id", [ ':biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
			return $biodata->getAttribute( $attribute );
		}
		return '';
	}
	public function afterSave( $insert, $changedAttributes ) {
		$city = Kabupaten::find()
		                 ->where( [ 'nama' => $this->kabupaten ] )
		                 ->one();
		if ( $city == null ) {
			$kab = new Location();
			$kab->createKabupaten( $this->kabupaten );
		}
		$country = Kecamatan::find()
		                    ->where( [ 'nama' => $this->kecamatan ] )
		                    ->one();
		if ( $country == null ) {
			$neg = new Location();
			$neg->createKecamatan( $this->kecamatan );
		}
		$country = Provinsi::find()
		                   ->where( [ 'nama' => $this->propinsi ] )
		                   ->one();
		if ( $country == null ) {
			$neg = new Location();
			$neg->createProvinsi( $this->propinsi );
		}
//		$rec = Location::find()
//		                   ->where( [
//			                   'tipe' => Location::RECRUITER_TYPE,
//			                   'nama' => $this->note_
//		                   ] )
//		                   ->one();
//		if ( $rec == null ) {
//			$neg = new Location();
//			$neg->createRecruiter( $this->note_ );
//		}
//		if ( array_key_exists( 'job_order_id', $changedAttributes ) ) {
//			$job = JobOrder::findOne( $this->job_order_id );
//			if ( $job != null ) {
//				if ( $job->qouta <= $job->getReadyNumber() ) {
//					throw new \Exception( 'Qouta untuk Job Order ' . $job->approval_date . ' sudah penuh.' );
//				}
//			}
//		}
		parent::afterSave( $insert, $changedAttributes ); // TODO: Change the autogenerated stub
	}
	public function getAge() {
		return date_diff( date_create( $this->birthdate ), date_create( 'now' ) )->y;
	}
	public function getLabelSex() {
		if ( $this->sex != null ) {
			return $this->sex == 'F' ? 'FEMALE' : 'MALE';
		}
		return '';
	}
//	public function getCabangName() {
//		if ( $this->cabang != null ) {
//			return $this->cabang->nama;
//		}
//		return '';
//	}
	public function getBerkasDiambil() {
		if ( $this->jobOrder != null ) {
			if ( $this->tiket_tgl != null ) {
				$pot = $this->jobOrder->lama_pot;
				if ( $pot == null ) {
					$pot = 0;
				}
				$pot += 1;
				try {
					$start       = new DateTime( $this->tiket_tgl );
					$month_later = clone $start;
					$month_later->add( new DateInterval( "P" . $pot . "M" ) );
					return $month_later->format( 'Y-m-d' );
				} catch ( \Exception $e ) {
					return $e->getMessage();
				}
			}
			return '';
		}
		return '';
	}
	public function getTotalPotongan() {
		if ( $this->jobOrder != null ) {
			return $this->jobOrder->jml_pot;
		}
		return '';
	}
	public function getLamaPotongan() {
		if ( $this->jobOrder != null ) {
			return $this->jobOrder->lama_pot;
		}
		return '';
	}
	public function getDetailsPotongan() {
		if ( $this->jobOrder != null ) {
			return ItemPot::find()->where( [
				'job_order_id' => $this->job_order_id
			] )->all();
		}
		return null;
	}
	public static function countTanpaJob() {
		return BiodataNojob::find()->count();
	}
	public static function countDenganJob() {
		return BiodataWithjob::find()->count();
	}
	public static function countResign() {
		return BiodataResign::find()->count();
	}
	public function getIndos() {
		return $this->getRiwayatKerjas()->where( [
			'tipe' => 0
		] )->all();
	}
	public function getMalays() {
		return $this->getRiwayatKerjas()->where( [
			'tipe' => 1
		] )->all();
	}

//    public function rules()
//    {
////        return parent::rules();
//        return $this->makeSafe(parent::rules());
//    }
}

<?php
namespace app\models\base;
use app\models\Biodata;
use app\models\RiwayatKerjaQuery;
use Yii;
/**
 * This is the model class for table "{{%riwayat_kerja}}".
 *
 * @property string $riwayat_kerja_id
 * @property int $tipe
 * @property string $nama_perusahaan Company Name
 * @property string $lokasi Location
 * @property string $posisi Position as
 * @property string $masa_kerja Working Period
 * @property string $biodata_id
 *
 * @property Biodata $biodata
 */
class RiwayatKerjaBase extends ModelBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%riwayat_kerja}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'biodata_id'], 'required'],
            [['tipe'], 'integer'],
            [[ 'biodata_id'], 'string', 'max' => 36],
            [['nama_perusahaan', 'lokasi', 'posisi', 'masa_kerja'], 'string', 'max' => 100],
//            [['riwayat_kerja_id'], 'unique'],
            [['biodata_id'], 'exist', 'skipOnError' => true, 'targetClass' => Biodata::className(), 'targetAttribute' => ['biodata_id' => 'biodata_id']],
	        [
		        [ 'nama_perusahaan', 'lokasi', 'posisi', 'masa_kerja' ],
		        'filter', 'filter' => 'strtoupper'
	        ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'riwayat_kerja_id' => Yii::t('app', 'Riwayat Kerja ID'),
            'tipe' => Yii::t('app', 'Tipe'),
            'nama_perusahaan' => Yii::t('app', 'Company Name'),
            'lokasi' => Yii::t('app', 'Location'),
            'posisi' => Yii::t('app', 'Position as'),
            'masa_kerja' => Yii::t('app', 'Working Period'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiodata()
    {
        return $this->hasOne(Biodata::className(), ['biodata_id' => 'biodata_id']);
    }
    /**
     * @inheritdoc
     * @return RiwayatKerjaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RiwayatKerjaQuery(get_called_class());
    }
}

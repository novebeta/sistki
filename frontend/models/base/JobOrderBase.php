<?php
namespace app\models\base;
use app\models\Company;
use app\models\ItemPot;
use app\models\JobOrderQuery;
use Yii;
/**
 * This is the model class for table "{{%job_order}}".
 *
 * @property string $job_order_id
 * @property string $job_title
 * @property string $no_register Register Number
 * @property string $approval_date KDN. Approval No/Date
 * @property string $levy_paid_receipt Levy Paid Receipt
 * @property int $qouta Total Number
 * @property string $age Age
 * @property string $sex Sex
 * @property string $education Education
 * @property string $expected_arrival Expected Arrival
 * @property string $durasi_kontrak Duration of Contract
 * @property string $working_hours Working hours/Week
 * @property string $basic_salary Basic Salary per Month
 * @property string $meal_allowance Meal Allowance
 * @property string $attendance_allowace Attendance Allowace
 * @property string $medical_allowance Medical Allowance
 * @property string $transport Transport
 * @property string $akomodasi Accommodation
 * @property string $return_air_ticket Return Air Ticket
 * @property string $bonus Bonus
 * @property string $draft_contract Draft Master Employment Contract
 * @property string $skill_allowance Skill Allowance
 * @property string $morning_shift Morning Shift
 * @property string $night_shift Night Shift
 * @property string $levy_plks_process Levy, PLKS & Process
 * @property string $payment_levy Payment of Levy
 * @property string $monthly_deduction Monthly Deduction
 * @property string $name_pptkis Name of PPTKIS
 * @property string $sipptki
 * @property string $address_agent
 * @property string $other_information Other Information
 * @property string $company_id
 * @property int $finish
 * @property string $tgl Date
 * @property int $lama_pot
 * @property int $disabled
 * @property string $jml_pot
 * @property int $closed
 *
 * @property Company $company
 * @property ItemPot[] $itemPot
 */
class JobOrderBase extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%job_order}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'job_order_id', 'job_title', 'tgl' ], 'required' ],
			[ [ 'qouta', 'finish', 'disabled', 'lama_pot', 'closed' ], 'integer' ],
			[ [ 'other_information', 'jml_pot' ], 'string' ],
			[ [ 'created_at', 'updated_at', 'tgl' ], 'safe' ],
			[ [ 'job_order_id' ], 'string', 'max' => 36 ],
			[
				[
					'no_register',
					'job_title',
					'approval_date',
					'levy_paid_receipt',
					'age',
					'sex',
					'education',
					'expected_arrival',
					'durasi_kontrak',
					'working_hours',
					'basic_salary',
					'meal_allowance',
					'attendance_allowace',
					'medical_allowance',
					'transport',
					'akomodasi',
					'return_air_ticket',
					'bonus',
					'draft_contract',
					'skill_allowance',
					'morning_shift',
					'night_shift',
					'levy_plks_process',
					'payment_levy',
					'monthly_deduction',
					'name_pptkis',
					'sipptki',
					'address_agent',
					'job_title'
				],
				'string',
				'max' => 100
			],
			[ [ 'job_order_id' ], 'unique' ],
			[
				[ 'company_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Company::className(),
				'targetAttribute' => [ 'company_id' => 'company_id' ]
			],
			[
				[
					'other_information',
					'jml_pot',
					'no_register',
					'job_title',
					'approval_date',
					'levy_paid_receipt',
					'age',
					'sex',
					'education',
					'expected_arrival',
					'durasi_kontrak',
					'working_hours',
					'basic_salary',
					'meal_allowance',
					'attendance_allowace',
					'medical_allowance',
					'transport',
					'akomodasi',
					'return_air_ticket',
					'bonus',
					'draft_contract',
					'skill_allowance',
					'morning_shift',
					'night_shift',
					'levy_plks_process',
					'payment_levy',
					'monthly_deduction',
					'name_pptkis',
					'sipptki',
					'address_agent',
					'job_title'
				],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'job_order_id'        => Yii::t( 'app', 'Job Order ID' ),
			'job_title'           => Yii::t( 'app', 'Job Title' ),
			'no_register'         => Yii::t( 'app', 'Register Number' ),
			'approval_date'       => Yii::t( 'app', 'KDN. Approval No' ),
			'levy_paid_receipt'   => Yii::t( 'app', 'Levy Paid Receipt' ),
			'qouta'               => Yii::t( 'app', 'Total Number' ),
			'age'                 => Yii::t( 'app', 'Age' ),
			'sex'                 => Yii::t( 'app', 'Sex' ),
			'education'           => Yii::t( 'app', 'Education' ),
			'expected_arrival'    => Yii::t( 'app', 'Expected Arrival' ),
			'durasi_kontrak'      => Yii::t( 'app', 'Duration of Contract' ),
			'working_hours'       => Yii::t( 'app', 'Working hours/Week' ),
			'basic_salary'        => Yii::t( 'app', 'Basic Salary per Month' ),
			'meal_allowance'      => Yii::t( 'app', 'Meal Allowance' ),
			'attendance_allowace' => Yii::t( 'app', 'Attendance Allowace' ),
			'medical_allowance'   => Yii::t( 'app', 'Medical Allowance' ),
			'transport'           => Yii::t( 'app', 'Transport' ),
			'akomodasi'           => Yii::t( 'app', 'Accommodation' ),
			'return_air_ticket'   => Yii::t( 'app', 'Return Air Ticket' ),
			'bonus'               => Yii::t( 'app', 'Bonus' ),
			'draft_contract'      => Yii::t( 'app', 'Draft Master Employment Contract' ),
			'skill_allowance'     => Yii::t( 'app', 'Skill Allowance' ),
			'morning_shift'       => Yii::t( 'app', 'Morning Shift' ),
			'night_shift'         => Yii::t( 'app', 'Night Shift' ),
			'levy_plks_process'   => Yii::t( 'app', 'Levy, PLKS & Process' ),
			'payment_levy'        => Yii::t( 'app', 'Payment of Levy' ),
			'monthly_deduction'   => Yii::t( 'app', 'Monthly Deduction' ),
			'name_pptkis'         => Yii::t( 'app', 'Name of P3MI' ),
			'sipptki'             => Yii::t( 'app', 'Sipptki' ),
			'address_agent'       => Yii::t( 'app', 'Address Agent' ),
			'other_information'   => Yii::t( 'app', 'Other Information' ),
			'company_id'          => Yii::t( 'app', 'Company ID' ),
			'finish'              => Yii::t( 'app', 'Finish' ),
			'totalNumberComplete' => Yii::t( 'app', 'Total Number' ),
			'tgl'                 => Yii::t( 'app', 'Date' ),
			'jml_pot'             => Yii::t( 'app', 'Total Potongan' ),
			'lama_pot'            => Yii::t( 'app', 'Lama Potongan' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCompany() {
		return $this->hasOne( Company::className(), [ 'company_id' => 'company_id' ] );
	}
	public function getCompanyName() {
		if ( $this->company == null ) {
			return '';
		}
		return $this->company->name;
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getItemPot() {
		return $this->hasMany( ItemPot::className(), [ 'job_order_id' => 'job_order_id' ] );
	}
	/**
	 * @inheritdoc
	 * @return JobOrderQuery the active query used by this AR class.
	 */
	public static function find() {
		return new JobOrderQuery( get_called_class() );
	}
}

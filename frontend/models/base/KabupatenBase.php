<?php
namespace app\models\base;
use app\models\KabupatenQuery;
use app\models\Location;
use Yii;
/**
 * This is the model class for table "{{%kabupaten}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class KabupatenBase extends Location
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kabupaten}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['location_id'], 'required'],
//            [['location_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 100],
	        [ 'nama', 'filter', 'filter' => 'strtoupper' ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id' => Yii::t('app', 'Location ID'),
            'nama' => Yii::t('app', 'Nama'),
        ];
    }
    /**
     * @inheritdoc
     * @return KabupatenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KabupatenQuery(get_called_class());
    }
}

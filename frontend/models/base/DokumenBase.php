<?php
namespace app\models\base;
use app\models\Biodata;
use app\models\Cabang;
use app\models\DokumenQuery;
use Yii;
/**
 * This is the model class for table "{{%dokumen}}".
 *
 * @property string $dokumen_id
 * @property string $tgl
 * @property string $penerima Penerima Berkas
 * @property string $note Keterangan
 * @property string $created_at
 * @property string $jenis_dokumen
 * @property int $tipe 1 masuk -1 keluar
 * @property string $grup
 *
 * @property string $cabang_id
 * @property string $biodata_id
 *
 * @property Cabang $cabang
 * @property Biodata $biodata
 */
class DokumenBase extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%dokumen}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'dokumen_id', 'tgl', 'biodata_id', 'cabang_id' ], 'required' ],
			[ [ 'tgl', 'created_at' ], 'safe' ],
			[ [ 'dokumen_id', 'biodata_id', 'cabang_id', 'grup' ], 'string', 'max' => 36 ],
			[ [ 'penerima' ], 'string', 'max' => 100 ],
			[ [ 'tipe' ], 'string', 'max' => 4 ],
			[ [ 'jenis_dokumen' ], 'string', 'max' => 50 ],
			[ [ 'note' ], 'string', 'max' => 600 ],
			[ [ 'dokumen_id' ], 'unique' ],
			[
				[ 'cabang_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Cabang::className(),
				'targetAttribute' => [ 'cabang_id' => 'cabang_id' ]
			],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'dokumen_id'    => Yii::t( 'app', 'Dokumen ID' ),
			'tgl'           => Yii::t( 'app', 'Tgl' ),
			'penerima'      => Yii::t( 'app', 'Penerima Berkas' ),
			'note'          => Yii::t( 'app', 'Keterangan' ),
			'created_at'    => Yii::t( 'app', 'Created At' ),
			'biodata_id'    => Yii::t( 'app', 'Biodata ID' ),
			'name'          => Yii::t( 'app', 'Biodata Name' ),
			'cabang_id'     => Yii::t( 'app', 'Cabang ID' ),
			'jenis_dokumen' => Yii::t( 'app', 'Jenis Dokumen' ),
			'grup'          => Yii::t( 'app', 'Grup' )
		];
	}
	/**
	 * @inheritdoc
	 * @return DokumenQuery the active query used by this AR class.
	 */
	public static function find() {
		return new DokumenQuery( get_called_class() );
	}
}

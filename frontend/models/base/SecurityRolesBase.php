<?php
namespace app\models\base;
use Yii;
/**
 * This is the model class for table "{{%security_roles}}".
 *
 * @property string $security_roles_id
 * @property string $role
 * @property string $ket
 * @property string $sections
 * @property int $up
 *
 * @property Users[] $users
 */
class SecurityRolesBase extends ModelBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%security_roles}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['security_roles_id', 'role'], 'required'],
            [['sections'], 'string'],
            [['up'], 'integer'],
            [['security_roles_id'], 'string', 'max' => 36],
            [['role'], 'string', 'max' => 20],
            [['ket'], 'string', 'max' => 255],
            [['security_roles_id'], 'unique'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'security_roles_id' => Yii::t('app', 'Security Roles ID'),
            'role' => Yii::t('app', 'Role'),
            'ket' => Yii::t('app', 'Ket'),
            'sections' => Yii::t('app', 'Sections'),
            'up' => Yii::t('app', 'Up'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['security_roles_id' => 'security_roles_id']);
    }
    /**
     * @inheritdoc
     * @return SecurityRolesQueryBase the active query used by this AR class.
     */
    public static function find()
    {
        return new SecurityRolesQueryBase(get_called_class());
    }
}

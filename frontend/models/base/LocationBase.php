<?php
namespace app\models\base;
use app\models\LocationQuery;
use Yii;
/**
 * This is the model class for table "{{%location}}".
 *
 * @property string $location_id
 * @property string $tipe Kecamatan, Kabupaten, Propinsi, Negara
 * @property string $nama
 */
class LocationBase extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%location}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
//            [['location_id', 'tipe'], 'required'],
//            [['location_id'], 'string', 'max' => 36],
			[ [ 'tipe' ], 'string', 'max' => 3 ],
			[ [ 'nama' ], 'string', 'max' => 100 ],
//            [['location_id'], 'unique'],
			[ 'nama', 'filter', 'filter' => 'strtoupper' ],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'location_id' => Yii::t( 'app', 'Location ID' ),
			'tipe'        => Yii::t( 'app', 'Kecamatan, Kabupaten, Propinsi, Negara' ),
			'nama'        => Yii::t( 'app', 'Nama' ),
		];
	}
	/**
	 * @inheritdoc
	 * @return LocationQuery the active query used by this AR class.
	 */
	public static function find() {
		return new LocationQuery( get_called_class() );
	}
}

<?php
namespace app\models\base;
use app\models\NegaraQuery;
use Yii;
/**
 * This is the model class for table "{{%negara}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class NegaraBase extends ModelBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%negara}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['location_id'], 'required'],
//            [['location_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 100],
	        [ 'nama', 'filter', 'filter' => 'strtoupper' ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id' => Yii::t('app', 'Location ID'),
            'nama' => Yii::t('app', 'Nama'),
        ];
    }
    /**
     * @inheritdoc
     * @return NegaraQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NegaraQuery(get_called_class());
    }
}

<?php
namespace app\models\base;
use app\models\CompanyQuery;
use Yii;
/**
 * This is the model class for table "{{%company}}".
 *
 * @property string $company_id
 * @property string $name Name
 * @property string $bisnis Line of Business
 * @property string $tgl Date of Establihment
 * @property string $address Location of Job Site
 * @property string $city City
 * @property string $country Country
 * @property string $no_license No. License
 * @property string $phone No. Telp
 * @property string $employees Total Employees
 * @property string $employee_ind Indonesian Employee
 */
class CompanyBase extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%company}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'name' ], 'required' ],
			[ [ 'tgl' ], 'safe' ],
//            [['company_id'], 'string', 'max' => 36],
			[
				[ 'name', 'bisnis', 'address', 'city', 'country', 'no_license', 'employees', 'employee_ind' ],
				'string',
				'max' => 100
			],
			[ [ 'phone' ], 'string', 'max' => 50 ],
//            [['company_id'], 'unique'],
			[
				[ 'name', 'bisnis', 'address', 'city', 'country', 'no_license', 'employees', 'employee_ind' ],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'company_id'   => Yii::t( 'app', 'Company ID' ),
			'name'         => Yii::t( 'app', 'Name' ),
			'bisnis'       => Yii::t( 'app', 'Line of Business' ),
			'tgl'          => Yii::t( 'app', 'Date of Establihment' ),
			'address'      => Yii::t( 'app', 'Address' ),
			'city'         => Yii::t( 'app', 'City' ),
			'country'      => Yii::t( 'app', 'Country' ),
			'no_license'   => Yii::t( 'app', 'ID. Company' ),
			'phone'        => Yii::t( 'app', 'No. Telp' ),
			'employees'    => Yii::t( 'app', 'Total Employees' ),
			'employee_ind' => Yii::t( 'app', 'Indonesian Employee' ),
		];
	}
	/**
	 * @inheritdoc
	 * @return CompanyQuery the active query used by this AR class.
	 */
	public static function find() {
		return new CompanyQuery( get_called_class() );
	}
}

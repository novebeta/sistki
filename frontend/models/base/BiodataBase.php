<?php
namespace app\models\base;
use app\models\Bayar;
use app\models\BiodataQuery;
use app\models\Cabang;
use app\models\Company;
use app\models\Dokumen;
use app\models\DokumenUpload;
use app\models\Fwcms;
use app\models\Isc;
use app\models\JobOrder;
use app\models\Medikal;
use app\models\MedikalLastexpired;
use app\models\Paspor;
use app\models\Rekomid;
use app\models\RekomPaspor;
use app\models\RiwayatKerja;
use app\modules\admin\models\User;
use Yii;
/**
 * This is the model class for table "{{%biodata}}".
 *
 * @property string $biodata_id
 * @property string $name Full Name
 * @property string $no_hp
 * @property string $address
 * @property string $kecamatan Kecamatan
 * @property string $kabupaten Kabupaten
 * @property string $propinsi
 * @property string $birthdate
 * @property string $tempat_lahir Place of birth
 * @property string $agama Religion
 * @property double $tb Height
 * @property double $bb Weight
 * @property string $suku Race
 * @property string $ayah Father Name
 * @property string $nikah Marital Status
 * @property string $pendidikan Education Graduated
 * @property string $sekolah Name of school
 * @property string $jurusan Majoring
 * @property string $lulus Year of graduated
 * @property int $punya_sodara Do you have a friend / family in Malaysia
 * @property string $alasan Spesify the reason to apply job in Malaysia
 * @property int $siap_kontrak Able to be bound by working contract for 2 years not go home
 * @property string $siap_kerja Able to working
 * @property string $path_foto
 * @property string $remark Remark
 * @property string $pl No. PL
 * @property string $status_proses Status
 * @property string $no_paspor No. Paspor
 * @property string $kode_rekomid Rekom ID
 * @property string $no_visa No. Visa
 * @property string $tgl_visa Visa Date
 * @property int $endorse Endorse
 * @property int $asuransi Asuransi
 * @property string $pap_tgl Jadwal Seminar
 * @property string $pap_note Note
 * @property string $tiket_tgl Tgl Tiket
 * @property string $tiket_note Note
 * @property string $no_pk No. PK
 * @property string $cabang_id
 * @property int $siap_ikut_aturan Able to follow all of rules imposed by entire company management
 * @property string $fex F/EX
 * @property string $job_order_id
 * @property string $companyName
 * @property string $company
 * @property string $apply_visa
 * @property string $kasus
 * @property string $note_kasus
 * @property int $resign
 * @property string $note_
 * @property string $resign_note
 * @property string $loan
 * @property string $loan_note
 * @property int $tuser
 * @property string $company_date
 *
 * @property Bayar[] $bayars
 * @property Cabang $cabang
 * @property JobOrder $jobOrder
 * @property Dokumen[] $dokumens
 * @property DokumenUpload[] $dokumenUploads
 * @property Fwcms[] $fwcms
 * @property Isc[] $iscs
 * @property Medikal[] $medikals
 * @property MedikalLastexpired $medikalLastexpired
 * @property Paspor $paspor
 * @property RekomPaspor $rekomPaspor
 * @property User $user
 * @property Rekomid[] $rekoms
 * @property string $companyCountry
 * @property string $jobOrderRegister
 * @property RiwayatKerja[] $riwayatIndo
 * @property array $siapkerja
 * @property RiwayatKerja[] $riwayatMalay
 * @property string $companyCity
 * @property RiwayatKerja[] $riwayatKerjas
 */
class BiodataBase extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%biodata}}';
	}
	/**
	 * @inheritdoc
	 * @return BiodataQuery the active query used by this AR class.
	 */
	public static function find() {
		return new BiodataQuery( get_called_class() );
	}
	/**
	 * @return array
	 */
	public function getSiapkerja(): array {
		return explode( ',', $this->siap_kerja );
	}
	/**
	 * @param array $siap_kerja
	 */
	public function setSiapkerja( $siap_kerja ) {
		if ( is_array( $siap_kerja ) ) {
			$this->siap_kerja = implode( ',', $siap_kerja );
		}
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'name', 'cabang_id' ], 'required' ],
			[
				'resign_note',
				'required',
				'when'       => function ( $model ) {
					return $model->resign == 1;
				},
				'whenClient' => "function (attribute, value) {
                    return $('#biodata-resign').is(':checked');
				}"
			],
			[
				[
					'birthdate',
					'tgl_visa',
					'apply_visa',
					'pap_tgl',
					'tiket_tgl',
					'job_order_id',
					'note_',
					'siapkerja',
					'company_date'
				],
				'safe'
			],
			[ [ 'tb', 'bb', 'biaya_copvisa', 'loan' ], 'number' ],
			[
				[
					'agama',
					'formal',
					'nikah',
					'pendidikan',
					'alasan',
					'siap_kerja',
					'remark',
					'status_proses',
					'pap_note',
					'tiket_note',
					'fex',
					'sex',
					'kasus',
					'note_kasus'
				],
				'string'
			],
			[
				[ 'punya_sodara', 'siap_kontrak', 'endorse', 'asuransi', 'siap_ikut_aturan', 'resign', 'tuser' ],
				'integer'
			],
			[ [ 'sekolah', 'jurusan', 'cabang_id', 'job_order_id' ], 'string', 'max' => 36 ],
			[
				[
					'name',
					'address',
					'kecamatan',
					'kabupaten',
					'propinsi',
					'suku',
					'ayah',
					'lulus',
					'pl',
					'no_paspor',
					'kode_rekomid',
					'no_visa',
					'no_pk',
					'tempat_lahir',
				],
				'string',
				'max' => 100
			],
			[ [ 'no_hp', 'nik' ], 'string', 'max' => 50 ],
			[ [ 'path_foto' ], 'string', 'max' => 300 ],
			[ [ 'no_absen' ], 'string', 'max' => 20 ],
			[ [ 'tipe_bayar_copvisa' ], 'string', 'max' => 10 ],
			[ [ 'resign_note', 'loan_note' ], 'string', 'max' => 600 ],
			[ [ 'biodata_id' ], 'unique' ],
			[
				[ 'cabang_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Cabang::className(),
				'targetAttribute' => [ 'cabang_id' => 'cabang_id' ]
			],
			[
				[ 'job_order_id' ],
				'availabelJobOrder',
				'skipOnEmpty' => true
			],
			[
				[
					'name',
					'address',
					'kecamatan',
					'kabupaten',
					'propinsi',
					'suku',
					'ayah',
					'lulus',
					'pl',
					'no_paspor',
					'kode_rekomid',
					'no_visa',
					'no_pk',
					'tempat_lahir',
					'sekolah',
					'jurusan',
					'agama',
					'formal',
					'nikah',
					'pendidikan',
					'alasan',
					'remark',
					'status_proses',
					'pap_note',
					'tiket_note',
					'fex',
					'sex',
					'kasus',
					'note_kasus',
					'no_hp',
					'nik',
					'no_absen',
					'tipe_bayar_copvisa',
					'resign_note',
					'loan_note'
				],
				'filter',
				'filter' => 'strtoupper'
			],
		];
	}
	public function availabelJobOrder( $attribute, $params, $validator ) {
		$job = JobOrder::findOne( $this->$attribute );
		if ( $job != null ) {
			$qouta = $job->qouta;
			$ready = $job->getReadyNumber();
			if ( $qouta <= $ready ) {
				$this->addError( $attribute, 'Qouta untuk Job Order ' . $job->approval_date . ' sudah penuh.' );
			}
		}
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'biodata_id'         => Yii::t( 'app', 'Biodata ID' ),
			'name'               => Yii::t( 'app', 'Full Name' ),
			'no_hp'              => Yii::t( 'app', 'No Hp' ),
			'address'            => Yii::t( 'app', 'Address' ),
			'kecamatan'          => Yii::t( 'app', 'Kecamatan' ),
			'kabupaten'          => Yii::t( 'app', 'Kabupaten' ),
			'propinsi'           => Yii::t( 'app', 'Propinsi' ),
			'birthdate'          => Yii::t( 'app', 'Birthdate' ),
			'tempat_lahir'       => Yii::t( 'app', 'Place of birth' ),
			'agama'              => Yii::t( 'app', 'Religion' ),
			'tb'                 => Yii::t( 'app', 'Height' ),
			'bb'                 => Yii::t( 'app', 'Weight' ),
			'suku'               => Yii::t( 'app', 'Race' ),
			'ayah'               => Yii::t( 'app', 'Father Name' ),
			'nikah'              => Yii::t( 'app', 'Marital Status' ),
			'pendidikan'         => Yii::t( 'app', 'Education Graduated' ),
			'sekolah'            => Yii::t( 'app', 'Name of school' ),
			'jurusan'            => Yii::t( 'app', 'Majoring' ),
			'lulus'              => Yii::t( 'app', 'Year of graduated' ),
			'punya_sodara'       => Yii::t( 'app', 'Do you have a friend / family in Malaysia' ),
			'alasan'             => Yii::t( 'app', 'Spesify the reason to apply job in Malaysia' ),
			'siap_kontrak'       => Yii::t( 'app', 'Able to be bound by working contract for 2 years not go home' ),
			'siap_kerja'         => Yii::t( 'app', 'Able to working' ),
			'siapkerja'          => Yii::t( 'app', 'Able to working' ),
			'path_foto'          => Yii::t( 'app', 'Path Foto' ),
			'remark'             => Yii::t( 'app', 'Remark' ),
			'pl'                 => Yii::t( 'app', 'Nama Cabang' ),
			'status_proses'      => Yii::t( 'app', 'Status' ),
			'no_paspor'          => Yii::t( 'app', 'No. Paspor' ),
			'kode_rekomid'       => Yii::t( 'app', 'Rekom ID' ),
			'no_visa'            => Yii::t( 'app', 'No. Visa' ),
			'tgl_visa'           => Yii::t( 'app', 'Visa Date' ),
			'endorse'            => Yii::t( 'app', 'Endorse' ),
			'asuransi'           => Yii::t( 'app', 'Asuransi' ),
			'pap_tgl'            => Yii::t( 'app', 'Pap Tgl' ),
			'pap_note'           => Yii::t( 'app', 'Pap Note' ),
			'tiket_tgl'          => Yii::t( 'app', 'Tiket Tgl' ),
			'tiket_note'         => Yii::t( 'app', 'Tiket Note' ),
			'no_pk'              => Yii::t( 'app', 'No. PK' ),
			'cabang_id'          => Yii::t( 'app', 'Cabang ID' ),
			'siap_ikut_aturan'   => Yii::t( 'app', 'Able to follow all of rules imposed by entire company management' ),
			'fex'                => Yii::t( 'app', 'F/EX' ),
			'job_order_id'       => Yii::t( 'app', 'Job Order ID' ),
			'job_order_register' => Yii::t( 'app', 'Job Order Register' ),
			'finish'             => Yii::t( 'app', 'Finish' ),
			'company_name'       => Yii::t( 'app', 'Company Name' ),
			'sex'                => Yii::t( 'app', 'Jenis Kelamin' ),
			'no_absen'           => Yii::t( 'app', 'No. Absensi' ),
			'nik'                => Yii::t( 'app', 'Nomor Induk Kependudukan' ),
			'apply_visa'         => Yii::t( 'app', 'Apply Visa' ),
			'kasus'              => Yii::t( 'app', 'Kasus' ),
			'note_kasus'         => Yii::t( 'app', 'Note Kasus' ),
			'resign'             => Yii::t( 'app', 'Resign' ),
			'formal'             => Yii::t( 'app', 'F/I' ),
			'biaya_copvisa'      => Yii::t( 'app', 'Biaya Copvisa' ),
			'tipe_bayar_copvisa' => Yii::t( 'app', 'Tipe Bayar Copvisa' ),
			'note_'              => Yii::t( 'app', 'Recruiter' ),
			'resign_note'        => Yii::t( 'app', 'Resign Note' ),
			'loan'               => Yii::t( 'app', 'Pinjaman' ),
			'loan_note'          => Yii::t( 'app', 'Keterangan' ),
			'tuser'              => Yii::t( 'app', 'User Name' ),
			'company_date'       => 'Company Date',
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBayars() {
		return $this->hasMany( Bayar::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDokumens() {
		return $this->hasMany( Dokumen::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDokumenUploads() {
		return $this->hasMany( DokumenUpload::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFwcms() {
		return $this->hasMany( Fwcms::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getIscs() {
		return $this->hasMany( Isc::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMedikals() {
		return $this->hasMany( Medikal::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMedikalLastexpired() {
		return $this->hasOne( MedikalLastexpired::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPaspor() {
		return $this->hasOne( Paspor::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRekomPaspor() {
		return $this->hasOne( RekomPaspor::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRekoms() {
		return $this->hasMany( Rekomid::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCabang() {
		return $this->hasOne( Cabang::className(), [ 'cabang_id' => 'cabang_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRiwayatKerjas() {
		return $this->hasMany( RiwayatKerja::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	public function getRiwayatIndo() {
		return $this->hasMany( RiwayatKerja::className(), [ 'biodata_id' => 'biodata_id' ] )->andOnCondition( [ 'tipe' => 0 ] );
	}
	public function getRiwayatMalay() {
		return $this->hasMany( RiwayatKerja::className(), [ 'biodata_id' => 'biodata_id' ] )->andOnCondition( [ 'tipe' => 1 ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getJobOrder() {
		return $this->hasOne( JobOrder::className(), [ 'job_order_id' => 'job_order_id' ] );
	}
	/* Getter for country name */
	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * ALTER TABLE `dutaw240_app`.`nscc_biodata` ADD COLUMN `tuser` int(11) NULL DEFAULT NULL AFTER `loan_note`;
	 *
	 */
	public function getUser() {
		return $this->hasOne( User::className(), [ 'id' => 'tuser' ] );
	}
	public function getJobOrderRegister() {
		if ( $this->jobOrder == null ) {
			return '';
		}
		return $this->jobOrder->no_register;
	}
//	public function getCompanyName() {
//		if ( $this->company == null ) {
//			return '';
//		}
//		return $this->company->name;
//	}
	public function getCompany() {
//		if ( $this->jobOrder != null ) {
		return $this->hasOne( Company::className(), [ 'company_id' => 'company_id' ] )
		            ->via( 'jobOrder' );
//		}
//		return $this->jobOrder;
	}
	public function getCompanyCity() {
		if ( $this->company == null ) {
			return '';
		}
		return $this->company->city;
	}
//	public function getMedikalCenter() {
//		if ( $this->medikalLastexpired == null ) {
//			return '';
//		}
//		return $this->medikalLastexpired->medikal_center;
//	}
	public function getCompanyCountry() {
		if ( $this->company == null ) {
			return '';
		}
		return $this->company->country;
	}
//	public function afterFind() {
//		parent::afterFind();
//		$this->loan = Yii::$app->formatter->asDecimal($this->loan,0);
//	}
}

<?php
namespace app\models\base;
use app\models\UsersQuery;
use Yii;
/**
 * This is the model class for table "{{%users}}".
 *
 * @property string $id id
 * @property string $user_id User Name
 * @property string $password Password
 * @property string $last_visit_date Last Visit
 * @property int $active Aktif
 * @property string $security_roles_id
 * @property string $name Name
 *
 * @property SecurityRoles $securityRoles
 */
class UsersBase extends ModelBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'security_roles_id'], 'required'],
            [['last_visit_date'], 'safe'],
            [['active'], 'integer'],
            [['id'], 'string', 'max' => 50],
            [['user_id'], 'string', 'max' => 60],
            [['password', 'name'], 'string', 'max' => 100],
            [['security_roles_id'], 'string', 'max' => 36],
            [['user_id'], 'unique'],
            [['id'], 'unique'],
            [['security_roles_id'], 'exist', 'skipOnError' => true, 'targetClass' => SecurityRoles::className(), 'targetAttribute' => ['security_roles_id' => 'security_roles_id']],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'password' => Yii::t('app', 'Password'),
            'last_visit_date' => Yii::t('app', 'Last Visit Date'),
            'active' => Yii::t('app', 'Active'),
            'security_roles_id' => Yii::t('app', 'Security Roles ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecurityRoles()
    {
        return $this->hasOne(SecurityRoles::className(), ['security_roles_id' => 'security_roles_id']);
    }
    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
}

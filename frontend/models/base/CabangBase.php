<?php
namespace app\models\base;
use app\models\Biodata;
use app\models\CabangQuery;
use Yii;
/**
 * This is the model class for table "{{%cabang}}".
 *
 * @property string $cabang_id
 * @property string $kode
 * @property string $nama
 *
 * @property Biodata[] $biodatas
 */
class CabangBase extends ModelBase {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%cabang}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
//            [['cabang_id'], 'required'],
//            [['cabang_id'], 'safe'],
//            [['cabang_id'], 'string', 'max' => 36],
			[ [ 'kode' ], 'string', 'max' => 50 ],
			[ [ 'nama' ], 'string', 'max' => 100 ],
			[
				[ 'nama' ],
				'filter', 'filter' => 'strtoupper'
			],
//            [['cabang_id'], 'unique'],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'cabang_id' => Yii::t( 'app', 'Cabang ID' ),
			'kode'      => Yii::t( 'app', 'Kode' ),
			'nama'      => Yii::t( 'app', 'Nama' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodatas() {
		return $this->hasMany( Biodata::className(), [ 'cabang_id' => 'cabang_id' ] );
	}
	/**
	 * @inheritdoc
	 * @return CabangQuery the active query used by this AR class.
	 */
	public static function find() {
		return new CabangQuery( get_called_class() );
	}
}

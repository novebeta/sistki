<?php
namespace app\models\base;
use app\models\RiwayatKerjaIndoQuery;
use Yii;
/**
 * This is the model class for table "{{%riwayat_kerja_indo}}".
 *
 * @property string $riwayat_kerja_id
 * @property string $nama_perusahaan Company Name
 * @property string $lokasi Location
 * @property string $posisi Position as
 * @property string $masa_kerja Working Period
 * @property string $biodata_id
 */
class RiwayatKerjaIndoBase extends ModelBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%riwayat_kerja_indo}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['biodata_id'], 'required'],
            [['biodata_id'], 'string', 'max' => 36],
            [['nama_perusahaan', 'lokasi', 'posisi', 'masa_kerja'], 'string', 'max' => 100],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'riwayat_kerja_id' => Yii::t('app', 'Riwayat Kerja ID'),
            'nama_perusahaan' => Yii::t('app', 'Company Name'),
            'lokasi' => Yii::t('app', 'Location'),
            'posisi' => Yii::t('app', 'Position as'),
            'masa_kerja' => Yii::t('app', 'Working Period'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
        ];
    }
    /**
     * @inheritdoc
     * @return RiwayatKerjaIndoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RiwayatKerjaIndoQuery(get_called_class());
    }
}

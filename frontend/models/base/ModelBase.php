<?php
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 2/27/18
 * Time: 9:28 PM
 */
namespace app\models\base;
use Yii;
use yii\helpers\ArrayHelper;
class ModelBase extends \yii\db\ActiveRecord {
//    public function behaviors()
//    {
//        $schema = self::getTableSchema();
//        $key = $schema->primaryKey;
//        return [
//            [
//                'class' => AttributeBehavior::className(),
//                'attributes' => [
//                    ActiveRecord::EVENT_BEFORE_INSERT => $key,
//                ],
//                'value' => function ($event) {
//                    return self::generate_uuid();
//                },
//            ],
//        ];
//    }
//	public function init() {
////		$this->scenario = 'upper';
//		parent::init();
//	}
	public static function generate_uuid() {
		$command = Yii::$app->getDb()
		                    ->createCommand( "SELECT UUID();" );
		return $command->queryScalar();
	}
	public function makeSafe( $arr1 ) {
//        $arr1 = $this->rules();
		$schema  = self::getTableSchema();
		$key     = $schema->primaryKey;
		$arr1ref = $arr1;
		recursive_unset( $arr1ref, $key[0] );
		$arr2  = [
			[ [ $key[0] ], 'safe' ]
		];
		$hasil = ArrayHelper::merge( $arr1ref, $arr2 );
		return $hasil;
	}
}
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%job_order_dashboard}}".
 *
 * @property string $tgl Date
 * @property string $name Name
 * @property int $qouta Total Number
 * @property int $jml
 * @property int $closed
 * @property string $Total
 */
class JobOrderDashboard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%job_order_dashboard}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl', 'name'], 'required'],
            [['tgl'], 'safe'],
            [['qouta', 'jml', 'closed'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['Total'], 'string', 'max' => 33],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tgl' => 'Tgl',
            'name' => 'Name',
            'qouta' => 'Qouta',
            'jml' => 'Jml',
            'closed' => 'Closed',
            'Total' => 'Total',
        ];
    }

    /**
     * {@inheritdoc}
     * @return JobOrderDashboardQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JobOrderDashboardQuery(get_called_class());
    }
}

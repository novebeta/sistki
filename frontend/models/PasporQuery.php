<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Paspor]].
 *
 * @see Paspor
 */
class PasporQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Paspor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Paspor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

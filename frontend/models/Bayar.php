<?php
namespace app\models;
use app\models\base\ModelBase;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%bayar}}".
 *
 * @property string $bayar_id
 * @property int $tipe_trans
 * @property string $amount Amount
 * @property int $loan Loan
 * @property string $voucher Voucher
 * @property string $note_ Note
 * @property string $biodata_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $parent_id
 *
 * @property Biodata $biodata
 */
class Bayar extends ModelBase {
	const MEDIKAL = '0';
	const PASPOR = '1';
	const FWCMS = '2';
	const ISC = '3';
	const COPVISA = '4';
	public function init() {
		// no search method is available in Gii generated Non search class
		if ( ! method_exists( $this, 'search' ) ) {
			$this->amount = 0;
		}
		parent::init();
	}
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value'              => new Expression( 'NOW()' ),
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%bayar}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'bayar_id', 'tipe_trans', 'biodata_id' ], 'required' ],
			[ [ 'amount' ], 'number' ],
			[ [ 'created_at', 'updated_at' ], 'safe' ],
			[ [ 'bayar_id', 'biodata_id', 'parent_id' ], 'string', 'max' => 36 ],
			[ [ 'loan' ], 'string', 'max' => 4 ],
			[ [ 'voucher' ], 'string', 'max' => 25 ],
			[ [ 'note_' ], 'string', 'max' => 600 ],
			[ [ 'bayar_id' ], 'unique' ],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
			[
				[ 'voucher', 'note_' ],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'bayar_id'   => Yii::t( 'app', 'Bayar ID' ),
			'tipe_trans' => Yii::t( 'app', 'Tipe Trans' ),
			'amount'     => Yii::t( 'app', 'Amount' ),
			'loan'       => Yii::t( 'app', 'Loan' ),
			'voucher'    => Yii::t( 'app', 'Voucher' ),
			'note_'      => Yii::t( 'app', 'Note' ),
			'biodata_id' => Yii::t( 'app', 'Biodata ID' ),
			'created_at' => Yii::t( 'app', 'Created At' ),
			'updated_at' => Yii::t( 'app', 'Updated At' ),
			'parent_id'  => Yii::t( 'app', 'Parent ID' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @inheritdoc
	 * @return BayarQuery the active query used by this AR class.
	 */
	public static function find() {
		return new BayarQuery( get_called_class() );
	}
	public function beforeValidate() {
		$this->amount = str_replace( 'Rp', '', str_replace( '.', '', $this->amount ) );
		return parent::beforeValidate();
	}
//	public function afterFind() {
//		parent::afterFind();
//		$this->amount = Yii::$app->formatter->asDecimal($this->amount,0);
//	}
}

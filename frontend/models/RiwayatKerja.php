<?php
namespace app\models;
use app\models\base\RiwayatKerjaBase;
/**
 * This is the model class for table "{{%riwayat_kerja}}".
 *
 * @property string $riwayat_kerja_id
 * @property int $tipe
 * @property string $nama_perusahaan Company Name
 * @property string $lokasi Location
 * @property string $posisi Position as
 * @property string $masa_kerja Working Period
 * @property string $biodata_id
 *
 * @property Biodata $biodata
 */
class RiwayatKerja extends RiwayatKerjaBase
{

}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MedicalCenter]].
 *
 * @see MedicalCenter
 */
class MedicalCenterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MedicalCenter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MedicalCenter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

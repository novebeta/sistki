<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Rekomid]].
 *
 * @see Rekomid
 */
class RekomidQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Rekomid[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Rekomid|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

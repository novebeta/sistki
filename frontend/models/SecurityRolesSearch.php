<?php
namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * SecurityRolesSearch represents the model behind the search form of `app\models\SecurityRoles`.
 */
class SecurityRolesSearch extends SecurityRoles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['security_roles_id', 'role', 'ket', 'sections'], 'safe'],
            [['up'], 'integer'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SecurityRoles::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'up' => $this->up,
        ]);
        $query->andFilterWhere(['like', 'security_roles_id', $this->security_roles_id])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'ket', $this->ket])
            ->andFilterWhere(['like', 'sections', $this->sections]);
        return $dataProvider;
    }
}

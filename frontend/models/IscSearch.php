<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * IscSearch represents the model behind the search form of `app\models\Isc`.
 */
class IscSearch extends Isc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isc_id', 'tgl', 'rbb_duta', 'pic', 'note_', 'biodata_id', 'created_at',  'kode_bayar', 'expired'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Isc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'created_at' => $this->created_at,
            'expired' => $this->expired,
        ]);

        $query->andFilterWhere(['like', 'isc_id', $this->isc_id])
            ->andFilterWhere(['like', 'rbb_duta', $this->rbb_duta])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'note_', $this->note_])
            ->andFilterWhere(['like', 'biodata_id', $this->biodata_id])
            ->andFilterWhere(['like', 'kode_bayar', $this->kode_bayar]);

        return $dataProvider;
    }
}

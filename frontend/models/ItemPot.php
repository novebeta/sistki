<?php

namespace app\models;

use app\models\base\ModelBase;
use Yii;

/**
 * This is the model class for table "{{%item_pot}}".
 *
 * @property string $item_pot_id
 * @property string $job_order_id
 * @property string $bulan
 * @property string $pot
 */
class ItemPot extends ModelBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%item_pot}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_pot_id', 'job_order_id'], 'required'],
            [['item_pot_id', 'job_order_id'], 'string', 'max' => 36],
            [['bulan', 'pot'], 'string', 'max' => 255],
            [['item_pot_id'], 'unique'],
	        [
		        [ 'bulan', 'pot' ],
		        'filter', 'filter' => 'strtoupper'
	        ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_pot_id' => Yii::t('app', 'Item Pot ID'),
            'job_order_id' => Yii::t('app', 'Job Order ID'),
            'bulan' => Yii::t('app', 'Bulan'),
            'pot' => Yii::t('app', 'Pot'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ItemPotQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ItemPotQuery(get_called_class());
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%biodata_resign}}".
 *
 * @property string $biodata_id
 * @property string $name Full Name
 * @property string $no_hp
 * @property string $address
 * @property string $kecamatan Kecamatan
 * @property string $kabupaten Kabupaten
 * @property string $propinsi
 * @property string $birthdate
 * @property string $tempat_lahir
 * @property string $agama
 * @property double $tb Height
 * @property double $bb Weight
 * @property string $suku Race
 * @property string $ayah Father Name
 * @property string $nikah Marital Status
 * @property string $pendidikan Education Graduated
 * @property string $sekolah Name of school
 * @property string $jurusan Majoring
 * @property string $lulus Year of graduated
 * @property int $punya_sodara Do you have a friend / family in Malaysia
 * @property string $alasan Spesify the reason to apply job in Malaysia
 * @property int $siap_kontrak Able to be bound by working contract for 2 years not go home
 * @property string $siap_kerja Able to working
 * @property string $path_foto
 * @property string $remark Remark
 * @property string $pl No. PL
 * @property string $status_proses Status
 * @property string $no_paspor No. Paspor
 * @property string $kode_rekomid Rekom ID
 * @property string $no_visa No. Visa
 * @property string $tgl_visa Visa Date
 * @property int $endorse Endorse
 * @property int $asuransi Asuransi
 * @property string $pap_tgl Jadwal Seminar
 * @property string $pap_note Note
 * @property string $tiket_tgl Tgl Tiket
 * @property string $tiket_note Note
 * @property string $no_pk No. PK
 * @property string $cabang_id
 * @property int $siap_ikut_aturan Able to follow all of rules imposed by entire company management
 * @property string $fex F/EX
 * @property string $job_order_id
 * @property string $sex Sex
 * @property string $no_absen No. Absensi
 * @property string $nik Nomor Induk Kependudukan
 * @property string $apply_visa
 * @property string $kasus
 * @property string $note_kasus
 * @property int $resign
 * @property string $tdate
 * @property string $formal
 * @property string $biaya_copvisa
 * @property string $tipe_bayar_copvisa
 * @property string $note_
 * @property string $resign_note
 */
class BiodataResign extends \yii\db\ActiveRecord
{
	public static function primaryKey() {
		return [ 'biodata_id' ];
	}
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%biodata_resign}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['biodata_id', 'name', 'cabang_id', 'fex', 'job_order_id'], 'required'],
            [['birthdate', 'tgl_visa', 'pap_tgl', 'tiket_tgl', 'apply_visa', 'tdate'], 'safe'],
            [['agama', 'nikah', 'pendidikan', 'alasan', 'remark', 'status_proses', 'pap_note', 'tiket_note', 'fex', 'sex', 'kasus', 'note_kasus', 'formal', 'note_'], 'string'],
            [['tb', 'bb', 'biaya_copvisa'], 'number'],
            [['punya_sodara', 'siap_kontrak', 'endorse', 'asuransi', 'siap_ikut_aturan', 'resign'], 'integer'],
            [['biodata_id', 'sekolah', 'jurusan', 'cabang_id', 'job_order_id'], 'string', 'max' => 36],
            [['name', 'address', 'kecamatan', 'kabupaten', 'propinsi', 'tempat_lahir', 'suku', 'ayah', 'lulus', 'pl', 'no_paspor', 'kode_rekomid', 'no_visa', 'no_pk'], 'string', 'max' => 100],
            [['no_hp', 'nik'], 'string', 'max' => 50],
            [['siap_kerja'], 'string', 'max' => 255],
            [['path_foto'], 'string', 'max' => 300],
            [['no_absen'], 'string', 'max' => 20],
            [['tipe_bayar_copvisa'], 'string', 'max' => 10],
            [['resign_note'], 'string', 'max' => 600],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'biodata_id' => Yii::t('app', 'Biodata ID'),
            'name' => Yii::t('app', 'Full Name'),
            'no_hp' => Yii::t('app', 'No Hp'),
            'address' => Yii::t('app', 'Address'),
            'kecamatan' => Yii::t('app', 'Kecamatan'),
            'kabupaten' => Yii::t('app', 'Kabupaten'),
            'propinsi' => Yii::t('app', 'Propinsi'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'tempat_lahir' => Yii::t('app', 'Tempat Lahir'),
            'agama' => Yii::t('app', 'Agama'),
            'tb' => Yii::t('app', 'Height'),
            'bb' => Yii::t('app', 'Weight'),
            'suku' => Yii::t('app', 'Race'),
            'ayah' => Yii::t('app', 'Father Name'),
            'nikah' => Yii::t('app', 'Marital Status'),
            'pendidikan' => Yii::t('app', 'Education Graduated'),
            'sekolah' => Yii::t('app', 'Name of school'),
            'jurusan' => Yii::t('app', 'Majoring'),
            'lulus' => Yii::t('app', 'Year of graduated'),
            'punya_sodara' => Yii::t('app', 'Do you have a friend / family in Malaysia'),
            'alasan' => Yii::t('app', 'Spesify the reason to apply job in Malaysia'),
            'siap_kontrak' => Yii::t('app', 'Able to be bound by working contract for 2 years not go home'),
            'siap_kerja' => Yii::t('app', 'Able to working'),
            'path_foto' => Yii::t('app', 'Path Foto'),
            'remark' => Yii::t('app', 'Remark'),
            'pl' => Yii::t('app', 'No. PL'),
            'status_proses' => Yii::t('app', 'Status'),
            'no_paspor' => Yii::t('app', 'No. Paspor'),
            'kode_rekomid' => Yii::t('app', 'Rekom ID'),
            'no_visa' => Yii::t('app', 'No. Visa'),
            'tgl_visa' => Yii::t('app', 'Visa Date'),
            'endorse' => Yii::t('app', 'Endorse'),
            'asuransi' => Yii::t('app', 'Asuransi'),
            'pap_tgl' => Yii::t('app', 'Jadwal Seminar'),
            'pap_note' => Yii::t('app', 'Note'),
            'tiket_tgl' => Yii::t('app', 'Tgl Tiket'),
            'tiket_note' => Yii::t('app', 'Note'),
            'no_pk' => Yii::t('app', 'No. PK'),
            'cabang_id' => Yii::t('app', 'Cabang ID'),
            'siap_ikut_aturan' => Yii::t('app', 'Able to follow all of rules imposed by entire company management'),
            'fex' => Yii::t('app', 'F/EX'),
            'job_order_id' => Yii::t('app', 'Job Order ID'),
            'sex' => Yii::t('app', 'Sex'),
            'no_absen' => Yii::t('app', 'No. Absensi'),
            'nik' => Yii::t('app', 'Nomor Induk Kependudukan'),
            'apply_visa' => Yii::t('app', 'Apply Visa'),
            'kasus' => Yii::t('app', 'Kasus'),
            'note_kasus' => Yii::t('app', 'Note Kasus'),
            'resign' => Yii::t('app', 'Resign'),
            'tdate' => Yii::t('app', 'Tdate'),
            'formal' => Yii::t('app', 'Formal'),
            'biaya_copvisa' => Yii::t('app', 'Biaya Copvisa'),
            'tipe_bayar_copvisa' => Yii::t('app', 'Tipe Bayar Copvisa'),
            'note_' => Yii::t('app', 'Note'),
            'resign_note' => Yii::t('app', 'Resign Note'),
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMedikalLastexpired() {
		return $this->hasOne( MedikalLastexpired::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCabang() {
		return $this->hasOne( Cabang::className(), [ 'cabang_id' => 'cabang_id' ] );
	}
	public function getCompanyName() {
		if ( $this->company == null ) {
			return '';
		}
		return $this->company->name;
	}
	public function getCompany() {
//		if ( $this->jobOrder != null ) {
		return $this->hasOne( Company::className(), [ 'company_id' => 'company_id' ] )
		            ->via( 'jobOrder' );
//		}
//		return $this->jobOrder;
	}
	public function getCabangName() {
		if ( $this->cabang != null ) {
			return $this->cabang->nama;
		}
		return '';
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getJobOrder() {
		return $this->hasOne( JobOrder::className(), [ 'job_order_id' => 'job_order_id' ] );
	}
	public function getLabelSex() {
		if ( $this->sex != null ) {
			return $this->sex == 'F' ? 'FEMALE' : 'MALE';
		}
		return '';
	}

    /**
     * {@inheritdoc}
     * @return BiodataResignQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BiodataResignQuery(get_called_class());
    }
}

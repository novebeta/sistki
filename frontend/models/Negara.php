<?php
namespace app\models;
use app\models\base\NegaraBase;
/**
 * This is the model class for table "{{%negara}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class Negara extends NegaraBase
{
    public static function primaryKey()
    {
        return ['location_id'];
    }
}

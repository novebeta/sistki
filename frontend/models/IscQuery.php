<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Isc]].
 *
 * @see Isc
 */
class IscQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Isc[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Isc|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

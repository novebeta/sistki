<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[BiodataResign]].
 *
 * @see BiodataResign
 */
class BiodataResignQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BiodataResign[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BiodataResign|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

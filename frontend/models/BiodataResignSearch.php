<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BiodataNojob;

/**
 * BiodataNojobSearch represents the model behind the search form of `app\models\BiodataNojob`.
 */
class BiodataResignSearch extends BiodataResign
{
	public $companyName;
	public $jobOrderRegister;
	public $cabangName;
//	public $lastExpiredMedikal;
	public $medikalCenter;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['biodata_id', 'name', 'no_hp', 'address', 'kecamatan', 'kabupaten', 'propinsi', 'birthdate', 'tempat_lahir', 'agama', 'suku', 'ayah', 'nikah', 'pendidikan', 'sekolah', 'jurusan', 'lulus', 'alasan', 'siap_kerja', 'path_foto', 'remark', 'pl', 'status_proses', 'no_paspor', 'kode_rekomid', 'no_visa', 'tgl_visa', 'pap_tgl', 'pap_note', 'tiket_tgl', 'tiket_note', 'no_pk', 'cabang_id', 'fex', 'job_order_id', 'sex', 'no_absen', 'nik', 'apply_visa', 'kasus', 'note_kasus', 'tdate', 'formal', 'tipe_bayar_copvisa', 'note_'], 'safe'],
            [['tb', 'bb', 'biaya_copvisa'], 'number'],
            [['punya_sodara', 'siap_kontrak', 'endorse', 'asuransi', 'siap_ikut_aturan', 'resign'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BiodataResign::find();

	    $query->joinWith( 'cabang', true, 'INNER JOIN' );
	    $query->joinWith( 'company', false, 'LEFT JOIN' );
	    $query->joinWith( 'medikalLastexpired', false, 'LEFT JOIN' );
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'birthdate' => $this->birthdate,
            'tb' => $this->tb,
            'bb' => $this->bb,
            'punya_sodara' => $this->punya_sodara,
            'siap_kontrak' => $this->siap_kontrak,
            'tgl_visa' => $this->tgl_visa,
            'endorse' => $this->endorse,
            'asuransi' => $this->asuransi,
            'pap_tgl' => $this->pap_tgl,
            'tiket_tgl' => $this->tiket_tgl,
            'siap_ikut_aturan' => $this->siap_ikut_aturan,
            'apply_visa' => $this->apply_visa,
            'resign' => $this->resign,
            'tdate' => $this->tdate,
            'biaya_copvisa' => $this->biaya_copvisa,
        ]);

        $query->andFilterWhere(['like', 'biodata_id', $this->biodata_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'no_hp', $this->no_hp])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'kecamatan', $this->kecamatan])
            ->andFilterWhere(['like', 'kabupaten', $this->kabupaten])
            ->andFilterWhere(['like', 'propinsi', $this->propinsi])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'agama', $this->agama])
            ->andFilterWhere(['like', 'suku', $this->suku])
            ->andFilterWhere(['like', 'ayah', $this->ayah])
            ->andFilterWhere(['like', 'nikah', $this->nikah])
            ->andFilterWhere(['like', 'pendidikan', $this->pendidikan])
            ->andFilterWhere(['like', 'sekolah', $this->sekolah])
            ->andFilterWhere(['like', 'jurusan', $this->jurusan])
            ->andFilterWhere(['like', 'lulus', $this->lulus])
            ->andFilterWhere(['like', 'alasan', $this->alasan])
            ->andFilterWhere(['like', 'siap_kerja', $this->siap_kerja])
            ->andFilterWhere(['like', 'path_foto', $this->path_foto])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'pl', $this->pl])
            ->andFilterWhere(['like', 'status_proses', $this->status_proses])
            ->andFilterWhere(['like', 'no_paspor', $this->no_paspor])
            ->andFilterWhere(['like', 'kode_rekomid', $this->kode_rekomid])
            ->andFilterWhere(['like', 'no_visa', $this->no_visa])
            ->andFilterWhere(['like', 'pap_note', $this->pap_note])
            ->andFilterWhere(['like', 'tiket_note', $this->tiket_note])
            ->andFilterWhere(['like', 'no_pk', $this->no_pk])
            ->andFilterWhere(['like', 'cabang_id', $this->cabang_id])
            ->andFilterWhere(['like', 'fex', $this->fex])
            ->andFilterWhere(['like', 'job_order_id', $this->job_order_id])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'no_absen', $this->no_absen])
            ->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'kasus', $this->kasus])
            ->andFilterWhere(['like', 'note_kasus', $this->note_kasus])
            ->andFilterWhere(['like', 'formal', $this->formal])
            ->andFilterWhere(['like', 'tipe_bayar_copvisa', $this->tipe_bayar_copvisa])
            ->andFilterWhere(['like', 'note_', $this->note_]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nscc_medikal_expired".
 *
 * @property string $name Full Name
 * @property string $no_hp
 * @property string $address
 * @property string $biodata_id
 * @property string $medikal_id
 * @property string $expired
 * @property string $company_name
 * @property string $job_title
 * @property int $diff
 */
class MedikalExpired extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nscc_medikal_expired';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'biodata_id'], 'required'],
            [['expired'], 'safe'],
            [['diff'], 'integer'],
            [['name', 'address','job_title','company_name'], 'string', 'max' => 100],
            [['no_hp'], 'string', 'max' => 50],
            [['biodata_id', 'medikal_id'], 'string', 'max' => 36],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'no_hp' => 'No Hp',
            'address' => 'Address',
            'biodata_id' => 'Biodata ID',
            'medikal_id' => 'Medikal ID',
            'expired' => 'Expired',
            'diff' => 'Diff',
        ];
    }
}

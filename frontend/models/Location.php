<?php
namespace app\models;
use app\models\base\LocationBase;
/**
 * This is the model class for table "{{%location}}".
 *
 * @property string $location_id
 * @property string $tipe Kecamatan, Kabupaten, Propinsi, Negara
 * @property string $nama
 */
class Location extends LocationBase {
	Const NEGARA_TYPE = 'NEG';
	Const KECAMATAN_TYPE = 'KEC';
	Const KABUPATEN_TYPE = 'KAB';
	Const PROVINSI_TYPE = 'PRO';
	Const MEDICAL_TYPE = 'MED';
	Const RECRUITER_TYPE = 'REC';
	public function createLocation( $nama, $tipe ) {
		if ( trim( $nama ) == null ) {
			return null;
		}
		$model             = new Location();
		$model->location_id = self::generate_uuid();
		$model->tipe       = $tipe;
		$model->nama       = $nama;
		$model->save();
		return $model;
	}
	public function createNegara( $nama ) {
		return self::createLocation( $nama, self::NEGARA_TYPE );
	}
	public function createProvinsi( $nama ) {
		return self::createLocation( $nama, self::PROVINSI_TYPE );
	}
	public function createKabupaten( $nama ) {
		return self::createLocation( $nama, self::KABUPATEN_TYPE );
	}
	public function createKecamatan( $nama ) {
		return self::createLocation( $nama, self::KECAMATAN_TYPE );
	}
	public function createMedical( $nama ) {
		return self::createLocation( $nama, self::MEDICAL_TYPE );
	}
	public function createRecruiter( $nama ) {
		return self::createLocation( $nama, self::RECRUITER_TYPE );
	}
}

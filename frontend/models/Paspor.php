<?php
namespace app\models;
use app\models\base\ModelBase;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%paspor}}".
 *
 * @property string $paspor_id
 * @property string $tgl Date
 * @property string $prov PROV CTKI
 * @property string $imigrasi Imigrasi
 * @property string $pic PIC
 * @property string $note_ Note
 * @property string $biodata_id
 * @property string $created_at
 * @property string $expired Expired
 *
 * @property Biodata $biodata
 * @property Bayar[] $bayar
 */
class Paspor extends ModelBase {
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value'              => new Expression( 'NOW()' ),
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%paspor}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'paspor_id', 'tgl', 'prov', 'biodata_id' ], 'required' ],
			[ [ 'tgl', 'created_at', 'updated_at' , 'expired'], 'safe' ],
			[ [ 'biaya' ], 'number' ],
			[ [ 'biaya' ], 'default', 'value' => 0 ],
			[ [ 'paspor_id', 'biodata_id' ], 'string', 'max' => 36 ],
			[ [ 'prov', 'imigrasi' ], 'string', 'max' => 100 ],
			[ [ 'pic' ], 'string', 'max' => 25 ],
			[ [ 'note_' ], 'string', 'max' => 600 ],
			[ [ 'paspor_id' ], 'unique' ],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
			[
				[ 'prov', 'imigrasi', 'pic', 'note_'],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'paspor_id'  => Yii::t( 'app', 'Paspor ID' ),
			'tgl'        => Yii::t( 'app', 'Date' ),
			'prov'       => Yii::t( 'app', 'PROV CTKI' ),
			'imigrasi'   => Yii::t( 'app', 'Imigrasi' ),
			'pic'        => Yii::t( 'app', 'PIC' ),
			'note_'      => Yii::t( 'app', 'Note' ),
			'biodata_id' => Yii::t( 'app', 'Biodata ID' ),
			'created_at' => Yii::t( 'app', 'Created At' ),
			'expired'    => Yii::t( 'app', 'Expired' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBayar() {
		return $this->hasMany( Bayar::className(), [ 'biodata_id' => 'biodata_id', ] )
		            ->where( [
			            'tipe_trans' => Bayar::PASPOR,
			            'parent_id'  => $this->paspor_id
		            ] );
	}
	/**
	 * @inheritdoc
	 * @return PasporQuery the active query used by this AR class.
	 */
	public static function find() {
		return new PasporQuery( get_called_class() );
	}
}

<?php
namespace app\models;
use app\models\base\ProvinsiBase;
/**
 * This is the model class for table "{{%provinsi}}".
 *
 * @property string $location_id
 * @property string $nama
 */
class Provinsi extends ProvinsiBase
{
    public static function primaryKey()
    {
        return ['location_id'];
    }
}

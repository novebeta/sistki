<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%dokumen_out}}".
 *
 * @property string $dokumen_id
 * @property string $tgl
 * @property string $penerima Penerima Berkas
 * @property int $KTP
 * @property int $KK
 * @property int $AKTE_KELAHIRAN
 * @property int $IJAZAH
 * @property int $BUKU_NIKAH
 * @property int $PASPOR
 * @property int $SURAT_IJIN_WALI
 * @property int $PERJANJIAN_PENEMPATAN
 * @property int $PERJANJIAN_KERJA
 * @property int $VISA_KERJA
 * @property int $KARTU_PESERTA_ASURANSI
 * @property int $SERTIFIKAT_KESEHATAN
 * @property int $AKI
 * @property string $note Keterangan
 * @property string $created_at
 * @property string $biodata_id
 * @property string $cabang_id
 * @property string $grup
 */
class DokumenOut extends Dokumen
{
	public static function primaryKey() {
		return [ 'dokumen_id' ];
	}
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%dokumen_out}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dokumen_id', 'tgl', 'biodata_id', 'cabang_id'], 'required'],
            [['tgl', 'created_at'], 'safe'],
            [['KTP', 'KK', 'AKTE_KELAHIRAN', 'IJAZAH', 'BUKU_NIKAH', 'PASPOR', 'SURAT_IJIN_WALI', 'PERJANJIAN_PENEMPATAN', 'PERJANJIAN_KERJA', 'VISA_KERJA', 'KARTU_PESERTA_ASURANSI', 'SERTIFIKAT_KESEHATAN', 'AKI'], 'integer'],
            [['dokumen_id', 'biodata_id', 'cabang_id', 'grup'], 'string', 'max' => 36],
            [['penerima'], 'string', 'max' => 100],
            [['note'], 'string', 'max' => 600],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dokumen_id' => Yii::t('app', 'Dokumen ID'),
            'tgl' => Yii::t('app', 'Tgl'),
            'penerima' => Yii::t('app', 'Penerima Berkas'),
            'KTP' => Yii::t('app', 'Ktp'),
            'KK' => Yii::t('app', 'Kk'),
            'AKTE_KELAHIRAN' => Yii::t('app', 'Akte Kelahiran'),
            'IJAZAH' => Yii::t('app', 'Ijazah'),
            'BUKU_NIKAH' => Yii::t('app', 'Buku Nikah'),
            'PASPOR' => Yii::t('app', 'Paspor'),
            'SURAT_IJIN_WALI' => Yii::t('app', 'Surat Ijin Wali'),
            'PERJANJIAN_PENEMPATAN' => Yii::t('app', 'Perjanjian Penempatan'),
            'PERJANJIAN_KERJA' => Yii::t('app', 'Perjanjian Kerja'),
            'VISA_KERJA' => Yii::t('app', 'Visa Kerja'),
            'KARTU_PESERTA_ASURANSI' => Yii::t('app', 'Kartu Peserta Asuransi'),
            'SERTIFIKAT_KESEHATAN' => Yii::t('app', 'Sertifikat Kesehatan'),
            'AKI' => Yii::t('app', 'Aki'),
            'note' => Yii::t('app', 'Keterangan'),
            'created_at' => Yii::t('app', 'Created At'),
            'biodata_id' => Yii::t('app', 'Biodata ID'),
            'cabang_id' => Yii::t('app', 'Cabang ID'),
            'grup' => Yii::t('app', 'Grup'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DokumenOutQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DokumenOutQuery(get_called_class());
    }
}

<?php
namespace app\models;
use app\models\base\ModelBase;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%fwcms}}".
 *
 * @property string $fwcms_id
 * @property string $tgl Date
 * @property string $no_paspor No Paspor
 * @property string $rbb_duta RBB/DUTA
 * @property string $pic PIC
 * @property string $note_ Note
 * @property string $biodata_id
 * @property string $created_at
 * @property string $mcu MCU
 * @property string $tgl_tugasan
 * @property string $expired Expired
 *
 * @property Biodata $biodata
 * @property Bayar[] $bayar
 */
class Fwcms extends ModelBase {
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value'              => new Expression( 'NOW()' ),
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%fwcms}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'fwcms_id', 'tgl', 'biodata_id' ], 'required' ],
			[ [ 'tgl', 'created_at', 'updated_at', 'tgl_tugasan', 'expired' ], 'safe' ],
			[ [ 'biaya' ], 'number' ],
			[ [ 'biaya' ], 'default', 'value' => 0 ],
			[ [ 'rbb_duta' ], 'string' ],
			[ [ 'fwcms_id', 'biodata_id' ], 'string', 'max' => 36 ],
			[ [ 'no_paspor' ], 'string', 'max' => 20 ],
			[ [ 'pic' ], 'string', 'max' => 25 ],
			[ [ 'note_' ], 'string', 'max' => 600 ],
			[ [ 'fwcms_id' ], 'unique' ],
			[
				[ 'biodata_id' ],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Biodata::className(),
				'targetAttribute' => [ 'biodata_id' => 'biodata_id' ]
			],
			[
				[ 'rbb_duta', 'pic', 'note_' ],
				'filter', 'filter' => 'strtoupper'
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'fwcms_id'    => Yii::t( 'app', 'Fwcms ID' ),
			'tgl'         => Yii::t( 'app', 'Date' ),
			'no_paspor'   => Yii::t( 'app', 'No Paspor' ),
			'rbb_duta'    => Yii::t( 'app', 'RBB/DUTA' ),
			'pic'         => Yii::t( 'app', 'PIC' ),
			'note_'       => Yii::t( 'app', 'Note' ),
			'biodata_id'  => Yii::t( 'app', 'Biodata ID' ),
			'created_at'  => Yii::t( 'app', 'Created At' ),
			'mcu'         => Yii::t( 'app', 'MCU' ),
			'tgl_tugasan' => Yii::t( 'app', 'Tgl Tugasan' ),
			'expired'     => Yii::t( 'app', 'Expired' ),
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBiodata() {
		return $this->hasOne( Biodata::className(), [ 'biodata_id' => 'biodata_id' ] );
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBayar() {
		return $this->hasMany( Bayar::className(), [ 'biodata_id' => 'biodata_id', ] )
		            ->where( [
			            'tipe_trans' => Bayar::FWCMS,
			            'parent_id'  => $this->fwcms_id
		            ] );
	}
	public function getMcu() {
		$biodata = null;
		if ( $this->isNewRecord ) {
			$biodata = Biodata::find()->andWhere( [ 'biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
		} else {
			$biodata = $this->biodata;
		}
		if ( $biodata == null ) {
			return '';
		}
		$medikal = $biodata->getLastExpiredMedikal();
		if ( $medikal == null ) {
			return '';
		}
		return $medikal->medikal_center;
	}
	public function getNo_Paspor() {
		if ( $this->isNewRecord ) {
			$biodata = Biodata::find()->andWhere( [ 'biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
			if ( $biodata != null ) {
				return $biodata->no_paspor;
			}
		}
		return $this->biodata->no_paspor;
	}
	/**
	 * @inheritdoc
	 * @return FwcmsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new FwcmsQuery( get_called_class() );
	}
}

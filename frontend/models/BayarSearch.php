<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bayar;

/**
 * BayarSearch represents the model behind the search form of `app\models\Bayar`.
 */
class BayarSearch extends Bayar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bayar_id', 'tipe', 'tipe_trans', 'loan', 'voucher', 'note_', 'biodata_id', 'created_at'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bayar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'amount' => $this->amount,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'bayar_id', $this->bayar_id])
            ->andFilterWhere(['like', 'tipe', $this->tipe])
            ->andFilterWhere(['like', 'tipe_trans', $this->tipe_trans])
            ->andFilterWhere(['like', 'loan', $this->loan])
            ->andFilterWhere(['like', 'voucher', $this->voucher])
            ->andFilterWhere(['like', 'note_', $this->note_])
            ->andFilterWhere(['like', 'biodata_id', $this->biodata_id]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Fwcms]].
 *
 * @see Fwcms
 */
class FwcmsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Fwcms[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Fwcms|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

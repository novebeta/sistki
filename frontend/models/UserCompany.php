<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_company}}".
 *
 * @property string $user_company_id
 * @property int $user_id
 * @property string $company_id
 */
class UserCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_company}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_company_id'], 'required'],
            [['user_id'], 'integer'],
            [['user_company_id', 'company_id'], 'string', 'max' => 36],
            [['user_company_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_company_id' => 'User Company ID',
            'user_id' => 'User ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UserCompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserCompanyQuery(get_called_class());
    }
}


//CREATE TABLE `dutaw240_app`.`nscc_user_company`  (
//`user_company_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
//  `user_id` int(11) NULL DEFAULT NULL,
//  `company_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
//  PRIMARY KEY (`user_company_id`) USING BTREE
//) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

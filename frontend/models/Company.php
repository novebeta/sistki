<?php
namespace app\models;
use app\models\base\CompanyBase;
use frontend\components\Cloud;
use OSS\Core\OssException;
use Yii;
/**
 * This is the model class for table "{{%company}}".
 *
 * @property string $company_id
 * @property string $name Name
 * @property string $bisnis Line of Business
 * @property string $tgl Date of Establihment
 * @property string $address Location of Job Site
 * @property string $city City
 * @property string $country Country
 * @property string $no_license No. License
 * @property string $phone No. Telp
 * @property string $employees Total Employees
 * @property string $employee_ind Indonesian Employee
 *
 * @property JobOrder[] $jobs
 */
class Company extends CompanyBase {
	public $photos = array();
	public $photos_config = array();
	public function afterFind() {
		$bucket     = Cloud::getBucketName();
		$ossClient  = Cloud::getOssClient();
		$prefix     = 'company/' . $this->company_id . '/';
		$delimiter  = '/';
		$nextMarker = '';
		$maxkeys    = 1000;
		$options    = array(
			'delimiter' => $delimiter,
			'prefix'    => $prefix,
			'max-keys'  => $maxkeys,
			'marker'    => $nextMarker,
		);
		try {
			$exist = $ossClient->doesObjectExist( $bucket, 'company' );
			if ( $exist === false ) {
				$ossClient->createObjectDir( $bucket, 'company' );
			}
			$exist = $ossClient->doesObjectExist( $bucket, $prefix );
			if ( $exist ) {
				$listObjectInfo = $ossClient->listObjects( $bucket, $options );
				$objectList     = $listObjectInfo->getObjectList();
				if ( ! empty( $objectList ) ) {
					$counter = 1;
					foreach ( $objectList as $objectInfo ) {
						if ( ! preg_match( "/\/$/", $objectInfo->getKey() ) ) {
							$this->photos[]        = 'https://' . $bucket . '.' . Cloud::getEndpoint() . '/' . $objectInfo->getKey();
							$this->photos_config[] = [
								'caption' => 'COMPANY',
								'sub'     => $counter,
								'key'     => $objectInfo->getKey()
							];
							$counter ++;
						}
					}
				}
			}
		} catch ( OssException $e ) {
			Yii::error( $e->getMessage(), 'cloud' );
		}
		if ( Yii::$app->controller->id == 'company' && Yii::$app->controller->action->id == 'View' ) {
			foreach ( $this->jobs as $job ) {
				$this->photos[]        = DokumenUpload::initialPreview( Dokumen::JOB_ORDER, $job->job_order_id, false );
				$this->photos_config[] = [
					'caption' => $job->no_register,
					'sub'     => $job->job_title
				];
			}
		}
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getJobs() {
		return $this->hasMany( JobOrder::className(), [ 'company_id' => 'company_id' ] );
	}
}

<?php
namespace frontend\widgets\dropdownpager;
use Yii;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\Dropdown;


class DropdownPager extends LinkPager
{
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		if ($this->registerLinkTags) {
			$this->registerLinkTags();
		}

		$linkOptions['data-toggle'] = 'dropdown';
//		$linkOptions['role']        = 'button';
//		$linkOptions['role']        = 'button';
		Html::addCssClass($linkOptions, 'dropdown-toggle');

		$html = Html::a(Yii::t('app', 'Page') . ' <span class="caret"></span>', '#', $linkOptions);
		$html .= Dropdown::widget([
			'items'        => $this->configureItems(),
			'encodeLabels' => false,
		]);

		echo $html;
	}

	/**
	 * Renders the page buttons.
	 * @return string the rendering result
	 */
	protected function configureItems()
	{
		$items = [];

		$pageCount = $this->pagination->getPageCount();
		if ($pageCount < 2 && $this->hideOnSinglePage) {
			return $items;
		}

		$currentPage = $this->pagination->getPage();

		// first page
		if ($this->firstPageLabel !== false) {
			$items[] = $this->createPageItem($this->firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
		}

		// prev page
		if ($this->prevPageLabel !== false) {
			if (($page = $currentPage - 1) < 0) {
				$page = 0;
			}
			$items[] = $this->createPageItem($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false);
		}

		// internal pages
		list($beginPage, $endPage) = $this->getPageRange();
		for ($i = $beginPage; $i <= $endPage; ++$i) {
			$items[] = $this->createPageItem($i + 1, $i, null, false, $i == $currentPage);
		}

		// next page
		if ($this->nextPageLabel !== false) {
			if (($page = $currentPage + 1) >= $pageCount - 1) {
				$page = $pageCount - 1;
			}
			$items[] = $this->createPageItem($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
		}

		// last page
		if ($this->lastPageLabel !== false) {
			$items[] = $this->createPageItem($this->lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
		}

		return $items;
	}

	/**
	 * Creates a configuration array for Dropdown item.
	 * Refer to description for \yii\bootstrap\Dropdown::$items property.
	 * @param string $label the text label for the button
	 * @param integer $page the page number
	 * @param string $class the CSS class for the page button.
	 * @param boolean $disabled whether this page button is disabled
	 * @param boolean $active whether this page button is active
	 * @return array
	 */
	protected function createPageItem($label, $page, $class, $disabled, $active)
	{
		$item = ['label' => $label];
		$options = ['class' => $class === '' ? null : $class];
		if ($active) {
			Html::addCssClass($options, $this->activePageCssClass);
		}
		if ($disabled) {
			Html::addCssClass($options, $this->disabledPageCssClass);
			$item['options'] = $options;
			return $item;
		}
		$linkOptions = $this->linkOptions;
		$linkOptions['data-page'] = $page;

		$item['options']     = $options;
		$item['linkOptions'] = $linkOptions;
		$item['url']         = $this->pagination->createUrl($page);

		return $item;
	}

}
<?php
namespace frontend\components;
use OSS\Core\OssException;
use OSS\OssClient;
use Yii;
class Cloud {
	private static $accessKeyId;
	private static $accessKeySecret;
	private static $endpoint;
	/**
	 * @return string
	 */
	public static function getEndpoint(): string {
		return Yii::$app->params['alibaba']['endpoint'];
	}
	private static $bucket;
	/**
	 * Cloud constructor.
	 */
	static function init() {
		self::$accessKeyId     = Yii::$app->params['alibaba']['accessKeyId'];
		self::$accessKeySecret = Yii::$app->params['alibaba']['accessKeySecret'];
		self::$endpoint        = 'https://' . Yii::$app->params['alibaba']['endpoint'];
		self::$bucket          = Yii::$app->params['alibaba']['bucket'];
	}
	/**
	 * Get an OSSClient instance according to config.
	 *
	 * @return OssClient An OssClient instance
	 */
	public static function getOssClient() {
		try {
			$ossClient = new OssClient( self::$accessKeyId, self::$accessKeySecret, self::$endpoint, false );
		} catch ( OssException $e ) {
			Yii::error( $e->getMessage() . "\n", 'cloud' );
			return null;
		}
		return $ossClient;
	}
	public static function getBucketName() {
		return self::$bucket;
	}
}
Cloud::init();
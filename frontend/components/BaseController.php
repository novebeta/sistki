<?php
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 2/25/18
 * Time: 12:20 PM
 */
namespace app\components;
use app\modules\admin\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
class BaseController extends Controller
{
    /** @var User $user */
    public $user;
    public $useTitleContent = false;

    public function init()
    {
        parent::init();
        $this->user = User::findIdentity(Yii::$app->user->id);
    }

//	public function behaviors() {
//		return [
//			'access' => [
//				'class'        => AccessControl::className(),
//				'only'         => [ 'logout', 'signup' ],
//				'rules'        => [
//					[
//						'actions' => [ 'signup' ],
//						'allow'   => true,
//						'roles'   => [ '?' ],
//					],
//					[
//						'actions' => [ 'logout' ],
//						'allow'   => true,
//						'roles'   => [ '@' ],
//					],
//				],
//				'denyCallback' => function ( $rule, $action ) {
//					return Yii::$app->response->redirect( [ 'site/login' ] );
//				},
//			],
//			'verbs'  => [
//				'class'   => VerbFilter::className(),
//				'actions' => [
//					'logout' => [ 'post' ],
//				],
//			],
//		];
//	}

    public function mustHaveBiodata()
    {
        if (!isset($_SESSION[ "BIODATA" ])) {
            Yii::$app->getSession()->setFlash('error', 'Silahkan pilih biodata dulu...');
            return $this->redirect(['/biodata/index']);
        }
    }
    public function generate_uuid()
    {
        $command = Yii::$app->getDb()
            ->createCommand("SELECT UUID();");
        return $command->queryScalar();
    }
    public function renderJson($draw, $model, $error = null)
    {
        /** @var ActiveDataProvider $model */
        $argh = [];
        /** @var ActiveRecord $dodol */
        foreach ($model->getModels() AS $dodol) {
            //foreach($model AS $SJP)
            $argh[] = $dodol->getAttributes();
        };
        $jsonArr = [
            'draw' => $draw,
            'recordsTotal' => $model->getTotalCount(),
            'recordsFiltered' => $model->getCount(),
            'data' => $argh,
            'error' => $error
        ];
//        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($argh) . '}';
        return Json::encode($jsonArr);
    }
}
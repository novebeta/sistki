<?php
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 6/1/18
 * Time: 5:09 PM
 */
namespace app\components;
use yii\rbac\Rule;
class RuleCabang extends Rule {
	public function execute( $user, $item, $params ) {
		$model = $params['post'];
		return $user->id == $model->author_id;
	}
}
<?php
namespace frontend\controllers;
use app\models\Bayar;
use app\models\Biodata;
use app\models\Paspor;
use app\models\PasporSearch;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * PasporController implements the CRUD actions for Paspor model.
 */
class PasporController extends Controller {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Paspor models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$this->layout = "//plain";
		$searchModel  = new PasporSearch();
		$searchModel->biodata_id     = $_SESSION[ "BIODATA" ];
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModelPaspor'  => $searchModel,
			'dataProviderPaspor' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Paspor model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		return $this->render( 'view', [
			'biodata' => $biodata,
			'model'   => $model,
		] );
	}
	/**
	 * Creates a new Paspor model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\db\Exception
	 */
	public function actionCreate() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$biodata = Biodata::find()->andWhere( [ 'biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
		$model   = new Paspor();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = Paspor::getDb()->beginTransaction();
			try {
				$model->paspor_id  = Paspor::generate_uuid();
				$model->biodata_id = $_SESSION[ "BIODATA" ];
				if ( ! $model->save() ) {
					throw new Exception( 'Paspor ' . Html::errorSummary( $model ) );
				}
				if ( isset( $_POST['item_bayar'] ) ) {
					$item_pots = json_decode( Yii::$app->request->post()['item_bayar'] );
					if ( $item_pots != null ) {
						foreach ( $item_pots->body as $row ) {
							if ( count( $row ) == 3 ) {
								$indo             = new Bayar();
								$indo->bayar_id   = Bayar::generate_uuid();
								$indo->tipe_trans = Bayar::PASPOR;
								$indo->biodata_id = $model->biodata_id;
								$indo->parent_id  = $model->paspor_id;
								$indo->voucher    = $row[1];
								$indo->loan       = '0';
								$indo->amount     = $row[0];
								$indo->note_      = $row[2];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $indo ) );
								}
							}
						}
					}
				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			}
			return $this->redirect( [
				'biodata/view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_5'
			] );
		}
		return $this->render( 'create', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Yii::$app->request->post()['item_bayar'],
		] );
	}
	/**
	 * Updates an existing Paspor model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id ) {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		$modelDP = Bayar::find()->where( [
			'AND',
			[
				'tipe_trans' => Bayar::PASPOR,
				'parent_id'  => $model->paspor_id,
				'biodata_id' => $model->biodata_id
			]
		] )->all();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = Paspor::getDb()->beginTransaction();
			try {
				if ( ! $model->save() ) {
					throw new Exception( 'Paspor ' . Html::errorSummary( $model ) );
				}
				Bayar::deleteAll( [
					'AND',
					[
						'tipe_trans' => Bayar::PASPOR,
						'parent_id'  => $model->paspor_id,
						'biodata_id' => $model->biodata_id
					]
				] );
				if ( isset( $_POST['item_bayar'] ) ) {
					$item_pots = json_decode( Yii::$app->request->post()['item_bayar'] );
					if ( $item_pots != null ) {
						foreach ( $item_pots->body as $row ) {
							if ( count( $row ) == 3 ) {
								$indo             = new Bayar();
								$indo->bayar_id   = Bayar::generate_uuid();
								$indo->tipe_trans = Bayar::PASPOR;
								$indo->biodata_id = $model->biodata_id;
								$indo->parent_id  = $model->paspor_id;
								$indo->voucher    = $row[1];
								$indo->loan       = '0';
								$indo->amount     = $row[0];
								$indo->note_      = $row[2];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $indo ) );
								}
							}
						}
					}
				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			}
			return $this->redirect( [
				'biodata/view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_5'
			] );
		}
		$itemArr = [];
		foreach ( $modelDP as $r ) {
			$itemArr[] = [ 'Rp'.Yii::$app->formatter->asDecimal($r->amount), $r->voucher, $r->note_ ];
		}
		return $this->render( 'update', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Json::encode( $itemArr )
		] );
	}
	/**
	 * Deletes an existing Paspor model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Paspor model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Paspor the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Paspor::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
}

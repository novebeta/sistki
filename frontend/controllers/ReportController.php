<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Biodata;
use app\models\JobOrder;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
class ReportController extends BaseController {
	public function actionFlight() {
		try {
			$path        = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
			                         'views' . DIRECTORY_SEPARATOR . 'report' .
			                         DIRECTORY_SEPARATOR . 'FLIGHT.xlsx' );
			$spreadsheet = IOFactory::load( $path );
			$worksheet   = $spreadsheet->getActiveSheet();
			$worksheet->getCell( 'A1' )->setValue( 'John' );
			$worksheet->getCell( 'A2' )->setValue( 'Smith' );
			$writer = IOFactory::createWriter( $spreadsheet, 'Xlsx' );
			header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
			header( 'Content-Disposition: attachment; filename="FLIGHT.xlsx"' );
			$writer->save( "php://output" );
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			echo $e->getMessage();
		}
	}
	public function actionAn05() {
		try {
			if ( \Yii::$app->request->post() ) {
				$biodatas    = Biodata::find()
				                      ->where( "tiket_tgl >= :awal AND tiket_tgl <= :akhir", [
					                      ':awal'  => $_POST['tgl_awal'],
					                      ':akhir' => $_POST['tgl_akhir']
				                      ] )
				                      ->orderBy( "tiket_tgl" )
				                      ->all();
				$counter     = 1;
				$path        = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
				                         'views' . DIRECTORY_SEPARATOR . 'report' .
				                         DIRECTORY_SEPARATOR . 'AN-05.xlsx' );
				$spreadsheet = IOFactory::load( $path );
				$worksheet   = $spreadsheet->getActiveSheet();
				$index       = 0;
				foreach ( $biodatas as $biodata ) {
					$index   = 5 + $counter;
					$medikal = $biodata->getLastExpiredMedikal();
					$rekomid = $biodata->getLastExpiredRekomid();
					$mcu     = ( $medikal == null ) ? '' : $medikal->medikal_center;
					$rekom   = ( $rekomid == null ) ? '' : $rekomid->rekomid;
					$worksheet->getCell( 'B' . $index )->setValue( $counter );
					$worksheet->getCell( 'C' . $index )->setValue( $biodata->no_pk );
					$worksheet->getCell( 'D' . $index )->setValue( $biodata->name );
					$worksheet->getCell( 'E' . $index )->setValue( ( $biodata->sex == 'M' ) ? 'P' : 'L' );
					$worksheet->getCell( 'F' . $index )->setValue( strtoupper( $biodata->tempat_lahir ) . ', ' . $biodata->birthdate );
					$worksheet->getCell( 'G' . $index )->setValue( strtoupper( $biodata->address ) );
					$worksheet->getCell( 'H' . $index )->setValue( strtoupper( $biodata->no_paspor ) );
					$worksheet->getCell( 'I' . $index )->setValue( strtoupper( $biodata->tiket_tgl ) );
					$worksheet->getCell( 'J' . $index )->setValue( strtoupper( $biodata->no_visa ) );
					$worksheet->getCell( 'K' . $index )->setValue( strtoupper( $biodata->jobOrder->no_register ) );
					$worksheet->getCell( 'L' . $index )->setValue( strtoupper( $biodata->jobOrder->company->name ) );
					$worksheet->getCell( 'M' . $index )->setValue( strtoupper( $biodata->jobOrder->company->no_license ) );
					$worksheet->getCell( 'N' . $index )->setValue( strtoupper( $biodata->jobOrder->company->address ) );
					$worksheet->getCell( 'O' . $index )->setValue( strtoupper( $biodata->jobOrder->company->phone ) );
					$worksheet->getCell( 'P' . $index )->setValue( strtoupper( $biodata->jobOrder->company->city ) );
					$worksheet->getCell( 'Q' . $index )->setValue( strtoupper( $biodata->jobOrder->company->country ) );
					$worksheet->getCell( 'S' . $index )->setValue( strtoupper( $biodata->jobOrder->job_title ) );
					$worksheet->getCell( 'T' . $index )->setValue( strtoupper( $biodata->formal ) );
//					if ( $counter != 1 ) {
//						$worksheet->duplicateStyle(
//							$worksheet->getStyle( 'B6:T6' ),
//							'B' . $index . ':T' . $index
//						);
//					}
//					$worksheet->getCell( 'L' . $index )->setValue( strtoupper( 'majikan' ) );
					$counter ++;
				}
				if ( $index > 6 ) {
//					$worksheet->duplicateStyle(
//						$worksheet->getStyle( 'B6:T6' ),
//						'B7:T' . $index
//					);
					$styleArray  = [
						'alignment' => [
							'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
						],
						'borders'   => [
							'allBorders' => [
								'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
								'color'       => [ 'argb' => '00000000' ],
							],
						],
					];
					$styleArray1 = [
						'alignment' => [
							'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
						],
						'borders'   => [
							'allBorders' => [
								'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
								'color'       => [ 'argb' => '00000000' ],
							],
						],
					];
					$worksheet->getStyle( 'B6:T' . $index )->applyFromArray( $styleArray1 );
					$worksheet->getStyle( 'G6:G' . $index )->applyFromArray( $styleArray );
					$worksheet->getStyle( 'D6:D' . $index )->applyFromArray( $styleArray );
					$worksheet->getStyle( 'L6:L' . $index )->applyFromArray( $styleArray );
					$worksheet->getStyle( 'N6:N' . $index )->applyFromArray( $styleArray );
				}
				foreach ( range( 'B', 'T' ) as $columnID ) {
					if ( $columnID == 'H' || $columnID == 'I' ) {
						continue;
					}
					$spreadsheet->getActiveSheet()->getColumnDimension( $columnID )
					            ->setAutoSize( true );
				}
				ob_start();
				$writer = IOFactory::createWriter( $spreadsheet, 'Xlsx' );
				header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
				header( 'Content-Disposition: attachment; filename="AN-05.xlsx"' );
				$writer->save( "php://output" );
				return ob_get_clean();
			} else {
				return $this->render( 'an05' );
			}
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			echo $e->getMessage();
		}
	}
	public function actionCallingVisa() {
		try {
			if ( \Yii::$app->request->post() ) {
				$no_visa     = $_POST['no_visa'];
				$biodatas    = Biodata::find()->where( [ 'no_visa' => $no_visa ] )->all();
				$counter     = 1;
				$path        = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
				                         'views' . DIRECTORY_SEPARATOR . 'report' .
				                         DIRECTORY_SEPARATOR . 'CALLING_VISA.xlsx' );
				$spreadsheet = IOFactory::load( $path );
				$worksheet   = $spreadsheet->getActiveSheet();
				foreach ( $biodatas as $biodata ) {
					$index   = 3 + $counter;
					$medikal = $biodata->getLastExpiredMedikal();
					$rekomid = $biodata->getLastExpiredRekomid();
					$mcu     = ( $medikal == null ) ? '' : $medikal->medikal_center;
					$rekom   = ( $rekomid == null ) ? '' : $rekomid->rekomid;
					if ( $counter != 1 ) {
						$worksheet->duplicateStyle(
							$worksheet->getStyle( 'A4:G4' ),
							'A' . $index . ':G' . $index
						);
					}
					$worksheet->getCell( 'A' . $index )->setValue( $counter );
					$worksheet->getCell( 'B' . $index )->setValue( $biodata->name );
					$worksheet->getCell( 'C' . $index )->setValue( $mcu );
					$worksheet->getCell( 'D' . $index )->setValue( $rekom );
					$worksheet->getCell( 'E' . $index )->setValue( $biodata->no_paspor );
					$worksheet->getCell( 'F' . $index )->setValue( $biodata->getCompanyName() );
					$worksheet->getCell( 'G' . $index )->setValue( $no_visa );
					$counter ++;
				}
				ob_start();
				$writer = IOFactory::createWriter( $spreadsheet, 'Xlsx' );
				header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
				header( 'Content-Disposition: attachment; filename="CALLING_VISA.xlsx"' );
				$writer->save( "php://output" );
				return ob_get_clean();
			} else {
				return $this->render( 'callingVisa' );
			}
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			echo $e->getMessage();
		}
	}
	public function actionProses() {
		try {
			if ( \Yii::$app->request->post() ) {
				$job_order_id = $_POST['job_order_id'];
				$jobOrder     = JobOrder::find()->where( [ 'job_order_id' => $job_order_id ] )->one();
				$biodatas     = Biodata::find()->where( [ 'job_order_id' => $job_order_id ] )->all();
				$counter      = 1;
				$path         = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
				                          'views' . DIRECTORY_SEPARATOR . 'report' .
				                          DIRECTORY_SEPARATOR . 'PROSES.xlsx' );
				$spreadsheet  = IOFactory::load( $path );
				$worksheet    = $spreadsheet->getActiveSheet();
				$worksheet->getCell( 'A1' )->setValue( 'DAFTAR PROSES ' . $jobOrder->getCompanyName() );
				foreach ( $biodatas as $biodata ) {
					$index     = 3 + $counter;
					$medikal   = $biodata->getLastExpiredMedikal();
					$rekomid   = $biodata->getLastExpiredRekomid();
					$lastisc   = $biodata->getLastIsc();
					$lastfwcms = $biodata->getLastFwcms();
					$mcu       = ( $medikal == null ) ? '' : $medikal->medikal_center;
					$rekom     = ( $rekomid == null ) ? '' : $rekomid->rekomid;
					$isc       = ( $lastisc == null ) ? '' : $lastisc->tgl;
					$fwcms     = ( $lastfwcms == null ) ? '' : $lastfwcms->tgl;
					$worksheet->getCell( 'A' . $index )->setValue( $counter );
					$worksheet->getCell( 'B' . $index )->setValue( $biodata->no_absen );
					$worksheet->getCell( 'C' . $index )->setValue( $biodata->name );
					$worksheet->getCell( 'D' . $index )->setValue( $biodata->kabupaten );
					$worksheet->getCell( 'E' . $index )->setValue( $mcu );
					$worksheet->getCell( 'F' . $index )->setValue( $rekom );
					$worksheet->getCell( 'G' . $index )->setValue( $biodata->no_paspor );
					$worksheet->getCell( 'H' . $index )->setValue( $isc );
					$worksheet->getCell( 'I' . $index )->setValue( $fwcms );
					$worksheet->getCell( 'J' . $index )->setValue( $biodata->no_hp );
					$worksheet->getCell( 'K' . $index )->setValue( $biodata->apply_visa );
					$worksheet->getCell( 'L' . $index )->setValue( $biodata->tgl_visa );
					$worksheet->getCell( 'M' . $index )->setValue( $biodata->pap_tgl );
					$worksheet->getCell( 'N' . $index )->setValue( $biodata->tiket_tgl );
					$worksheet->getCell( 'O' . $index )->setValue( $biodata->pl );
					$worksheet->getCell( 'P' . $index )->setValue( " " );
					$worksheet->getCell( 'Q' . $index )->setValue( " " );
//					if ( $counter != 1 ) {
//						$worksheet->duplicateStyle(
//							$worksheet->getStyle( 'A4:M4' ),
//							'A' . $index . ':M' . $index
//						);
//						$worksheet->getStyle( 'A' . $index . ':M' . $index )->getAlignment()->setHorizontal( 'left' );
//					}
					$counter ++;
				}
				$styleArray  = [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
					],
					'borders'   => [
						'allBorders' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
							'color'       => [ 'argb' => '00000000' ],
						],
					],
				];
				$styleArray1 = [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					],
					'borders'   => [
						'allBorders' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
							'color'       => [ 'argb' => '00000000' ],
						],
					],
				];
				if ( $index > 3 ) {
					$worksheet->getStyle( 'A4:Q' . $index )->applyFromArray( $styleArray1 );
					$worksheet->getStyle( 'C4:C' . $index )->applyFromArray( $styleArray );
					foreach ( range( 'C', 'Q' ) as $columnID ) {
						$spreadsheet->getActiveSheet()->getColumnDimension( $columnID )
						            ->setAutoSize( true );
					}
				}
				ob_start();
				$writer = IOFactory::createWriter( $spreadsheet, 'Xlsx' );
				header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
				header( 'Content-Disposition: attachment; filename="PROSES.xlsx"' );
				$writer->save( "php://output" );
				return ob_get_clean();
			} else {
				return $this->render( 'proses' );
			}
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			echo $e->getMessage();
		}
	}
	public function actionRangkuman() {
		try {
			if ( \Yii::$app->request->post() ) {
				$job_order_id = $_POST['job_order_id'];
				$jobOrder     = JobOrder::find()->where( [ 'job_order_id' => $job_order_id ] )->one();
				$biodatas     = Biodata::find()->where( [ 'job_order_id' => $job_order_id ] )
//				                               ->limit(45)
                                       ->all();
				$path         = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
				                          'views' . DIRECTORY_SEPARATOR . 'report' .
				                          DIRECTORY_SEPARATOR . 'RANGKUMAN.xlsx' );
				$spreadsheet  = IOFactory::load( $path );
				$worksheet    = $spreadsheet->getActiveSheet();
				$TITLE        = 'DAFTAR RANGKUMAN BIAYA ' . $jobOrder->getCompanyName();
				$worksheet->getCell( 'A1' )->setValue( $TITLE );
				$output_file_name = 'RANGKUMAN.xlsx';
				$value_arr        = [];
				$counter          = 1;
				$index            = 5;
				foreach ( $biodatas as $biodata ) {
					$line    = [
						'counter'  => $counter . '.',
						'no_absen' => $biodata->no_absen,
						'name'     => $biodata->name,
						'pl'       => $biodata->pl,
					];
					$mcu     = $biodata->getLastExpiredMedikal()->bayar;
					$pass    = $biodata->getLastPaspor()->bayar;
					$isc     = $biodata->getLastIsc()->bayar;
					$fwcms   = $biodata->getLastFwcms()->bayar;
					$s_mcu   = sizeof( $mcu );
					$s_pass  = sizeof( $pass );
					$s_isc   = sizeof( $isc );
					$s_fwcms = sizeof( $fwcms );
					$max     = max( [ $s_mcu, $s_pass, $s_isc, $s_fwcms ] );
					if ( $max == 0 ) {
						$line        = array_merge( $line, [
							'mcu_amount'   => '',
							'mcu_voucher'  => '',
							'pass_amount'  => '',
							'pass_voucher' => '',
							'isc_amount'   => '',
							'isc_voucher'  => '',
							'fw_amount'    => '',
							'fw_voucher'   => ''
						] );
						$value_arr[] = $line;
						$index ++;
					} else {
						for ( $i = 0; $i < $max; $i ++ ) {
							if ( $s_mcu > $i ) {
								$line = array_merge( $line, [
									'mcu_amount'  => $mcu[ $i ]->amount,
									'mcu_voucher' => $mcu[ $i ]->voucher
								] );
							} else {
								$line = array_merge( $line, [
									'mcu_amount'  => '',
									'mcu_voucher' => ''
								] );
							}
							if ( $s_pass > $i ) {
								$line = array_merge( $line, [
									'pass_amount'  => $pass[ $i ]->amount,
									'pass_voucher' => $pass[ $i ]->voucher
								] );
							} else {
								$line = array_merge( $line, [
									'pass_amount'  => '',
									'pass_voucher' => ''
								] );
							}
							if ( $s_isc > $i ) {
								$line = array_merge( $line, [
									'isc_amount'  => $isc[ $i ]->amount,
									'isc_voucher' => $isc[ $i ]->voucher
								] );
							} else {
								$line = array_merge( $line, [
									'isc_amount'  => '',
									'isc_voucher' => ''
								] );
							}
							if ( $s_fwcms > $i ) {
								$line = array_merge( $line, [
									'fw_amount'  => $fwcms[ $i ]->amount,
									'fw_voucher' => $fwcms[ $i ]->voucher
								] );
							} else {
								$line = array_merge( $line, [
									'fw_amount'  => '',
									'fw_voucher' => ''
								] );
							}
							$value_arr[] = $line;
							$line        = [
								'counter'  => '',
								'no_absen' => '',
								'name'     => '',
								'pl'       => '',
							];
							$index ++;
						}
					}
					$counter ++;
				}
				$worksheet->fromArray( $value_arr, null, 'A5' );
				$styleArray  = [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
					],
					'borders'   => [
						'allBorders' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
							'color'       => [ 'argb' => '00000000' ],
						],
					],
				];
				$styleArray1 = [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					],
					'borders'   => [
						'allBorders' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
							'color'       => [ 'argb' => '00000000' ],
						],
					],
				];
				if ( $index > 4 ) {
					$worksheet->getStyle( 'A5:L' . $index )->applyFromArray( $styleArray1 );
					$worksheet->getStyle( 'C5:C' . $index )->applyFromArray( $styleArray );
					foreach ( range( 'A', 'L' ) as $columnID ) {
						$spreadsheet->getActiveSheet()->getColumnDimension( $columnID )
						            ->setAutoSize( true );
					}
				}
				ob_start();
				$writer = IOFactory::createWriter( $spreadsheet, 'Xlsx' );
				header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
				header( 'Content-Disposition: attachment; filename="RANGKUMAN.xlsx"' );
				$writer->save( "php://output" );
				return ob_get_clean();
			} else {
				return $this->render( 'rangkuman' );
			}
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			YII::error( $e->getMessage() );
		}
	}
}

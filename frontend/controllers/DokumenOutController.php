<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Dokumen;
use app\models\DokumenOut;
use app\models\DokumenOutSearch;
use kartik\helpers\Html;
use Yii;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * DokumenOutController implements the CRUD actions for DokumenOut model.
 */
class DokumenOutController extends BaseController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all DokumenOut models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new DokumenOutSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Displays a single DokumenOut model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		$model = DokumenOut::findOne( [ 'grup' => $id ] );
		return $this->render( 'view', [
			'model' => $model,
		] );
	}
	/**
	 * Creates a new DokumenOut model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new DokumenOut();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = DokumenOut::getDb()->beginTransaction();
			try {
				$grup = Dokumen::generate_uuid();
				foreach ( Dokumen::JENIS_ARR as $item ) {
					if ( $model->getAttribute( $item ) ) {
						$model1             = new Dokumen();
						$model1->attributes = $model->attributes;
						$model1->dokumen_id = Dokumen::generate_uuid();
						$model1->tipe       = Dokumen::KELUAR;
						$model1->load( Yii::$app->request->post() );
						$model1->grup          = $grup;
						$model1->jenis_dokumen = $item;
						$model1->cabang_id     = $this->user->cabang_id;
						if ( ! $model1->save() ) {
							throw new UserException( Html::errorSummary( $model1 ) );
						}
					}
				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'index' ] );
		}
		$model->tgl = date( 'Y-m-d' );
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing DokumenOut model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = DokumenOut::findOne( [ 'grup' => $id ] );
		if ( Yii::$app->request->post() != null ) {
			$model->load( Yii::$app->request->post() );
			$transaction = DokumenOut::getDb()->beginTransaction();
			Dokumen::deleteAll( [ 'grup' => $id ] );
			try {
				foreach ( Dokumen::JENIS_ARR as $item ) {
					if ( $model->getAttribute( $item ) ) {
						$model1             = new Dokumen();
						$model1->attributes = $model->attributes;
						$model1->dokumen_id = Dokumen::generate_uuid();
						$model1->tipe       = Dokumen::KELUAR;
						$model1->load( Yii::$app->request->post() );
						$model1->jenis_dokumen = $item;
						$model1->grup          = $id;
						$model1->cabang_id     = $this->user->cabang_id;
						if ( ! $model1->save() ) {
							throw new UserException( Html::errorSummary( $model1 ) );
						}
					}
				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'view', 'id' => $id ] );
		}
//		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//			return $this->redirect( [ 'view', 'id' => $model->dokumen_id ] );
//		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing DokumenOut model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		Dokumen::deleteAll( [ 'grup' => $id ] );
//		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the DokumenOut model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return DokumenOut the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = DokumenOut::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}

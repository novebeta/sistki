<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\JobOrderDashboard;
use app\models\MedikalExpired;
use app\models\MpExpired;
use app\models\UserCompany;
use common\models\LoginForm;
use frontend\components\Cloud;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use OSS\Core\OssException;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
/**
 * Site controller
 */
class SiteController extends BaseController {
	/**
	 * @inheritdoc
	 */
	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [
//			'error'   => [
//				'class' => 'yii\web\ErrorAction',
//				"view"  => "@frontend/views/site/error.php",
//			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	public function actionError() {
		$exception = Yii::$app->errorHandler->exception;
		if ( $exception !== null ) {
			$statusCode = $exception->statusCode;
			$name       = $exception->getName();
			$message    = $exception->getMessage();
//			$this->layout = 'your_error_layout';
			return $this->render( 'error', [
				'exception'  => $exception,
				'statusCode' => $statusCode,
				'name'       => $name,
				'message'    => $message
			] );
		}
	}
	public function actionIndex() {
		/** @var UserCompany $u */
		$u = UserCompany::findOne([ 'user_id' => $this->user->id ]);
		if ( $u != null ) {
			\Yii::$app->session->set('company_id',$u->company_id);
			$this->redirect( 'job-order/index-company' );
		}
		$jobOrder       = JobOrderDashboard::find()->asArray()->all();
		$medikalExpired = MedikalExpired::find()->asArray()->all();
		$mpExpired      = MpExpired::find()->asArray()->all();
		return $this->render( 'index', [
			'jobOrders'      => $jobOrder,
			'medikalExpired' => $medikalExpired,
			'mpExpired'      => $mpExpired,
		] );
	}
	public function actionHello() {
		ini_set( 'max_execution_time', 0 );
		set_time_limit( 0 );
		self::uploadOss( '/home/nove/Desktop/dutawibawa' );
	}
	private function uploadOss( $path ) {
		$bucket    = Cloud::getBucketName();
		$ossClient = Cloud::getOssClient();
//		$object    = 'company';
		$options = null;
		try {
			$files = scandir( $path );
			foreach ( $files as $file ) {
				if ( is_file( $path . '/' . $file ) ) {
					echo $path . '/' . $file . "</br>";
					$object = str_replace( '/home/nove/Desktop/dutawibawa/', '', $path . '/' . $file );
					echo $object . "</br>";
					$ossClient->uploadFile( $bucket, $object, $path . '/' . $file, $options );
				} else {
					if ( $file == '.' || $file == '..' ) {
						continue;
					}
					echo $path . '/' . $file . "[D] </br>";
					$object = str_replace( '/home/nove/Desktop/dutawibawa/', '', $path . '/' . $file );
					echo $object . "</br>";
					$exist = $ossClient->doesObjectExist( $bucket, $object );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, $object );
					}
					self::uploadOss( $path . '/' . $file );
				}
			}
		} catch ( OssException $e ) {
			echo $e->getMessage();
		}
	}
	public function actionTest() {
		return 'test';
	}
	/**
	 * Logs in a user.
	 *
	 * @return mixed
	 */
	public function actionLogin() {
		if ( ! Yii::$app->user->isGuest ) {
			return $this->redirect( [ '/', 't' => md5( date( 'Y-m-d H:i:s' ) ) ] );
		}
		$model = new LoginForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
			return $this->redirect( [ '/', 't' => md5( date( 'Y-m-d H:i:s' ) ) ] );
		} else {
			$this->layout = '//main-login';
			return $this->render( 'login', [
				'model' => $model,
			] );
		}
	}
	/**
	 * Logs out the current user.
	 *
	 * @return mixed
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		$session = Yii::$app->session;
// check if a session is already open
		if ( $session->isActive ) {
			$session->open();
			$session->close();
			$session->destroy();
		}
		return $this->redirect( [ 'site/login', 't' => md5( date( 'Y-m-d H:i:s' ) ) ] );
	}
	/**
	 * Displays contact page.
	 *
	 * @return mixed
	 */
	public function actionContact() {
		$model = new ContactForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
			if ( $model->sendEmail( Yii::$app->params['adminEmail'] ) ) {
				Yii::$app->session->setFlash( 'success', 'Thank you for contacting us. We will respond to you as soon as possible.' );
			} else {
				Yii::$app->session->setFlash( 'error', 'There was an error sending your message.' );
			}
			return $this->refresh();
		} else {
			return $this->render( 'contact', [
				'model' => $model,
			] );
		}
	}
	/**
	 * Displays about page.
	 *
	 * @return mixed
	 */
	public function actionAbout() {
		return $this->render( 'about' );
	}
	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionSignup() {
		$model = new SignupForm();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $user = $model->signup() ) {
				if ( Yii::$app->getUser()->login( $user ) ) {
					return $this->goHome();
				}
			}
		}
		return $this->render( 'signup', [
			'model' => $model,
		] );
	}
	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset() {
		$model = new PasswordResetRequestForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
			if ( $model->sendEmail() ) {
				Yii::$app->session->setFlash( 'success', 'Check your email for further instructions.' );
				return $this->goHome();
			} else {
				Yii::$app->session->setFlash( 'error', 'Sorry, we are unable to reset password for the provided email address.' );
			}
		}
		return $this->render( 'requestPasswordResetToken', [
			'model' => $model,
		] );
	}
	/**
	 * Resets password.
	 *
	 * @param string $token
	 *
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword( $token ) {
		try {
			$model = new ResetPasswordForm( $token );
		} catch ( InvalidParamException $e ) {
			throw new BadRequestHttpException( $e->getMessage() );
		}
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() && $model->resetPassword() ) {
			Yii::$app->session->setFlash( 'success', 'New password saved.' );
			return $this->goHome();
		}
		return $this->render( 'resetPassword', [
			'model' => $model,
		] );
	}
}

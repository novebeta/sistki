<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Kabupaten;
use app\models\KabupatenSearch;
use app\models\Location;
use Yii;
use yii\bootstrap\BaseHtml;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * KabupatenController implements the CRUD actions for Kabupaten model.
 */
class KabupatenController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Kabupaten models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KabupatenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Kabupaten model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Creates a new Kabupaten model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Location();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->location_id == null) {
                $model->location_id = $this->generate_uuid();
            }

	        $model->tipe = Location::KABUPATEN_TYPE;
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Save Success!');
            } else {
                Yii::$app->getSession()->setFlash('failed', BaseHtml::errorSummary($model));
            }
//            return $this->redirect(['view', 'id' => $model->location_id]);
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Kabupaten model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

	        $model->tipe = Location::KABUPATEN_TYPE;
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Save Success!');
            } else {
                Yii::$app->getSession()->setFlash('failed', BaseHtml::errorSummary($model));
            }
            return $this->redirect(['view', 'id' => $model->location_id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing Kabupaten model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    /**
     * Finds the Kabupaten model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kabupaten the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kabupaten::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

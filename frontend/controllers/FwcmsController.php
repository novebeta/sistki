<?php
namespace frontend\controllers;
use app\models\Bayar;
use app\models\Biodata;
use app\models\Fwcms;
use app\models\FwcmsSearch;
use DateInterval;
use DateTime;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * FwcmsController implements the CRUD actions for Fwcms model.
 */
class FwcmsController extends Controller {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Fwcms models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$this->layout            = "//plain";
		$searchModel             = new FwcmsSearch();
		$searchModel->biodata_id = $_SESSION[ "BIODATA" ];
		$dataProvider            = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModelFwcms'  => $searchModel,
			'dataProviderFwcms' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Fwcms model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		return $this->render( 'view', [
			'biodata' => $biodata,
			'model'   => $model,
		] );
	}
	/**
	 * Creates a new Fwcms model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\db\Exception
	 */
	public function actionCreate() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$biodata         = Biodata::find()->andWhere( [ 'biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
		$model           = new Fwcms();
		$model->rbb_duta = 'DUTA';
		$modelDP         = new Bayar();
		$modelLunas      = new Bayar();
		if ( $model->load( Yii::$app->request->post() ) ) {
//			if ( $_POST['refresh'] == 1 ) {
//				$start       = new DateTime( $model->tgl );
//				$month_later = clone $start;
//				$month_later->add( new DateInterval( "P3M" ) );
//				$model->expired = $month_later->format( 'Y-m-d' );
//			} else {
			$transaction = Fwcms::getDb()->beginTransaction();
			try {
				$model->fwcms_id   = Fwcms::generate_uuid();
				$model->biodata_id = $_SESSION[ "BIODATA" ];
				if ( ! $model->save() ) {
					throw new Exception( 'FWCMS ' . Html::errorSummary( $model ) );
				}
				if ( isset( $_POST['item_bayar'] ) ) {
					$item_pots = json_decode( Yii::$app->request->post()['item_bayar'] );
					if ( $item_pots != null ) {
						foreach ( $item_pots->body as $row ) {
							if ( count( $row ) == 3 ) {
								$indo             = new Bayar();
								$indo->bayar_id   = Bayar::generate_uuid();
								$indo->tipe_trans = Bayar::FWCMS;
								$indo->biodata_id = $model->biodata_id;
								$indo->parent_id  = $model->fwcms_id;
								$indo->voucher    = $row[1];
								$indo->loan       = '0';
								$indo->amount     = $row[0];
								$indo->note_      = $row[2];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $indo ) );
								}
							}
						}
					}
				}
//				$modelDP->bayar_id   = Bayar::generate_uuid();
//				$modelDP->tipe_trans = Bayar::FWCMS;
//				$modelDP->tipe       = Bayar::DP;
//				$modelDP->biodata_id = $model->biodata_id;
//				$modelDP->parent_id  = $model->fwcms_id;
//				$modelDP->voucher    = $_POST['voucher_dp'];
//				$modelDP->loan       = $_POST['loan_dp'];
//				$modelDP->amount     = $_POST['amount_dp'];
//				if ( ! $modelDP->save() ) {
//					throw new Exception( 'Bayar Down Payment ' . Html::errorSummary( $modelDP ) );
//				}
//				$modelLunas->bayar_id   = Bayar::generate_uuid();
//				$modelLunas->tipe_trans = Bayar::FWCMS;
//				$modelLunas->tipe       = Bayar::LUNAS;
//				$modelLunas->biodata_id = $model->biodata_id;
//				$modelLunas->parent_id  = $model->fwcms_id;
//				$modelLunas->voucher    = $_POST['voucher_lunas'];
//				$modelLunas->loan       = $_POST['loan_lunas'];
//				$modelLunas->amount     = $_POST['amount_lunas'];
//				if ( ! $modelLunas->save() ) {
//					throw new Exception( 'Bayar Pelunasan ' . Html::errorSummary( $modelLunas ) );
//				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			}
			//			return $this->redirect( [ 'biodata/view', 'id' => $model->biodata_id ] );
			return $this->redirect( [
				'biodata/view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_6'
			] );
		}
//		}
		$modelDP->loan    = 0;
		$modelLunas->loan = 0;
		return $this->render( 'create', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Yii::$app->request->post()['item_bayar'],
		] );
	}
	/**
	 * Updates an existing Fwcms model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id ) {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		$modelDP = Bayar::find()->where( [
			'AND',
			[
				'tipe_trans' => Bayar::FWCMS,
				'parent_id'  => $model->fwcms_id,
				'biodata_id' => $model->biodata_id
			]
		] )->all();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $_POST['refresh'] == 1 ) {
				$start       = new DateTime( $model->tgl );
				$month_later = clone $start;
				$month_later->add( new DateInterval( "P3M" ) );
				$model->expired = $month_later->format( 'Y-m-d' );
			} else {
				$transaction = Fwcms::getDb()->beginTransaction();
				try {
					if ( ! $model->save() ) {
						throw new Exception( 'FWCMS ' . Html::errorSummary( $model ) );
					}
					Bayar::deleteAll( [
						'AND',
						[
							'tipe_trans' => Bayar::FWCMS,
							'parent_id'  => $model->fwcms_id,
							'biodata_id' => $model->biodata_id
						]
					] );
					if ( isset( $_POST['item_bayar'] ) ) {
						$item_pots = json_decode( Yii::$app->request->post()['item_bayar'] );
						if ( $item_pots != null ) {
							foreach ( $item_pots->body as $row ) {
								if ( count( $row ) == 3 ) {
									$indo             = new Bayar();
									$indo->bayar_id   = Bayar::generate_uuid();
									$indo->tipe_trans = Bayar::FWCMS;
									$indo->biodata_id = $model->biodata_id;
									$indo->parent_id  = $model->fwcms_id;
									$indo->voucher    = $row[1];
									$indo->loan       = '0';
									$indo->amount     = $row[0];
									$indo->note_      = $row[2];
									if ( ! $indo->save() ) {
										throw new UserException( Html::errorSummary( $indo ) );
									}
								}
							}
						}
					}
					$transaction->commit();
				} catch ( \Exception $e ) {
					$transaction->rollBack();
					Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
					return $this->render( 'update', [
						'biodata' => $biodata,
						'model'   => $model,
						'item'    => Yii::$app->request->post()['item_bayar'],
					] );
				} catch ( \Throwable $e ) {
					$transaction->rollBack();
					Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
					return $this->render( 'update', [
						'biodata' => $biodata,
						'model'   => $model,
						'item'    => Yii::$app->request->post()['item_bayar'],
					] );
				}
				//			return $this->redirect( [ 'biodata/view', 'id' => $model->biodata_id ] );
				return $this->redirect( [
					'biodata/view',
					'id' => $model->biodata_id,
					't'  => md5( date( 'Y-m-d H:i:s' ) ),
					'#'  => 'tab_6'
				] );
			}
		}
		$itemArr = [];
		foreach ( $modelDP as $r ) {
			$itemArr[] = [ 'Rp'.Yii::$app->formatter->asDecimal($r->amount), $r->voucher, $r->note_ ];
		}
		return $this->render( 'update', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Json::encode( $itemArr ),
		] );
	}
	/**
	 * Deletes an existing Fwcms model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Fwcms model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Fwcms the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Fwcms::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
}

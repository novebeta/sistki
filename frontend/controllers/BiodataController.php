<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Bayar;
use app\models\Biodata;
use app\models\BiodataNojobSearch;
use app\models\BiodataResignSearch;
use app\models\BiodataSearch;
use app\models\BiodataWithjobSearch;
use app\models\Cabang;
use app\models\Dokumen;
use app\models\Fwcms;
use app\models\FwcmsSearch;
use app\models\Isc;
use app\models\IscSearch;
use app\models\JobOrder;
use app\models\MedikalSearch;
use app\models\Mp;
use app\models\Paspor;
use app\models\PasporSearch;
use app\models\Pra;
use app\models\Rekomid;
use app\models\RekomidSearch;
use app\models\RekomPaspor;
use app\models\RekomPasporSearch;
use app\models\RiwayatKerja;
use app\models\UserCompany;
use DateTime;
use frontend\components\Cloud;
use OSS\OssClient;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
/**
 * BiodataController implements the CRUD actions for Biodata model.
 */
class BiodataController extends BaseController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams,
			Biodata::find()->searchBiodata() );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionNojobs() {
		$searchModel  = new BiodataNojobSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'nojobs', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionNoJobsCompany() {
		$searchModel  = new BiodataNojobSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'nojobscompany', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionWithjobs() {
		$searchModel  = new BiodataWithjobSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'withjobs', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionResign() {
		$searchModel  = new BiodataResignSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'resign', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionKasus() {
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams,
			Biodata::find()->searchKasus() );
//		$dataProvider->query->where( [ '>', 'LENGTH(kasus)', 0 ] );
		return $this->render( 'kasus', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionLoan() {
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams,
			Biodata::find()->searchLoan() );
//		$dataProvider->query->where( [ '>', 'loan', 0 ] );
		return $this->render( 'loan', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionIndexcompany() {
		$u = UserCompany::find()->where( [ 'user_id' => $this->user->id ] )->count();
		if ( $u == 0 ) {
			return $this->redirect( [
				'/site/index',
				't' => md5( date( 'Y-m-d H:i:s' ) )
			] );
		}
		$query        = Biodata::find()->searchBiodataCompany();
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams, $query );
		$jobOrder     = JobOrder::findOne( \Yii::$app->session->get( 'job_order_id' ) );
		return $this->render( 'index-company', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'jobOrder'     => $jobOrder,
		] );
	}
	public function actionSelected() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$id                          = $_POST[ 'id' ];
		$status                      = $_POST[ 'checked' ];
		$bio                         = Biodata::findOne( $id );
		if ( $bio != null ) {
			if ( $status == 'OK' ) {
				$bio->company_date = new Expression( 'NOW()' );
			} else {
				$bio->company_date = new Expression( 'NULL' );
			}
			if ( $bio->save() ) {
				$bio->refresh();
				return [ 'status' => true, 'msg' => 'Berhasil disimpan.' ];
			} else {
				return [ 'status' => false, 'msg' => Html::errorSummary( $bio ) ];
			}
		} else {
			return [ 'status' => false, 'msg' => 'Data tidak ditemukan.' ];
		}
	}
	public function actionSelectJoborder() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$biodata_id                  = $_POST[ 'biodata_id' ];
		$job_order_id                = $_POST[ 'job_order_id' ];
		$bio                         = Biodata::findOne( $biodata_id );
		if ( $bio != null ) {
			if ( $job_order_id == '' ) {
				$bio->job_order_id = new Expression( 'NULL' );
			} else {
				$bio->job_order_id = $job_order_id;
			}
			if ( $bio->save() ) {
				$bio->refresh();
				return [ 'status' => true, 'msg' => 'Berhasil disimpan.' ];
			} else {
				return [ 'status' => false, 'msg' => Html::errorSummary( $bio ) ];
			}
		} else {
			return [ 'status' => false, 'msg' => 'Data tidak ditemukan.' ];
		}
	}
	/**
	 * Lists all Biodata models.
	 * @return mixed
	 */
	public function actionProses() {
		$query        = Biodata::find();
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams, $query );
		return $this->render( 'proses', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	public function actionRptBiodataProses() {
		$query        = Biodata::find();
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams, $query );
		return $this->render( 'rpt-biodata-proses', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	public function actionClear() {
		unset( $_SESSION[ "BIODATA" ] );
		return $this->redirect( [ 'biodata/index' ] );
	}
	public function actionSelect( $id ) {
		$_SESSION[ "BIODATA" ] = $id;
		return $this->redirect( [ 'biodata/index' ] );
//        $searchModel = new BiodataSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
	}
	public function actionAjax() {
		$query        = Biodata::find();
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams, $query );
		return $this->renderJson( 1, $dataProvider );
	}
	/**
	 * Displays a single Biodata model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		$_SESSION[ "BIODATA" ] = $id;
		$model                 = $this->findModel( $id );
		$pra                   = Pra::find()->where( [ 'biodata_id' => $id ] )->one();
		if ( $pra == null ) {
			$pra = new Pra;
		}
		$mp = Mp::find()->where( [ 'biodata_id' => $id ] )->one();
		if ( $mp == null ) {
			$mp = new Mp;
		}
		$searchModelMedikal                 = new MedikalSearch();
		$searchModelMedikal->biodata_id     = $id;
		$dataProviderMedikal                = $searchModelMedikal->search( Yii::$app->request->queryParams );
		$searchModelRekomid                 = new RekomidSearch();
		$searchModelRekomid->biodata_id     = $id;
		$dataProviderRekomid                = $searchModelRekomid->search( Yii::$app->request->queryParams );
		$searchModelRekomPaspor             = new RekomPasporSearch();
		$searchModelRekomPaspor->biodata_id = $id;
		$dataProviderRekomPaspor            = $searchModelRekomPaspor->search( Yii::$app->request->queryParams );
		$searchModelPaspor                  = new PasporSearch();
		$searchModelPaspor->biodata_id      = $id;
		$dataProviderPaspor                 = $searchModelPaspor->search( Yii::$app->request->queryParams );
		$searchModelFwcms                   = new FwcmsSearch();
		$searchModelFwcms->biodata_id       = $id;
		$dataProviderFwcms                  = $searchModelFwcms->search( Yii::$app->request->queryParams );
		$searchModelIsc                     = new IscSearch();
		$searchModelIsc->biodata_id         = $id;
		$dataProviderIsc                    = $searchModelIsc->search( Yii::$app->request->queryParams );
		$indo                               = RiwayatKerja::find()
		                                                  ->select( 'nama_perusahaan,lokasi,posisi,masa_kerja' )
		                                                  ->where( [
			                                                  'biodata_id' => $id,
			                                                  'tipe'       => 0
		                                                  ] )->asArray()->all();
		$malay                              = RiwayatKerja::find()
		                                                  ->select( 'nama_perusahaan,lokasi,posisi,masa_kerja' )
		                                                  ->where( [
			                                                  'biodata_id' => $id,
			                                                  'tipe'       => 1
		                                                  ] )->asArray()->all();
		$indo                               = array_map( 'array_values', $indo );
		$malay                              = array_map( 'array_values', $malay );
		return $this->render( 'view', [
			'model'                   => $model,
			'pra'                     => $pra,
			'mp'                      => $mp,
			'indo'                    => Json::encode( $indo ),
			'malay'                   => Json::encode( $malay ),
			'searchModelMedikal'      => $searchModelMedikal,
			'dataProviderMedikal'     => $dataProviderMedikal,
			'dataProviderRekomid'     => $dataProviderRekomid,
			'searchModelRekomid'      => $searchModelRekomid,
			'dataProviderRekomPaspor' => $dataProviderRekomPaspor,
			'searchModelRekomPaspor'  => $searchModelRekomPaspor,
			'dataProviderPaspor'      => $dataProviderPaspor,
			'searchModelPaspor'       => $searchModelPaspor,
			'dataProviderFwcms'       => $dataProviderFwcms,
			'searchModelFwcms'        => $searchModelFwcms,
			'dataProviderIsc'         => $dataProviderIsc,
			'searchModelIsc'          => $searchModelIsc,
		] );
	}
	/**
	 * Finds the Biodata model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Biodata the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Biodata::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
	/**
	 * Creates a new Biodata model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws Exception
	 * @throws \Exception
	 * @throws Throwable
	 */
	public function actionCreate() {
		$model = new Biodata();
		/**
		 * $post Request[]
		 */
		if ( is_string( $_POST[ 'Biodata' ][ 'siapkerja' ] ) ) {
			$_POST[ 'Biodata' ][ 'siapkerja' ] = [];
//			unset($_POST['Biodata']['siapkerja']);
		}
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = Biodata::getDb()->beginTransaction();
			try {
				$model->biodata_id = $model::generate_uuid();
				$model->tuser      = Yii::$app->user->id;
				$model->cabang_id  = $this->user->cabang_id;
				if ( ! $model->save() ) {
					throw new UserException( Html::errorSummary( $model ) );
				}
				if ( isset( $_POST[ 'riwayat_indo' ] ) ) {
					$riwayatIndo = json_decode( Yii::$app->request->post()[ 'riwayat_indo' ] );
					if ( $riwayatIndo != null ) {
						foreach ( $riwayatIndo->body as $row ) {
							if ( count( $row ) == 4 ) {
								$indo                   = new RiwayatKerja();
								$indo->riwayat_kerja_id = RiwayatKerja::generate_uuid();
								$indo->biodata_id       = $model->biodata_id;
								$indo->tipe             = 0;
								$indo->nama_perusahaan  = $row[ 0 ];
								$indo->lokasi           = $row[ 1 ];
								$indo->posisi           = $row[ 2 ];
								$indo->masa_kerja       = $row[ 3 ];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $model ) );
								}
							}
						}
					}
				}
				$model->fex = 'F';
				if ( isset( $_POST[ 'riwayat_malay' ] ) ) {
					$riwayatMalay = json_decode( Yii::$app->request->post()[ 'riwayat_malay' ] );
					if ( $riwayatMalay != null ) {
						$model->fex = 'EX';
						foreach ( $riwayatMalay->body as $row ) {
							if ( count( $row ) == 4 ) {
								$indo                   = new RiwayatKerja();
								$indo->riwayat_kerja_id = RiwayatKerja::generate_uuid();
								$indo->biodata_id       = $model->biodata_id;
								$indo->tipe             = 1;
								$indo->nama_perusahaan  = $row[ 0 ];
								$indo->lokasi           = $row[ 1 ];
								$indo->posisi           = $row[ 2 ];
								$indo->masa_kerja       = $row[ 3 ];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $model ) );
								}
							}
						}
					}
				}
				$transaction->commit();
				$image = null;
				if ( isset( $_FILES[ Dokumen::PAS_PHOTO ] ) ) {
					$image      = UploadedFile::getInstanceByName( Dokumen::PAS_PHOTO );
					$uploadPath = Yii::$app->params[ 'uploadDir' ][ 'path' ] . '/' . Dokumen::PAS_PHOTO;
					$namafile   = $model->biodata_id . ".jpg";
					if ( ! is_null( $image ) ) {
						Yii::$app->params[ 'uploadPath' ] = realpath( Yii::$app->basePath ) . DS . 'web' . DS . $uploadPath;
						if ( ! file_exists( Yii::$app->params[ 'uploadPath' ] ) ) {
							mkdir( Yii::$app->params[ 'uploadPath' ], 0777, true );
						}
						$path = Yii::$app->params[ 'uploadPath' ] . DS . $namafile;
						$image->saveAs( $path );
					}
				}
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
				] );
//				throw $e;
			} catch ( Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
//				throw $e;
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
//			return $this->redirect( [ 'view', 'id' => $model->biodata_id ] );
			return $this->redirect( [
				'view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_1'
			] );
		}
		if ( ! Yii::$app->user->can( 'akes_tab' ) ) {
			$cabang = Cabang::find()->where( [ 'cabang_id' => $this->user->cabang_id ] )->one();
			if ( $cabang != null ) {
				$model->pl = $cabang->nama;
			}
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	public function actionGaji( $biodata_id ) {
		$this->layout = '//plain';
		/** @var Biodata $model */
		$model = Biodata::find()->where( [ 'biodata_id' => $biodata_id ] )->one();
		return $this->render( 'gaji', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Biodata model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws Exception
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Exception
	 * @throws Throwable
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = Biodata::getDb()->beginTransaction();
			try {
				if ( $model->status_proses == '' ) {
					unset( $model->status_proses );
				}
				$model->tuser = Yii::$app->user->id;
				if ( ! $model->save() ) {
					throw new UserException( Html::errorSummary( $model ) );
				}
				if ( isset( Yii::$app->request->post()[ 'riwayat_indo' ] ) ) {
					RiwayatKerja::deleteAll( [ 'biodata_id' => $model->biodata_id, 'tipe' => 0 ] );
					$riwayatIndo = json_decode( Yii::$app->request->post()[ 'riwayat_indo' ] );
					foreach ( $riwayatIndo->body as $row ) {
						if ( count( $row ) == 4 ) {
							$indo                   = new RiwayatKerja();
							$indo->riwayat_kerja_id = RiwayatKerja::generate_uuid();
							$indo->biodata_id       = $model->biodata_id;
							$indo->tipe             = 0;
							$indo->nama_perusahaan  = $row[ 0 ];
							$indo->lokasi           = $row[ 1 ];
							$indo->posisi           = $row[ 2 ];
							$indo->masa_kerja       = $row[ 3 ];
							if ( ! $indo->save() ) {
								throw new UserException( Html::errorSummary( $model ) );
							}
						}
					}
				}
				if ( isset( Yii::$app->request->post()[ 'riwayat_malay' ] ) ) {
					RiwayatKerja::deleteAll( [ 'biodata_id' => $model->biodata_id, 'tipe' => 1 ] );
					$riwayatMalay = json_decode( Yii::$app->request->post()[ 'riwayat_malay' ] );
					foreach ( $riwayatMalay->body as $row ) {
						if ( count( $row ) == 4 ) {
							$indo                   = new RiwayatKerja();
							$indo->riwayat_kerja_id = RiwayatKerja::generate_uuid();
							$indo->biodata_id       = $model->biodata_id;
							$indo->tipe             = 1;
							$indo->nama_perusahaan  = $row[ 0 ];
							$indo->lokasi           = $row[ 1 ];
							$indo->posisi           = $row[ 2 ];
							$indo->masa_kerja       = $row[ 3 ];
							if ( ! $indo->save() ) {
								throw new UserException( Html::errorSummary( $model ) );
							}
						}
					}
				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
//				return $this->render( 'create', [
//					'model' => $model,
//				] );
			} catch ( Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
//				return $this->render( 'create', [
//					'model' => $model,
//				] );
			}
		}
//		$model->refresh();
		return $this->redirect( [
			'view',
			'id' => $model->biodata_id,
			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_1'
		] );
//		return $this->render( 'create', [
//			'model' => $model,
//		] );
	}
	public function actionVisa() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model = $this->findModel( $_SESSION[ "BIODATA" ] );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			Yii::$app->getSession()->setFlash( 'notif', Yii::t( 'app', 'success save' ) );
			return $this->redirect( [ 'biodata/index' ] );
		}
		return $this->render( 'visa', [
			'model' => $model,
		] );
	}
	public function actionRangkuman() {
		$this->layout = "//plain";
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model = $this->findModel( $_SESSION[ "BIODATA" ] );
		return $this->render( 'rangkuman-biaya', [
			'model' => $model,
		] );
	}
	public function actionEndorse() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		/** @var Biodata $model */
		$model = $this->findModel( $_SESSION[ "BIODATA" ] );
		if ( $model->no_visa == null ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan no visa di isi terlebih dahulu...' );
			return $this->redirect( [ 'biodata/visa' ] );
		}
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			Yii::$app->getSession()->setFlash( 'notif', Yii::t( 'app', 'success save' ) );
			return $this->redirect( [ 'biodata/index' ] );
		}
		return $this->render( 'endorse', [
			'model' => $model,
		] );
	}
	public function actionPap() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model = $this->findModel( $_SESSION[ "BIODATA" ] );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			Yii::$app->getSession()->setFlash( 'notif', Yii::t( 'app', 'success save' ) );
			return $this->redirect( [ 'biodata/index' ] );
		}
		return $this->render( 'pap', [
			'model' => $model,
		] );
	}
	public function actionMundur() {
		$searchModel  = new BiodataSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'mundur', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	public function actionJadwalTerbang() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model = $this->findModel( $_SESSION[ "BIODATA" ] );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			Yii::$app->getSession()->setFlash( 'notif', Yii::t( 'app', 'success save' ) );
			return $this->redirect( [ 'biodata/index' ] );
		}
		return $this->render( 'jadwal-terbang', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Biodata model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Exception
	 * @throws Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDeletePhoto( $id ) {
		$bucket       = Cloud::getBucketName();
		$ossClient    = Cloud::getOssClient();
		$object       = Yii::$app->params[ 'uploadDir' ][ 'path' ] . DS . Dokumen::PAS_PHOTO . DS . $id . '.jpg';
		$object_thumb = Yii::$app->params[ 'uploadDir' ][ 'path' ] . DS . Dokumen::PAS_PHOTO . DS . 'thumb' . DS . $id . '.jpg';
		$result       = $ossClient->deleteObject( $bucket, $object );
		$result       = $ossClient->deleteObject( $bucket, $object_thumb );
		return $this->redirect( [
			'view',
			'id' => $id,
			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_1'
		] );
	}
	public function actionJoborderDelete() {
		if ( isset( $_POST[ 'id' ] ) ) {
			$bio = Biodata::findOne( $_POST[ 'id' ] );
			if ( $bio != null ) {
				$bio->job_order_id = null;
				$bio->save();
			}
			$this->redirect( [ 'biodata/indexcompany', 'page' => $_POST[ 'page' ] ] );
		} else {
			$this->redirect( [ 'biodata/indexcompany' ] );
		}
	}
	public function actionDelete( $id ) {
		try {
			$bucket    = Cloud::getBucketName();
			$ossClient = Cloud::getOssClient();
			foreach ( Dokumen::JENIS_ARR as $item ) {
				$object = Yii::$app->params[ 'uploadDir' ][ 'path' ] . DS . $item . DS . $id . '.jpg';
				$ossClient->deleteObject( $bucket, $object );
				$object = Yii::$app->params[ 'uploadDir' ][ 'path' ] . DS . $item . DS . $id . '.pdf';
				$ossClient->deleteObject( $bucket, $object );
				$object_thumb = Yii::$app->params[ 'uploadDir' ][ 'path' ] . DS . $item . DS . 'thumb' . DS . $id . '.jpg';
				$ossClient->deleteObject( $bucket, $object_thumb );
			}
			Bayar::deleteAll( [ 'biodata_id' => $id ] );
			Dokumen::deleteAll( [ 'biodata_id' => $id ] );
			Fwcms::deleteAll( [ 'biodata_id' => $id ] );
			Isc::deleteAll( [ 'biodata_id' => $id ] );
			Mp::deleteAll( [ 'biodata_id' => $id ] );
			Paspor::deleteAll( [ 'biodata_id' => $id ] );
			Pra::deleteAll( [ 'biodata_id' => $id ] );
			Rekomid::deleteAll( [ 'biodata_id' => $id ] );
			RekomPaspor::deleteAll( [ 'biodata_id' => $id ] );
			RiwayatKerja::deleteAll( [ 'biodata_id' => $id ] );
			$this->findModel( $id )->delete();
		} catch ( StaleObjectException $e ) {
			return $e->getMessage();
		} catch ( NotFoundHttpException $e ) {
			return $e->getMessage();
		} catch ( Throwable $e ) {
			return $e->getMessage();
		}
		return $this->redirect( [ 'index' ] );
	}
//	public function actionUpload() {
//		$post = $_POST;
//		/** @var UploadedFile $image */
//		$image      = null;
//		$uploadPath = '';
//		$namafile   = '';
//		$path       = '';
//		if ( isset( $_FILES['pas_foto'] ) ) {
//			$image      = UploadedFile::getInstanceByName( 'pas_foto' );
//			$uploadPath = Yii::$app->params['uploadDir']['pas_foto'];
//			$namafile   = $post['biodata_id'] . ".jpg";
//		}
//		if ( ! is_null( $image ) ) {
//			Yii::$app->params['uploadPath'] = realpath( Yii::$app->basePath ) . DS . 'web' . DS . $uploadPath;
//			$path                           = Yii::$app->params['uploadPath'] . DS . $namafile;
//			$image->saveAs( $path );
//		}
//		return Json::encode( [ 'result' => true ] );
//	}
	public function actionList( $q = null, $id = null ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out                         = [ 'results' => [ 'id' => '', 'text' => '' ] ];
		$query                       = new Query;
		if ( ! is_null( $q ) ) {
			$query->select( 'biodata_id as id,name as text' )
			      ->from( 'nscc_biodata' )
			      ->where( [ 'like', 'name', $q ] )
			      ->orderBy( 'name' )
			      ->limit( 20 );
			$command          = $query->createCommand();
			$data             = $command->queryAll();
			$out[ 'results' ] = array_values( $data );
		} elseif ( $id > 0 ) {
			$out[ 'results' ] = [ 'id' => $id, 'text' => Biodata::find( $id )->name ];
		}
		return $out;
	}
	/**
	 * @param $id
	 *
	 * @return string
	 * @throws \PhpOffice\PhpWord\Exception\CopyFileException
	 * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionPrint( $id ) {
		$bucket       = Cloud::getBucketName();
		$ossClient    = Cloud::getOssClient();
		$object_thumb = Yii::$app->params[ 'uploadDir' ][ 'path' ] . DS . Dokumen::PAS_PHOTO . DS . 'thumb' . DS . $id . '.jpg';
		$localfile    = Yii::getAlias( '@runtime' ) . DS . $id . '.jpg';
		$options      = [
			OssClient::OSS_FILE_DOWNLOAD => $localfile,
		];
		try {
			$biodata = Biodata::find()->where( [ 'biodata_id' => $id ] )->one();
			if ( $biodata == null ) {
				throw new UserException( 'Biodata tidak ditemukan' );
			}
			$path              = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
			                               'views' . DIRECTORY_SEPARATOR . 'biodata' .
			                               DIRECTORY_SEPARATOR . 'biodata.docx' );
			$formatter         = \Yii::$app->formatter;
			$formatter->locale = 'id-ID';
			$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor( $path );
			$templateProcessor->setValue( 'no_absen', $biodata->no_absen );
			$templateProcessor->setValue( 'no_hp', $biodata->no_hp );
			$templateProcessor->setValue( 'suku', $biodata->suku );
			$templateProcessor->setValue( 'age', $biodata->age );
			$templateProcessor->setValue( 'agama', $biodata->agama );
			$templateProcessor->setValue( 'ayah', $biodata->ayah );
			$templateProcessor->setValue( 'tb', $biodata->tb );
			$templateProcessor->setValue( 'bb', $biodata->bb );
			$templateProcessor->setValue( 'sekolah', $biodata->sekolah );
			$templateProcessor->setValue( 'jurusan', $biodata->jurusan );
			$templateProcessor->setValue( 'lulus', $biodata->lulus );
			$templateProcessor->setValue( 'punya_sodara', $biodata->punya_sodara == '1' ? 'Yes' : 'No' );
			$templateProcessor->setValue( 'kontrak', $biodata->siap_kontrak == '1' ? 'Yes' : 'No' );
			$templateProcessor->setValue( 'siap_ikut_aturan', $biodata->siap_ikut_aturan == '1' ? 'Yes' : 'No' );
			$templateProcessor->setValue( 'hari', $formatter->asDate( $biodata->tdate, 'cccc' ) );
			$templateProcessor->setValue( 'tgl', $formatter->asDate( $biodata->tdate, 'dd' ) );
			$templateProcessor->setValue( 'bulan', $formatter->asDate( $biodata->tdate, 'MMMM' ) );
			$templateProcessor->setValue( 'tahun', $formatter->asDate( $biodata->tdate, 'yyyy' ) );
			$templateProcessor->setValue( 'name', strtoupper( $biodata->name ) );
			$templateProcessor->setValue( 'paspor', strtoupper( $biodata->no_paspor ) );
			$lastPaspor = $biodata->lastPaspor;
			$imigrasi   = $masa_berlaku = '';
			if ( $lastPaspor != null ) {
				$imigrasi = $lastPaspor->imigrasi;
				if ( $lastPaspor->tgl != null ) {
					$date = new DateTime( $lastPaspor->tgl );
					$date->modify( '+5 years' );
					$masa_berlaku = $formatter->asDate( $date, 'dd-MM-yyyy' );
				}
			}
			$templateProcessor->setValue( 'imigrasi', strtoupper( $imigrasi ) );
			$templateProcessor->setValue( 'masa_berlaku', $masa_berlaku );
			$templateProcessor->setValue( 'tempat_lahir', strtoupper( $biodata->tempat_lahir ) );
			$templateProcessor->setValue( 'tgl_lahir', $formatter->asDate( $biodata->birthdate, 'dd-MM-yyyy' ) );
			$templateProcessor->setValue( 'bio_address', strtoupper( $biodata->address ) );
			$templateProcessor->setValue( 'bio_kecamatan', strtoupper( $biodata->kecamatan ) );
			$templateProcessor->setValue( 'bio_kabupaten', strtoupper( $biodata->kabupaten ) );
			$templateProcessor->setValue( 'bio_propinsi', strtoupper( $biodata->propinsi ) );
			$jobOrder = $biodata->jobOrder;
			if ( $jobOrder != null ) {
				$templateProcessor->setValue( 'job_title', $jobOrder->job_title );
				$templateProcessor->setValue( 'durasi_kontrak', $jobOrder->durasi_kontrak );
				$templateProcessor->setValue( 'basic_salary', $jobOrder->basic_salary );
				$company = $jobOrder->company;
				if ( $company != null ) {
					$templateProcessor->setValue( 'cauntry_caps', strtoupper( $company->country ) );
					$templateProcessor->setValue( 'company_name', $company->name );
					$templateProcessor->setValue( 'address', $company->address );
					$templateProcessor->setValue( 'city', $company->city );
					$templateProcessor->setValue( 'country', $company->country );
				} else {
					$templateProcessor->setValue( 'cauntry_caps', '' );
					$templateProcessor->setValue( 'company_name', '' );
					$templateProcessor->setValue( 'address', '' );
					$templateProcessor->setValue( 'city', '' );
					$templateProcessor->setValue( 'country', '' );
				}
			} else {
				$templateProcessor->setValue( 'job_title', '' );
				$templateProcessor->setValue( 'durasi_kontrak', '' );
				$templateProcessor->setValue( 'basic_salary', '' );
			}
			$inline = new TextRun();
			$style  = [ 'name' => 'calibri', 'size' => 9 ];
			$inline->addText( 'SD', ( $biodata->pendidikan == 'SD' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$inline->addText( ' / ', $style );
			$inline->addText( 'SMP', ( $biodata->pendidikan == 'SMP' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$inline->addText( ' / ', $style );
			$inline->addText( 'SMU', ( $biodata->pendidikan == 'SMU' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$inline->addText( ' / ', $style );
			$inline->addText( 'SMK', ( $biodata->pendidikan == 'SMK' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$inline->addText( ' / ', $style );
			$inline->addText( 'D1', ( $biodata->pendidikan == 'D1' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$inline->addText( ' / ', $style );
			$inline->addText( 'D3', ( $biodata->pendidikan == 'D3' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$inline->addText( ' / ', $style );
			$inline->addText( 'S1', ( $biodata->pendidikan == 'S1' ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$templateProcessor->setComplexValue( 'sd', $inline );
			$siapKerja    = new TextRun();
			$style1       = [ 'name' => 'calibri', 'size' => 9 ];
			$style2       = [ 'name' => 'calibri', 'size' => 8 ];
			$siapKerjaarr = explode( ',', $biodata->siap_kerja );
			$siapKerja->addText( 'RAJIN ', ( in_array( 'Rajin', $siapKerjaarr ) ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$siapKerja->addText( 'Diligent', ( in_array( 'Rajin', $siapKerjaarr ) ) ? array_merge( $style2, [ 'color' => '0008ff' ] ) : $style2 );
			$siapKerja->addText( ' / ', $style1 );
			$siapKerja->addText( 'DISIPLIN ', ( in_array( 'Disiplin', $siapKerjaarr ) ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$siapKerja->addText( 'Discipline', ( in_array( 'Disiplin', $siapKerjaarr ) ) ? array_merge( $style2, [ 'color' => '0008ff' ] ) : $style2 );
			$siapKerja->addText( ' / ', $style1 );
			$siapKerja->addText( 'KERJA KERAS ', ( in_array( 'KerjaKeras', $siapKerjaarr ) ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$siapKerja->addText( 'Hard working', ( in_array( 'KerjaKeras', $siapKerjaarr ) ) ? array_merge( $style2, [ 'color' => '0008ff' ] ) : $style2 );
			$siapKerja->addText( ' / ', $style1 );
			$siapKerja->addText( 'DIPERINTAH ', ( in_array( 'Diperintah', $siapKerjaarr ) ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$siapKerja->addText( 'Obedient', ( in_array( 'Diperintah', $siapKerjaarr ) ) ? array_merge( $style2, [ 'color' => '0008ff' ] ) : $style2 );
			$siapKerja->addText( ' / ', $style1 );
			$siapKerja->addText( 'SETIAP HARI KERJA 12 JAM ', ( in_array( 'TiapHari', $siapKerjaarr ) ) ? array_merge( $style, [ 'color' => '0008ff' ] ) : $style );
			$siapKerja->addText( 'Working for 12 hours everyday ', ( in_array( 'TiapHari', $siapKerjaarr ) ) ? array_merge( $style2, [ 'color' => '0008ff' ] ) : $style2 );
			$templateProcessor->setComplexValue( 'siapkerja', $siapKerja );
			$localfile_uncek = Yii::getAlias( '@webroot' ) . DS . 'images/uncek.png';
			$localfile_cek   = Yii::getAlias( '@webroot' ) . DS . 'images/cek.png';
			$templateProcessor->setImageValue( 'Married', [
				'path'   => ( $biodata->nikah == 'Married' ) ? $localfile_cek : $localfile_uncek,
				'width'  => 16,
				'height' => 16,
				'ration' => false
			] );
			$templateProcessor->setImageValue( 'Single', [
				'path'   => ( $biodata->nikah == 'Single' ) ? $localfile_cek : $localfile_uncek,
				'width'  => 16,
				'height' => 16,
				'ration' => false
			] );
			$templateProcessor->setImageValue( 'Divorce', [
				'path'   => ( $biodata->nikah == 'Divorce' ) ? $localfile_cek : $localfile_uncek,
				'width'  => 16,
				'height' => 16,
				'ration' => false
			] );
			$indos   = [];
			$counter = 1;
			foreach ( $biodata->getIndos() as $row ) {
				$indos[] = [
					'indo_company' => $counter . '. ' . $row->nama_perusahaan,
					'indo_loc'     => $row->lokasi,
					'indo_pos'     => $row->posisi,
					'indo_masa'    => $row->masa_kerja,
				];
			}
			if ( sizeof( $indos ) == 0 ) {
				$indos = [
					[
						'indo_company' => '1. ',
						'indo_loc'     => '',
						'indo_pos'     => '',
						'indo_masa'    => '',
					],
					[
						'indo_company' => '2. ',
						'indo_loc'     => '',
						'indo_pos'     => '',
						'indo_masa'    => '',
					]
				];
			}
			$templateProcessor->cloneRowAndSetValues( 'indo_company', $indos );
			$malays  = [];
			$counter = 1;
			foreach ( $biodata->getMalays() as $row ) {
				$malays[] = [
					'malay_company' => $counter . '. ' . $row->nama_perusahaan,
					'malay_loc'     => $row->lokasi,
					'malay_pos'     => $row->posisi,
					'malay_masa'    => $row->masa_kerja,
				];
			}
			if ( sizeof( $malays ) == 0 ) {
				$malays = [
					[
						'malay_company' => '1. ',
						'malay_loc'     => '',
						'malay_pos'     => '',
						'malay_masa'    => '',
					],
					[
						'malay_company' => '2. ',
						'malay_loc'     => '',
						'malay_pos'     => '',
						'malay_masa'    => '',
					]
				];
			}
			$templateProcessor->cloneRowAndSetValues( 'malay_company', $malays );
			$exist = $ossClient->doesObjectExist( $bucket, $object_thumb );
			if ( $exist !== false ) {
				$ossClient->getObject( $bucket, $object_thumb, $options );
				$templateProcessor->setImageValue( 'photo', [
					'path'   => $localfile,
					'width'  => 152,
					'height' => 200,
					'ration' => false
				] );
			} else {
				$templateProcessor->setValue( 'photo', '' );
			}
			$filename = strtoupper( str_replace( ' ', '_', 'BIODATA_' . $biodata->name ) ) . ".docx";
//			$templateProcessor->saveAs( Yii::getAlias( '@runtime' ) . DS . $filename );
			ob_start();
			header( "Content-Description: File Transfer" );
			header( "Content-Disposition: attachment; filename=" . $filename );
			header( 'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document' );
			header( 'Content-Transfer-Encoding: binary' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Expires: 0' );
			$templateProcessor->saveAs( 'php://output' );
			return ob_get_clean();
//			flush();
//			readfile( Yii::getAlias( '@runtime' ) . DS . $filename );
//			unlink( Yii::getAlias( '@runtime' ) . DS . $filename );
		} catch ( CopyFileException $e ) {
			return $e->getMessage();
		} catch ( CreateTemporaryFileException $e ) {
			return $e->getMessage();
		} catch ( InvalidConfigException $e ) {
			return $e->getMessage();
		} catch ( UserException $e ) {
			return $e->getMessage();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}

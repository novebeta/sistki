<?php
namespace frontend\controllers;
use app\models\Mp;
use app\models\MpSearch;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * MpController implements the CRUD actions for Mp model.
 */
class MpController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Mp models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new MpSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Mp model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Mp model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function actionCreate( $id ) {
		$model = new Mp();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$model->mp_id      = Mp::generate_uuid();
			$model->biodata_id = $id;
			if ( ! $model->save() ) {
				throw new Exception( 'Mp ' . Html::errorSummary( $model ) );
			}
		}
		return $this->redirect( [
			'biodata/view',
			'id' => $model->biodata_id,
			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_18'
		] );
	}
	/**
	 * Updates an existing Mp model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found*@throws Exception
	 * @throws Exception
	 */
	public function actionUpdate( $id ) {
		$model = Mp::find()->where( [ 'biodata_id' => $id ] )->one();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				throw new Exception( 'Mp ' . Html::errorSummary( $model ) );
			}
		}
		return $this->redirect( [
			'biodata/view',
			'id' => $model->biodata_id,
			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_18'
		] );
	}
	/**
	 * Deletes an existing Mp model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Mp model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Mp the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Mp::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}

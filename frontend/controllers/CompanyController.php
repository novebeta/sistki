<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Company;
use app\models\CompanySearch;
use app\models\JobOrder;
use frontend\components\Cloud;
use OSS\Core\OssException;
use Yii;
use yii\bootstrap\BaseHtml;
use yii\bootstrap\Html;
use yii\db\Query;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends BaseController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Company models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new CompanySearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Company model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Company model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model   = new Company();
		$options = null;
		if ( $model->load( Yii::$app->request->post() ) ) {
			$model->company_id = self::generate_uuid();
			if ( $model->save() ) {
				Yii::$app->getSession()->setFlash( 'success', 'Save Success!' );
				return $this->redirect( [ 'company/view', 'id' => $model->company_id ] );
			} else {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
		} else {
			return $this->render( 'create', [
				'model' => $model,
			] );
		}
	}
	/**
	 * Updates an existing Company model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $model->save() ) {
				Yii::$app->getSession()->setFlash( 'success', 'Save Success!' );
			} else {
				Yii::$app->getSession()->setFlash( 'failed', BaseHtml::errorSummary( $model ) );
			}
			return $this->redirect( [ 'view', 'id' => $model->company_id ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Company model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDeletePhoto() {
		$bucket    = Cloud::getBucketName();
		$ossClient = Cloud::getOssClient();
		$result    = $ossClient->deleteObject( $bucket, $_POST['key'] );
		return Json::encode( [ 'result' => $result['x-oss-request-id'] ] );
	}
	public function actionDelete( $id ) {
		$bucket     = Cloud::getBucketName();
		$ossClient  = Cloud::getOssClient();
		$prefix     = 'company/' . $id . '/';
		$delimiter  = '/';
		$nextMarker = '';
		$maxkeys    = 1000;
		$options    = array(
			'delimiter' => $delimiter,
			'prefix'    => $prefix,
			'max-keys'  => $maxkeys,
			'marker'    => $nextMarker,
		);
		try {
//			$company = Company::findOne($id);
			$exist = $ossClient->doesObjectExist( $bucket, 'company' );
			if ( $exist !== false ) {
				$exist = $ossClient->doesObjectExist( $bucket, $prefix );
				if ( $exist ) {
					$listObjectInfo = $ossClient->listObjects( $bucket, $options );
					$objectList     = $listObjectInfo->getObjectList();
					if ( ! empty( $objectList ) ) {
						foreach ( $objectList as $objectInfo ) {
							$ossClient->deleteObject( $bucket, $objectInfo->getKey() );
						}
					}
				}
			}
			JobOrder::deleteAll( [ 'company_id' => $id ] );
			$this->findModel( $id )->delete();
			return $this->redirect( [ 'index' ] );
		} catch ( OssException $e ) {
			Yii::error( $e->getMessage(), 'cloud' );
			Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
		} catch ( NotFoundHttpException $e ) {
			Yii::error( $e->getMessage(), 'cloud' );
			Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
		} catch ( StaleObjectException $e ) {
			Yii::error( $e->getMessage(), 'cloud' );
			Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
		} catch ( \Throwable $e ) {
			Yii::error( $e->getMessage(), 'cloud' );
			Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
		}
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Company model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Company the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Company::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
	public function actionList( $q = null, $id = null ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out                         = [ 'results' => [ 'id' => '', 'text' => '' ] ];
		$query                       = new Query;
		if ( ! is_null( $q ) ) {
			$query->select( 'company_id as id,name as text' )
			      ->from( 'nscc_company' )
			      ->where( [ 'like', 'name', $q ] )
			      ->orderBy( 'name' )
			      ->limit( 20 );
			$command        = $query->createCommand();
			$data           = $command->queryAll();
			$out['results'] = array_values( $data );
		} elseif ( $id > 0 ) {
			$out['results'] = [ 'id' => $id, 'text' => Company::find( $id )->name ];
		}
		return $out;
	}
	public function actionUpload() {
		$post       = $_POST;
		$type_image = '';
		/** @var UploadedFile $image */
		$image      = null;
		$uploadPath = '';
		$namafile   = '';
		$path       = '';
		$bucket     = Cloud::getBucketName();
		$ossClient  = Cloud::getOssClient();
		$object     = 'company';
		$options    = null;
		try {
			$exist = $ossClient->doesObjectExist( $bucket, $object );
			if ( $exist === false ) {
				$ossClient->createObjectDir( $bucket, $object );
			}
			$object .= '/' . $post['company_id'];
			$exist  = $ossClient->doesObjectExist( $bucket, $object );
			if ( $exist === false ) {
				$ossClient->createObjectDir( $bucket, $object );
			}
			foreach ( $_FILES['photos']['name'] as $key => $item ) {
				$name          = $_FILES['photos']['name'][ $key ];
				$tmp           = $_FILES['photos']['tmp_name'][ $key ];
				$imageFileType = strtolower( pathinfo( $name, PATHINFO_EXTENSION ) );
//				$image      = UploadedFile::getInstanceByName( $item );
//				$uploadPath = $object;
				if ( $imageFileType != 'pdf' ) {
					$imageFileType = 'jpg';
				}
				$namafile = self::generate_uuid() . '.' . $imageFileType;
				if ( trim( $name ) != '' ) {
					$path = $object . DS . $namafile;
					move_uploaded_file( $tmp, Yii::getAlias( '@runtime' ) . DS . $namafile );
//					$image->saveAs( Yii::getAlias( '@runtime' ) . DS . $namafile );
					$ossClient->uploadFile( $bucket, $path, Yii::getAlias( '@runtime' ) . DS . $namafile, $options );
					unlink( Yii::getAlias( '@runtime' ) . DS . $namafile );
				}
			}
		} catch ( OssException $e ) {
			return Json::encode( [ 'error' => $e->getMessage() ] );
		}
		return Json::encode( [ 'result' => true ] );
	}
}

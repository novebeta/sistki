<?php
namespace frontend\controllers;
use app\models\Biodata;
use app\models\Dokumen;
use app\models\DokumenUpload;
use app\models\DokumenUploadSearch;
use frontend\components\Cloud;
use OSS\Core\OssException;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
/**
 * DokumenUploadController implements the CRUD actions for DokumenUpload model.
 */
class DokumenUploadController extends Controller {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all DokumenUpload models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new DokumenUploadSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	public function actionBiodata() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$this->layout = "//plain";
		return $this->render( 'biodata', [
			'model' => Biodata::findOne( $_SESSION[ "BIODATA" ] )
		] );
	}
	/**
	 * Displays a single DokumenUpload model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new DokumenUpload model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new DokumenUpload();
		$model->load( Yii::$app->request->post() );
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing DokumenUpload model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
//        $model = $this->findModel($id);
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->dokumen_upload_id]);
//        }
		$model = DokumenUpload::find()->where( 'biodata_id = :biodata_id',
			[ ':biodata_id' => $id ] )->one();
		if ( $model == null ) {
			$model             = new DokumenUpload();
			$model->biodata_id = $id;
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	public function actionUpload() {
		$post       = $_POST;
		$type_image = '';
		/** @var UploadedFile $image */
		$image      = null;
		$uploadPath = '';
		$namafile   = '';
		$path       = '';
		$bucket     = Cloud::getBucketName();
		$ossClient  = Cloud::getOssClient();
		$object     = Yii::$app->params['uploadDir']['path'];
		$options    = null;
		try {
			$exist = $ossClient->doesObjectExist( $bucket, $object );
			if ( $exist === false ) {
				$ossClient->createObjectDir( $bucket, $object );
			}
			foreach ( Dokumen::JENIS_ARR as $item ) {
				if ( isset( $_FILES[ $item ] ) ) {
					$image         = $_FILES[ $item ]['name'];
					$tmp           = $_FILES[ $item ]['tmp_name'];
					$imageFileType = strtolower( pathinfo( $image, PATHINFO_EXTENSION ) );
					if ( $imageFileType != 'pdf' ) {
						$imageFileType = 'jpg';
					}
//					$image      = UploadedFile::getInstanceByName( $item );
					$uploadPath = $object . '/' . $item;
					$namafile   = $post['biodata_id'] . '.' . $imageFileType;
					$type_image = $item;
				}
			}
			if ( trim( $image ) != null ) {
				Yii::$app->params['uploadPath']      = $uploadPath;
				Yii::$app->params['uploadthumbPath'] = Yii::$app->params['uploadPath'] . '/thumb';
				$exist                               = $ossClient->doesObjectExist( $bucket, Yii::$app->params['uploadPath'] );
				if ( $exist === false ) {
					$ossClient->createObjectDir( $bucket, Yii::$app->params['uploadPath'] );
				}
				$exist = $ossClient->doesObjectExist( $bucket, Yii::$app->params['uploadthumbPath'] );
				if ( $exist === false ) {
					$ossClient->createObjectDir( $bucket, Yii::$app->params['uploadthumbPath'] );
				}
				$path      = Yii::$app->params['uploadPath'] . DS . $namafile;
				$pathThumb = Yii::$app->params['uploadthumbPath'] . DS . $namafile;
//				$image->saveAs( Yii::getAlias( '@runtime' ) . DS . $namafile );
				move_uploaded_file( $tmp, Yii::getAlias( '@runtime' ) . DS . $namafile );
				if ( $type_image != Dokumen::PERJANJIAN_KERJA && $type_image != Dokumen::PERJANJIAN_PENEMPATAN ) {
					if ( $type_image == Dokumen::PAS_PHOTO ) {
						Image::thumbnail( Yii::getAlias( '@runtime' ) . DS . $namafile, 152, 200 )
						     ->save( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, [ 'quality' => 80 ] );
					} else {
						Image::thumbnail( Yii::getAlias( '@runtime' ) . DS . $namafile, 290, 200 )
						     ->save( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, [ 'quality' => 80 ] );
					}
					$ossClient->uploadFile( $bucket, $pathThumb, Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, $options );
					unlink( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile );
				}
				$ossClient->uploadFile( $bucket, $path, Yii::getAlias( '@runtime' ) . DS . $namafile, $options );
				unlink( Yii::getAlias( '@runtime' ) . DS . $namafile );
			}
		} catch ( OssException $e ) {
			return Json::encode( [ 'error' => $e->getMessage() ] );
		}
		return Json::encode( [ 'result' => true ] );
//	    if ($model->save()) {
//            return Json::encode(['result' => true]); //return $this->redirect(['view', 'id' => $model->dokumen_upload_id]);
//        } else {
//            var_dump($model->getErrors());
//            die();
//        }
	}
	public function actionDeleteObject() {
		try {
			$item      = $_POST['item'];
			$id        = $_POST['id'];
			$bucket    = Cloud::getBucketName();
			$ossClient = Cloud::getOssClient();
			$object    = Yii::$app->params['uploadDir']['path'] . DS . $item . DS . $id . '.jpg';
			$ossClient->deleteObject( $bucket, $object );
			$object = Yii::$app->params['uploadDir']['path'] . DS . $item . DS . $id . '.pdf';
			$ossClient->deleteObject( $bucket, $object );
			$object_thumb = Yii::$app->params['uploadDir']['path'] . DS . $item . DS . 'thumb' . DS . $id . '.jpg';
			$ossClient->deleteObject( $bucket, $object_thumb );
		} catch ( OssException $e ) {
			Yii::error( $e->getMessage(), 'error' );
		}
		return $this->redirect( [
			'biodata/view',
			'id' => $id,
//			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_12'
		] );
	}
	/**
	 * Deletes an existing DokumenUpload model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the DokumenUpload model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return DokumenUpload the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = DokumenUpload::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
}

<?php
namespace frontend\controllers;
use app\models\Pra;
use app\models\PraSearch;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * PraController implements the CRUD actions for Pra model.
 */
class PraController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Pra models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new PraSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Pra model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Pra model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws Exception
	 */
	public function actionCreate( $id ) {
		$model = new Pra();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$model->pra_id     = Pra::generate_uuid();
			$model->biodata_id = $id;
			if ( ! $model->save() ) {
				throw new Exception( 'Pra ' . Html::errorSummary( $model ) );
			}
		}
		return $this->redirect( [
			'biodata/view',
			'id' => $model->biodata_id,
			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_17'
		] );
	}
	/**
	 * Updates an existing Pra model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws Exception
	 */
	public function actionUpdate( $id ) {
		$model = Pra::find()->where( [ 'biodata_id' => $id ] )->one();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				throw new Exception( 'Pra ' . Html::errorSummary( $model ) );
			}
		}
		return $this->redirect( [
			'biodata/view',
			'id' => $model->biodata_id,
			't'  => md5( date( 'Y-m-d H:i:s' ) ),
			'#'  => 'tab_17'
		] );
	}
	/**
	 * Deletes an existing Pra model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Pra model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Pra the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Pra::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}

<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Biodata;
use app\models\Rekomid;
use app\models\RekomidSearch;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * RekomidController implements the CRUD actions for Rekomid model.
 */
class RekomidController extends BaseController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Rekomid models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$this->layout = "//plain";
		$searchModel  = new RekomidSearch();
		$searchModel->biodata_id     = $_SESSION[ "BIODATA" ];
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModelRekomid'  => $searchModel,
			'dataProviderRekomid' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Rekomid model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		return $this->render( 'view', [
			'biodata' => $biodata,
			'model'   => $model,
		] );
	}
	/**
	 * Creates a new Rekomid model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$biodata         = Biodata::find()->andWhere( [ 'biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
		$model           = new Rekomid();
		$model->rbb_duta = 'DUTA';
		if ( $model->load( Yii::$app->request->post() ) ) {
			$model->rekom_id   = $model::generate_uuid();
			$model->biodata_id = $_SESSION[ "BIODATA" ];
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
				] );
			}
//			return $this->redirect( [ 'biodata/view', 'id' => $model->biodata_id ] )
			return $this->redirect( [
				'biodata/view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_3'
			] );
		}
		return $this->render( 'create', [
			'biodata' => $biodata,
			'model'   => $model,
		] );
	}
	/**
	 * Updates an existing Rekomid model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'update', [
					'biodata' => $biodata,
					'model'   => $model,
				] );
			}
			return $this->redirect( [
				'biodata/view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_3'
			] );
		}
		return $this->render( 'update', [
			'biodata' => $biodata,
			'model'   => $model,
		] );
	}
	/**
	 * Deletes an existing Rekomid model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Rekomid model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Rekomid the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Rekomid::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
	/**
	 * @param $id
	 *
	 */
	public function actionPrint( $id ) {
		try {
			$biodata = Biodata::find()->where( [ 'biodata_id' => $id ] )->one();
			if ( $biodata == null ) {
				throw new UserException( 'Biodata tidak ditemukan' );
			}
			$jobOrder = $biodata->jobOrder;
			if ( $jobOrder == null ) {
				throw new UserException( 'JobOrder tidak ditemukan' );
			}
			$company = $jobOrder->company;
			if ( $company == null ) {
				throw new UserException( 'Company tidak ditemukan' );
			}
			$path              = realpath( \Yii::$app->basePath . DIRECTORY_SEPARATOR .
			                               'views' . DIRECTORY_SEPARATOR . 'biodata' .
			                               DIRECTORY_SEPARATOR . 'penempatan.docx' );
			$formatter         = \Yii::$app->formatter;
			$formatter->locale = 'id-ID';
			$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor( $path );
			$templateProcessor->setValue( 'cauntry_caps', strtoupper( $company->country ) );
			$templateProcessor->setValue( 'hari', $formatter->asDate( $biodata->tdate, 'cccc' ) );
			$templateProcessor->setValue( 'tgl', $formatter->asDate( $biodata->tdate, 'dd' ) );
			$templateProcessor->setValue( 'bulan', $formatter->asDate( $biodata->tdate, 'MMMM' ) );
			$templateProcessor->setValue( 'tahun', $formatter->asDate( $biodata->tdate, 'yyyy' ) );
			$templateProcessor->setValue( 'name', strtoupper( $biodata->name ) );
			$templateProcessor->setValue( 'tempat_lahir', strtoupper( $biodata->tempat_lahir ) );
			$templateProcessor->setValue( 'tgl_lahir', $formatter->asDate( $biodata->birthdate, 'dd-MM-yyyy' ) );
			$templateProcessor->setValue( 'bio_address', strtoupper( $biodata->address ) );
			$templateProcessor->setValue( 'bio_kecamatan', strtoupper( $biodata->kecamatan ) );
			$templateProcessor->setValue( 'bio_kabupaten', strtoupper( $biodata->kabupaten ) );
			$templateProcessor->setValue( 'bio_propinsi', strtoupper( $biodata->propinsi ) );
			$templateProcessor->setValue( 'company_name', $company->name );
			$templateProcessor->setValue( 'address', $company->address );
			$templateProcessor->setValue( 'city', $company->city );
			$templateProcessor->setValue( 'country', $company->country );
			$templateProcessor->setValue( 'job_title', $jobOrder->job_title );
			$templateProcessor->setValue( 'durasi_kontrak', $jobOrder->durasi_kontrak );
			$templateProcessor->setValue( 'basic_salary', $jobOrder->basic_salary );
//			$templateProcessor->setValue(
//				[ 'city', 'street' ],
//				[ 'Sunnydale, 54321 Wisconsin', '123 International Lane' ] );
//			header( "Content-Description: File Transfer" );
			$filename = strtoupper( str_replace( ' ', '_', 'PERJANJIAN_PENEMPATAN_' . $biodata->name ) ) . ".docx";
			$templateProcessor->saveAs( Yii::getAlias( '@runtime' ) . DS . $filename );
			header( "Content-Disposition: attachment; filename=" . $filename );
//			header( 'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document' );
//			header( 'Content-Transfer-Encoding: binary' );
//			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
//			header( 'Expires: 0' );
			flush();
			readfile( Yii::getAlias( '@runtime' ) . DS . $filename );
			unlink( Yii::getAlias( '@runtime' ) . DS . $filename );
//			$templateProcessor->saveAs( 'php://output' );
//			$templateProcessor->saveAs('helloWorld.docx');
		} catch ( CopyFileException $e ) {
			return $e->getMessage();
		} catch ( CreateTemporaryFileException $e ) {
			return $e->getMessage();
		} catch ( InvalidConfigException $e ) {
			return $e->getMessage();
		} catch ( UserException $e ) {
			return $e->getMessage();
		}
	}
}

<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Bayar;
use app\models\Biodata;
use app\models\Medikal;
use app\models\MedikalSearch;
use DateInterval;
use DateTime;
use Yii;
use yii\base\UserException;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * MedikalController implements the CRUD actions for Medikal model.
 */
class MedikalController extends BaseController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Medikal models.
	 * @return mixed
	 */
	public function actionIndex() {
		$this->layout = "//plain";
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$searchModel             = new MedikalSearch();
		$searchModel->biodata_id = $_SESSION[ "BIODATA" ];
		$dataProvider            = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModelMedikal'  => $searchModel,
			'dataProviderMedikal' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Medikal model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		$modelDP = Bayar::find()->where( [
			'AND',
			[
				'tipe_trans' => Bayar::MEDIKAL,
				'parent_id'  => $model->medikal_id,
				'biodata_id' => $model->biodata_id
			]
		] )->all();
		$itemArr = [];
		foreach ( $modelDP as $r ) {
			$itemArr[] = [ 'Rp'.Yii::$app->formatter->asDecimal($r->amount), $r->voucher, $r->note_ ];
		}
		return $this->render( 'view', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Json::encode( $itemArr ),
		] );
	}
	/**
	 * Creates a new Medikal model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws Exception
	 */
	public function actionCreate() {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$biodata = Biodata::find()->andWhere( [ 'biodata_id' => $_SESSION[ "BIODATA" ] ] )->one();
		$model   = new Medikal();
//		$modelDP    = new Bayar();
//		$modelLunas = new Bayar();
		if ( $model->load( Yii::$app->request->post() ) ) {
//			if ( $_POST['refresh'] == 1 ) {
//				$start       = new DateTime( $model->tgl );
//				$month_later = clone $start;
//				$month_later->add( new DateInterval( "P3M" ) );
//				$model->expired = $month_later->format( 'Y-m-d' );
//			} else {
			$transaction = Medikal::getDb()->beginTransaction();
			try {
				$model->medikal_id = Medikal::generate_uuid();
				$model->biodata_id = $_SESSION[ "BIODATA" ];
				if ( ! $model->save() ) {
					throw new Exception( 'Medikal ' . Html::errorSummary( $model ) );
				}
				if ( isset( $_POST['item_bayar'] ) ) {
					$item_pots = json_decode( Yii::$app->request->post()['item_bayar'] );
					if ( $item_pots != null ) {
						foreach ( $item_pots->body as $row ) {
							if ( count( $row ) == 3 ) {
								$indo             = new Bayar();
								$indo->bayar_id   = Bayar::generate_uuid();
								$indo->tipe_trans = Bayar::MEDIKAL;
								$indo->biodata_id = $model->biodata_id;
								$indo->parent_id  = $model->medikal_id;
								$indo->voucher    = $row[1];
								$indo->loan       = '0';
								$indo->amount     = $row[0];
								$indo->note_      = $row[2];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $indo ) );
								}
							}
						}
					}
				}
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'biodata' => $biodata,
					'model'   => $model,
					'item'    => Yii::$app->request->post()['item_bayar'],
				] );
			}
			return $this->redirect( [
				'biodata/view',
				'id' => $model->biodata_id,
				't'  => md5( date( 'Y-m-d H:i:s' ) ),
				'#'  => 'tab_2'
			] );
		}
//		}
//		$modelDP->loan    = 0;
//		$modelLunas->loan = 0;
		return $this->render( 'create', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Yii::$app->request->post()['item_bayar'],
		] );
	}
	/**
	 * Updates an existing Medikal model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionUpdate( $id ) {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$model   = $this->findModel( $id );
		$biodata = $model->biodata;
		$modelDP = Bayar::find()->where( [
			'AND',
			[
				'tipe_trans' => Bayar::MEDIKAL,
				'parent_id'  => $model->medikal_id,
				'biodata_id' => $model->biodata_id
			]
		] )->all();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $_POST['refresh'] == 1 ) {
				$start       = new DateTime( $model->tgl );
				$month_later = clone $start;
				$month_later->add( new DateInterval( "P3M" ) );
				$model->expired = $month_later->format( 'Y-m-d' );
			} else {
				$transaction = Medikal::getDb()->beginTransaction();
				try {
					if ( ! $model->save() ) {
						throw new Exception( 'Medikal ' . Html::errorSummary( $model ) );
					}
					Bayar::deleteAll( [
						'AND',
						[
							'tipe_trans' => Bayar::MEDIKAL,
							'parent_id'  => $model->medikal_id,
							'biodata_id' => $model->biodata_id
						]
					] );
					if ( isset( $_POST['item_bayar'] ) ) {
						$item_pots = json_decode( Yii::$app->request->post()['item_bayar'] );
						if ( $item_pots != null ) {
							foreach ( $item_pots->body as $row ) {
								if ( count( $row ) == 3 ) {
									$indo             = new Bayar();
									$indo->bayar_id   = Bayar::generate_uuid();
									$indo->tipe_trans = Bayar::MEDIKAL;
									$indo->biodata_id = $model->biodata_id;
									$indo->parent_id  = $model->medikal_id;
									$indo->voucher    = $row[1];
									$indo->loan       = '0';
									$indo->amount     = $row[0];
									$indo->note_      = $row[2];
									if ( ! $indo->save() ) {
										throw new UserException( Html::errorSummary( $indo ) );
									}
								}
							}
						}
					}
					$transaction->commit();
				} catch ( \Exception $e ) {
					$transaction->rollBack();
					Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
					return $this->render( 'update', [
						'biodata' => $biodata,
						'model'   => $model,
						'item'    => Yii::$app->request->post()['item_bayar'],
					] );
				} catch ( \Throwable $e ) {
					$transaction->rollBack();
					Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
					return $this->render( 'update', [
						'biodata' => $biodata,
						'model'   => $model,
						'item'    => Yii::$app->request->post()['item_bayar'],
					] );
				}
//			return $this->redirect( [ 'biodata/view', 'id' => $model->biodata_id ] );
				return $this->redirect( [
					'biodata/view',
					'id' => $model->biodata_id,
					't'  => md5( date( 'Y-m-d H:i:s' ) ),
					'#'  => 'tab_2'
				] );
			}
		}
		$itemArr = [];
		foreach ( $modelDP as $r ) {
			$itemArr[] = [ 'Rp'.Yii::$app->formatter->asDecimal($r->amount), $r->voucher, $r->note_ ];
		}
		return $this->render( 'update', [
			'biodata' => $biodata,
			'model'   => $model,
			'item'    => Json::encode( $itemArr ),
		] );
	}
	/**
	 * Deletes an existing Medikal model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		if ( ! isset( $_SESSION[ "BIODATA" ] ) ) {
			Yii::$app->getSession()->setFlash( 'error', 'Silahkan pilih biodata dulu...' );
			return $this->redirect( [ 'biodata/index' ] );
		}
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'biodata/view', 'id' => $_SESSION[ "BIODATA" ] ] );
	}
	/**
	 * Finds the Medikal model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Medikal the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Medikal::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
}

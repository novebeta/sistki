<?php
namespace frontend\controllers;
use app\components\BaseController;
use app\models\Biodata;
use app\models\Dokumen;
use app\models\ItemPot;
use app\models\JobOrder;
use app\models\JobOrderSearch;
use frontend\components\Cloud;
use Yii;
use yii\base\UserException;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
/**
 * JobOrderController implements the CRUD actions for JobOrder model.
 */
class JobOrderController extends BaseController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all JobOrder models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new JobOrderSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Lists all JobOrder models.
	 * @return mixed
	 */
	public function actionIndexCompany() {
		$searchModel  = new JobOrderSearch();
		$id           = \Yii::$app->session->get( 'company_id' );
		$dataProvider = $searchModel->search( [ 'JobOrderSearch' => [ 'company_id' => $id ] ] );
		return $this->render( 'index-company', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	public function actionSelectJobOrder() {
		\Yii::$app->session->remove( 'job_order_id' );
		\Yii::$app->session->set( 'job_order_id', $_GET['job_order_id'] );
		$this->redirect( [ 'biodata/indexcompany', 't' => md5( date( 'Y-m-d H:i:s' ) ) ] );
	}
	/**
	 * Displays a single JobOrder model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new JobOrder model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\db\Exception
	 */
	public function actionCreate() {
		$model = new JobOrder();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = Biodata::getDb()->beginTransaction();
			try {
				$model->job_order_id = $model::generate_uuid();
				if ( ! $model->save() ) {
					throw new UserException( Html::errorSummary( $model ) );
				}
				if ( isset( $_POST['item_pot'] ) ) {
					$item_pots = json_decode( Yii::$app->request->post()['item_pot'] );
					if ( $item_pots != null ) {
						foreach ( $item_pots->body as $row ) {
							if ( count( $row ) == 2 ) {
								$indo               = new ItemPot();
								$indo->item_pot_id  = ItemPot::generate_uuid();
								$indo->job_order_id = $model->job_order_id;
								$indo->bulan        = $row[0];
								$indo->pot          = $row[1];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $model ) );
								}
							}
						}
					}
				}
				$image = null;
				if ( isset( $_FILES[ Dokumen::JOB_ORDER ] ) ) {
					$bucket    = Cloud::getBucketName();
					$ossClient = Cloud::getOssClient();
					$object    = Yii::$app->params['uploadDir']['path'];
					$options   = null;
					$exist     = $ossClient->doesObjectExist( $bucket, $object );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, $object );
					}
					$object .= DS . Dokumen::JOB_ORDER;
					$exist  = $ossClient->doesObjectExist( $bucket, $object );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, $object );
					}
					Yii::$app->params['uploadthumbPath'] = $object . '/thumb';
					$exist                               = $ossClient->doesObjectExist( $bucket, Yii::$app->params['uploadthumbPath'] );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, Yii::$app->params['uploadthumbPath'] );
					}
					$image         = $_FILES[ Dokumen::JOB_ORDER ]['name'];
					$tmp           = $_FILES[ Dokumen::JOB_ORDER ]['tmp_name'];
					$imageFileType = strtolower( pathinfo( $image, PATHINFO_EXTENSION ) );
//					$image      = UploadedFile::getInstanceByName( Dokumen::JOB_ORDER );
//					$uploadPath = Yii::$app->params['uploadDir']['path'] . '/' . Dokumen::JOB_ORDER;
					if ( $imageFileType != 'pdf' ) {
						$imageFileType = 'jpg';
					}
					$namafile  = $model->job_order_id . '.' . $imageFileType;
					$pathThumb = Yii::$app->params['uploadthumbPath'] . DS . $namafile;
					if ( trim( $image ) != null ) {
						$path = $object . DS . $namafile;
						move_uploaded_file( $tmp, Yii::getAlias( '@runtime' ) . DS . $namafile );
						Image::thumbnail( Yii::getAlias( '@runtime' ) . DS . $namafile, 290, 200 )
						     ->save( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, [ 'quality' => 80 ] );
						$ossClient->uploadFile( $bucket, $path, Yii::getAlias( '@runtime' ) . DS . $namafile, $options );
						$ossClient->uploadFile( $bucket, $pathThumb, Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, $options );
						unlink( Yii::getAlias( '@runtime' ) . DS . $namafile );
						unlink( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile );
					}
				}
				$transaction->commit();
				Yii::$app->getSession()->setFlash( 'success', 'Save Success!' );
				return $this->redirect( [ 'view', 'id' => $model->job_order_id ] );
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
					'item'  => Yii::$app->request->post()['item_pot'],
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				return $this->render( 'create', [
					'model' => $model,
					'item'  => Yii::$app->request->post()['item_pot'],
				] );
			}
		}
		return $this->render( 'create', [
			'model' => $model,
			'item'  => null
		] );
	}
	/**
	 * Updates an existing JobOrder model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			$transaction = Biodata::getDb()->beginTransaction();
			try {
				ItemPot::deleteAll( [ 'job_order_id' => $id ] );
				if ( ! $model->save() ) {
					throw new UserException( Html::errorSummary( $model ) );
				}
				if ( isset( $_POST['item_pot'] ) ) {
					$item_pots = json_decode( Yii::$app->request->post()['item_pot'] );
					if ( $item_pots != null ) {
						foreach ( $item_pots->body as $row ) {
							if ( count( $row ) == 2 ) {
								$indo               = new ItemPot();
								$indo->item_pot_id  = ItemPot::generate_uuid();
								$indo->job_order_id = $model->job_order_id;
								$indo->bulan        = $row[0];
								$indo->pot          = $row[1];
								if ( ! $indo->save() ) {
									throw new UserException( Html::errorSummary( $model ) );
								}
							}
						}
					}
				}
				$image = null;
				if ( isset( $_FILES[ Dokumen::JOB_ORDER ] ) ) {
					$bucket    = Cloud::getBucketName();
					$ossClient = Cloud::getOssClient();
					$object    = Yii::$app->params['uploadDir']['path'];
					$options   = null;
					$exist     = $ossClient->doesObjectExist( $bucket, $object );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, $object );
					}
					$object .= DS . Dokumen::JOB_ORDER;
					$exist  = $ossClient->doesObjectExist( $bucket, $object );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, $object );
					}
					Yii::$app->params['uploadthumbPath'] = $object . '/thumb';
					$exist                               = $ossClient->doesObjectExist( $bucket, Yii::$app->params['uploadthumbPath'] );
					if ( $exist === false ) {
						$ossClient->createObjectDir( $bucket, Yii::$app->params['uploadthumbPath'] );
					}
					$image         = $_FILES[ Dokumen::JOB_ORDER ]['name'];
					$tmp           = $_FILES[ Dokumen::JOB_ORDER ]['tmp_name'];
					$imageFileType = strtolower( pathinfo( $image, PATHINFO_EXTENSION ) );
//					$image      = UploadedFile::getInstanceByName( Dokumen::JOB_ORDER );
//					$uploadPath = Yii::$app->params['uploadDir']['path'] . '/' . Dokumen::JOB_ORDER;
					if ( $imageFileType != 'pdf' ) {
						$imageFileType = 'jpg';
					}
					$namafile  = $model->job_order_id . '.' . $imageFileType;
					$pathThumb = Yii::$app->params['uploadthumbPath'] . DS . $namafile;
					if ( trim( $image ) != null ) {
						$path = $object . DS . $namafile;
						move_uploaded_file( $tmp, Yii::getAlias( '@runtime' ) . DS . $namafile );
						Image::thumbnail( Yii::getAlias( '@runtime' ) . DS . $namafile, 290, 200 )
						     ->save( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, [ 'quality' => 80 ] );
						$ossClient->uploadFile( $bucket, $path, Yii::getAlias( '@runtime' ) . DS . $namafile, $options );
						$ossClient->uploadFile( $bucket, $pathThumb, Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile, $options );
						unlink( Yii::getAlias( '@runtime' ) . DS . $namafile );
						unlink( Yii::getAlias( '@runtime' ) . DS . 'thumb_' . $namafile );
					}
				}
				$transaction->commit();
				Yii::$app->getSession()->setFlash( 'success', 'Save Success!' );
				return $this->redirect( [ 'view', 'id' => $model->job_order_id ] );
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				$item_pots = ItemPot::find()->where( [
					'job_order_id' => $id,
				] )->all();
				$itemArr   = [];
				foreach ( $item_pots as $r ) {
					$itemArr[] = [ $r->bulan, $r->pot ];
				}
				return $this->render( 'create', [
					'model' => $model,
					'item'  => Json::encode( $itemArr )
				] );
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				Yii::$app->getSession()->setFlash( 'error', $e->getMessage() );
				$item_pots = ItemPot::find()->where( [
					'job_order_id' => $id,
				] )->all();
				$itemArr   = [];
				foreach ( $item_pots as $r ) {
					$itemArr[] = [ $r->bulan, $r->pot ];
				}
				return $this->render( 'create', [
					'model' => $model,
					'item'  => Json::encode( $itemArr )
				] );
			}
		}
//		if ( $model->load( Yii::$app->request->post() ) ) {
//			if ( $model->save() ) {
//				Yii::$app->getSession()->setFlash( 'success', 'Save Success!' );
//				return $this->redirect( [ 'view', 'id' => $model->job_order_id ] );
//			} else {
//				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
//			}
//		}
		$item_pots = ItemPot::find()->where( [
			'job_order_id' => $id,
		] )->all();
		$itemArr   = [];
		foreach ( $item_pots as $r ) {
			$itemArr[] = [ $r->bulan, $r->pot ];
		}
		return $this->render( 'update', [
			'model' => $model,
			'item'  => Json::encode( $itemArr ),
		] );
	}
	/**
	 * Deletes an existing JobOrder model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the JobOrder model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return JobOrder the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = JobOrder::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
	/**
	 * Lists all JobOrder models.
	 *
	 * @param null $q
	 * @param null $id
	 *
	 * @return mixed
	 * @throws \yii\db\Exception
	 */
	public function actionList( $q = null, $id = null ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out                         = [ 'results' => [ 'id' => '', 'text' => '' ] ];
		$query                       = new Query;
		if ( ! is_null( $q ) ) {
			$query->select( 'job_order_id as id,job_title,jo.tgl,no_register,c.name as text' )
			      ->from( 'nscc_job_order AS jo' )
			      ->innerJoin( 'nscc_company AS c', 'jo.company_id = c.company_id' )
			      ->where( [
				      'AND',
				      [
					      'AND',
					      [ '=', 'finish', '0' ],
					      [ '=', 'disabled', '0' ],
//					      [ '=', 'closed', '0' ],
					      [ '<=', 'jo.tgl', new Expression( 'DATE_ADD(jo.tgl, INTERVAL 1 YEAR)' ) ]
				      ],
				      [
					      'OR',
					      [ 'like', 'approval_date', $q ],
					      [ 'like', 'job_title', $q ],
					      [ 'like', 'jo.tgl', $q ],
					      [ 'like', 'c.name', $q ]
				      ]
			      ] )
			      ->orderBy( 'jo.tgl' )
			      ->limit( 20 );
			$command        = $query->createCommand();
			$data           = $command->queryAll();
			$out['results'] = array_values( $data );
		}
//		elseif ( $id > 0 ) {
//			$out['results'] = [ 'id' => $id, 'text' => JobOrder::find( $id )->tgl ];
//		}
		return $out;
	}
}

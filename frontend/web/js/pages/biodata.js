$('#table-indo').DataTable({
    dom: 'Bfrtip',
    select: true,
    buttons: [
        {
            text: 'Add Riwayat',
            action: function (e, dt, node, config) {
                var modal = $('#modal-riwayat');
                modal.find('.modal-title').text('Riwayat Perusahaan Indonesia');
                $('#riwayat-type').val('0');
                modal.modal();
            },
            className: 'dt-button'
        },
        {
            text: 'Delete Riwayat',
            action: function (e, dt, node, config) {
                this.row('.selected').remove().draw(false);
            },
            className: 'dt-button'
        }
    ]
});
$('#btn-riwayat-add-row').on('click', function () {
    var nama = $('#riwayat-nama').val();
    var lokasi = $('#riwayat-lokasi').val();
    var posisi = $('#riwayat-posisi').val();
    var masakerja = $('#riwayat-masa-kerja').val();
    var type = $('#riwayat-type').val();
    var t = $(type == '0' ? '#table-indo' : '#table-malay').DataTable();
    t.row.add([
        nama,
        lokasi,
        posisi,
        masakerja
    ]).draw(false);
    $('#modal-riwayat').find('input').not('#riwayat-type').val('');
});
$('#table-malay').DataTable({
    dom: 'Bfrtip',
    select: true,
    buttons: [
        {
            text: 'Add Riwayat',
            action: function (e, dt, node, config) {
                var modal = $('#modal-riwayat');
                modal.find('.modal-title').text('Riwayat Perusahaan Malaysia');
                $('#riwayat-type').val('1');
                modal.modal();
            },
            className: 'dt-button'
        },
        {
            text: 'Delete Riwayat',
            action: function (e, dt, node, config) {
                this.row('.selected').remove().draw(false);
            },
            className: 'dt-button'
        }
    ]
});
var form = $('#biodata_basic_id').submit(function () {
    // alert( "Handler for .submit() called." );
    // event.preventDefault();
    var dt_indo = $('#table-indo').DataTable();
    var indo = dt_indo.buttons.exportData();
    var riwayat_indo = JSON.stringify(indo);
    var dt_malay = $('#table-malay').DataTable();
    var malay = dt_malay.buttons.exportData();
    var riwayat_malay = JSON.stringify(malay);
    $("<input>").attr({
        'type': 'hidden',
        'name': 'riwayat_indo'
    }).val(riwayat_indo).appendTo(this);
    $("<input>").attr({
        'type': 'hidden',
        'name': 'riwayat_malay'
    }).val(riwayat_malay).appendTo(this);
});
$('#btn-pot-add-row').on('click', function () {
    var bulan = $('#bulan-pot').val();
    var jml = $('#jml-pot').val();
    var idx = $('#updateId').val();
    // var type = $('#riwayat-type').val();
    var t = $("#table-pot").DataTable();
    if(idx != ''){
        var d = t.row(idx).data();
        d[0] = bulan;
        d[1] = jml;
        t.row(idx).data(d).draw();
        $('#modal-pot').modal('hide');
    }else {
        t.row.add([
            bulan,
            jml,
        ]).draw(false);
        $('#modal-pot').find('input').val('');
    }

});
$('#table-pot').DataTable({
    dom: 'Brtip',
    select: true,
    buttons: [
        {
            text: 'Add Item',
            action: function (e, dt, node, config) {
                var modal = $('#modal-pot');
                modal.find('input').val('');
                $('#updateId').val(null);
                modal.modal();
            },
            className: 'dt-button'
        },
        {
            text: 'Edit Item',
            action: function (e, dt, node, config) {
                var modal = $('#modal-pot');
                var data = this.row('.selected').data();
                $('#bulan-pot').val(data[0]);
                $('#jml-pot').val(data[1]);
                $('#updateId').val(this.row('.selected').index());
                modal.modal();
            },
            className: 'dt-button'
        },
        {
            text: 'Delete Item',
            action: function (e, dt, node, config) {
                this.row('.selected').remove().draw(false);
            },
            className: 'dt-button'
        }
    ]
});
var form = $('#job-order-form-id').submit(function () {
    var dt_indo = $('#table-pot').DataTable();
    var indo = dt_indo.buttons.exportData();
    var riwayat_indo = JSON.stringify(indo);
    $("<input>").attr({
        'type': 'hidden',
        'name': 'item_pot'
    }).val(riwayat_indo).appendTo(this);
});
$('#table-medikal-bayar').DataTable({
    dom: 'Bfrtip',
    select: true,
    buttons: [
        {
            text: 'Add Item',
            action: function (e, dt, node, config) {
                var modal = $('#modal-medikal-bayar');
                modal.modal();
            },
            className: 'dt-button'
        },
        {
            text: 'Delete Item',
            action: function (e, dt, node, config) {
                this.row('.selected').remove().draw(false);
            },
            className: 'dt-button'
        }
    ]
});
var form = $('#medikal_form_input').submit(function () {
    var dt_indo = $('#table-medikal-bayar').DataTable();
    var indo = dt_indo.buttons.exportData();
    var riwayat_indo = JSON.stringify(indo);
    $("<input>").attr({
        'type': 'hidden',
        'name': 'item_bayar'
    }).val(riwayat_indo).appendTo(this);
});
$('#btn-medikal-bayar-add-row').on('click', function () {
    var amount = $('#amount-medikal-bayar').val();
    amount = 'Rp'+formatNumber(amount);
    var voucher = $('#voucher-medikal-bayar').val();
    var note = $('#note-medikal-bayar').val();
    // var type = $('#riwayat-type').val();
    var t = $("#table-medikal-bayar").DataTable();
    t.row.add([
        amount,
        voucher,
        note,
    ]).draw(false);
    $('#modal-medikal-bayar').find('input').val('');
});
function toJSONLocal (date) {
    var local = new Date(date);
    local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
}
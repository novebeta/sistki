<?php
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 3/6/18
 * Time: 8:36 PM
 */
namespace console\controllers;

use Yii;
use yii\console\Controller;
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        // add "createPost" permission
        $createPost = $auth->createPermission('createBiodata');
        $createPost->description = 'Create a Biodata';
        $auth->add($createPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updateBiodata');
        $updatePost->description = 'Update Biodata';
        $auth->add($updatePost);

        // add "author" role and give this role the "createPost" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
//        $auth->assign($author, 2);
        $auth->assign($admin, 1);
    }
}
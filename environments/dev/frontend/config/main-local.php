<?php
use yii\filters\AccessControl;
$config = [
	'components' => [
		'i18n'             => [
			'translations' => [
				'*' => [
					'class'          => 'yii\i18n\PhpMessageSource',
					'basePath'       => '@frontend/messages', // if advanced application, set @frontend/messages
					'sourceLanguage' => 'en',
					'fileMap'        => [
						//'main' => 'main.php',
					],
				],
			],
		],
		'request'          => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'fe68af1a-12c8-11e8-8362-6eb54127018f',
		],
		'db'               => [
			'class'       => '\yii\db\Connection',
			'dsn'         => 'mysql:host=localhost;dbname=sistki',
			'tablePrefix' => 'nscc_',
			'username'    => 'rCpp9uvJpztw',
			'password'    => 'feZHrlnzcx+l7E0M',
			'charset'     => 'utf8',
		],
		'session'          => [
			'class'        => 'yii\web\DbSession',
			// 'db' => 'mydb',  // the application component ID of the DB connection. Defaults to 'db'.
			'sessionTable' => 'nscc_session_front', // session table name. Defaults to 'session'.
		],
		'assetManager'     => [
			'bundles' => [
				'frontend\assets\AdminLtePluginAsset' => [
					'skin' => 'skin-red',
				],
			],
		],
		'as beforeRequest' => [
			'class'        => AccessControl::className(),
			'only'         => [ 'logout', 'signup' ],
			'rules'        => [
				[
					'actions' => [ 'signup' ],
					'allow'   => true,
					'roles'   => [ '?' ],
				],
				[
					'actions' => [ 'logout' ],
					'allow'   => true,
					'roles'   => [ '@' ],
				],
			],
			'denyCallback' => function ( $rule, $action ) {
				return Yii::$app->response->redirect( [ 'site/login' ] );
			},
		],
	],
];
if ( ! YII_ENV_TEST ) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]                             = 'debug';
	$config['modules']['debug']                        = [
		'class' => 'yii\debug\Module',
	];
	$config['bootstrap'][]                             = 'gii';
	$config['modules']['gii']                          = [
		'class'      => 'yii\gii\Module',
		'allowedIPs' => [ '127.0.0.1', '::1' ],
		'generators' => [ //here
			'crud' => [
				'class'     => 'yii\gii\generators\crud\Generator',
				'templates' => [
					'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
				]
			]
		],
	];
	$config['components']['assetManager']['forceCopy'] = true;
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//        'generators' => [
//            'doubleModel' => [
//                'class' => 'claudejanz\mygii\generators\model\Generator',
//            ],
//        ],
//    ];
}
return $config;

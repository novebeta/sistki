<?php
return [
    'uploadDir' => [
        'path' => 'uploads',
        'ktp' => 'uploads/ktp',
        'kk' => 'uploads/kk',
        'akte' => 'uploads/akte',
        'ijasah' => 'uploads/ijasah',
        'buku_nikah' => 'uploads/buku_nikah',
        'paspor' => 'uploads/paspor',
        'surat_ijin' => 'uploads/surat_ijin',
        'pas_foto' => 'uploads/pas_foto',
    ],
];

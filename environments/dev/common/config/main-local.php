<?php
return [
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
//                    'userClassName' => 'mdm\admin\models\User',
                    'idField' => 'user_id',
//                    'usernameField' => 'username',
//                    'fullnameField' => 'profile.full_name',
//                    'extraColumns' => [
//                        [
//                            'attribute' => 'full_name',
//                            'label' => 'Full Name',
//                            'value' => function ($model, $key, $index, $column) {
//                                return $model->profile->full_name;
//                            },
//                        ],
//                        [
//                            'attribute' => 'dept_name',
//                            'label' => 'Department',
//                            'value' => function ($model, $key, $index, $column) {
//                                return $model->profile->dept->name;
//                            },
//                        ],
//                        [
//                            'attribute' => 'post_name',
//                            'label' => 'Post',
//                            'value' => function ($model, $key, $index, $column) {
//                                return $model->profile->post->name;
//                            },
//                        ],
//                    ],
//                    'searchClass' => 'app\models\UserSearch'
                    'searchClass' => 'mdm\admin\models\searchs\User'
                ],
            ],
        ],
        'dynagrid'=> [
            'class'=>'\kartik\dynagrid\Module',
            // other module settings
        ],
        'gridview'=> [
            'class'=>'\kartik\grid\Module',
            // other module settings
        ],
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'user' => [
//            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'db' => [
            'class' => '\yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=sistki',
            'tablePrefix' => 'nscc_',
            'username' => 'rCpp9uvJpztw',
            'password' => 'feZHrlnzcx+l7E0M',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d-M-Y',
            'datetimeFormat' => 'php:d-M-Y H:i:s',
            'timeFormat' => 'php:H:i:s',
        ]
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'admin/*', // add or remove allowed actions to this list
        ]
    ],
    'aliases' => [
        '@mdm/admin' => '@vendor/mdmsoft/yii2-admin',
    ]
];

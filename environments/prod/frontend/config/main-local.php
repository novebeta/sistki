<?php
use yii\filters\AccessControl;
$config = [
	'components' => [
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@frontend/messages', // if advanced application, set @frontend/messages
					'sourceLanguage' => 'en',
					'fileMap' => [
						//'main' => 'main.php',
					],
				],
			],
		],
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'fe68af1a-12c8-11e8-8362-6eb54127018f',
		],
		'db' => [
			'class' => '\yii\db\Connection',
			'dsn' => 'mysql:host=localhost;dbname=dutaw240_app',
			'tablePrefix' => 'nscc_',
			'username' => 'dutaw240_app',
			'password' => 'dut4w1b4w4app',
			'charset' => 'utf8',
		],
		'session' => [
			'class' => 'yii\web\DbSession',
			// 'db' => 'mydb',  // the application component ID of the DB connection. Defaults to 'db'.
			'sessionTable' => 'nscc_session_front', // session table name. Defaults to 'session'.
		],
		'assetManager' => [
			'bundles' => [
				'frontend\assets\AdminLtePluginAsset' => [
					'skin' => 'skin-red',
				],
			],
		],
		'as beforeRequest' => [
			'class'        => AccessControl::className(),
			'only'         => [ 'logout', 'signup' ],
			'rules'        => [
				[
					'actions' => [ 'signup' ],
					'allow'   => true,
					'roles'   => [ '?' ],
				],
				[
					'actions' => [ 'logout' ],
					'allow'   => true,
					'roles'   => [ '@' ],
				],
			],
			'denyCallback' => function ( $rule, $action ) {
				return Yii::$app->response->redirect( [ 'site/login' ] );
			},
		],
	],
];
return $config;